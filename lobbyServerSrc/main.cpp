#include "GSEGL/Network/SocketUtil.hpp"
#include "GSEGL/Network/OutMemBitStream.hpp"
#include "GSEGL/Network/InMemBitStream.hpp"
#include "GSEGL/TimeUtil.hpp"
#include "GSEGL/Network/SocketAddress.hpp"
#include "GSEGL/LobbyServerConstants.hpp"
#include "GSEGL/RandGenUtil.hpp"
#include <iostream>
#include <unordered_map>
#include <vector>

struct LobbyData
{
	LobbyData(const SocketAddress& initServerExternalAddr, const SocketAddress& initServerInternalAddr):
	serverExternalAddr(initServerExternalAddr),
	serverInternalAddr(initServerInternalAddr),
	lastAlivePacketTime(TimeUtil::Get().GetTimeSinceStart()),
	timeStart(TimeUtil::Get().GetTimeSinceStart()),
	explicitlyClosed(false)
	{}
	SocketAddress serverExternalAddr;
	SocketAddress serverInternalAddr;
	double lastAlivePacketTime;
	double timeStart;
	bool explicitlyClosed;
};

std::vector<LobbyData>* GetQueueFromName(std::unordered_map<UTF8String, std::vector<LobbyData>>& queues, const UTF8String& name)
{
	auto it = queues.find(name);
	if(it != queues.end())
		return &(it->second);
	else
		return nullptr;
}

LobbyData* GetRandomLobbyFromQueue(std::vector<LobbyData>& queue)
{
	if(queue.empty())
		return nullptr;
	else
		return &(queue[RandGenUtil::Get().GetRandomUInt64(0, queue.size()-1)]);
}

LobbyData* GetLobbyFromServerExternalAddr(std::vector<LobbyData>& queue, const SocketAddress& addr)
{
	auto it = std::find_if(queue.begin(), queue.end(), [&addr](const LobbyData& lobby){ return addr == lobby.serverExternalAddr; });
	if(it != queue.end())
		return &(*it);
	else
		return nullptr;
}

LobbyData* GetLobbyFromServerExternalAddr(std::unordered_map<UTF8String, std::vector<LobbyData>>& queues, const SocketAddress& addr)
{
	for(auto& queuePair : queues)
	{
		LobbyData* lobby = GetLobbyFromServerExternalAddr(queuePair.second, addr);
		if(lobby)
			return lobby;
	}
	return nullptr;
}

void ProcessPacket(UDPSocketPtr& sock, Packet& packet, std::unordered_map<UTF8String, std::vector<LobbyData>>& queues)
{
	InMemBitStream inStream(packet.data, packet.size);
	LobbyPacketType packetType = LOBBYPT_SetupLobby;
	if(!inStream.ReadBits(packetType)) return;
	uint64_t currentVersion = 0;
	if(!inStream.ReadBits(currentVersion)) return;
	if(currentVersion != CURRENT_VERSION)
	{
		OutMemBitStream outStream;
		outStream.WriteBits(LOBBYPT_DifferentVersion);
		sock->SendTo(outStream.GetBufferPtr(), outStream.GetByteLength(), packet.from);
		return;
	}
	switch(packetType)
	{
		case LOBBYPT_SetupLobby:
		{
			UTF8String queueName;
			if(!queueName.Read(inStream)) return;
			if(queueName.IsEmpty() || queueName.GetStrLength() > 16)
			{
				OutMemBitStream outStream;
				outStream.WriteBits(LOBBYPT_IncorrectString);
				sock->SendTo(outStream.GetBufferPtr(), outStream.GetByteLength(), packet.from);
				return;
			}
			std::vector<LobbyData>* queue = GetQueueFromName(queues, queueName);
			bool separateLobby = false;
			if(!inStream.ReadBits(separateLobby)) return;
			if(!queue)
			{
				// Even if name does not exist, check if address does. Every address may only have 1 lobby.
				if(!GetLobbyFromServerExternalAddr(queues, packet.from))
				{
					SocketAddress internalAddr;
					if(!internalAddr.Read(inStream)) return;
					auto it = queues.emplace(queueName, std::vector<LobbyData>()).first;
					it->second.emplace_back(packet.from, internalAddr);
					OutMemBitStream outStream;
					outStream.WriteBits(LOBBYPT_SuccessfulCreation);
					sock->SendTo(outStream.GetBufferPtr(), outStream.GetByteLength(), packet.from);
				}
				else
				{
					OutMemBitStream outStream;
					outStream.WriteBits(LOBBYPT_AlreadyHosting);
					sock->SendTo(outStream.GetBufferPtr(), outStream.GetByteLength(), packet.from);
				}
			}
			else if(GetLobbyFromServerExternalAddr(*queue, packet.from)) // Hosting client probably lost success packet from server, resend data
			{
				OutMemBitStream outStream;
				outStream.WriteBits(LOBBYPT_SuccessfulCreation);
				sock->SendTo(outStream.GetBufferPtr(), outStream.GetByteLength(), packet.from);
			}
			else
			{
				if(separateLobby)
				{
					OutMemBitStream outStream;
					outStream.WriteBits(LOBBYPT_NameTaken);
					sock->SendTo(outStream.GetBufferPtr(), outStream.GetByteLength(), packet.from);
				}
				else
				{
					SocketAddress internalAddr;
					if(!internalAddr.Read(inStream)) return;
					queue->emplace_back(packet.from, internalAddr);
					OutMemBitStream outStream;
					outStream.WriteBits(LOBBYPT_SuccessfulCreation);
					sock->SendTo(outStream.GetBufferPtr(), outStream.GetByteLength(), packet.from);
				}
			}
			return;
		}
		case LOBBYPT_ExplicitlyCloseLobby:
		{
			LobbyData* lobby = GetLobbyFromServerExternalAddr(queues, packet.from);
			if(lobby)
				lobby->explicitlyClosed = true;
			return;
		}
		case LOBBYPT_ConnectTo:
		{
			UTF8String queueName;
			if(!queueName.Read(inStream)) return;
			std::vector<LobbyData>* queue = GetQueueFromName(queues, queueName);
			if(queue)
			{
				OutMemBitStream outStreamToClient;
				outStreamToClient.WriteBits(LOBBYPT_LobbyAvailable);
				LobbyData* lobby = GetRandomLobbyFromQueue(*queue);
				if(packet.from.SameIPv4Addr(lobby->serverExternalAddr)) // If from same external address, they are behind the same NAT
					lobby->serverInternalAddr.Write(outStreamToClient);
				else
				{
					lobby->serverExternalAddr.Write(outStreamToClient);
					// Send incoming connection packet to server to open port (STUN)
					OutMemBitStream outStreamToServer;
					outStreamToServer.WriteBits(LOBBYPT_IncomingConnection);
					packet.from.Write(outStreamToServer);
					sock->SendTo(outStreamToServer.GetBufferPtr(), outStreamToServer.GetByteLength(), lobby->serverExternalAddr);
				}
				sock->SendTo(outStreamToClient.GetBufferPtr(), outStreamToClient.GetByteLength(), packet.from);
			}
			else
			{
				OutMemBitStream outStream;
				outStream.WriteBits(LOBBYPT_LobbyNotAvailable);
				sock->SendTo(outStream.GetBufferPtr(), outStream.GetByteLength(), packet.from);
			}
			return;
		}
		case LOBBYPT_IsSameVersion:
		{
			OutMemBitStream outStream;
			outStream.WriteBits(LOBBYPT_SameVersion);
			sock->SendTo(outStream.GetBufferPtr(), outStream.GetByteLength(), packet.from);
			return;
		}
		default:
			return;
	}
}

int main()
{
	const uint16_t port = 38694;
	SocketUtil::Get().Init();
	UDPSocketPtr sock = SocketUtil::Get().CreateUDPSocket(INET);
	SocketAddress serverAddr(port, INET);
	if(!sock)
	{
		std::cout << "Failed to create socket\n";
		return ErrorUtil::Get().GetErrorCode();
	}
	if(sock->Bind(serverAddr) < 0)
	{
		std::cout << "Failed to bind to address\n";
		return ErrorUtil::Get().GetErrorCode();
	}
	Packet receivedPacket;
	std::unordered_map<UTF8String, std::vector<LobbyData>> queues;
	TimeUtil::Get().Init();
	RandGenUtil::Get().Init();
	std::cout << "Hosting server on port " << port << " for players of version " << FIRST_VERSION - CURRENT_VERSION << '\n';
	for(;;)
	{
		receivedPacket.size = sock->ReceiveFrom(receivedPacket.data, MAX_PACKET_SIZE, receivedPacket.from);
		if(receivedPacket.size < 0)
			continue;
		TimeUtil::Get().Update();
		// Erase separate lobbies
		for(auto& queuePair : queues)
		{
			queuePair.second.erase(std::remove_if(queuePair.second.begin(), queuePair.second.end(), [](const LobbyData& lobby)
			{ return lobby.explicitlyClosed||(lobby.timeStart + LOBBYSTART_TIMEOUT < TimeUtil::Get().GetTimeSinceStart());}), queuePair.second.end());
		}
		// Erase whole queues when empty
		for(auto it = queues.begin(); it != queues.end();)
		{
			if(it->second.empty())
				it = queues.erase(it);
			else
				++it;
		}
		ProcessPacket(sock, receivedPacket, queues);
	}
	SocketUtil::Get().Cleanup();
	return 0;
}
