#pragma once
#include <random>
#include <cstdint>

class RandGenUtil
{
	public:
		static RandGenUtil& Get();
		void Init();
		uint64_t GetRandomUInt64(uint64_t inMin, uint64_t inMax);
		int64_t GetRandomInt64(int64_t inMin, int64_t inMax);
		double GetRandomDouble(double inMin, double inMax);
	private:
		std::mt19937_64 numberGenerator;
};

