#include "OutMemBitStream.hpp"
#include "../ErrorUtil.hpp"
#include <cstring>
#include "../libs/miniz.h"

OutMemBitStream::OutMemBitStream():
buffer(nullptr),
bitHead(0),
bitCapacity(0)
{
	ReallocBuffer(256);
}

OutMemBitStream::OutMemBitStream(const OutMemBitStream& outStream):
buffer(nullptr),
bitHead(outStream.bitHead),
bitCapacity(0)
{
	ReallocBuffer(bitHead);
	std::memcpy(buffer, outStream.buffer, GetByteLength());
}

OutMemBitStream::OutMemBitStream(OutMemBitStream&& outStream):
buffer(outStream.buffer),
bitHead(outStream.bitHead),
bitCapacity(outStream.bitCapacity)
{
	outStream.buffer = nullptr;
}

void OutMemBitStream::operator=(const OutMemBitStream& outStream)
{
	bitHead = outStream.bitHead;
	ReallocBuffer(bitHead);
	std::memcpy(buffer, outStream.buffer, GetByteLength());
}

void OutMemBitStream::operator=(OutMemBitStream&& outStream)
{
	buffer = outStream.buffer;
	bitHead = outStream.bitHead;
	bitCapacity = outStream.bitCapacity;
	outStream.buffer = nullptr;
}

OutMemBitStream::~OutMemBitStream()
{
	if(buffer)
		std::free(buffer);
}

void OutMemBitStream::WriteCompressedData(const void* data, uint64_t byteCount)
{
	void* compressedHeap = nullptr;
	size_t compressedHeapSize = 0;
	compressedHeap = tdefl_compress_mem_to_heap(data, byteCount, &compressedHeapSize, 4095);
	uint64_t compressedSize64 = compressedHeapSize;
	if(compressedHeap)
	{
		WriteBits(compressedSize64);
		WriteBits(static_cast<const void*>(compressedHeap), compressedSize64 * 8);
		mz_free(compressedHeap);
	}
	else
		ErrorUtil::Get().SetError("Memory", "OutMemBitStream::WriteCompressedData Failed to compress data to heap");
}

void OutMemBitStream::WriteBits(const void* data, uint64_t bitCount)
{
	const uint8_t* srcByte = static_cast<const uint8_t*>(data);
	while(bitCount > 8)
	{
		WriteBitsToByte(*srcByte, 8);
		++srcByte;
		bitCount -= 8;
	}
	if(bitCount > 0)
		WriteBitsToByte(*srcByte, bitCount);
}

void OutMemBitStream::WriteBits(const bool data)
{
	WriteBits(static_cast<const void*>(&data), 1);
}

void OutMemBitStream::WriteFixedFloat(const float data, float min, float precision, uint64_t bitCount)
{
	uint64_t numToWrite = static_cast<uint64_t>((data-min)/precision);
	WriteBits(numToWrite, bitCount);
}

const uint8_t* OutMemBitStream::GetBufferPtr() const
{
	return buffer;
}

uint64_t OutMemBitStream::GetBitLength() const
{
	return bitHead;
}

uint64_t OutMemBitStream::GetByteLength() const
{
	return (bitHead + 7) >> 3;
}

void OutMemBitStream::WriteBitsToByte(uint8_t data, uint64_t bitCount)
{
	uint64_t nextBitHead = bitHead + bitCount;
	if(nextBitHead >= bitCapacity)
		ReallocBuffer(std::max(bitCapacity * 2, nextBitHead));
	uint64_t byteOffset = bitHead >> 3; // Divide by 8
	uint64_t bitOffset = bitHead & 0x7; // Take last 3 bits (modulo 8)
	uint8_t currentMask = ~(0xff << bitOffset);
	buffer[byteOffset] = (buffer[byteOffset] & currentMask)|(data << bitOffset);
	uint64_t bitsFreeThisByte = 8 - bitOffset;
	if(bitsFreeThisByte < bitCount)
		buffer[byteOffset + 1] = data >> bitsFreeThisByte;
	bitHead = nextBitHead;
}

void OutMemBitStream::ReallocBuffer(uint64_t newBitCapacity)
{
	uint8_t* tempBuffer = static_cast<uint8_t*>(std::realloc(buffer, newBitCapacity));
	if(tempBuffer == nullptr)
	{
		ErrorUtil::Get().SetError("Memory", "OutMemBitStream::ReallocBuffer");
		return;
	}
	buffer = tempBuffer;
	bitCapacity = newBitCapacity;
}
