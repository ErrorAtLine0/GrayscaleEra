#pragma once
#include "OutMemBitStream.hpp"
#include "InMemBitStream.hpp"
#include "../TimeUtil.hpp"
#include <deque>
#include <memory>
#define TIME_TO_TIMEOUT 12.0
#define PACKET_TIMEOUT_TIME 1.2
#define MIN_PACKET_RATE 0.07
#define MAX_PACKET_RATE 0.2
#define RTT_AVERAGE_ACCURACY 50

class DeliveryNotificationManager;

enum TransmissionDataType
{
	TRANSMISSIONDATA_UNRELIABLE,
	TRANSMISSIONDATA_UNRELIABLE_HEADERLESS,
	TRANSMISSIONDATA_RELIABLE,
	TRANSMISSIONDATA_REPLICATION,
	TRANSMISSIONDATA_MAX
};

class TransmissionData
{
	public:
		virtual ~TransmissionData() {}
		virtual void HandleDeliverySuccess(DeliveryNotificationManager* deliveryNotifier) = 0;
		virtual void HandleDeliveryFailure(DeliveryNotificationManager* deliveryNotifier) = 0;
};

typedef uint16_t SequenceNumberType;
#define SEQUENCE_NUMBER_WRAP_MIN 10000
#define SEQUENCE_NUMBER_WRAP_MAX 50000

class InFlightPacket
{
	public:
		InFlightPacket(SequenceNumberType initSequenceNumber):
		sequenceNumber(initSequenceNumber),
		timeDispatched(TimeUtil::Get().GetTimeSinceStart()),
		transmissionData(nullptr)
		{}
		void SetTransmissionData(TransmissionDataType type, std::shared_ptr<TransmissionData> transmissionData)
		{
			this->type = type;
			this->transmissionData = transmissionData;
		}
		std::shared_ptr<TransmissionData> GetTransmissionData(TransmissionDataType type)
		{
			if(transmissionData == nullptr || this->type != type)
				return nullptr;
			else
				return transmissionData;
		}
		void HandleDeliverySuccess(DeliveryNotificationManager* deliveryNotMan) const
		{
			if(transmissionData)
				transmissionData->HandleDeliverySuccess(deliveryNotMan);
		}
		void HandleDeliveryFailure(DeliveryNotificationManager* deliveryNotMan) const
		{
			if(transmissionData)
				transmissionData->HandleDeliveryFailure(deliveryNotMan);
		}
		SequenceNumberType GetSequenceNumber() const { return sequenceNumber; }
		double GetTimeDispatched() const { return timeDispatched; }
	private:
		SequenceNumberType sequenceNumber;
		double timeDispatched;
		TransmissionDataType type;
		std::shared_ptr<TransmissionData> transmissionData;
};

class DeliveryNotificationManager
{
	public:
		DeliveryNotificationManager():
		nextOutgoingSequenceNumber(0),
		nextExpectedSequenceNumber(0),
		dispatchedPacketCount(0),
		droppedPacketCount(0),
		deliveredPacketCount(0),
		timeLastSuccessReceived(TimeUtil::Get().GetTimeSinceStart()),
		timeLastPacketReceived(TimeUtil::Get().GetTimeSinceStart()),
		estimatedSendRate(0.0),
		oldRTTs(RTT_AVERAGE_ACCURACY, 0.1),
		RTT(0.1)
		{}
		
		InFlightPacket* WritePacketHeader(OutMemBitStream& packet)
		{
			// Write Acks
			bool hasAcks = pendingAcks.size() > 0;
			packet.WriteBits(hasAcks);
			if(hasAcks)
			{
				pendingAcks.front().Write(packet);
				pendingAcks.pop_front();
			}
			// Write Sequence Number
			packet.WriteBits(nextOutgoingSequenceNumber);
			inFlightPackets.emplace_back(nextOutgoingSequenceNumber);
			++nextOutgoingSequenceNumber;
			++dispatchedPacketCount;
			return &inFlightPackets.back();
		}
		int ProcessPacketHeader(InMemBitStream& packet) // 0 = Memory error, 1 = Not new packet, 2 = New Packet
		{
			// Process Acks
			bool hasAcks = false;
			if(!packet.ReadBits(hasAcks)) return 0;
			if(hasAcks)
			{
				AckRange ackRange;
				if(!ackRange.Read(packet)) return 0;
				SequenceNumberType nextAckdSequenceNumber = ackRange.GetStart();
				SequenceNumberType onePastAckdRange = nextAckdSequenceNumber + ackRange.GetCount();
				while(((nextAckdSequenceNumber < onePastAckdRange)||
					   (nextAckdSequenceNumber > SEQUENCE_NUMBER_WRAP_MAX && onePastAckdRange < SEQUENCE_NUMBER_WRAP_MIN))&&
					   !inFlightPackets.empty())
				{
					const InFlightPacket& nextInFlightPacket = inFlightPackets.front();
					SequenceNumberType nextInFlightPacketSequenceNumber = nextInFlightPacket.GetSequenceNumber();
					if((nextInFlightPacketSequenceNumber < nextAckdSequenceNumber)||
					   (nextInFlightPacketSequenceNumber > SEQUENCE_NUMBER_WRAP_MAX && nextAckdSequenceNumber < SEQUENCE_NUMBER_WRAP_MIN))
					{
						InFlightPacket copyOfInFlightPacket = nextInFlightPacket;
						inFlightPackets.pop_front();
						HandlePacketDeliveryFailure(copyOfInFlightPacket);
					}
					else if(nextInFlightPacketSequenceNumber == nextAckdSequenceNumber)
					{
						HandlePacketDeliverySuccess(nextInFlightPacket);
						inFlightPackets.pop_front();
						++nextAckdSequenceNumber;
					}
					else if((nextInFlightPacketSequenceNumber > nextAckdSequenceNumber)||
							(nextInFlightPacketSequenceNumber < SEQUENCE_NUMBER_WRAP_MIN && nextAckdSequenceNumber > SEQUENCE_NUMBER_WRAP_MAX))
					{
						nextAckdSequenceNumber = nextInFlightPacketSequenceNumber;
					}
				}
			}
			// Process Sequence Number
			SequenceNumberType sequenceNumber = 0;
			if(!packet.ReadBits(sequenceNumber)) return 0;
			if((sequenceNumber >= nextExpectedSequenceNumber && !(sequenceNumber > SEQUENCE_NUMBER_WRAP_MAX && nextExpectedSequenceNumber < SEQUENCE_NUMBER_WRAP_MIN))||
			  (sequenceNumber < SEQUENCE_NUMBER_WRAP_MIN && nextExpectedSequenceNumber > SEQUENCE_NUMBER_WRAP_MAX))
			{
				nextExpectedSequenceNumber = sequenceNumber + 1;
				if(pendingAcks.size() == 0 || !pendingAcks.back().ExtendIfShould(sequenceNumber))
					pendingAcks.emplace_back(sequenceNumber);
			}
			else
				return 1;
			estimatedSendRate = TimeUtil::Get().GetTimeSinceStart() - timeLastPacketReceived;
			timeLastPacketReceived = TimeUtil::Get().GetTimeSinceStart();
			return 2;
		}
		void ProcessTimedOutPackets()
		{
			double timeoutTime = TimeUtil::Get().GetTimeSinceStart() - PACKET_TIMEOUT_TIME;
			while(!inFlightPackets.empty())
			{
				const InFlightPacket& nextInFlightPacket = inFlightPackets.front();
				if(nextInFlightPacket.GetTimeDispatched() < timeoutTime)
				{
					HandlePacketDeliveryFailure(nextInFlightPacket);
					inFlightPackets.pop_front();
				}
				else
					break;
			}
		}
		double GetPacketSendRate(double minPacketRate = MIN_PACKET_RATE, double maxPacketRate = MAX_PACKET_RATE) const
		{
			if(deliveredPacketCount == 0)
				return maxPacketRate;
			double lossToDeliveredRate = static_cast<double>(droppedPacketCount) / deliveredPacketCount;
			if(lossToDeliveredRate < minPacketRate)
				return minPacketRate;
			if(lossToDeliveredRate > maxPacketRate)
				return maxPacketRate;
			return lossToDeliveredRate;
		}
		double GetEstimatedSendRate() const { return estimatedSendRate; }
		double GetRTT() const { return RTT; }
		bool IsConnected() const
		{
			return timeLastSuccessReceived + TIME_TO_TIMEOUT > TimeUtil::Get().GetTimeSinceStart();
		}
		std::deque<InFlightPacket> GetInFlightPackets() const { return inFlightPackets; }
		
	private:
		class AckRange
		{
			public:
				AckRange():
				start(0),
				count(0)
				{}
				AckRange(SequenceNumberType initStart):
				start(initStart),
				count(1)
				{}
				
				bool ExtendIfShould(SequenceNumberType sequenceNumber)
				{
					if(sequenceNumber == start + count)
					{
						count++;
						return true;
					}
					else
						return false;
				}
				void Write(OutMemBitStream& packet)
				{
					packet.WriteBits(start);
					bool hasCount = count > 1;
					packet.WriteBits(hasCount);
					if(hasCount)
						packet.WriteBits(count - 1, 7);
				}
				bool Read(InMemBitStream& packet)
				{
					if(!packet.ReadBits(start))	return false;
					bool hasCount = false;
					if(!packet.ReadBits(hasCount)) return false;
					if(hasCount)
					{
						if(!packet.ReadBits(count, 7)) return false;
						++count;
					}
					else
						count = 1;
					return true;
				}
				
				SequenceNumberType GetStart() const { return start; }
				uint8_t  GetCount() const { return count; }
				
			private:
				SequenceNumberType start;
				uint8_t count;
		};
		
		void HandlePacketDeliverySuccess(const InFlightPacket& inFlightPacket)
		{
			++deliveredPacketCount;
			timeLastSuccessReceived = TimeUtil::Get().GetTimeSinceStart();
			oldRTTs.emplace_back(TimeUtil::Get().GetTimeSinceStart() - inFlightPacket.GetTimeDispatched());
			oldRTTs.pop_front();
			RTT = 0.0;
			for(double oldRTT : oldRTTs)
				RTT += oldRTT;
			RTT /= oldRTTs.size();
			inFlightPacket.HandleDeliverySuccess(this);
		}
		
		void HandlePacketDeliveryFailure(const InFlightPacket& inFlightPacket)
		{
			++droppedPacketCount;
			inFlightPacket.HandleDeliveryFailure(this);
		}
		
		SequenceNumberType nextOutgoingSequenceNumber;
		SequenceNumberType nextExpectedSequenceNumber;
		
		uint64_t dispatchedPacketCount;
		uint64_t droppedPacketCount;
		uint64_t deliveredPacketCount;
		double timeLastSuccessReceived;
		double timeLastPacketReceived;
		double estimatedSendRate;
		std::deque<double> oldRTTs; // For calculating average
		double RTT;
		
		std::deque<AckRange>      pendingAcks;
		std::deque<InFlightPacket> inFlightPackets;
};
