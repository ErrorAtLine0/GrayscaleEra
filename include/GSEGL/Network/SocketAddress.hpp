#pragma once
#include <memory>
#include <cstring>
#include <cstdint>
#ifdef _WIN32
	#include <ws2tcpip.h>
#else
	#include <netinet/in.h>
#endif
#include "InMemBitStream.hpp"
#include "OutMemBitStream.hpp"

enum SocketAddressFamily
{
	INET = AF_INET,
	INET6 = AF_INET6
};

class SocketAddress
{
	public:
		SocketAddress(uint16_t port, SocketAddressFamily family) // Listen to all addresses on port
		{
			std::memset(&rawAddr, 0, sizeof(sockaddr_storage));
			if(family == INET)
			{
				GetAsSockAddrIn()->sin_family = AF_INET;
				GetAsSockAddrIn()->sin_addr.s_addr = htonl(INADDR_ANY);
				GetAsSockAddrIn()->sin_port = htons(port);
			}
			else
			{
				GetAsSockAddrIn6()->sin6_family = AF_INET6;
				GetAsSockAddrIn6()->sin6_addr = in6addr_any;
				GetAsSockAddrIn6()->sin6_port = htons(port);
			}
		}
		SocketAddress(const sockaddr& addr)
		{
			std::memset(&rawAddr, 0, sizeof(sockaddr_storage));
			if(addr.sa_family == AF_INET)
				std::memcpy(&rawAddr, &addr, sizeof(sockaddr_in));
			else
				std::memcpy(&rawAddr, &addr, sizeof(sockaddr_in6));
		}
		SocketAddress(const sockaddr_in& addr)
		{
			std::memset(&rawAddr, 0, sizeof(sockaddr_storage));
			std::memcpy(&rawAddr, &addr, sizeof(sockaddr_in));
		}
		SocketAddress(const sockaddr_in6& addr)
		{
			std::memset(&rawAddr, 0, sizeof(sockaddr_storage));
			std::memcpy(&rawAddr, &addr, sizeof(sockaddr_in6));
		}
		SocketAddress(const SocketAddress& addr)
		{
			std::memset(&rawAddr, 0, sizeof(sockaddr_storage));
			std::memcpy(&rawAddr, &addr.rawAddr, addr.GetSize());
		}
		SocketAddress()
		{
			std::memset(&rawAddr, 0, sizeof(sockaddr_storage)); 
		}
		size_t GetSize() const { return rawAddr.ss_family == AF_INET ? sizeof(sockaddr_in) : sizeof(sockaddr_in6); }
		sockaddr_in* GetAsSockAddrIn() { return reinterpret_cast<sockaddr_in*>(&rawAddr); }
		sockaddr_in6* GetAsSockAddrIn6() { return reinterpret_cast<sockaddr_in6*>(&rawAddr); }
		const sockaddr_in* GetAsSockAddrIn() const { return reinterpret_cast<const sockaddr_in*>(&rawAddr); }
		const sockaddr_in6* GetAsSockAddrIn6() const { return reinterpret_cast<const sockaddr_in6*>(&rawAddr); }
		
		bool Read(InMemBitStream& inStream)
		{
			std::memset(&rawAddr, 0, sizeof(sockaddr_storage));
			bool isINET6 = false;
			if(!inStream.ReadBits(isINET6)) return false;
			if(isINET6)
			{
				rawAddr.ss_family = INET6;
				if(!inStream.ReadBits(reinterpret_cast<void*>(&(GetAsSockAddrIn6()->sin6_addr)), sizeof(in6_addr))) return false;
				if(!inStream.ReadBitsNoFlip(GetAsSockAddrIn6()->sin6_port)) return false;
			}
			else
			{
				rawAddr.ss_family = INET;
				if(!inStream.ReadBitsNoFlip(GetAsSockAddrIn()->sin_addr.s_addr)) return false;
				if(!inStream.ReadBitsNoFlip(GetAsSockAddrIn()->sin_port)) return false;
			}
			return true;
		}
		
		void Write(OutMemBitStream& outStream) const
		{
			if(rawAddr.ss_family == INET6)
			{
				outStream.WriteBits(true);
				outStream.WriteBits(reinterpret_cast<const void*>(&(GetAsSockAddrIn6()->sin6_addr)), sizeof(in6_addr));
				outStream.WriteBitsNoFlip(GetAsSockAddrIn6()->sin6_port);
			}
			else
			{
				outStream.WriteBits(false);
				outStream.WriteBitsNoFlip(GetAsSockAddrIn()->sin_addr.s_addr);
				outStream.WriteBitsNoFlip(GetAsSockAddrIn()->sin_port);
			}
		}
		
		bool SameIPv4Addr(const SocketAddress& addr) const
		{
			if(addr.rawAddr.ss_family == AF_INET && rawAddr.ss_family == AF_INET)
				return addr.GetAsSockAddrIn()->sin_addr.s_addr == GetAsSockAddrIn()->sin_addr.s_addr;
			return false;
		}
		
		bool operator==(const SocketAddress& addr) const
		{
			if(addr.rawAddr.ss_family == rawAddr.ss_family)
			{
				if(rawAddr.ss_family == AF_INET)
				{
					return (addr.GetAsSockAddrIn()->sin_addr.s_addr == GetAsSockAddrIn()->sin_addr.s_addr)&&
							(addr.GetAsSockAddrIn()->sin_port == GetAsSockAddrIn()->sin_port);
				}
				else
				{
					return !std::memcmp(reinterpret_cast<const char*>(&(addr.rawAddr)),
									    reinterpret_cast<const char*>(&rawAddr),
									    sizeof(sockaddr_in6));
				}
			}
			return false;
		}
		bool operator!=(const SocketAddress& addr) const { return !(*this == addr); }
		sockaddr_storage rawAddr;
};

typedef std::shared_ptr<SocketAddress> SocketAddressPtr;
