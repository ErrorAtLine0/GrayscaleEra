#pragma once
#include "DeliveryNotificationManager.hpp"
#include <list>

class ReliableDataManager
{
	public:
		ReliableDataManager():
		nextSequenceNumber(0),
		nextExpectedSequenceNumber(0),
		nextExpectedOnOtherSide(0)
		{}
		
		void QueueDataToSend(const OutMemBitStream& outStream)
		{
			dataToSend.emplace_back(std::make_shared<ReliableTransmissionData>(this, nextSequenceNumber++, outStream));
		}
		void WriteDataToPacket(OutMemBitStream& outStream, InFlightPacket* inFlightPacket)
		{
			std::shared_ptr<ReliableTransmissionData>& data = dataToSend.front();
			outStream.WriteBits(data->reliableSequenceNumber);
			outStream.WriteBits(reinterpret_cast<const void*>(data->outStream.GetBufferPtr()), data->outStream.GetBitLength());
			inFlightPacket->SetTransmissionData(TRANSMISSIONDATA_RELIABLE, data);
			dataToSend.pop_front();
		}
		int CheckIfNewData(InMemBitStream& inStream) // 0 = Memory Error, 1 = Not new packet, 2 = OK
		{
			SequenceNumberType receivedSequenceNumber;
			if(!inStream.ReadBits(receivedSequenceNumber)) return 0;
			if(receivedSequenceNumber == nextExpectedSequenceNumber)
			{
				++nextExpectedSequenceNumber;
				return 2;
			}
			return 1;
		}
		bool ContainsDataToWrite() const { return !dataToSend.empty(); }
		
	private:
		class ReliableTransmissionData : public TransmissionData, public std::enable_shared_from_this<ReliableTransmissionData>
		{
			public:
				ReliableTransmissionData(ReliableDataManager* initReliableDataManager, SequenceNumberType initReliableSequenceNumber, const OutMemBitStream& initOutStream):
				reliableDataManager(initReliableDataManager),
				reliableSequenceNumber(initReliableSequenceNumber),
				outStream(initOutStream)
				{}
				
				void HandleDeliverySuccess(DeliveryNotificationManager* deliveryNotifier)
				{
					if(reliableDataManager->nextExpectedOnOtherSide != reliableSequenceNumber)
						ResendData();
					else
						++reliableDataManager->nextExpectedOnOtherSide;
				}
				void HandleDeliveryFailure(DeliveryNotificationManager* deliveryNotifier)
				{
					ResendData();
				}
				void ResendData()
				{
					for(auto it = reliableDataManager->dataToSend.begin(); it != reliableDataManager->dataToSend.end(); ++it)
					{
						if(((*it)->reliableSequenceNumber < SEQUENCE_NUMBER_WRAP_MIN && reliableSequenceNumber > SEQUENCE_NUMBER_WRAP_MAX)||
						   ((*it)->reliableSequenceNumber > reliableSequenceNumber))
						{
							reliableDataManager->dataToSend.emplace(it, shared_from_this());
							return;
						}
					}
					reliableDataManager->dataToSend.emplace_back(shared_from_this());
				}
				ReliableDataManager* reliableDataManager;
				SequenceNumberType reliableSequenceNumber;
				OutMemBitStream outStream;
		};
		
		std::list<std::shared_ptr<ReliableTransmissionData>> dataToSend;
		SequenceNumberType nextSequenceNumber;
		SequenceNumberType nextExpectedSequenceNumber;
		SequenceNumberType nextExpectedOnOtherSide;
};
