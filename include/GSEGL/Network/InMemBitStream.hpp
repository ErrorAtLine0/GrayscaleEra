#pragma once
#include <cstdint>
#include <type_traits>
#include <memory>
#include "ByteUtil.hpp"

class InMemBitStream
{
	public:
		InMemBitStream(const uint8_t* nBuffer, uint64_t byteCapacity, bool makeCopy = true);
		~InMemBitStream();
		std::shared_ptr<InMemBitStream> ReadCompressedData();
		bool ReadBits(void* data, uint64_t bitCount);
		template<typename T> bool ReadBits(T& data, uint64_t bitCount = sizeof(T) * 8)
		{
			static_assert(std::is_arithmetic<T>::value||std::is_enum<T>::value,
						  "Generic ReadBits only supports primitive data types");
			if(ByteUtil::Get().IsBigEndian())
			{
				T readData;
				if(!ReadBits(static_cast<void*>(&readData), bitCount))
					return false;
				data = ByteUtil::Get().ByteSwap(readData);
				return true;
			}
			else
				return ReadBits(static_cast<void*>(&data), bitCount);
		}
		template<typename T> bool ReadBitsNoFlip(T& data, uint64_t bitCount = sizeof(T) * 8)
		{
			static_assert(std::is_arithmetic<T>::value||std::is_enum<T>::value,
						  "Generic ReadBitsNoFlip only supports primitive data types");
			return ReadBits(static_cast<void*>(&data), bitCount);
		}
		bool ReadBits(bool& data);
		bool ReadFixedFloat(float& data, float min, float precision, uint64_t bitCount = sizeof(float) * 8);
		uint64_t GetRemainingBits() const;
		uint64_t GetRemainingBytes() const;
		const uint8_t* GetBufferPtr() const;
	private:
		void ReadBitsFromByte(uint8_t* data, uint64_t bitCount);
		const uint8_t* buffer;
		uint64_t bitHead;
		uint64_t bitCapacity;
		bool ownsData;
};
