#pragma once
#include "SocketAddress.hpp"
#include "UDPSocket.hpp"
#include "TCPSocket.hpp"
#include "../UTF8String.hpp"
#ifdef _WIN32
	#include <ws2tcpip.h>
#else
	#include <netdb.h>
	#include <arpa/inet.h>
#endif
#include <vector>
#include <algorithm>
#include "ByteUtil.hpp"
#include "../ErrorUtil.hpp"

template <class T>
class SocketSet
{
	public:
		SocketSet() : max(0)
		{
			FD_ZERO(&rawSet);
		}
		void Clear()
		{
			FD_ZERO(&rawSet);
			socketVec.clear();
			max = 0;
		}
		void RemoveSocket(const T& sock)
		{
			FD_CLR(sock->rawSock, &rawSet);
			socketVec.erase(std::remove(socketVec.begin(), socketVec.end(), sock), socketVec.end());
		#ifndef _WIN32
			if(sock->rawSock == max)
				RecalcMaxDescriptor();
		#endif
		}
		void AddSocket(const T& sock)
		{
			FD_SET(sock->rawSock, &rawSet);
			socketVec.push_back(sock);
		#ifndef _WIN32
			if(sock->rawSock > max)
				max = sock->rawSock;
		#endif
		}
		const std::vector<T> GetVector() const
		{
			return socketVec;
		}
		fd_set CopyRawSet() const
		{
			return rawSet;
		}
		size_t GetSize() const
		{
			return socketVec.size();
		}
		int GetMaximumDescriptor() const
		{
			return max;
		}
	private:
		int max;
		fd_set rawSet;
		std::vector<T> socketVec;
	#ifndef _WIN32
		void RecalcMaxDescriptor()
		{
			auto it = std::max_element(socketVec.begin(), socketVec.end(),
										[] (const T& s1, const T& s2)
										{
											return s1->rawSock < s2->rawSock;
										});
			max = (*it)->rawSock;
		}
	#endif
};

class SocketUtil
{
	public:
		static SocketUtil& Get()
		{
			static SocketUtil sInstance;
			return sInstance;
		}
		void Init()
		{
			ByteUtil::Get().CalculateEndianess();
		#ifdef _WIN32
			WSADATA unused;
			WSAStartup(MAKEWORD(2,2), &unused);
		#endif
		}
		void Cleanup()
		{
		#ifdef _WIN32
			WSACleanup();
		#endif
		}
		
		UDPSocketPtr CreateUDPSocket(SocketAddressFamily family)
		{
			SOCKET s = socket(family, SOCK_DGRAM, IPPROTO_UDP);
			if(s == INVALID_SOCKET)
			{
				ErrorUtil::Get().SetError("Network", "SocketUtil::CreateUDPSocket Could not create socket");
				return nullptr;
			}
			int optionVal = 1;
			setsockopt(s, IPPROTO_IPV6, IPV6_V6ONLY, reinterpret_cast<char*>(&optionVal), sizeof(optionVal));
			return UDPSocketPtr(new UDPSocket(s));
		}
		
		TCPSocketPtr CreateTCPSocket(SocketAddressFamily family)
		{
			SOCKET s = socket(family, SOCK_STREAM, IPPROTO_TCP);
			int optionVal = 0;
			setsockopt(s, IPPROTO_IPV6, IPV6_V6ONLY, reinterpret_cast<char*>(&optionVal), sizeof(optionVal));
			if(s == INVALID_SOCKET)
			{
				ErrorUtil::Get().SetError("Network", "SocketUtil::CreateTCPSocket Could not create socket");
				return nullptr;
			}
			return TCPSocketPtr(new TCPSocket(s));
		}
		
		int Select(const SocketSet<TCPSocketPtr>* inReadSet, SocketSet<TCPSocketPtr>* outReadSet,
				   const SocketSet<TCPSocketPtr>* inWriteSet, SocketSet<TCPSocketPtr>* outWriteSet,
				   const SocketSet<TCPSocketPtr>* inExceptSet, SocketSet<TCPSocketPtr>* outExceptSet,
				   double timeout)
		{
			fd_set read, write, except;
			fd_set* readPtr = CopySetToPtr(read, inReadSet);
			fd_set* writePtr = CopySetToPtr(write, inWriteSet);
			fd_set* exceptPtr = CopySetToPtr(except, inExceptSet);
		#ifdef _WIN32
			int toRet = selectWithTimeout(0, readPtr, writePtr, exceptPtr, timeout);
		#else
			int ndfs, readMax = 0, writeMax = 0, exceptMax = 0;
			if(inReadSet)
				readMax = inReadSet->GetMaximumDescriptor();
			if(inWriteSet)
				writeMax = inWriteSet->GetMaximumDescriptor();
			if(inExceptSet)
				exceptMax = inExceptSet->GetMaximumDescriptor();
			if(readMax >= writeMax && readMax >= exceptMax)
				ndfs = readMax;
			else if(writeMax >= exceptMax)
				ndfs = writeMax;
			else
				ndfs = exceptMax;
			int toRet = selectWithTimeout(ndfs+1, readPtr, writePtr, exceptPtr, timeout);
		#endif
			if(toRet > 0)
			{
				FillSet(outReadSet, inReadSet, read);
				FillSet(outWriteSet, inWriteSet, write);
				FillSet(outExceptSet, inExceptSet, except);
			}
			return toRet;
		}
		
		int Select(const SocketSet<UDPSocketPtr>* inReadSet, SocketSet<UDPSocketPtr>* outReadSet,
				   const SocketSet<UDPSocketPtr>* inWriteSet, SocketSet<UDPSocketPtr>* outWriteSet,
				   const SocketSet<UDPSocketPtr>* inExceptSet, SocketSet<UDPSocketPtr>* outExceptSet,
				   double timeout)
		{
			fd_set read, write, except;
			fd_set* readPtr = CopySetToPtr(read, inReadSet);
			fd_set* writePtr = CopySetToPtr(write, inWriteSet);
			fd_set* exceptPtr = CopySetToPtr(except, inExceptSet);
		#ifdef _WIN32
			int toRet = selectWithTimeout(0, readPtr, writePtr, exceptPtr, timeout);
		#else
			int ndfs, readMax = 0, writeMax = 0, exceptMax = 0;
			if(inReadSet)
				readMax = inReadSet->GetMaximumDescriptor();
			if(inWriteSet)
				writeMax = inWriteSet->GetMaximumDescriptor();
			if(inExceptSet)
				exceptMax = inExceptSet->GetMaximumDescriptor();
			if(readMax >= writeMax && readMax >= exceptMax)
				ndfs = readMax;
			else if(writeMax >= exceptMax)
				ndfs = writeMax;
			else
				ndfs = exceptMax;
			int toRet = selectWithTimeout(ndfs+1, readPtr, writePtr, exceptPtr, timeout);
		#endif
			if(toRet > 0)
			{
				FillSet(outReadSet, inReadSet, read);
				FillSet(outWriteSet, inWriteSet, write);
				FillSet(outExceptSet, inExceptSet, except);
			}
			return toRet;
		}
		
		SocketAddressPtr GetLocalIPv4Addr(const SocketAddressPtr& server)
		{
		    SOCKET sock = socket(INET, SOCK_DGRAM, 0);
		    if(sock < 0)
		    {
				ErrorUtil::Get().SetError("Network", "SocketUtil::GetLocalIPv4Addr Failed to create socket");
				return nullptr;
			}
			if(!server)
			{
				ErrorUtil::Get().SetError("Network", "SocketUtil::GetLocalIPv4Addr Invalid server specified");
				return nullptr;
		    }
		    if(connect(sock, reinterpret_cast<const sockaddr*>(&server->rawAddr), sizeof(sockaddr_in)) < 0)
			{
				ErrorUtil::Get().SetError("Network", "SocketUtil::GetLocalIPv4Addr Failed to connect to server");
				return nullptr;
		    }
		    sockaddr_in name;
		    socklen_t namelen = sizeof(name);
		    if(getsockname(sock, reinterpret_cast<sockaddr*>(&name), &namelen) == -1)
			{
				ErrorUtil::Get().SetError("Network", "SocketUtil::GetLocalIPv4Addr Could not get name of socket");
				return nullptr;
			}
		#ifdef _WIN32
			closesocket(sock);
		#else
			close(sock);
		#endif
			return std::make_shared<SocketAddress>(name);
		}
		
		SocketAddressPtr CreateAddrFromString(const UTF8String& host, const UTF8String& service, SocketAddressFamily addrFamily)
		{
			addrinfo hint;
			memset(&hint, 0, sizeof(hint));
			hint.ai_family |= addrFamily;
			addrinfo* result = nullptr;
			int error = getaddrinfo(host.GetCString(), service.GetCString(), &hint, &result);
			if(error != 0 && result != nullptr)
			{
				freeaddrinfo(result);
				ErrorUtil::Get().SetError("Network", "SocketUtil::CreateAddrFromString Could not find address specified");
				return nullptr;
			}
			else if(result == nullptr)
			{
				ErrorUtil::Get().SetError("Network", "SocketUtil::CreateAddrFromString Could not find address specified");
				return nullptr;
			}
			while(!result->ai_addr && result->ai_next)
			{
				result = result->ai_next;
			}
			if(!result->ai_addr)
			{
				freeaddrinfo(result);
				ErrorUtil::Get().SetError("Network", "SocketUtil::CreateAddrFromString Could not find address specified");
				return nullptr;
			}
			SocketAddressPtr toRet = std::make_shared<SocketAddress>(*result->ai_addr);
			freeaddrinfo(result);
			return toRet;
		}
	private:
		void FillSet(SocketSet<TCPSocketPtr>* outSocks, const SocketSet<TCPSocketPtr>* inSocks, const fd_set& set)
		{
			if(inSocks && outSocks)
			{
				outSocks->Clear();
				for(const TCPSocketPtr& sock : inSocks->GetVector())
					if(FD_ISSET(sock->rawSock, &set))
						outSocks->AddSocket(sock);
			}
		}

		void FillSet(SocketSet<UDPSocketPtr>* outSocks, const SocketSet<UDPSocketPtr>* inSocks, const fd_set& set)
		{
			if(inSocks && outSocks)
			{
				outSocks->Clear();
				for(const UDPSocketPtr& sock : inSocks->GetVector())
					if(FD_ISSET(sock->rawSock, &set))
						outSocks->AddSocket(sock);
			}
		}
		
		fd_set* CopySetToPtr(fd_set& rawSet, const SocketSet<TCPSocketPtr>* socketSet)
		{
			if(socketSet)
			{
				rawSet = socketSet->CopyRawSet();
				return &rawSet;
			}
			else
				return nullptr;
		}
		
		fd_set* CopySetToPtr(fd_set& rawSet, const SocketSet<UDPSocketPtr>* socketSet)
		{
			if(socketSet)
			{
				rawSet = socketSet->CopyRawSet();
				return &rawSet;
			}
			else
				return nullptr;
		}
		
		int selectWithTimeout(int ndfs, fd_set* readfds, fd_set* writefds, fd_set* exceptfds, double floatTimeout)
		{
			if(floatTimeout < 0)
				return select(ndfs, readfds, writefds, exceptfds, nullptr);
			else
			{
				timeval timeout;
				timeout.tv_sec = static_cast<long>(floatTimeout);
				timeout.tv_usec = static_cast<long>((floatTimeout - static_cast<long>(floatTimeout)) * 1000000);
				return select(ndfs, readfds, writefds, exceptfds, &timeout);
			}
		}
};
