#pragma once
#include "BasicSocket.hpp"

class TCPSocket;
typedef std::shared_ptr<TCPSocket> TCPSocketPtr;

class TCPSocket : public BasicSocket
{
	public:
		TCPSocket(SOCKET s) : BasicSocket(s) {}
		~TCPSocket() { }
		
		int Bind(const SocketAddress& address)
		{
			int err = bind(rawSock, reinterpret_cast<const sockaddr*>(&address.rawAddr), address.GetSize());
			if(err != 0)
			{
				ErrorUtil::Get().SetError("Network", "TCPSocket::Bind");
				return -ErrorUtil::Get().GetErrorCode();
			}
			return 0;
		}
		
		int Connect(const SocketAddress& address)
		{
			int err = connect(rawSock, reinterpret_cast<const sockaddr*>(&address.rawAddr), address.GetSize());
			if(err != 0)
			{
				ErrorUtil::Get().SetError("Network", "TCPSocket::Connect");
				return -ErrorUtil::Get().GetErrorCode();
			}
			return 0;
		}
		
		int Listen(int backLog)
		{
			int err = listen(rawSock, backLog);
			if(err != 0)
			{
				ErrorUtil::Get().SetError("Network", "TCPSocket::Listen");
				return -ErrorUtil::Get().GetErrorCode();
			}
			return 0;
		}
		
		TCPSocketPtr Accept(SocketAddress& from)
		{
		#ifdef _WIN32
			int length = from.GetSize();
		#else
			unsigned length = from.GetSize();
		#endif
			SOCKET newSock = accept(rawSock, reinterpret_cast<sockaddr*>(&from.rawAddr), &length);
			if(newSock == INVALID_SOCKET)
			{
				ErrorUtil::Get().SetError("Network", "TCPSocket::Accept");
				return nullptr;
			}
			return TCPSocketPtr(new TCPSocket(newSock));
		}
		
		int Send(const void* data, int len)
		{
			int byteSentCount = send(rawSock,
									   static_cast<const char*>(data),
									   len, 0);
			if(byteSentCount < 0)
			{
				#ifdef _WIN32
					if(ErrorUtil::Get().GetErrorCode() != WSAEWOULDBLOCK)
						ErrorUtil::Get().SetError("Network", "TCPSocket::Send");
				#else
					if(ErrorUtil::Get().GetErrorCode() != EAGAIN)
						ErrorUtil::Get().SetError("Network", "TCPSocket::Send");
				#endif
				return -ErrorUtil::Get().GetErrorCode();
			}
			return byteSentCount;
		}
		
		int Receive(void* buffer, int len)
		{
			int readByteCount = recv(rawSock,
									 static_cast<char*>(buffer),
									 len, 0);
			if(readByteCount < 0)
			{
				#ifdef _WIN32
					if(ErrorUtil::Get().GetErrorCode() != WSAEWOULDBLOCK)
						ErrorUtil::Get().SetError("Network", "TCPSocket::Receive");
				#else
					if(ErrorUtil::Get().GetErrorCode() != EAGAIN)
						ErrorUtil::Get().SetError("Network", "TCPSocket::Receive");
				#endif
				return -ErrorUtil::Get().GetErrorCode();
			}
			return readByteCount;
		}
};
