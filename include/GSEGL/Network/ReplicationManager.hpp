#pragma once
#include <unordered_map>
#include <vector>
#include "DeliveryNotificationManager.hpp"
#include "ByteUtil.hpp"
#include <GSEGL/Graphics/Math.hpp>

typedef uint32_t NetworkID;
typedef uint16_t PropertyBitField;
typedef uint8_t ClassID;

#define CLASS_IDENTIFICATION(inClass)\
static ClassID classID;\
virtual ClassID GetClassID() const { return classID; }\
static GameObject* CreateInstance()  { return new inClass(); }

class GameObject
{
	public:
		GameObject():
		netID(0),
		isSetToDestroy(false),
		changedProperties(0)
		{}
		virtual ~GameObject() { }
		virtual ClassID GetClassID() const = 0;
		
		virtual bool Read(InMemBitStream& inStream) = 0;
		virtual void Write(OutMemBitStream& outStream, PropertyBitField propertyBitField) const = 0;
		virtual void ClientUpdate() = 0;
		virtual void ServerUpdate() = 0;
		virtual void CollisionCheck() = 0;
		virtual void SetToNextPosition() = 0;
		virtual void Draw(const Vec2& cameraPos, float cameraZoom) = 0;
		virtual void ServerDestroy() { }
		
		void ForceDestroy() { isSetToDestroy = true; }
		bool IsSetToDestroy() const { return isSetToDestroy; }
		PropertyBitField GetChangedProperties() const { return changedProperties; }
		void ClearChangedProperties() { changedProperties = 0; }
		
		NetworkID netID;
	protected:
		bool isSetToDestroy;
		PropertyBitField changedProperties;
};

class LinkingContext
{
	public:
		LinkingContext():
		nextNetID(0)
		{}
		
		GameObject* GetGameObject(NetworkID netID)
		{
			auto it = netIDToGameObjectMap.find(netID);
			if(it != netIDToGameObjectMap.end())
				return it->second;
			else
				return nullptr;
		}
		NetworkID GetNetID(GameObject* gameObject, bool createIfNotFound)
		{
			if(!gameObject)
				return 0;
			if(gameObject->netID != 0)
				return gameObject->netID;
			else if(createIfNotFound)
			{
				NetworkID newNetID = GetNextNetID();
				AddGameObject(gameObject, newNetID);
				return newNetID;
			}
			else
				return 0;
		}
		void RemoveGameObject(GameObject* gameObject)
		{
			netIDToGameObjectMap.erase(gameObject->netID);
			gameObject->netID = 0;
		}
		void AddGameObject(GameObject* gameObject, NetworkID netID)
		{
			newObjects.emplace_back(gameObject);
			netIDToGameObjectMap[netID] = gameObject;
			gameObject->netID = netID;
		}
		std::vector<GameObject*>& GetNewObjectsThisFrame() { return newObjects; }
		std::unordered_map<NetworkID, GameObject*>& GetNetIDToGameObjectMap() { return netIDToGameObjectMap; }
	private:
		NetworkID GetNextNetID()
		{
			NetworkID toRet = nextNetID++;
			while(netIDToGameObjectMap.find(toRet) != netIDToGameObjectMap.end() || toRet == 0)
				toRet = nextNetID++;
			return toRet;
		}
		
		std::vector<GameObject*> newObjects;
		std::unordered_map<NetworkID, GameObject*> netIDToGameObjectMap;
		NetworkID nextNetID;
};

typedef GameObject* (*GameObjectCreationFunc)();

class ObjectCreationRegistry
{
	public:
		static ObjectCreationRegistry& Get()
		{
			static ObjectCreationRegistry sInstance;
			return sInstance;
		}
		template<class T> void RegisterCreationFunction()
		{
			T::classID = nextClassID++;
			creationFunctions.push_back(T::CreateInstance);
		}
		GameObject* CreateGameObject(ClassID classID)
		{
			if(classID >= nextClassID)
				return nullptr;
			else
				return creationFunctions[classID]();
		}
	private:
		ObjectCreationRegistry():
		nextClassID(0)
		{}
		ClassID nextClassID;
		std::vector<GameObjectCreationFunc> creationFunctions;
};

class ReplicationManager
{
	public:
		ReplicationManager(LinkingContext* initLinkingContext):
		nextNetIDToWrite(0),
		linkingContext(initLinkingContext)
		{}
		
		bool ProcessReplicationCommand(InMemBitStream& inStream)
		{
			ReplicationHeader rh;
			if(!rh.Read(inStream)) return false;
			switch(rh.action)
			{
				case RA_Create:
				{
					GameObject* gameObj = linkingContext->GetGameObject(rh.netID);
					if(gameObj)
					{
						if(!gameObj->Read(inStream)) return false;
					}
					else
					{
						gameObj = ObjectCreationRegistry::Get().CreateGameObject(rh.classID);
						linkingContext->AddGameObject(gameObj, rh.netID);
						if(!gameObj->Read(inStream)) return false;
					}
					break;
				}
				case RA_Update:
				{
					GameObject* gameObj = linkingContext->GetGameObject(rh.netID);
					if(gameObj)
					{
						if(!gameObj->Read(inStream)) return false;
					}
					else
					{
						gameObj = ObjectCreationRegistry::Get().CreateGameObject(rh.classID);
						if(!gameObj->Read(inStream)) return false;
						delete gameObj;
					}
					break;
				}
				case RA_Destroy:
				{
					GameObject* gameObj = linkingContext->GetGameObject(rh.netID);
					if(gameObj)
						linkingContext->RemoveGameObject(gameObj);
					// The object's memory is freed/deleted by the user (in NetworkManagerClient)
					break;
				}
				default:
					break;
			}
			return true;
		}
		void WriteBatchedCommands(OutMemBitStream& outStream, InFlightPacket* inFlightPacket)
		{
			std::shared_ptr<ReplicationTransmissionData> repTransData = nullptr;
			auto it = netIDToRepCommand.find(nextNetIDToWrite);
			std::size_t commandsWrittenToPacket = 0;
			for(;commandsWrittenToPacket < netIDToRepCommand.size() && outStream.GetByteLength() < 1000; ++it, commandsWrittenToPacket++)
			{
				if(it == netIDToRepCommand.end())
					it = netIDToRepCommand.begin();
				ReplicationCommand& replicationCommand = it->second;
				if(replicationCommand.HasUpdates())
				{
					NetworkID netID = it->first;
					ReplicationAction action = replicationCommand.GetAction();
					if(action == RA_Destroy)
					{
						ReplicationHeader rh(action, netID);
						rh.Write(outStream);
						if(!repTransData)
						{
							repTransData = std::make_shared<ReplicationTransmissionData>(this);
							inFlightPacket->SetTransmissionData(TRANSMISSIONDATA_REPLICATION, repTransData);
						}
						repTransData->AddReplication(netID, action);
						replicationCommand.ClearUpdates();
					}
					else
					{
						GameObject* gameObj = linkingContext->GetGameObject(netID);
						if(gameObj)
						{
							ReplicationHeader rh(action, netID, gameObj->GetClassID());
							rh.Write(outStream);
							PropertyBitField propertyBitField = replicationCommand.GetPropertyBitField();
							gameObj->Write(outStream, propertyBitField);
							if(!repTransData)
							{
								repTransData = std::make_shared<ReplicationTransmissionData>(this);
								inFlightPacket->SetTransmissionData(TRANSMISSIONDATA_REPLICATION, repTransData);
							}
							repTransData->AddReplication(netID, action, propertyBitField);
							replicationCommand.ClearUpdates();
						}
					}
				}
			}
			if(netIDToRepCommand.size() > 0)
			{
				if(it == netIDToRepCommand.end())
					it = netIDToRepCommand.begin();
				nextNetIDToWrite = it->first;
			}
		}
		void BatchCreate(NetworkID netID, PropertyBitField propertyBitField)
		{
			auto it = netIDToRepCommand.find(netID);
			if(it == netIDToRepCommand.end())
				netIDToRepCommand.emplace(netID, ReplicationCommand(propertyBitField));
			else
				it->second.AddPropertiesToUpdate(propertyBitField);
		}
		void BatchUpdate(NetworkID netID, PropertyBitField propertyBitField) { netIDToRepCommand[netID].AddPropertiesToUpdate(propertyBitField); }
		void BatchDestroy(NetworkID netID) { netIDToRepCommand[netID].SetActionToDestroy(); }
	private:
		enum ReplicationAction
		{
			RA_Create,
			RA_Update,
			RA_Destroy,
			RA_MAX
		};
		class ReplicationHeader
		{
			public:
				ReplicationHeader() {}
				ReplicationHeader(ReplicationAction initAction, NetworkID initNetID, ClassID initClassID = 0):
				action(initAction),
				netID(initNetID),
				classID(initClassID)
				{}
				void Write(OutMemBitStream& outStream) const
				{
					outStream.WriteBits(action, ByteUtil::GetRequiredBits<RA_MAX>::Value);
					outStream.WriteBits(netID);
					if(action != RA_Destroy)
						outStream.WriteBits(classID);
				}
				bool Read(InMemBitStream& inStream)
				{
					action = RA_Create;
					if(!inStream.ReadBits(action, ByteUtil::GetRequiredBits<RA_MAX>::Value)) return false;
					if(!inStream.ReadBits(netID)) return false;
					if(action != RA_Destroy)
						if(!inStream.ReadBits(classID)) return false;
					return true;
				}
				ReplicationAction action;
				NetworkID netID;
				ClassID classID;
		};
		class ReplicationCommand
		{
			public:
				ReplicationCommand():
				action(RA_Create),
				propertyBitField(0)
				{}
				ReplicationCommand(PropertyBitField initPropertyBitField):
				action(RA_Create),
				propertyBitField(initPropertyBitField)
				{}
				void AddPropertiesToUpdate(PropertyBitField propertiesToAdd) { propertyBitField |= propertiesToAdd; }
				void SetActionToUpdate()
				{
					if(action != RA_Destroy)
						action = RA_Update;
				}
				void SetActionToDestroy() { action = RA_Destroy; }
				void ClearUpdates()
				{
					propertyBitField = 0;
					if(action == RA_Destroy)
						action = RA_Update;
				}
				bool HasUpdates() const { return (action == RA_Destroy) || (propertyBitField != 0); }
				PropertyBitField GetPropertyBitField() const { return propertyBitField; }
				ReplicationAction GetAction() const { return action; }
			private:
				ReplicationAction action;
				PropertyBitField propertyBitField;
		};
		class ReplicationTransmissionData : public TransmissionData
		{
			public:
				ReplicationTransmissionData(ReplicationManager* initReplicationManager):
				replicationManager(initReplicationManager)
				{}
				void AddReplication(NetworkID netID, ReplicationAction action, PropertyBitField propertyBitField = 0)
				{
					replications.emplace_back(netID, action, propertyBitField);
				}
				void HandleDeliverySuccess(DeliveryNotificationManager* deliveryNotifier)
				{
					for(const ReplicationTransmission& rt : replications)
					{
						NetworkID netID = rt.GetNetID();
						switch(rt.GetAction())
						{
							case RA_Create:
							{
								replicationManager->HandleCreateAckd(netID);
								break;
							}
							case RA_Destroy:
							{
								replicationManager->RemoveFromReplication(netID);
								break;
							}
							default:
								break;
						}
					}
				}
				void HandleDeliveryFailure(DeliveryNotificationManager* deliveryNotifier)
				{
					for(const ReplicationTransmission& rt : replications)
					{
						NetworkID netID = rt.GetNetID();
						GameObject* go;
						switch(rt.GetAction())
						{
							case RA_Create:
							{
								go = replicationManager->GetLinkingContext()->GetGameObject(netID);
								if(go)
									replicationManager->BatchCreate(netID, rt.GetPropertyBitField());
								break;
							}
							case RA_Update:
							{
								go = replicationManager->GetLinkingContext()->GetGameObject(netID);
								if(go)
								{
									PropertyBitField propertyBitField = rt.GetPropertyBitField();
									for(auto& inFlightPacket : deliveryNotifier->GetInFlightPackets())
									{
										std::shared_ptr<ReplicationTransmissionData> repTransData = std::static_pointer_cast<ReplicationTransmissionData>(inFlightPacket.GetTransmissionData(TRANSMISSIONDATA_REPLICATION));
										if(repTransData)
											for(const ReplicationTransmission& otherRT : repTransData->replications)
												if(otherRT.GetNetID() == netID)
													propertyBitField &= ~otherRT.GetPropertyBitField();
									}
									if(propertyBitField)
										replicationManager->BatchUpdate(netID, propertyBitField);
								}
								break;
							}
							case RA_Destroy:
							{
								replicationManager->BatchDestroy(netID);
								break;
							}
							default:
								break;
						}
					}
				}
			private:
				class ReplicationTransmission
				{
					public:
						ReplicationTransmission(NetworkID initNetID, ReplicationAction initAction, PropertyBitField initPropertyBitField):
						netID(initNetID),
						action(initAction),
						propertyBitField(initPropertyBitField)
						{}
						NetworkID GetNetID() const { return netID; }
						ReplicationAction GetAction() const { return action; }
						PropertyBitField GetPropertyBitField() const { return propertyBitField; }
					private:
						NetworkID netID;
						ReplicationAction action;
						PropertyBitField propertyBitField;
				};
				ReplicationManager* replicationManager;
				std::vector<ReplicationTransmission> replications;
		};
		void HandleCreateAckd(NetworkID netID) { netIDToRepCommand[netID].SetActionToUpdate(); }
		void RemoveFromReplication(NetworkID netID) { netIDToRepCommand.erase(netID); }
		LinkingContext* GetLinkingContext() { return linkingContext; }
		
		NetworkID nextNetIDToWrite;
		LinkingContext* linkingContext;
		std::unordered_map<NetworkID, ReplicationCommand> netIDToRepCommand;
};
