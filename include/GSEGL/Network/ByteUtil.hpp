#pragma once
#include <cstdint>

inline uint16_t ByteSwap2(uint16_t data)
{
	return (data >> 8) | (data << 8);
}

inline uint32_t ByteSwap4(uint32_t data)
{
	return ((data >> 24) & 0x000000ff)|
		   ((data >> 8)  & 0x0000ff00)|
		   ((data << 8)  & 0x00ff0000)|
		   ((data << 24) & 0xff000000);
}

inline uint64_t ByteSwap8(uint64_t data)
{
	return ((data >> 56) & 0x00000000000000ff)|
		   ((data >> 40) & 0x000000000000ff00)|
		   ((data >> 24) & 0x0000000000ff0000)|
		   ((data >> 8)  & 0x00000000ff000000)|
		   ((data << 8)  & 0x000000ff00000000)|
		   ((data << 24) & 0x0000ff0000000000)|
		   ((data << 40) & 0x00ff000000000000)|
		   ((data << 56) & 0xff00000000000000);
}

template <typename tFrom, typename tTo>
class TypeAliaser
{
	public:
		TypeAliaser(tFrom fromVal) : asFromType(fromVal) {}
		tTo& Get() {return asToType;}
		
		union
		{
			tFrom asFromType;
			tTo   asToType;
		};
};

template <typename T, std::size_t tSize> class ByteSwapper;

template <typename T>
class ByteSwapper<T, 1>
{
	public:
		T Swap(T inData) const {return inData;}
};

template <typename T>
class ByteSwapper<T, 2>
{
	public:
		T Swap(T inData) const
		{
			uint16_t result = ByteSwap2(TypeAliaser<T, uint16_t>(inData).Get());
			return TypeAliaser<uint16_t, T>(result).Get();
		}
};

template <typename T>
class ByteSwapper<T, 4>
{
	public:
		T Swap(T inData) const
		{
			uint32_t result = ByteSwap4(TypeAliaser<T, uint32_t>(inData).Get());
			return TypeAliaser<uint32_t, T>(result).Get();
		}
};

template <typename T>
class ByteSwapper<T, 8>
{
	public:
		T Swap(T inData) const
		{
			uint64_t result = ByteSwap8(TypeAliaser<T, uint64_t>(inData).Get());
			return TypeAliaser<uint64_t, T>(result).Get();
		}
};

template<int tValue, int tBits>
struct GetRequiredBitsHelper
{
	enum {Value = GetRequiredBitsHelper<(tValue >> 1), tBits + 1>::Value};
};

template<int tBits>
struct GetRequiredBitsHelper<0, tBits>
{
	enum {Value = tBits};
};

class ByteUtil
{
	public:
		static ByteUtil& Get()
		{
			static ByteUtil sInstance;
			return sInstance;
		}
		template <typename T>
		T ByteSwap(T inData)
		{
			return ByteSwapper<T, sizeof(T)>().Swap(inData);
		}
		template<int tValue>
		struct GetRequiredBits
		{
			enum {Value = GetRequiredBitsHelper<tValue, 0>::Value};
		};
		uint32_t GetRequiredBitsNonConst(uint32_t val)
		{
			uint32_t toRet = 0;
			while(val != 0)
			{
				val = val >> 1;
				++toRet;
			}
			return toRet;
		}
		void CalculateEndianess()
		{
		    union {
		        uint32_t i;
		        char c[4];
		    } un = {0x01020304};
		
		    isBigEndian = (un.c[0] == 1); 
		}
		bool IsBigEndian() const {return isBigEndian;}
	private:
		bool isBigEndian;
};
