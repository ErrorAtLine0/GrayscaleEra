#pragma once
#include <cstdint>
#include <type_traits>
#include "ByteUtil.hpp"

class OutMemBitStream
{
	public:
		OutMemBitStream();
		OutMemBitStream(const OutMemBitStream& outStream);
		OutMemBitStream(OutMemBitStream&& outStream);
		void operator=(const OutMemBitStream& outStream);
		void operator=(OutMemBitStream&& outStream);
		~OutMemBitStream();
		void WriteCompressedData(const void* data, uint64_t byteCount);
		void WriteBits(const void* data, uint64_t bitCount);
		template<typename T> void WriteBits(const T& data, uint64_t bitCount = sizeof(T) * 8)
		{
			static_assert(std::is_arithmetic<T>::value||std::is_enum<T>::value,
						  "Generic WriteBits only supports primitive data types");
			if(ByteUtil::Get().IsBigEndian())
			{
				T swappedData = ByteUtil::Get().ByteSwap(data);
				WriteBits(static_cast<const void*>(&swappedData), bitCount);
			}
			else
				WriteBits(static_cast<const void*>(&data), bitCount);
		}
		template<typename T> void WriteBitsNoFlip(const T& data, uint64_t bitCount = sizeof(T) * 8)
		{
			static_assert(std::is_arithmetic<T>::value||std::is_enum<T>::value, "Generic WriteBitsNoFlip only supports primitive data types");
			WriteBits(static_cast<const void*>(&data), bitCount);
		}
		void WriteBits(const bool data);
		void WriteFixedFloat(const float data, float min, float precision, uint64_t bitCount = sizeof(float) * 8);
		const uint8_t* GetBufferPtr() const;
		uint64_t GetBitLength()  const;
		uint64_t GetByteLength() const;
	private:
		void WriteBitsToByte(uint8_t data, uint64_t bitCount);
		void ReallocBuffer(uint64_t newBitCapacity);
		uint8_t* buffer;
		uint64_t bitHead;
		uint64_t bitCapacity;
};
