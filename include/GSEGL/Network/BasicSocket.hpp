#pragma once
#ifdef _WIN32
	#ifndef WIN32_LEAN_AND_MEAN
		#define WIN32_LEAN_AND_MEAN
	#endif
	#include <winsock2.h>
#else
	#include <errno.h>
	#include <sys/socket.h>
	#include <unistd.h>
	#include <fcntl.h>
#endif
#include "../ErrorUtil.hpp"
#include "SocketAddress.hpp"

#ifndef _WIN32
#define INVALID_SOCKET -1
typedef int SOCKET;
#endif

class BasicSocket
{
	public:
		BasicSocket(SOCKET s) : rawSock(s) {}
		~BasicSocket()
		{
		#ifdef _WIN32
			closesocket(rawSock);
		#else
			close(rawSock);
		#endif
		}
		int SetBlockingMode(bool block)
		{
		#ifdef _WIN32
			u_long arg = block ? 0 : 1;
			int result = ioctlsocket(rawSock, FIONBIO, &arg);
		#else
			int flags = fcntl(rawSock, F_GETFL, 0);
			flags = block ? (flags & ~O_NONBLOCK) : (flags | O_NONBLOCK);
			int result = fcntl(rawSock, F_SETFL, flags);
		#endif
			if(result == -1)
			{
				ErrorUtil::Get().SetError("Network", "BasicSocket::SetBlockingMode");
				return ErrorUtil::Get().GetErrorCode();
			}
			return 0;
		}
		SOCKET rawSock;
};
typedef std::shared_ptr<BasicSocket> BasicSocketPtr;
