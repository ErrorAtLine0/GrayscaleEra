#include "InMemBitStream.hpp"
#include "../ErrorUtil.hpp"
#include <cstring>
#include "../libs/miniz.h"
#include <cmath>

InMemBitStream::InMemBitStream(const uint8_t* nBuffer, uint64_t byteCapacity, bool makeCopy):
buffer(nullptr),
bitHead(0),
bitCapacity(byteCapacity * 8),
ownsData(makeCopy)
{
	if(makeCopy)
	{
		buffer = static_cast<uint8_t*>(std::malloc(byteCapacity));
		std::memcpy(const_cast<uint8_t*>(buffer), nBuffer, byteCapacity);
	}
	else
		buffer = nBuffer;
}

InMemBitStream::~InMemBitStream()
{
	if(ownsData)
		std::free(const_cast<uint8_t*>(buffer));
}

std::shared_ptr<InMemBitStream> InMemBitStream::ReadCompressedData()
{
	uint64_t compressedHeapSize = 0;
	if(!ReadBits(compressedHeapSize)) return nullptr;
	void* compressedHeap = std::malloc(compressedHeapSize);
	if(!compressedHeap)
	{
		ErrorUtil::Get().SetError("Memory", "InMemBitStream::ReadCompressedData Failed to allocate memory for compressed data");
		return nullptr;
	}
	if(!ReadBits(compressedHeap, compressedHeapSize * 8)) return nullptr;
	size_t uncompressedHeapSize = 0;
	void* uncompressedHeap = tinfl_decompress_mem_to_heap(compressedHeap, compressedHeapSize, &uncompressedHeapSize, 0);
	std::free(compressedHeap);
	if(!uncompressedHeap)
	{
		ErrorUtil::Get().SetError("Memory", "InMemBitStream::ReadCompressedData Failed to allocate memory for uncompressed data");
		return nullptr;
	}
	std::shared_ptr<InMemBitStream> toRet = std::make_shared<InMemBitStream>(static_cast<const uint8_t*>(uncompressedHeap), uncompressedHeapSize);
	mz_free(uncompressedHeap);
	return toRet;
}

bool InMemBitStream::ReadBits(void* data, uint64_t bitCount)
{
	if(GetRemainingBits() < bitCount)
	{
		ErrorUtil::Get().SetError("Memory", "InMemBitStream::ReadBits Asking to read more bits than available");
		return false;
	}
	uint8_t* destByte = static_cast<uint8_t*>(data);
	while(bitCount > 8)
	{
		ReadBitsFromByte(destByte, 8);
		++destByte;
		bitCount -= 8;
	}
	if(bitCount > 0)
		ReadBitsFromByte(destByte, bitCount);
	return true;
}

bool InMemBitStream::ReadBits(bool& data)
{
	return ReadBits(static_cast<void*>(&data), 1);
}

bool InMemBitStream::ReadFixedFloat(float& data, float min, float precision, uint64_t bitCount)
{
	uint64_t uintData = 0;
	if(!ReadBits(uintData, bitCount))
		return false;
	data = static_cast<float>(uintData) * precision + min;
	if(std::abs(data) < precision)
		data = 0;
	return true;
}

uint64_t InMemBitStream::GetRemainingBits() const
{
	return bitCapacity - bitHead;
}

uint64_t InMemBitStream::GetRemainingBytes() const
{
	return ((bitCapacity - bitHead) + 7) >> 3;
}

const uint8_t* InMemBitStream::GetBufferPtr() const
{
	return buffer;
}

void InMemBitStream::ReadBitsFromByte(uint8_t* data, uint64_t bitCount)
{
	uint64_t bitOffset = bitHead & 0x7;
	uint64_t byteOffset = bitHead >> 3;
	uint8_t byte = buffer[byteOffset];
	*data = byte>>bitOffset & (~(0xff << bitCount));
	if(8-bitOffset < bitCount)
	{
		uint64_t bitsLeftToRead = bitCount-(8-bitOffset);
		uint8_t newByte = buffer[byteOffset+1];
		uint8_t newReadData = (~(0xff << bitsLeftToRead)) & newByte;
		*data = (newReadData << (8-bitOffset)) | *data;
	}
	bitHead += bitCount;
}
