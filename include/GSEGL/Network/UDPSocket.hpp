#pragma once
#include "BasicSocket.hpp"

class UDPSocket : public BasicSocket
{
	public:
		UDPSocket(SOCKET s) : BasicSocket(s) {}
		~UDPSocket() { }
		
		int Bind(const SocketAddress& address)
		{
			int err = bind(rawSock, reinterpret_cast<const sockaddr*>(&address.rawAddr), address.GetSize());
			if(err != 0)
			{
				ErrorUtil::Get().SetError("Network", "UDPSocket::Bind");
				return -ErrorUtil::Get().GetErrorCode();
			}
			return 0;
		}
		
		int SendTo(const void* data, int len, const SocketAddress& to)
		{
			int byteSentCount = sendto(rawSock,
									   static_cast<const char*>(data),
									   len,
									   0, reinterpret_cast<const sockaddr*>(&to.rawAddr), to.GetSize());
			
			if(byteSentCount < 0)
			{
				#ifdef _WIN32
					if(ErrorUtil::Get().GetErrorCode() != WSAEWOULDBLOCK)
						ErrorUtil::Get().SetError("Network", "UDPSocket::SendTo");
				#else
					if(ErrorUtil::Get().GetErrorCode() != EAGAIN)
						ErrorUtil::Get().SetError("Network", "UDPSocket::SendTo");
				#endif
				return -ErrorUtil::Get().GetErrorCode();
			}
			return byteSentCount;
		}
		
		int ReceiveFrom(void* buffer, int len, SocketAddress& from)
		{
		#ifdef _WIN32
			int fromLen = from.GetSize();
		#else
			unsigned fromLen = from.GetSize();
		#endif
			int readByteCount = recvfrom(rawSock,
										 static_cast<char*>(buffer),
										 len,
										 0, reinterpret_cast<sockaddr*>(&from.rawAddr),
										 &fromLen);
			if(readByteCount < 0)
			{
			#ifdef _WIN32
				if(ErrorUtil::Get().GetErrorCode() != WSAEWOULDBLOCK)
					ErrorUtil::Get().SetError("Network", "UDPSocket::ReceiveFrom");
			#else
				if(ErrorUtil::Get().GetErrorCode() != EAGAIN)
					ErrorUtil::Get().SetError("Network", "UDPSocket::ReceiveFrom");
			#endif
				return -ErrorUtil::Get().GetErrorCode();
			}
			return readByteCount;
		}
};
typedef std::shared_ptr<UDPSocket> UDPSocketPtr;
