#pragma once
#if defined(ENABLE_OPENGL_RENDERER) || defined(ENABLE_OPENGLES_RENDERER)
#include "../libs/glad/glad.h"
#include "../UTF8String.hpp"

class Shader
{
	public:
		void Init(const UTF8String& vertexPath, const UTF8String& fragmentPath);
		void Use();
		GLint GetUniLoc(const GLchar* name);
		void BindAttribLoc(GLuint index, const GLchar* name);
	private:
		GLuint ID;
		
		// Prevents binding shader multiple times
		static GLuint* currentlyBoundShader;
};
#endif
