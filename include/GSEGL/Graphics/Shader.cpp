#if defined(ENABLE_OPENGL_RENDERER) || defined(ENABLE_OPENGLES_RENDERER)
#include "Shader.hpp"
#include "../FileUtil.hpp"
#include "../ErrorUtil.hpp"

GLuint* Shader::currentlyBoundShader = nullptr;

void Shader::Init(const UTF8String& vertexPath, const UTF8String& fragmentPath)
{
	std::shared_ptr<InMemBitStream> vertexStream = FileUtil::Get().ReadFromFile(vertexPath);
	std::shared_ptr<InMemBitStream> fragmentStream = FileUtil::Get().ReadFromFile(fragmentPath);
	if(!vertexStream || !fragmentStream)
	{
		ErrorUtil::Get().SetError("Memory", "Shader::Init Error reading files");
		return;
	}
	UTF8String vertexCode(vertexStream->GetBufferPtr(), vertexStream->GetRemainingBytes());
	UTF8String fragmentCode(fragmentStream->GetBufferPtr(), fragmentStream->GetRemainingBytes());
	
	const GLchar* vShaderCode = vertexCode.GetCString();
	const GLchar* fShaderCode = fragmentCode.GetCString();
	
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vShaderCode, NULL);
	glCompileShader(vertexShader);
	GLint success;
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if(!success)
	{
		GLint errorLen;
		glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &errorLen);
		if(errorLen > 1)
		{
			GLchar* errorLog = static_cast<GLchar*>(std::malloc(errorLen));
			glGetShaderInfoLog(vertexShader, errorLen, NULL, errorLog);
			ErrorUtil::Get().SetError("Memory", errorLog);
			std::free(errorLog);
		}
	}
	
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fShaderCode, NULL);
	glCompileShader(fragmentShader);
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if(!success)
	{
		GLint errorLen;
		glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, &errorLen);
		if(errorLen > 1)
		{
			GLchar* errorLog = static_cast<GLchar*>(std::malloc(errorLen));
			glGetShaderInfoLog(fragmentShader, errorLen, NULL, errorLog);
			ErrorUtil::Get().SetError("Memory", errorLog);
			std::free(errorLog);
		}
	}
	
	ID = glCreateProgram();
	glAttachShader(ID, vertexShader);
	glAttachShader(ID, fragmentShader);
	glLinkProgram(ID);
	glGetProgramiv(ID, GL_LINK_STATUS, &success);
	if(!success)
	{
		GLint errorLen;
		glGetProgramiv(ID, GL_INFO_LOG_LENGTH, &errorLen);
		if(errorLen > 1)
		{
			GLchar* errorLog = static_cast<GLchar*>(std::malloc(errorLen));
			glGetProgramInfoLog(ID, errorLen, NULL, errorLog);
			ErrorUtil::Get().SetError("Memory", errorLog);
			std::free(errorLog);
		}
	}
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
}

void Shader::Use()
{
	if(currentlyBoundShader != &ID)
	{
		glUseProgram(ID);
		currentlyBoundShader = &ID;
	}
}

GLint Shader::GetUniLoc(const GLchar* name)
{
	return glGetUniformLocation(ID, name);
}

void Shader::BindAttribLoc(GLuint index, const GLchar* name)
{
	glBindAttribLocation(ID, index, name);
}

#endif
