#pragma once
#include "WindowUtil.hpp"
#include "../Network/OutMemBitStream.hpp"
#include "../Network/InMemBitStream.hpp"
#include <array>
#include <utility>

enum ControllerButton : uint32_t
{
	CONTROLLER_UP,      // W
	CONTROLLER_DOWN,    // S
	CONTROLLER_LEFT,    // A
	CONTROLLER_RIGHT,   // D
	CONTROLLER_ABILITY0, // W
	CONTROLLER_ABILITY1, // Left Click
	CONTROLLER_ABILITY2, // Right Click
	CONTROLLER_ABILITY3, // Q
	CONTROLLER_ABILITY4, // E
	CONTROLLER_TRIGGER, // S
	CONTROLLER_PAUSE,   // Escape
	CONTROLLER_CHAT,    // Enter
	CONTROLLER_LIST,    // Tab
	
	// Cursor bindings
	CONTROLLER_CURSOR_UP,
	CONTROLLER_CURSOR_DOWN,
	CONTROLLER_CURSOR_LEFT,
	CONTROLLER_CURSOR_RIGHT,
	
	// Scroll bindings
	CONTROLLER_SCROLL_UP,
	CONTROLLER_SCROLL_DOWN,
	
	CONTROLLER_MAX
};

enum InputDeviceType
{
	INPUTDEVICE_KEYBOARD,
	INPUTDEVICE_MOUSE,
	INPUTDEVICE_JOYSTICK,
	INPUTDEVICE_MAX
};
	
struct ButtonInfo
{
	ButtonInfo();
	ButtonInfo(InputDeviceType initInputDevice, float initAxisSetting, int32_t initButton);
	void Write(OutMemBitStream& outStream) const;
	bool Read(InMemBitStream& inStream);
	InputDeviceType inputDevice;
	float axisSetting; // For joysticks
	int32_t button;
};

class ControllerUtil
{
	public:
		static ControllerUtil& Get();
		void Init();
		void Update();
		void SetBinding(const ButtonInfo& buttonInfo, ControllerButton controllerButton);
		void SetScrollSpeed(float speed);
		void SetCursorSpeed(float speed);
		void SetCursorToGameMode(bool gameMode);
		int GetCurrentJoystickID();
		const Vec2& GetCursorPos();
		float GetScrollOffset();
		float GetButtonPower(ControllerButton button);
		KeyState GetButtonState(ControllerButton button);
	private:
		std::array<std::pair<bool, int>, CONTROLLER_MAX> keyboardBindings; // pair<isMouseInput, Key>
		int joystickID;
		std::array<std::pair<float, int>, CONTROLLER_MAX> joystickBindings; // pair<axisSetting, Axis>
		
		// Cursor specific
		float cursorSpeed;
		Vec2 cursorPos;
		Vec2 lastMousePos; // Check if mouse moved. If it did, use mouse as cursor
		bool isGameMode;
		
		// Scroll specific
		double lastScrollInput; // Scroll input must only be taken as if the game was running at 60FPS
		float scrollSpeed;
		float scrollOffset;
};
