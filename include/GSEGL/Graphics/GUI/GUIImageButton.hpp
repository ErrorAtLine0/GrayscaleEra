#pragma once
#include "GUIButton.hpp"

class GUIImageButton : public GUIButton
{
	public:
		GUIImageButton(const UTF8String& initTextureName, const Vec2& initRelPos, const Vec2& initPixPos, const Vec2& initRelSize, const Vec2& initPixSize, const BVec2& initFlip = BVec2(false, false), void (*initClickButtonCallback)(const std::shared_ptr<void>&) = nullptr, const std::shared_ptr<void>& initClickButtonCallbackParam = nullptr):
		GUIButton(initRelPos, initPixPos, initRelSize, initPixSize, initClickButtonCallback, initClickButtonCallbackParam),
		texture(DrawUtil::Get().GetSprite(initTextureName)),
		flip(initFlip)
		{}
		GUIImageButton(const TexturePtr& initTexture, const Vec2& initRelPos, const Vec2& initPixPos, const Vec2& initRelSize, const Vec2& initPixSize, const BVec2& initFlip = BVec2(false, false), void (*initClickButtonCallback)(const std::shared_ptr<void>&) = nullptr, const std::shared_ptr<void>& initClickButtonCallbackParam = nullptr):
		GUIButton(initRelPos, initPixPos, initRelSize, initPixSize, initClickButtonCallback, initClickButtonCallbackParam),
		texture(initTexture),
		flip(initFlip)
		{}
		virtual ~GUIImageButton() {}
		virtual void Draw(bool isSelectedElement)
		{
			if(WithinBoundries())
			{
				Vec2 totalPos = pos;
				totalPos.x -= size.x / 8.0f;
				totalPos.y -= size.y / 8.0f;
				Vec2 highlightSize = Vec2(size.x * 1.25f, size.y * 1.25f);
				Vec2 winSize = WindowUtil::Get().GetWindowSize();
				// Bounds check resized button
				if(totalPos.x < 0.0f)
					totalPos.x = 0.0f;
				else if(totalPos.x + highlightSize.x > winSize.x)
					totalPos.x = winSize.x - highlightSize.x;
				if(totalPos.y < 0.0f)
					totalPos.y = 0.0f;
				else if(totalPos.y + highlightSize.y > winSize.y)
					totalPos.y = winSize.y - highlightSize.y;
				DrawUtil::Get().DrawSprite(layer != 0 ? layer-1 : 0, texture, totalPos, highlightSize, 0.0f, NORMAL_TRANSPARENCY, 0.6f, flip);
			}
			else
				DrawUtil::Get().DrawSprite(layer, texture, pos, size, 0.0f, NORMAL_TRANSPARENCY, 1.0f, flip);
		}
	private:
		TexturePtr texture;
		BVec2 flip;
};
