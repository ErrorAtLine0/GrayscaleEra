#pragma once
#include "GUIUtil.hpp"
#include "../../TimeUtil.hpp"
#include "../../UTF8String.hpp"
#include "../WindowUtil.hpp"

class GUITextEntryState : public GUIElementState
{
	public:
		virtual ~GUITextEntryState() { }
		GUITextEntryState() { }
		UTF8String text;
};

class GUITextEntry : public GUIElement
{
	public:
		GUITextEntry(const UTF8String& initDefaultText, const Vec2& initRelPos, const Vec2& initPixPos, const Vec2& initRelSize, const Vec2& initPixSize, uint32_t initLimit = 0, UTF8String* initValPtr = nullptr):
		GUIElement(initRelPos, initPixPos, initRelSize, initPixSize),
		timeSinceBackspaceHeld(0.0),
		defaultText(initDefaultText),
		state(new GUITextEntryState()),
		limit(initLimit),
		drawRectangleBehind(false),
		valPtr(initValPtr)
		{}
		
		virtual ~GUITextEntry() { delete state; }
		virtual void Update(bool isSelectedElement)
		{
			UTF8String& text = state->text;
			if(isSelectedElement)
			{
				// Text entry operations
				if(limit == 0 || text.GetStrLength() < limit)
				{
					const uint32_t* codepoint = WindowUtil::Get().GetMostRecentCodepoint();
					if(codepoint)
						text.AddCodepoint(*codepoint);
				}
				// Backspace operations
				if(WindowUtil::Get().GetKeyState(GLFW_KEY_BACKSPACE) == KEYSTATE_PRESSED)
					text.PopBack();
				if(WindowUtil::Get().GetKeyState(GLFW_KEY_BACKSPACE) == KEYSTATE_HELD)
				{
					timeSinceBackspaceHeld += TimeUtil::Get().GetDeltaTime();
					if(timeSinceBackspaceHeld >= 1.0)
						text.PopBack();
				}
				else
					timeSinceBackspaceHeld = 0.0;
				// Clipboard operations
				if(WindowUtil::Get().IsControlHeld() && WindowUtil::Get().GetKeyState(GLFW_KEY_V) == KEYSTATE_PRESSED)
				{
					if(limit == 0 || (UTF8String(WindowUtil::Get().GetClipboardStr()).GetStrLength() + text.GetStrLength()) <= limit)
						text += WindowUtil::Get().GetClipboardStr();
				}
				else if((WindowUtil::Get().IsControlHeld() && WindowUtil::Get().GetKeyState(GLFW_KEY_C) == KEYSTATE_PRESSED) && !text.IsEmpty())
					WindowUtil::Get().SetClipboardStr(text.GetCString());
				else if((WindowUtil::Get().IsControlHeld() && WindowUtil::Get().GetKeyState(GLFW_KEY_X) == KEYSTATE_PRESSED) && !text.IsEmpty())
				{
					WindowUtil::Get().SetClipboardStr(text.GetCString());
					text.Clear();
				}
			}
		}
		virtual void Draw(bool isSelectedElement)
		{
			if(drawRectangleBehind)
				DrawUtil::Get().DrawRectangle(layer, Vec2(pos.x, pos.y+5.0f), Vec2(size.x, size.y+5.0f));
			else
				DrawUtil::Get().DrawSprite(layer, "GUITextEntry", pos, Vec2(size.x, 10.0f));
			if(state->text.IsEmpty() && !isSelectedElement)
				DrawUtil::Get().DrawText(layer, defaultText, Vec2(pos.x, pos.y+10.0f), size.y, drawRectangleBehind ? 1.0f : 0.3f);
			else
				DrawUtil::Get().DrawText(layer, state->text, Vec2(pos.x, pos.y+10.0f), size.y, drawRectangleBehind ? 1.0f : 0.3f);
		}
		virtual GUIElementState* GrabState() { return state; }
		virtual void UpdateValPtr() { if(valPtr) *valPtr = state->text; }
		// Useful for visibility
		GUITextEntry* DrawRectangleBehind() { drawRectangleBehind = true; return this; }
	private:
		double timeSinceBackspaceHeld;
		const UTF8String defaultText;
		GUITextEntryState* state;
		uint32_t limit;
		bool drawRectangleBehind;
		UTF8String* valPtr;
};
