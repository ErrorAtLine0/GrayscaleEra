#pragma once
#include "GUIUtil.hpp"

class GUICheckBoxState : public GUIElementState
{
	public:
		virtual ~GUICheckBoxState() { }
		GUICheckBoxState():
		val(false)
		{}
		bool val;
};

class GUICheckBox : public GUIElement
{
	public:
		GUICheckBox(const Vec2& initRelPos, const Vec2& initPixPos, const Vec2& initRelSize, const Vec2& initPixSize, bool* initValPtr = nullptr):
		GUIElement(initRelPos, initPixPos, initRelSize, initPixSize),
		state(new GUICheckBoxState()),
		valPtr(initValPtr)
		{}
		virtual ~GUICheckBox() { delete state; }
		virtual void Update(bool isSelectedElement)
		{
			if(ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY1) == KEYSTATE_PRESSED && WithinBoundries())
				state->val = !state->val;
		}
		virtual void Draw(bool isSelectedElement)
		{
			if(state->val)
				DrawUtil::Get().DrawSprite(layer, "CheckedBox", pos, size);
			else
				DrawUtil::Get().DrawSprite(layer, "CheckBox", pos, size);
		}
		virtual GUIElementState* GrabState() { return state; }
		virtual void UpdateValPtr() { if(valPtr) *valPtr = state->val; }
	protected:
		GUICheckBoxState* state;
		bool* valPtr;
};
