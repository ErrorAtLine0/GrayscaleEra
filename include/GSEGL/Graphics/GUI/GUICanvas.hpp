#pragma once
#include "GUIUtil.hpp"
#include <cstring>

class GUICanvasState : public GUIElementState
{
	public:
		GUICanvasState(const UVec2& initTextureSize, const Vec2& initPos, const Vec2& initSize):
		selectedColor(0),
		currentBrushSize(1),
		pos(initPos),
		size(initSize),
		textureSize(initTextureSize),
		data(static_cast<uint8_t*>(std::malloc(textureSize.x * textureSize.y)))
		{
			for(uint32_t i = 0; i < GetTextureByteSize(); i++)
				data[i] = 255;
			texture = std::make_shared<Texture>(data, textureSize.x, textureSize.y);
		}
		void DrawAtPos(const Vec2& posToDraw)
		{
			UVec2 posOnCanvas = GetFlippedYPosOnCanvas(posToDraw);
			uint32_t brushSizeHalf = currentBrushSize / 2.0f;
			for(uint8_t i = 0; i < currentBrushSize; i++)
				for(uint8_t j = 0; j < currentBrushSize; j++)
				{
					uint32_t xFinal = (posOnCanvas.x - brushSizeHalf) + i;
					if(xFinal < textureSize.x)
					{
						uint32_t yFinal = ((posOnCanvas.y - brushSizeHalf) * textureSize.x) + (j * textureSize.x);
						uint32_t finalDataIndex = xFinal + yFinal;
						if(finalDataIndex < GetTextureByteSize())
							data[finalDataIndex] = selectedColor;
					}
				}
			texture->ChangeData(data, 0, 0, textureSize.x, textureSize.y);
		}
		uint8_t GetColorAt(const Vec2& posToGet)
		{
			UVec2 posOnCanvas = GetFlippedYPosOnCanvas(posToGet);
			uint32_t dataIndex = static_cast<uint32_t>(posOnCanvas.x + posOnCanvas.y * textureSize.x);
			if(dataIndex < GetTextureByteSize())
				return data[dataIndex];
			else
				return 0;
		}
		virtual ~GUICanvasState()
		{
			std::free(data);
		}
		const UVec2& GetTextureSize() const { return textureSize; }
		uint32_t GetTextureByteSize() const { return textureSize.x * textureSize.y; }
		void SyncCanvas() { texture->ChangeData(data, 0, 0, textureSize.x, textureSize.y); }
		const uint8_t* GetCanvasData() const { return data; }
		void SetCanvasData(const uint8_t* data) { std::memcpy(this->data, data, GetTextureByteSize()); }
		
		uint8_t selectedColor;
		uint8_t currentBrushSize;
		TexturePtr texture;
	private:
		Vec2 GetFlippedYPosOnCanvas(const Vec2& pos)
		{
			return UVec2(
				(pos.x - this->pos.x) * (textureSize.x / size.x), textureSize.y - (pos.y - this->pos.y) * (textureSize.y / size.y));
		}
		const Vec2& pos;
		const Vec2& size;
		UVec2 textureSize;
		uint8_t* data;
};

class GUICanvas : public GUIElement
{
	public:
		GUICanvas(const Vec2& initRelPos, const Vec2& initPixPos, const Vec2& initRelSize, const Vec2& initPixSize, const UVec2& initTextureSize):
		GUIElement(initRelPos, initPixPos, initRelSize, initPixSize),
		state(new GUICanvasState(initTextureSize, pos, size))
		{ }
		virtual ~GUICanvas() { delete state; }
		virtual void Update(bool isSelectedElement) { }
		virtual void Draw(bool isSelectedElement)
		{
			DrawUtil::Get().DrawSprite(layer, state->texture, pos, size, 0.0f, NORMAL_TRANSPARENCY);
		}
		virtual GUIElementState* GrabState() { return state; }
	private:
		GUICanvasState* state;
};
