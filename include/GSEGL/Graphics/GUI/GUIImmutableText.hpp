#pragma once
#include "GUIUtil.hpp"
#include "../../UTF8String.hpp"

class GUIImmutableTextState : public GUIElementState
{
	public:
		GUIImmutableTextState(const UTF8String& initText):
		text(initText)
		{}
		UTF8String text;
};

class GUIImmutableText : public GUIElement
{
	public:
		GUIImmutableText(const UTF8String& initText, const Vec2& initRelPos, const Vec2& initPixPos, float initRelHeight, float initPixHeight):
		GUIElement(initRelPos, initPixPos, Vec2(0.0f, initRelHeight), Vec2(0.0f, initPixHeight)),
		state(new GUIImmutableTextState(initText))
		{}
		~GUIImmutableText() { delete state; }
		virtual void Update(bool isSelectedElement) {}
		virtual void Draw(bool isSelectedElement)
		{
			DrawUtil::Get().DrawText(layer, state->text, Vec2(pos.x, pos.y+10.0f), size.y, 0.3f);
		}
		virtual GUIElementState* GrabState() { return state; }
	private:
		GUIImmutableTextState* state;
};
