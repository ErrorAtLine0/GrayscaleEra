#pragma once
#include "../../UTF8String.hpp"
#include "../DrawUtil.hpp"
#include <unordered_map>

#define GUI_LAYER 10

class GUIElementState
{
	public:
		virtual ~GUIElementState();
};

class GUIElement
{
	public:
		GUIElement(const Vec2& initRelPos, const Vec2& initPixPos, const Vec2& initRelSize, const Vec2& initPixSize);
		virtual ~GUIElement();
		virtual void Update(bool isSelectedElement) = 0;
		virtual void Draw(bool isSelectedElement) = 0;
		virtual GUIElementState* GrabState() = 0;
		virtual void UpdateValPtr() {}
		virtual void CalculatePos(const Vec2& windowPosition, const Vec2& windowSize);
		bool WithinBoundries();
		GUIElement* SetFinalSizePos(const Vec2& finalPos);
		virtual GUIElement* PushUp();
		virtual GUIElement* PushDown();
		
		const Vec2& GetPos() const;
		const Vec2& GetSize() const;
		
		void SetEnabledState(bool isEnabled);
		bool IsEnabled() const;
	protected:
		Vec2 relPos;
		Vec2 pixPos;
		Vec2 pos;
		Vec2 relSize;
		Vec2 pixSize;
		Vec2 size;
		Vec2 finalSizePos;
		bool enabled;
		uint32_t layer;
};

class GUIUtil
{
	public:
		static GUIUtil& Get();
		~GUIUtil();
		void Init();
		void Update();
		void Draw();
		void AddElement(const UTF8String& elementName, GUIElement* elementPtr);
		void RemoveElement(const UTF8String& elementName);
		void EnableElement(const UTF8String& elementName);
		void DisableElement(const UTF8String& elementName);
		GUIElementState* GrabState(const UTF8String& elementName);
		void ClearAll();
		bool IsSelectedElement(const GUIElement* element);
		bool IsSelectedElement(const UTF8String& elementName);
		void SelectElement(const UTF8String& elementName);
		void UnselectElement();
		void CalculatePos();
		void SetGUISize(float GUISize);
		float GetGUISize();
		void UpdateValPtrs();
		void Cleanup();
	private:
		GUIUtil();
		std::vector<std::pair<UTF8String, GUIElement*>> GUIelements;
		GUIElement* selectedGUIElement;
		float GUISize;
};
