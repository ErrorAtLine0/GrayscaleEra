#pragma once
#include "../DrawUtil.hpp"

class GUISliderState : public GUIElementState
{
	public:
		GUISliderState(int32_t initVal):
		val(initVal),
		update(true)
		{}
		int32_t GetVal() const { return val; }
		void SetVal(int32_t val)
		{
			this->val = val;
			update = true;
		}
		int32_t val;
		bool update;
};

class GUISlider : public GUIElement
{
	public:
		friend class GUISliderState;
		GUISlider(const Vec2& initRelPos, const Vec2& initPixPos, const Vec2& initRelSize, const Vec2& initPixSize, int32_t initVal, int32_t initMin, int32_t initMax, int32_t initDiff, int32_t* initValPtr = nullptr):
		GUIElement(initRelPos, initPixPos, initRelSize, initPixSize),
		state(new GUISliderState(initVal)),
		min(initMin),
		max(initMax),
		diff(initDiff),
		valPtr(initValPtr)
		{
			strVal = UTF8String::IntToStr(state->val);
		}
		virtual ~GUISlider()
		{
			delete state;
		}
		virtual void Update(bool isSelectedElement)
		{
			if(state->update)
			{
				state->update = false;
				strVal = UTF8String::IntToStr(state->val);
			}
			if(isSelectedElement && ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY1) == KEYSTATE_HELD && WithinBoundries())
			{
				float percentOnSlider = (ControllerUtil::Get().GetCursorPos().x - pos.x) / size.x;
				float unroundedVal = (max * percentOnSlider) + (min * (1.0f - percentOnSlider));
				float roundingInterval = std::fmod(unroundedVal, diff);
				if(roundingInterval > 0.0f)
				{
					if(roundingInterval < diff / 2.0f)
						state->val = std::roundf(unroundedVal - roundingInterval);
					else
						state->val = std::roundf(unroundedVal + (diff - roundingInterval));
				}
				else if(roundingInterval < 0.0f)
				{
					if(std::fabs(roundingInterval) > diff / 2.0f)
						state->val = std::roundf(unroundedVal - (diff + roundingInterval));
					else
						state->val = std::roundf(unroundedVal - roundingInterval);
				}
				strVal = UTF8String::IntToStr(state->val);
			}
		}
		virtual void Draw(bool isSelectedElement)
		{
			DrawUtil::Get().DrawRectangle(layer, Vec2(pos.x, pos.y + size.y/3.0f), Vec2(size.x, size.y/3.0f), 0.7f, 1.0f);
			float rectXPercentOnScale = (state->val - min) / static_cast<float>(max - min);
			DrawUtil::Get().DrawRectangle(layer, Vec2(pos.x + rectXPercentOnScale * size.x - 10.0f, pos.y), Vec2(20.0f, size.y), 0.7f, 1.0f);
			DrawUtil::Get().DrawText(layer, strVal, Vec2(pos.x+size.y, pos.y), size.y);
		}
		virtual GUIElementState* GrabState() { return state; }
		virtual void UpdateValPtr() { if(valPtr) *valPtr = state->val; }
	private:
		GUISliderState* state;
		int32_t min;
		int32_t max;
		int32_t diff;
		UTF8String strVal;
		int32_t* valPtr;
};
