#pragma once
#include "GUIUtil.hpp"
#include "../ControllerUtil.hpp"
#include <algorithm>
#define SCROLL_BAR_WIDTH 15.0f

class GUIScrollAreaState : public GUIElementState
{
	public:
		GUIScrollAreaState():
		scrollPercent(0.0f)
		{}
		~GUIScrollAreaState()
		{
			ClearAll();
		}
		void ClearAll()
		{
			for(GUIElement* element : elements)
				delete element;
			elements.clear();
		}
		void RemoveElement(uint32_t elementIndex)
		{
			delete elements[elementIndex];
			elements.erase(elements.begin() + elementIndex);
		}
		void RemoveElement(GUIElement* elementPtr)
		{
			delete elementPtr;
			elements.erase(std::remove(elements.begin(), elements.end(), elementPtr), elements.end());
		}
		void AddElement(GUIElement* newElement)
		{
			elements.emplace_back(newElement);
		}
		float scrollPercent;
		std::vector<GUIElement*> elements;
};

class GUIScrollArea : public GUIElement
{
	public:
		GUIScrollArea(const Vec2& initRelPos, const Vec2& initPixPos, const Vec2& initRelSize, const Vec2& initPixSize, float initScrollOffset, float initScrollAreaEmptySpace = 0.0f):
		GUIElement(initRelPos, initPixPos, initRelSize, initPixSize),
		scrollOffset(initScrollOffset),
		scrollAreaEmptySpace(initScrollAreaEmptySpace),
		selectedGUIElement(nullptr),
		scrollBarHeight(0.0f),
		scrollAreaHeightNegative(0.0f),
		state(new GUIScrollAreaState())
		{}
		~GUIScrollArea() { delete state; }
		virtual void Update(bool isSelectedElement)
		{
			scrollAreaHeightNegative = GetLowestElementY();
			if(WithinBoundries() && WindowUtil::Get().GetCursorPos().x > (pos.x + size.x) - GUIUtil::Get().GetGUISize() * SCROLL_BAR_WIDTH && ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY1) == KEYSTATE_HELD)
				state->scrollPercent = 1.0f - ((WindowUtil::Get().GetCursorPos().y - pos.y) / size.y);
			if(isSelectedElement && WithinBoundries() && ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY1) == KEYSTATE_PRESSED)
			{
				selectedGUIElement = nullptr;
				for(GUIElement* element : state->elements)
					if(element->WithinBoundries())
					{
						selectedGUIElement = element;
						break;
					}
			}
			for(GUIElement* element : state->elements)
				if(IsElementWithinArea(element))
					element->Update(element == selectedGUIElement);
			if(WithinBoundries() && ControllerUtil::Get().GetScrollOffset() > 0 && scrollAreaHeightNegative != 0.0f)
				state->scrollPercent += scrollOffset / scrollAreaHeightNegative;
			else if(WithinBoundries() && ControllerUtil::Get().GetScrollOffset() < 0 && scrollAreaHeightNegative != 0.0f)
				state->scrollPercent -= scrollOffset / scrollAreaHeightNegative;
			if(state->scrollPercent < 0.0f)
				state->scrollPercent = 0.0f;
			else if(state->scrollPercent > 1.0f)
				state->scrollPercent = 1.0f;
			scrollBarHeight = scrollAreaHeightNegative != 0.0f ? -(size.y * (size.y / (scrollAreaHeightNegative - size.y))) : 0.0f;
			if(scrollBarHeight < 0.0f)
				scrollBarHeight = 0.0f;
			else if(scrollBarHeight > size.y)
			{
				scrollBarHeight = 0.0f;
				state->scrollPercent = 0.0f; // There's nothing to scroll for if the scroll bar is as large as the scroll area
			}
			float scrollAmount = CalculateScrollAmount();
			for(GUIElement* element : state->elements)
				element->CalculatePos(Vec2(pos.x, pos.y + size.y - scrollAmount), size);
		}
		virtual void Draw(bool isSelectedElement)
		{
			for(GUIElement* element : state->elements)
				if(IsElementWithinArea(element))
					element->Draw(element == selectedGUIElement);
			if(scrollBarHeight != 0.0f)
			{
				float scrollBarWidth = GUIUtil::Get().GetGUISize() * SCROLL_BAR_WIDTH;
				bool hoveringOverScrollbar = WithinBoundries() && WindowUtil::Get().GetCursorPos().x > (pos.x + size.x) - GUIUtil::Get().GetGUISize() * SCROLL_BAR_WIDTH;
				DrawUtil::Get().DrawRectangle(layer, Vec2(pos.x + size.x - scrollBarWidth, pos.y), Vec2(scrollBarWidth, size.y), 0.8f, hoveringOverScrollbar ? 1.0f : 0.8f);
				DrawUtil::Get().DrawRectangle(layer, Vec2(pos.x + size.x - scrollBarWidth, pos.y + (size.y - scrollBarHeight) * (1.0f - state->scrollPercent)), Vec2(scrollBarWidth, scrollBarHeight), 0.2f, hoveringOverScrollbar ? 1.0f : 0.8f);
			}
		}
		virtual void UpdateValPtr()
		{
			for(GUIElement* element : state->elements)
				element->UpdateValPtr();
		}
		virtual GUIElementState* GrabState() { return state; }
	private:
		bool IsElementWithinArea(GUIElement* element)
		{
			return element->GetPos().y + element->GetSize().y >= pos.y && element->GetPos().y < pos.y + size.y;
		}
		bool SingleElementVisible()
		{
			for(GUIElement* element : state->elements)
				if(IsElementWithinArea(element))
					return true;
			return false;
		}
		bool ContainsElementsUp()
		{
			for(GUIElement* element : state->elements)
				if(element->GetPos().y + element->GetSize().y > pos.y + size.y)
					return true;
			return false;
		}
		bool ContainsElementsDown()
		{
			for(GUIElement* element : state->elements)
				if(element->GetPos().y < pos.y)
					return true;
			return false;
		}
		float GetLowestElementY()
		{
			if(!state->elements.empty())
			{
				float lowestY = state->elements[0]->GetPos().y;
				for(GUIElement* element : state->elements)
				{
					if(element->GetPos().y < lowestY)
						lowestY = element->GetPos().y;
				}
				return lowestY + CalculateScrollAmount() - scrollAreaEmptySpace * GUIUtil::Get().GetGUISize();
			}
			else
				return 0.0f;
		}
		float CalculateScrollAmount()
		{
			if(scrollAreaHeightNegative != 0.0f)
				return scrollAreaHeightNegative * state->scrollPercent;
			else
				return 0.0f;
		}
		const float scrollOffset;
		const float scrollAreaEmptySpace;
		GUIElement* selectedGUIElement;
		float scrollBarHeight;
		float scrollAreaHeightNegative;
		GUIScrollAreaState* state;
};
