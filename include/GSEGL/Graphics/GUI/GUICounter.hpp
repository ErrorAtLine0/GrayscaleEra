#pragma once
#include "GUIImageButton.hpp"
#include "../DrawUtil.hpp"

class GUICounterState : public GUIElementState
{
	public:
		GUICounterState(double initVal):
		val(initVal),
		update(true)
		{}
		double GetVal() const { return val; }
		void SetVal(double val)
		{
			this->val = val;
			update = true;
		}
		double val;
		bool update;
};

class GUICounter : public GUIElement
{
	public:
		friend class GUICounterState;
		GUICounter(const Vec2& initRelPos, const Vec2& initPixPos, const Vec2& initRelSize, const Vec2& initPixSize, double initVal, double initMin, double initMax, double initDiff, double* initValPtr = nullptr):
		GUIElement(initRelPos, initPixPos, initRelSize, initPixSize),
		state(new GUICounterState(initVal)),
		min(initMin),
		max(initMax),
		diff(initDiff),
		decreaseButton(new GUIImageButton("MinusIcon", initRelPos, initPixPos, Vec2(initRelSize.y), Vec2(initPixSize.y), BVec2(true, false))),
		increaseButton(new GUIImageButton("AddIcon", Vec2((initRelPos.x + initRelSize.x) - initRelSize.y, initRelPos.y), Vec2((initPixPos.x + initPixSize.x) - initPixSize.y, initPixPos.y), Vec2(initRelSize.y), Vec2(initPixSize.y))),
		valPtr(initValPtr)
		{
			strVal = UTF8String::IntToStr(state->val);
		}
		virtual ~GUICounter()
		{
			delete state;
			delete decreaseButton;
			delete increaseButton;
		}
		virtual void Update(bool isSelectedElement)
		{
			if(state->update)
			{
				state->update = false;
				strVal = UTF8String::DoubleToStr(state->val);
			}
			decreaseButton->Update(false);
			increaseButton->Update(false);
			if(static_cast<GUIButtonState*>(decreaseButton->GrabState())->buttonState == GUIBUTTON_PRESSED)
			{
				if(state->val != min)
					state->SetVal(state->val - diff);
			}
			else if(static_cast<GUIButtonState*>(increaseButton->GrabState())->buttonState == GUIBUTTON_PRESSED)
			{
				if(state->val != max)
					state->SetVal(state->val + diff);
			}
		}
		virtual void CalculatePos(const Vec2& winPos, const Vec2& winSize)
		{
			GUIElement::CalculatePos(winPos, winSize);
			increaseButton->CalculatePos(winPos, winSize);
			decreaseButton->CalculatePos(winPos, winSize);
		}
		virtual void Draw(bool isSelectedElement)
		{
			DrawUtil::Get().DrawText(layer, strVal, Vec2(pos.x+size.y, pos.y), size.y);
			increaseButton->Draw(false);
			decreaseButton->Draw(false);
		}
		virtual GUIElementState* GrabState() { return state; }
		virtual void UpdateValPtr()
		{
			if(valPtr) *valPtr = state->val;
		}
		virtual GUIElement* PushUp()
		{
			decreaseButton->PushUp();
			increaseButton->PushUp();
			return GUIElement::PushUp();
		}
		virtual GUIElement* PushDown()
		{
			decreaseButton->PushDown();
			increaseButton->PushDown();
			return GUIElement::PushDown();
		}
	private:
		GUICounterState* state;
		double min;
		double max;
		double diff;
		GUIImageButton* decreaseButton;
		GUIImageButton* increaseButton;
		UTF8String strVal;
		double* valPtr;
};
