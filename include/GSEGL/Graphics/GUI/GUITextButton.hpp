#pragma once
#include "GUIButton.hpp"
#include "../../UTF8String.hpp"

class GUITextButton : public GUIButton
{
	public:
		GUITextButton(const UTF8String initText, const Vec2& initRelPos, const Vec2& initPixPos, const Vec2& initRelSize, const Vec2& initPixSize, void (*initClickButtonCallback)(const std::shared_ptr<void>&) = nullptr, const std::shared_ptr<void>& initClickButtonCallbackParam = nullptr):
		GUIButton(initRelPos, initPixPos, initRelSize, initPixSize, initClickButtonCallback, initClickButtonCallbackParam),
		text(initText)
		{}
		virtual ~GUITextButton() { }
		virtual void Draw(bool isSelectedElement)
		{
			bool cursorHover = WithinBoundries();
			DrawUtil::Get().DrawRectangle(layer, pos, size, 0.7f, cursorHover ? 0.6f : 1.0f);
			DrawUtil::Get().DrawRectangle(layer, Vec2(pos.x+5.0f, pos.y+5.0f), Vec2(size.x-10.0f, size.y-10.0f), cursorHover ? 1.0f : 0.9f, cursorHover ? 0.6f : 1.0f);
			DrawUtil::Get().DrawText(layer, text, pos, size.y, cursorHover ? 0.1f : 0.3f);
		}
	private:
		const UTF8String text;
};
