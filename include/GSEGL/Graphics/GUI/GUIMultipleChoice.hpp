#pragma once
#include "GUIUtil.hpp"
#include "GUIButton.hpp"

class GUIMultipleChoiceState : public GUIElementState
{
	public:
		virtual ~GUIMultipleChoiceState() { }
		GUIMultipleChoiceState():
		choiceIndex(0)
		{}
		uint32_t choiceIndex;
};

class GUIMultipleChoice : public GUIElement
{
	public:
		GUIMultipleChoice(const Vec2& initRelPos, const Vec2& initPixPos, float initRelHeight, float initPixHeight, const std::vector<UTF8String>& initChoiceList, uint32_t* initValPtr = nullptr):
		GUIElement(initRelPos, initPixPos, Vec2(0.0f, initRelHeight), Vec2(0.0f, initPixHeight)),
		choiceList(initChoiceList),
		state(new GUIMultipleChoiceState()),
		valPtr(initValPtr)
		{
			for(uint32_t i = 0; i < choiceList.size(); i++)
				radioButtons.emplace_back(new GUIButton(Vec2(relPos.x, relPos.y + i*relSize.y*1.25f), Vec2(pixPos.x, pixPos.y + i*pixSize.y*1.25f), Vec2(relSize.y), Vec2(pixSize.y)));
		}
		virtual ~GUIMultipleChoice() { delete state; }
		virtual void Update(bool isSelectedElement)
		{
			for(GUIButton* radioButton : radioButtons)
				radioButton->Update(false);
			for(uint32_t i = 0; i < radioButtons.size(); i++)
				if(static_cast<GUIButtonState*>(radioButtons[i]->GrabState())->buttonState == GUIBUTTON_PRESSED)
				{
					state->choiceIndex = i;
					break;
				}
		}
		virtual void CalculatePos(const Vec2& winPos, const Vec2& winSize)
		{
			GUIElement::CalculatePos(winPos, winSize);
			for(GUIButton* rB : radioButtons)
				rB->CalculatePos(winPos, winSize);
		}
		virtual void Draw(bool isSelectedElement)
		{
			for(uint32_t i = 0; i < choiceList.size(); i++)
			{
				const Vec2& radioButtonPos = radioButtons[i]->GetPos();
				DrawUtil::Get().DrawSprite(layer, (i == state->choiceIndex ? "RadioButtonFull" : "RadioButtonEmpty"), radioButtonPos, radioButtons[i]->GetSize());
				DrawUtil::Get().DrawText(layer, choiceList[i], Vec2(radioButtonPos.x + size.y + 10.0f, radioButtonPos.y), size.y);
			}
		}
		virtual GUIElementState* GrabState() { return state; }
		virtual void UpdateValPtr() { if(valPtr) *valPtr = state->choiceIndex; }
	protected:
		std::vector<UTF8String> choiceList;
		std::vector<GUIButton*> radioButtons;
		GUIMultipleChoiceState* state;
		uint32_t* valPtr;
};
