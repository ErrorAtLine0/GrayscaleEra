#include "GUIUtil.hpp"
#include "../ControllerUtil.hpp"
#include "../WindowUtil.hpp"
#include <algorithm>

GUIElementState::~GUIElementState() { }

GUIElement::GUIElement(const Vec2& initRelPos, const Vec2& initPixPos, const Vec2& initRelSize, const Vec2& initPixSize):
relPos(initRelPos),
pixPos(initPixPos),
relSize(initRelSize),
pixSize(initPixSize),
finalSizePos(0.0f),
enabled(true),
layer(GUI_LAYER)
{
	CalculatePos(Vec2(0.0f), WindowUtil::Get().GetWindowSize());
}

GUIElement::~GUIElement() { }

void GUIElement::CalculatePos(const Vec2& winPos, const Vec2& winSize) {
	pos.x = winSize.x * relPos.x + (pixPos.x * GUIUtil::Get().GetGUISize()) + winPos.x;
	pos.y = winSize.y * relPos.y + (pixPos.y * GUIUtil::Get().GetGUISize()) + winPos.y;
	// Negative values in relative size means calculating based on opposite axis
	size.x = (relSize.x >= 0.0f ? (winSize.x * relSize.x) : (winSize.y * (-relSize.x))) + (pixSize.x * GUIUtil::Get().GetGUISize());
	size.y = (relSize.y >= 0.0f ? (winSize.y * relSize.y) : (winSize.x * (-relSize.y))) + (pixSize.y * GUIUtil::Get().GetGUISize());
	// Final size position, position calculations made after the size is determined. Is rarely used
	if(finalSizePos.x != 0.0f)
		pos.x += size.x * finalSizePos.x;
	if(finalSizePos.y != 0.0f)
		pos.y += size.y * finalSizePos.y;
}

bool GUIElement::WithinBoundries()
{
	const Vec2& cursorPos = ControllerUtil::Get().GetCursorPos();
	if(cursorPos.x >= pos.x && cursorPos.x <= (pos.x + size.x))
		if(cursorPos.y >= pos.y && cursorPos.y <= (pos.y + size.y))
			return true;
	return false;
}

GUIElement* GUIElement::SetFinalSizePos(const Vec2& finalSizePos)
{
	this->finalSizePos = finalSizePos;
	CalculatePos(Vec2(0.0f), WindowUtil::Get().GetWindowSize());
	return this;
}

GUIElement* GUIElement::PushUp() { --layer; return this; }
GUIElement* GUIElement::PushDown() { ++layer; return this; }
const Vec2& GUIElement::GetPos() const { return pos; }
const Vec2& GUIElement::GetSize() const { return size; }
void GUIElement::SetEnabledState(bool isEnabled) { enabled = isEnabled; }
bool GUIElement::IsEnabled() const { return enabled; }

GUIUtil& GUIUtil::Get()
{
	static GUIUtil sInstance;
	return sInstance;
}

GUIUtil::~GUIUtil()
{
	ClearAll();
}

void GUIUtil::Init() {}

void GUIUtil::Update()
{
	if(WindowUtil::Get().IsWindowResized())
		CalculatePos();
	if(ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY1) == KEYSTATE_PRESSED)
	{
		selectedGUIElement = nullptr;
		for(auto& elementPair : GUIelements)
			if(elementPair.second->WithinBoundries())
			{
				selectedGUIElement = elementPair.second;
				break;
			}
	}
	for(auto& elementPair : GUIelements)
		if(elementPair.second->IsEnabled())
			elementPair.second->Update(IsSelectedElement(elementPair.second));
}

void GUIUtil::Draw()
{
	for(auto& elementPair : GUIelements)
		if(elementPair.second->IsEnabled())
			elementPair.second->Draw(IsSelectedElement(elementPair.second));
}

void GUIUtil::AddElement(const UTF8String& elementName, GUIElement* elementPtr) { GUIelements.emplace_back(elementName, elementPtr); }

void GUIUtil::RemoveElement(const UTF8String& elementName)
{
	auto it = std::find_if(GUIelements.begin(), GUIelements.end(), [&elementName](const std::pair<UTF8String, GUIElement*>& p){ return elementName == p.first; });
	if(it != GUIelements.end())
	{
		if(it->second == selectedGUIElement)
			selectedGUIElement = nullptr;
		delete it->second;
		GUIelements.erase(it);
	}
}

void GUIUtil::EnableElement(const UTF8String& elementName)
{
	auto it = std::find_if(GUIelements.begin(), GUIelements.end(), [&elementName](const std::pair<UTF8String, GUIElement*>& p){ return elementName == p.first; });
	if(it != GUIelements.end())
		it->second->SetEnabledState(true);
}

void GUIUtil::DisableElement(const UTF8String& elementName)
{
	auto it = std::find_if(GUIelements.begin(), GUIelements.end(), [&elementName](const std::pair<UTF8String, GUIElement*>& p){ return elementName == p.first; });
	if(it != GUIelements.end())
	{
		it->second->SetEnabledState(false);
		if(it->second == selectedGUIElement)
			selectedGUIElement = nullptr;
	}
}

GUIElementState* GUIUtil::GrabState(const UTF8String& elementName)
{
	auto it = std::find_if(GUIelements.begin(), GUIelements.end(), [&elementName](const std::pair<UTF8String, GUIElement*>& p){ return elementName == p.first; });
	if(it != GUIelements.end())
		return it->second->GrabState();
	else
		return nullptr;
}

void GUIUtil::ClearAll()
{
	for(auto& elementPair : GUIelements)
		delete elementPair.second;
	GUIelements.clear();
	selectedGUIElement = nullptr;
}

bool GUIUtil::IsSelectedElement(const GUIElement* element) { return (element == selectedGUIElement); }
bool GUIUtil::IsSelectedElement(const UTF8String& elementName)
{
	auto it = std::find_if(GUIelements.begin(), GUIelements.end(), [&elementName](const std::pair<UTF8String, GUIElement*>& p){ return elementName == p.first; });
	if(it != GUIelements.end())
		return it->second == selectedGUIElement;
	else
		return false;
}

void GUIUtil::SelectElement(const UTF8String& elementName)
{
	auto it = std::find_if(GUIelements.begin(), GUIelements.end(), [&elementName](const std::pair<UTF8String, GUIElement*>& p){ return elementName == p.first; });
	if(it != GUIelements.end())
		selectedGUIElement = it->second;
}

void GUIUtil::UnselectElement() { selectedGUIElement = nullptr; }

void GUIUtil::CalculatePos()
{
	for(auto& elementPair : GUIelements)
		elementPair.second->CalculatePos(Vec2(0.0f), WindowUtil::Get().GetWindowSize());
}

void GUIUtil::SetGUISize(float GUISize)
{
	this->GUISize = GUISize;
	CalculatePos();
}
float GUIUtil::GetGUISize() { return GUISize; }

void GUIUtil::UpdateValPtrs()
{
	for(auto& elementPair : GUIelements)
		elementPair.second->UpdateValPtr();
}

void GUIUtil::Cleanup() { ClearAll(); }

GUIUtil::GUIUtil():
selectedGUIElement(nullptr),
GUISize(1.0f)
{}
