#pragma once
#include "GUIUtil.hpp"
#include "../ControllerUtil.hpp"
#include <memory>

enum GUIButtonStateEnum
{
	GUIBUTTON_RELEASED,
	GUIBUTTON_PRESSED,
	GUIBUTTON_HELD
};

class GUIButtonState : public GUIElementState
{
	public:
		virtual ~GUIButtonState() { }
		GUIButtonState():
		buttonState(GUIBUTTON_RELEASED)
		{}
		GUIButtonStateEnum buttonState;
};

class GUIButton : public GUIElement
{
	public:
		GUIButton(const Vec2& initRelPos, const Vec2& initPixPos, const Vec2& initRelSize, const Vec2& initPixSize, void (*initClickButtonCallback)(const std::shared_ptr<void>&) = nullptr, const std::shared_ptr<void>& initClickButtonCallbackParam = nullptr):
		GUIElement(initRelPos, initPixPos, initRelSize, initPixSize),
		clickButtonCallback(initClickButtonCallback),
		clickButtonCallbackParam(initClickButtonCallbackParam),
		state(new GUIButtonState())
		{}
		virtual ~GUIButton() { delete state; }
		virtual void Update(bool isSelectedElement)
		{
			GUIButtonStateEnum& buttonState = state->buttonState;
			if(buttonState == GUIBUTTON_RELEASED)
			{
				if(ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY1) == KEYSTATE_PRESSED && WithinBoundries())
				{
					if(clickButtonCallback)
						clickButtonCallback(clickButtonCallbackParam);
					buttonState = GUIBUTTON_PRESSED;
				}
			}
			else if(buttonState == GUIBUTTON_PRESSED)
			{
				if(ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY1) != KEYSTATE_RELEASED && WithinBoundries())
					buttonState = GUIBUTTON_HELD;
				else
					buttonState = GUIBUTTON_RELEASED;
			}
			else if(buttonState == GUIBUTTON_HELD)
				if(ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY1) == KEYSTATE_RELEASED || !WithinBoundries())
					buttonState = GUIBUTTON_RELEASED;
		}
		virtual void Draw(bool isSelectedElement) { }
		virtual GUIElementState* GrabState() { return state; }
	protected:
		void (*clickButtonCallback)(const std::shared_ptr<void>&);
		std::shared_ptr<void> clickButtonCallbackParam;
		GUIButtonState* state;
};
