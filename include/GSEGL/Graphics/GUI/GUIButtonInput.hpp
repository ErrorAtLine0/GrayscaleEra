#pragma once
#include "GUIImageButton.hpp"
#include <GSEGL/Graphics/DrawUtil.hpp>
#include "../../TimeUtil.hpp"

#define GRABINPUT_TIMEOUT 3.0

class GUIButtonInputState : public GUIElementState
{
	public:
		GUIButtonInputState(const ButtonInfo& initVal):
		val(initVal),
		update(false)
		{}
		void SetVal(const ButtonInfo& val)
		{
			this->val = val;
			update = true;
		}
		ButtonInfo val;
		bool update;
};

class GUIButtonInput : public GUIElement
{
	public:
		friend class GUIButtonInputState;
		GUIButtonInput(const Vec2& initRelPos, const Vec2& initPixPos, float initRelHeight, float initPixHeight, const ButtonInfo& initVal, ButtonInfo* initValPtr):
		GUIElement(initRelPos, initPixPos, Vec2(initRelHeight), Vec2(initPixHeight)),
		state(new GUIButtonInputState(initVal)),
		inputButton(new GUIImageButton("InputIcon", initRelPos, initPixPos, Vec2(initRelHeight), Vec2(initPixHeight))),
		waitingForInput(false),
		timeWaitForInput(TimeUtil::Get().GetTimeSinceStart()),
		valPtr(initValPtr)
		{
			RefreshKeyString();
		}
		virtual ~GUIButtonInput()
		{
			delete state;
			delete inputButton;
		}
		virtual void Update(bool isSelectedElement)
		{
			if(state->update)
			{
				state->update = false;
				RefreshKeyString();
			}
			inputButton->Update(false);
			if(static_cast<GUIButtonState*>(inputButton->GrabState())->buttonState == GUIBUTTON_PRESSED)
			{
				strButton = "Press a button...";
				waitingForInput = true;
				timeWaitForInput = TimeUtil::Get().GetTimeSinceStart();
				// Grab joystick axes to detect change
				currentJoystick = ControllerUtil::Get().GetCurrentJoystickID();
				if(currentJoystick != -1)
				{
					const auto& axisVector = WindowUtil::Get().GetJoyInfo().find(currentJoystick)->second.axes;
					for(const auto& axisPair : axisVector)
						currentJoystickAxes.emplace_back(std::roundf(axisPair.first));
				}
			}
			else if(waitingForInput)
			{
				if(timeWaitForInput + GRABINPUT_TIMEOUT < TimeUtil::Get().GetTimeSinceStart()) // Timeout, set to NONE
				{
					state->SetVal(ButtonInfo(INPUTDEVICE_KEYBOARD, 0.0f, -1));
					waitingForInput = false;
				}
				else if(WindowUtil::Get().GetRecentKeyPress() != -1)
				{
					state->SetVal(ButtonInfo(INPUTDEVICE_KEYBOARD, 0.0f, WindowUtil::Get().GetRecentKeyPress()));
					StopInput();
				}
				else if(WindowUtil::Get().GetRecentMousePress() != -1)
				{
					state->SetVal(ButtonInfo(INPUTDEVICE_MOUSE, 0.0f, WindowUtil::Get().GetRecentMousePress()));
					StopInput();
				}
				else if(currentJoystick != -1)
				{
					int joystickID = ControllerUtil::Get().GetCurrentJoystickID(); // Check if joystick is still connected
					if(joystickID == currentJoystick)
					{
						const auto& buttonVector = WindowUtil::Get().GetJoyInfo().find(joystickID)->second.buttons;
						for(uint32_t i = 0; i < buttonVector.size(); i++)
						{
							if(buttonVector[i] == KEYSTATE_PRESSED)
							{
								state->SetVal(ButtonInfo(INPUTDEVICE_JOYSTICK, 0.0f, i));
								StopInput();
								break;
							}
						}
						if(waitingForInput)
						{
							const auto& axisVector = WindowUtil::Get().GetJoyInfo().find(joystickID)->second.axes;
							for(uint32_t i = 0; i < axisVector.size(); i++)
							{
								float roundedAxis = std::roundf(axisVector[i].first);
								if(roundedAxis != currentJoystickAxes[i] && roundedAxis != 0.0f)
								{
									state->SetVal(ButtonInfo(INPUTDEVICE_JOYSTICK, roundedAxis, i));
									waitingForInput = false;
									break;
								}
							}
						}
					}
				}
			}
		}
		virtual void CalculatePos(const Vec2& winPos, const Vec2& winSize)
		{
			GUIElement::CalculatePos(winPos, winSize);
			inputButton->CalculatePos(winPos, winSize);
		}
		virtual void Draw(bool isSelectedElement)
		{
			DrawUtil::Get().DrawText(layer, strButton, Vec2(pos.x+size.y, pos.y), size.y);
			inputButton->Draw(false);
		}
		virtual GUIElementState* GrabState() { return state; }
		virtual void UpdateValPtr() { if(valPtr) *valPtr = state->val; }
	private:
		void RefreshKeyString()
		{
			if(state->val.button < 0)
				strButton = UTF8String("NONE");
			else if(state->val.inputDevice == INPUTDEVICE_JOYSTICK)
			{
				strButton = "JOY_";
				if(state->val.axisSetting != 0.0f) // Joystick Axis
					strButton += UTF8String("AXIS_") + UTF8String::IntToStr(state->val.button) + UTF8String("_") + UTF8String::IntToStr(state->val.axisSetting);
				else // Joystick Button
					strButton += UTF8String("BUTTON_") + UTF8String::IntToStr(state->val.button);
			}
			else if(state->val.inputDevice == INPUTDEVICE_MOUSE)
				strButton = UTF8String("MOUSE_") + UTF8String::IntToStr(state->val.button);
			else
			{
				const char* keyboardKeyName = WindowUtil::Get().GetKeyboardKeyName(state->val.button);
				if(keyboardKeyName == nullptr)
					strButton = UTF8String("KEYBOARD_") + UTF8String::IntToStr(state->val.button);
				else
					strButton = UTF8String("KEYBOARD_") + keyboardKeyName;
			}
		}
		void StopInput()
		{
			waitingForInput = false;
			currentJoystickAxes.clear();
			currentJoystick = -1;
		}
		GUIButtonInputState* state;
		GUIImageButton* inputButton;
		UTF8String strButton;
		bool waitingForInput;
		double timeWaitForInput;
		int currentJoystick;
		std::vector<float> currentJoystickAxes;
		ButtonInfo* valPtr;
};
