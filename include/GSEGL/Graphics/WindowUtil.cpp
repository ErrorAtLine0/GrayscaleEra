#include "WindowUtil.hpp"
#include <algorithm>
#include "../ErrorUtil.hpp"
#include "GUI/GUIUtil.hpp"
#include "DrawUtil.hpp"

JoystickInfo::JoystickInfo() {}
JoystickInfo::JoystickInfo(const UTF8String& initName, int buttonCount, int axisCount):
name(initName),
buttons(buttonCount),
axes(axisCount)
{}

WindowUtil& WindowUtil::Get()
{
	static WindowUtil sInstance;
	return sInstance;
}

int WindowUtil::Init(const UTF8String& windowName, uint32_t width, uint32_t height, RendererEnum rendererToUse)
{
	glfwInit();

#ifdef ENABLE_OPENGLES_RENDERER
	if(rendererToUse == RENDERER_OPENGLES)
	{
		glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
		glfwWindowHint(GLFW_CONTEXT_CREATION_API, GLFW_NATIVE_CONTEXT_API);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	}
#endif
#ifdef ENABLE_OPENGL_RENDERER
	if(rendererToUse == RENDERER_OPENGL)
	{
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	#ifdef __APPLE__
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	#endif
	}
#endif
#ifdef ENABLE_VULKAN_RENDERER
	if(rendererToUse == RENDERER_VULKAN)
	{
		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	}
#endif

	window = glfwCreateWindow(width, height, windowName.GetCString(), NULL, NULL);
	if(window == NULL)
	{
		glfwTerminate();
		ErrorUtil::Get().SetError("Memory", "WindowUtil::Init Failed to create GLFW window");
		return -2;
	}

#if defined(ENABLE_OPENGL_RENDERER) || defined(ENABLE_OPENGLES_RENDERER)
	if(rendererToUse == RENDERER_OPENGL || rendererToUse == RENDERER_OPENGLES)
		glfwMakeContextCurrent(window);
#endif

#ifdef ENABLE_OPENGLES_RENDERER
	if(rendererToUse == RENDERER_OPENGLES)
	{
		if(!gladLoadGLES2Loader((GLADloadproc)glfwGetProcAddress))
		{
			ErrorUtil::Get().SetError("Memory", "WindowUtil::Init Failed to initialize GLAD (OpenGL ES)");
			return -1;
		}
	}
#endif
#ifdef ENABLE_OPENGL_RENDERER
	if(rendererToUse == RENDERER_OPENGL)
	{
		if(!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
		{
			ErrorUtil::Get().SetError("Memory", "WindowUtil::Init Failed to initialize GLAD (OpenGL)");
			return -1;
		}
	}
#endif
#ifdef ENABLE_VULKAN_RENDERER
	if(rendererToUse == RENDERER_VULKAN)
	{
		VulkanUtil::Get().Init(window);
	}
#endif
	
#if defined(ENABLE_OPENGL_RENDERER) || defined(ENABLE_OPENGLES_RENDERER)
	if(rendererToUse == RENDERER_OPENGL || rendererToUse == RENDERER_OPENGLES)
	{
		glViewport(0, 0, width, height);
		// IF THINGS DONT WORK OUT, MOVE THESE THREE STATEMENTS AFTER THE CALLBACK SETUPS
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	}
#endif
	
	glfwSetFramebufferSizeCallback(window, WindowUtilFramebufferSizeCallback);
	glfwSetCursorPosCallback(window, WindowUtilCursorPosCallback);
	glfwSetKeyCallback(window, WindowUtilKeyCallback);
	glfwSetMouseButtonCallback(window, WindowUtilMouseButtonCallback);
	glfwSetCharCallback(window, WindowUtilCharCallback);
	glfwSetScrollCallback(window, WindowUtilScrollCallback);
	glfwSetDropCallback(window, WindowUtilDropCallback);
	glfwSetJoystickCallback(WindowUtilJoystickCallback);
	
	setSize.x = trueSize.x = width;
	setSize.y = trueSize.y = height;
	
	SetVSync(true);
	
	// Initialize joystick list
	for(uint32_t i = 0; i <= GLFW_JOYSTICK_LAST; i++)
	{
		int present = glfwJoystickPresent(i);
		if(present == GLFW_TRUE) // Add joystick
		{
			int buttonCount = 0;
			int axisCount = 0;
			glfwGetJoystickButtons(i, &buttonCount);
			glfwGetJoystickAxes(i, &axisCount);
			const char* joyName = glfwGetJoystickName(i);
			joysticks.emplace(std::piecewise_construct, std::forward_as_tuple(i), std::forward_as_tuple(joyName, buttonCount, axisCount));
		}
	}
	
	renderer = rendererToUse;
	
	return 0;
}

void WindowUtil::Cleanup() const
{
#ifdef ENABLE_VULKAN_RENDERER
	if(renderer == RENDERER_VULKAN)
		VulkanUtil::Get().Cleanup();
#endif
	glfwTerminate();
}

void WindowUtil::PollEvents()
{
	codepointJustReceived = false;
	droppedFileNames.clear();
	if(keySetToHeldNextPoll >= 0)
	{
		keyStates[keySetToHeldNextPoll] = KEYSTATE_HELD;
		keySetToHeldNextPoll = -1;
	}
	if(mouseButtonSetToHeldNextPoll >= 0)
	{
		mouseButtonStates[mouseButtonSetToHeldNextPoll] = KEYSTATE_HELD;
		mouseButtonSetToHeldNextPoll = -1;
	}
	scrollOffset.x = 0.0f;
	scrollOffset.y = 0.0f;
	recentWindowResize = false;
	
	glfwPollEvents();
	
	for(auto& joyPair : joysticks)
	{
		int buttonCount = 0;
		const unsigned char* joyButtons = glfwGetJoystickButtons(joyPair.first, &buttonCount);
		for(int i = 0; i < buttonCount; i++)
		{
			if(joyButtons[i] == GLFW_PRESS)
			{
				if(joyPair.second.buttons[i] != KEYSTATE_RELEASED)
					joyPair.second.buttons[i] = KEYSTATE_HELD;
				else
					joyPair.second.buttons[i] = KEYSTATE_PRESSED;
			}
			else
				joyPair.second.buttons[i] = KEYSTATE_RELEASED;
		}
		int axisCount = 0;
		const float* joyAxes = glfwGetJoystickAxes(joyPair.first, &axisCount);
		for(int i = 0; i < axisCount; i++)
		{
			if(joyPair.second.axes[i].first == joyAxes[i]) // Axis has been held
				joyPair.second.axes[i].second = true;
			else
			{
				joyPair.second.axes[i].first = joyAxes[i];
				joyPair.second.axes[i].second = false;
			}
		}
	}
}

void WindowUtil::SwapBuffers()
{
#if defined(ENABLE_OPENGL_RENDERER) || defined(ENABLE_OPENGLES_RENDERER)
	if(renderer == RENDERER_OPENGL || renderer == RENDERER_OPENGLES)
		glfwSwapBuffers(window);
#endif
#ifdef ENABLE_VULKAN_RENDERER
	if(renderer == RENDERER_VULKAN)
	{
		VulkanUtil::Get().EndImage();
		VulkanUtil::Get().StartImage();
	}
#endif
}
void WindowUtil::SetClearColor(float red, float green, float blue) const
{
#if defined(ENABLE_OPENGL_RENDERER) || defined(ENABLE_OPENGLES_RENDERER)
	if(renderer == RENDERER_OPENGL || renderer == RENDERER_OPENGLES)
		glClearColor(red, green, blue, 1.0f);
#endif
#ifdef ENABLE_VULKAN_RENDERER
	if(renderer == RENDERER_VULKAN)
		VulkanUtil::Get().SetClearColor(red, green, blue, 1.0f);
#endif
}
void WindowUtil::ClearColor() const
{
#if defined(ENABLE_OPENGL_RENDERER) || defined(ENABLE_OPENGLES_RENDERER)
	if(renderer == RENDERER_OPENGL || renderer == RENDERER_OPENGLES)
		glClear(GL_COLOR_BUFFER_BIT);
#endif // NOT IMPLEMENTED FOR VULKAN YET
}
bool WindowUtil::ShouldWindowClose() const { return glfwWindowShouldClose(window); }

void WindowUtil::ToggleWindowedFullscreen()
{
	GLFWmonitor* primaryMonitor = glfwGetPrimaryMonitor();
	const GLFWvidmode* mode = glfwGetVideoMode(primaryMonitor);
	if(isWindowedFullscreen)
		glfwSetWindowMonitor(window, nullptr, (mode->width / 2) - (setSize.x / 2), (mode->height / 2) - (setSize.y / 2), setSize.x, setSize.y, mode->refreshRate);
	else
	{
		glfwWindowHint(GLFW_RED_BITS, mode->redBits);
		glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
		glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
		glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);
		glfwSetWindowMonitor(window, primaryMonitor, 0, 0, mode->width, mode->height, mode->refreshRate);
	}
	isWindowedFullscreen = !isWindowedFullscreen;
}

void WindowUtil::SetVSync(bool isVsync)
{
	vsync = isVsync;
	glfwSwapInterval(vsync ? 1 : 0);
}

void WindowUtil::SetCursorHidden(bool isHidden)
{
	glfwSetInputMode(window, GLFW_CURSOR, isHidden ? GLFW_CURSOR_HIDDEN : GLFW_CURSOR_NORMAL);
}

void WindowUtil::SetClipboardStr(const char* str) const { glfwSetClipboardString(window, str); }  
const char* WindowUtil::GetClipboardStr()         const { return glfwGetClipboardString(window); }

bool                                         WindowUtil::IsWindowResized()               const { return recentWindowResize; }
const Vec2&                                  WindowUtil::GetWindowSize()                 const { return trueSize; }
const Vec2&                                  WindowUtil::GetCursorPos()                  const { return cursorPos; }
const Vec2&                                  WindowUtil::GetScrollOffset()               const { return scrollOffset; }
KeyState                                     WindowUtil::GetKeyState(int key)            const { return keyStates[key]; }
KeyState                                     WindowUtil::GetMouseButtonState(int button) const { return mouseButtonStates[button]; }
const std::unordered_map<int, JoystickInfo>& WindowUtil::GetJoyInfo()                    const { return joysticks; }
const uint32_t*                              WindowUtil::GetMostRecentCodepoint()        const { return (codepointJustReceived ? &mostRecentCodepoint : nullptr); }
bool                                         WindowUtil::IsControlHeld()                 const { return modifierBits&GLFW_MOD_CONTROL; }
bool                                         WindowUtil::IsWindowedFullscreen()          const { return isWindowedFullscreen; }
const std::vector<UTF8String>&               WindowUtil::GetDroppedFileNames()           const { return droppedFileNames; }
const char*                                  WindowUtil::GetKeyboardKeyName(int key)     const { return glfwGetKeyName(key, 0); }
int                                          WindowUtil::GetRecentKeyPress()             const { return keySetToHeldNextPoll; }
int                                          WindowUtil::GetRecentMousePress()           const { return mouseButtonSetToHeldNextPoll; }
RendererEnum                                 WindowUtil::GetRenderer()                   const { return renderer; }
WindowUtil::WindowUtil():
mostRecentCodepoint(0),
codepointJustReceived(false),
keySetToHeldNextPoll(-1),
mouseButtonSetToHeldNextPoll(-1),
modifierBits(0)
{
	std::fill_n(keyStates, GLFW_KEY_LAST, KEYSTATE_RELEASED);
	std::fill_n(mouseButtonStates, GLFW_MOUSE_BUTTON_LAST, KEYSTATE_RELEASED);
}

inline void WindowUtilFramebufferSizeCallback(GLFWwindow* window, int width, int height)
{
	WindowUtil::Get().trueSize.x = width;
	WindowUtil::Get().trueSize.y = height;
	WindowUtil::Get().recentWindowResize = true;
#if defined(ENABLE_OPENGL_RENDERER) || defined(ENABLE_OPENGLES_RENDERER)
	if(WindowUtil::Get().renderer == RENDERER_OPENGL || WindowUtil::Get().renderer == RENDERER_OPENGLES)
		glViewport(0, 0, width, height);
#endif
}

inline void WindowUtilCursorPosCallback(GLFWwindow* window, double xpos, double ypos)
{
	WindowUtil::Get().cursorPos.x = xpos;
	if(WindowUtil::Get().cursorPos.x < 0.0f)
		WindowUtil::Get().cursorPos.x = 0.0f;
	else if(WindowUtil::Get().cursorPos.x > WindowUtil::Get().trueSize.x)
		WindowUtil::Get().cursorPos.x = WindowUtil::Get().trueSize.x;
	// Make sure Y is flipped, since origin is at bottom left corner, not at top left
	WindowUtil::Get().cursorPos.y = (-ypos) + WindowUtil::Get().trueSize.y;
	if(WindowUtil::Get().cursorPos.y < 0.0f)
		WindowUtil::Get().cursorPos.y = 0.0f;
	else if(WindowUtil::Get().cursorPos.y > WindowUtil::Get().trueSize.y)
		WindowUtil::Get().cursorPos.y = WindowUtil::Get().trueSize.y;
}

inline void WindowUtilKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if((key >= 0 && key <= GLFW_KEY_LAST) && action == GLFW_PRESS)
	{
		WindowUtil::Get().keyStates[key] = KEYSTATE_PRESSED;
		WindowUtil::Get().keySetToHeldNextPoll = key;
	}
	else if((key >= 0 && key <= GLFW_KEY_LAST) && action == GLFW_RELEASE)
		WindowUtil::Get().keyStates[key] = KEYSTATE_RELEASED;
	WindowUtil::Get().modifierBits = mods;
}

inline void WindowUtilMouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
	if((button >= 0 && button <= GLFW_KEY_LAST) && action == GLFW_PRESS)
	{
		WindowUtil::Get().mouseButtonStates[button] = KEYSTATE_PRESSED;
		WindowUtil::Get().mouseButtonSetToHeldNextPoll = button;
	}
	else if((button >= 0 && button <= GLFW_KEY_LAST) && action == GLFW_RELEASE)
		WindowUtil::Get().mouseButtonStates[button] = KEYSTATE_RELEASED;
}

inline void WindowUtilCharCallback(GLFWwindow* window, unsigned int codepoint)
{
	WindowUtil::Get().mostRecentCodepoint = codepoint;
	WindowUtil::Get().codepointJustReceived = true;
}

inline void WindowUtilScrollCallback(GLFWwindow* window, double xoffset, double yoffset)
{
	WindowUtil::Get().scrollOffset.x = xoffset;
	WindowUtil::Get().scrollOffset.y = yoffset;
}

inline void WindowUtilDropCallback(GLFWwindow* window, int count, const char** paths)
{
	for(int i = 0; i < count; i++)
		WindowUtil::Get().droppedFileNames.emplace_back(paths[i]);
}

inline void WindowUtilJoystickCallback(int joy, int event)
{
	if(event == GLFW_CONNECTED)
	{
		int buttonCount = 0;
		int axisCount = 0;
		glfwGetJoystickButtons(joy, &buttonCount);
		glfwGetJoystickAxes(joy, &axisCount);
		const char* joyName = glfwGetJoystickName(joy);
		WindowUtil::Get().joysticks.emplace(std::piecewise_construct, std::forward_as_tuple(joy), std::forward_as_tuple(joyName, buttonCount, axisCount));
	}
	else if(event == GLFW_DISCONNECTED)
		WindowUtil::Get().joysticks.erase(joy);
}
