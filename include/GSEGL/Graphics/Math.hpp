#pragma once
#include <cstdint>
#include <cmath>
#include <functional>

template<class vec_type> class TemplateVec2
{
	public:
		TemplateVec2():
		x(0),
		y(0)
		{}
		
		TemplateVec2(vec_type initVal):
		x(initVal),
		y(initVal)
		{}
		
		TemplateVec2(vec_type initX, vec_type initY):
		x(initX),
		y(initY)
		{}
		
		template <class vecb_type> operator TemplateVec2<vecb_type>() const
		{
			return TemplateVec2<vecb_type>(x, y);
		}
		
		template <class vecb_type> TemplateVec2<vec_type> operator+(const TemplateVec2<vecb_type>& vecB) const
		{
			return TemplateVec2<vec_type>(x + vecB.x, y + vecB.y);
		}
		
		template <class vecb_type> TemplateVec2<vec_type> operator-(const TemplateVec2<vecb_type>& vecB) const
		{
			return TemplateVec2<vec_type>(x - vecB.x, y - vecB.y);
		}
		
		template <class vecb_type> void operator+=(const TemplateVec2<vecb_type>& vecB)
		{
			x += vecB.x;
			y += vecB.y;
		}
		
		template <class vecb_type> void operator-=(const TemplateVec2<vecb_type>& vecB)
		{
			x -= vecB.x;
			y -= vecB.y;
		}
		
		TemplateVec2<vec_type> operator+(vec_type num) const
		{
			return TemplateVec2<vec_type>(x + num, y + num);
		}
		
		TemplateVec2<vec_type> operator-(vec_type num) const
		{
			return TemplateVec2<vec_type>(x - num, y - num);
		}
		
		TemplateVec2<vec_type> operator*(vec_type num) const
		{
			return TemplateVec2<vec_type>(x * num, y * num);
		}
		
		TemplateVec2<vec_type> operator/(vec_type num) const
		{
			return TemplateVec2<vec_type>(x / num, y / num);
		}
		
		void operator+=(vec_type num)
		{
			x += num;
			y += num;
		}
		
		void operator-=(vec_type num)
		{
			x -= num;
			y -= num;
		}
		
		void operator*=(vec_type num)
		{
			x *= num;
			y *= num;
		}
		
		void operator/=(vec_type num)
		{
			x /= num;
			y /= num;
		}
		
		template <class vecb_type> bool operator!=(const TemplateVec2<vecb_type>& vecB) const
		{
			return (x != vecB.x || y != vecB.y);
		}
		
		template <class vecb_type> bool operator==(const TemplateVec2<vecb_type>& vecB) const
		{
			return (x == vecB.x && y == vecB.y);
		}
		
		vec_type x;
		vec_type y;
};

namespace std
{
	template<class vec_type> struct hash<TemplateVec2<vec_type>>
	{
		std::size_t operator()(const TemplateVec2<vec_type>& vec) const
		{
			std::size_t h1 = std::hash<vec_type>()(vec.x);
			std::size_t h2 = std::hash<vec_type>()(vec.y);
		    return (h1 ^ (h2 << 1));
		}
	};
	template<class vec_type> struct hash<const TemplateVec2<vec_type>>
	{
		std::size_t operator()(const TemplateVec2<vec_type>& vec) const
		{
			std::size_t h1 = std::hash<vec_type>()(vec.x);
			std::size_t h2 = std::hash<vec_type>()(vec.y);
		    return (h1 ^ (h2 << 1));
		}
	};
}

using Vec2 = TemplateVec2<float>;
using UVec2 = TemplateVec2<uint32_t>;
using BVec2 = TemplateVec2<bool>;
using IVec2 = TemplateVec2<int32_t>;

inline Vec2 Normalize(const Vec2& vec)
{
	return vec / std::sqrt(vec.x * vec.x + vec.y * vec.y);
}

inline void identityMat(float (&mat)[4][4])
{
	mat[0][0] = 1.0f; mat[1][0] = 0.0f; mat[2][0] = 0.0f; mat[3][0] = 0.0f;
	mat[0][1] = 0.0f; mat[1][1] = 1.0f; mat[2][1] = 0.0f; mat[3][1] = 0.0f;
	mat[0][2] = 0.0f; mat[1][2] = 0.0f; mat[2][2] = 1.0f; mat[3][2] = 0.0f;
	mat[0][3] = 0.0f; mat[1][3] = 0.0f; mat[2][3] = 0.0f; mat[3][3] = 1.0f;
}

inline void orthoMat(float (&mat)[4][4], float right, float left, float top, float bot) {
	identityMat(mat);
	mat[0][0] = 2 / (right-left);
	mat[1][1] = 2 / (top-bot);
	mat[3][0] = -(right+left) / (right-left);
	mat[3][1] = -(top+bot) / (top-bot);
}
