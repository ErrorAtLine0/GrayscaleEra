#define STB_IMAGE_IMPLEMENTATION
#include "../libs/stb_image.h"
#include "../ErrorUtil.hpp"
#include "../FileUtil.hpp"
#include "Texture.hpp"
#include "WindowUtil.hpp"

#if defined(ENABLE_OPENGL_RENDERER) || defined(ENABLE_OPENGLES_RENDERER)
	const GLuint* Texture::currentlyBoundTexture = nullptr;
#endif
#ifdef ENABLE_VULKAN_RENDERER
	const VulkanTextureData* Texture::currentlyBoundVulkanTexture = nullptr;
#endif

unsigned char* Texture::LoadImageData(const UTF8String& path, int32_t* width, int32_t* height, int32_t* nrChannels, int32_t componentsPerPixel)
{
	return stbi_load(path.GetCString(), width, height, nrChannels, componentsPerPixel);
}

void Texture::FreeImageData(unsigned char* data)
{
	stbi_image_free(data);
}

Texture::Texture(const UTF8String& imagePath)
{
	int32_t width, height, nrChannels;
	unsigned char* data = LoadImageData(FileUtil::Get().GetTruePath(imagePath), &width, &height, &nrChannels, 0);
	if(data)
	{
		Init(data, width, height);
		FreeImageData(data);
	}
	else
		ErrorUtil::Get().SetError("Memory", "Texture::Init Failed to load texture");
}

Texture::Texture(const unsigned char* data, int32_t width, int32_t height)
{
	Init(data, width, height);
}

void Texture::Init(const unsigned char* data, int32_t width, int32_t height)
{
#ifdef ENABLE_OPENGLES_RENDERER
	if(WindowUtil::Get().GetRenderer() == RENDERER_OPENGLES)
	{
		glGenTextures(1, &ID);
		glBindTexture(GL_TEXTURE_2D, ID);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, width, height, 0, GL_ALPHA, GL_UNSIGNED_BYTE, data);
	}
#endif
#ifdef ENABLE_OPENGL_RENDERER
	if(WindowUtil::Get().GetRenderer() == RENDERER_OPENGL)
	{
		glGenTextures(1, &ID);
		glBindTexture(GL_TEXTURE_2D, ID);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, width, height, 0, GL_RED, GL_UNSIGNED_BYTE, data);
	}
#endif
#ifdef ENABLE_VULKAN_RENDERER
	if(WindowUtil::Get().GetRenderer() == RENDERER_VULKAN)
		vulkanTextureData = VulkanUtil::Get().CreateTexture(data, width, height);
#endif
}

void Texture::ChangeData(const unsigned char* data, int32_t xoffset, int32_t yoffset, int32_t width, int32_t height)
{
#ifdef ENABLE_OPENGLES_RENDERER
	if(WindowUtil::Get().GetRenderer() == RENDERER_OPENGLES)
	{
		Bind();
		glTexSubImage2D(GL_TEXTURE_2D, 0, xoffset, yoffset, width, height, GL_ALPHA, GL_UNSIGNED_BYTE, data);
	}
#endif
#ifdef ENABLE_OPENGL_RENDERER
	if(WindowUtil::Get().GetRenderer() == RENDERER_OPENGL)
	{
		Bind();
		glTexSubImage2D(GL_TEXTURE_2D, 0, xoffset, yoffset, width, height, GL_RED, GL_UNSIGNED_BYTE, data);
	}
#endif
#ifdef ENABLE_VULKAN_RENDERER
	if(WindowUtil::Get().GetRenderer() == RENDERER_VULKAN)
		VulkanUtil::Get().ChangeTextureData(vulkanTextureData, data, xoffset, yoffset, width, height);
#endif
}

void Texture::Bind() const
{
#if defined(ENABLE_OPENGLES_RENDERER) || defined(ENABLE_OPENGL_RENDERER)
	if(WindowUtil::Get().GetRenderer() == RENDERER_OPENGL || WindowUtil::Get().GetRenderer() == RENDERER_OPENGLES)
	{
		if(currentlyBoundTexture != &ID)
		{
			glBindTexture(GL_TEXTURE_2D, ID);
			currentlyBoundTexture = &ID;
		}
	}
#endif
#ifdef ENABLE_VULKAN_RENDERER
	if(WindowUtil::Get().GetRenderer() == RENDERER_VULKAN)
	{
		if(currentlyBoundVulkanTexture != &vulkanTextureData)
		{
			VulkanUtil::Get().BindTexture(vulkanTextureData);
			currentlyBoundVulkanTexture = &vulkanTextureData;
		}
	}
#endif
}

Texture::~Texture()
{
#if defined(ENABLE_OPENGLES_RENDERER) || defined(ENABLE_OPENGL_RENDERER)
	if(WindowUtil::Get().GetRenderer() == RENDERER_OPENGL || WindowUtil::Get().GetRenderer() == RENDERER_OPENGLES)
	{
		if(currentlyBoundTexture == &ID)
			currentlyBoundTexture = nullptr;
		glDeleteTextures(1, &ID);
	}
#endif
#ifdef ENABLE_VULKAN_RENDERER
	if(WindowUtil::Get().GetRenderer() == RENDERER_VULKAN)
	{
		if(currentlyBoundVulkanTexture == &vulkanTextureData)
			currentlyBoundVulkanTexture = nullptr;
		VulkanUtil::Get().DeleteTexture(vulkanTextureData);
	}
#endif
}
