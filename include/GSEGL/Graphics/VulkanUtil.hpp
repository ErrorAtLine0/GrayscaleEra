#pragma once
#ifdef ENABLE_VULKAN_RENDERER
#include <vector>
#include "../UTF8String.hpp"
#include "Math.hpp"
#include "../libs/vk_mem_alloc.h"
#include "WindowUtil.hpp"

#define MAX_FRAMES_IN_FLIGHT 2

struct VulkanVertexData
{
	void* data;
	uint32_t bufferSize;
	uint32_t vertexCount;
	VkVertexInputBindingDescription bindingDescription;
	std::vector<VkVertexInputAttributeDescription> attributeDescriptions;
};

struct VulkanUniformData
{
	void* data;
	uint32_t bufferSize;
};

struct VulkanPipelineData
{
	UTF8String vertexShaderPath;
	UTF8String fragmentShaderPath;
	VulkanVertexData vertexData;
};

struct VulkanTextureData
{
	VkImage image;
	VmaAllocation imageMemory;
	VkImageView imageView;
	VkDescriptorSet descriptorSet;
};

class VulkanUtil
{
	public:
		static VulkanUtil& Get();
		void Init(GLFWwindow* window);
		void SetPipelineData(const VulkanPipelineData& pipelineData);
		VulkanTextureData CreateTexture(const void* rawData, uint32_t texWidth, uint32_t texHeight);
		void DeleteTexture(const VulkanTextureData& textureData);
		void BindTexture(const VulkanTextureData& textureData);
		void ChangeTextureData(const VulkanTextureData& textureData, const void* rawData, int32_t xoffset, int32_t yoffset, uint32_t width, uint32_t height);
		void UpdateUniformData(const VulkanUniformData& uniformData);
		void SetClearColor(float red, float green, float blue, float alpha);
		void RefreshSwapChain();
		void StartImage();
		void EndImage();
		
		void Draw();
		void Cleanup();
	private:
		// Structures
		struct QueueFamilyIndices
		{
			int32_t graphicsFamily = -1;
			int32_t presentFamily = -1;
			bool isComplete() { return graphicsFamily >= 0 && presentFamily >= 0; }
		};
		
		struct SwapChainSupportDetails
		{
			VkSurfaceCapabilitiesKHR capabilities;
			std::vector<VkSurfaceFormatKHR> formats;
			std::vector<VkPresentModeKHR> presentModes;
		};
		
		struct TempUniformData
		{
			VkBuffer buffer;
			VmaAllocation deviceMemory;
			VkDescriptorSet descriptorSet;
		};
		
		// Functions
		// Called for initialization
		void CreateInstance();
		void SetupDebugCallback();
		void CreateSurface();
		void PickPhysicalDevice();
		void CreateLogicalDevice();
		void CreateAllocator();
		
		void CreateSwapChain();
		void CreateImageViews();
		void CreateRenderPass();
		void CreateDescriptorSetLayouts();
		void CreateGraphicsPipeline();
		void CreateFramebuffers();
		void CreateCommandPool();
		std::pair<VkImage, VmaAllocation> CreateTextureImage(const void* imageRawData, uint32_t texWidth, uint32_t texHeight);
		void CreateTextureImageSampler();
		void CreateDescriptorPools();
		void CreateCommandBuffers();
		void CreateSyncObjects();
		
	#ifdef GSE_DEBUG
		static VKAPI_ATTR VkBool32 VKAPI_CALL DebugCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType, uint64_t obj, size_t location, int32_t code, const char* layerPrefix, const char* msg, void* userData);
		static VkResult CreateDebugReportCallbackEXT(VkInstance instance, const VkDebugReportCallbackCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugReportCallbackEXT* pCallback);
		static void DestroyDebugReportCallbackEXT(VkInstance instance, VkDebugReportCallbackEXT callback, VkAllocationCallbacks* pAllocator);
	#endif
		std::vector<const char*> GetRequiredExtensions();
		bool IsDeviceSuitable(VkPhysicalDevice device);
		bool CheckDeviceExtensionSupport(VkPhysicalDevice device);
		QueueFamilyIndices FindQueueFamilies(VkPhysicalDevice device);
		SwapChainSupportDetails QuerySwapChainSupport(VkPhysicalDevice device);
		
		VkImageView CreateImageView(VkImage image, VkFormat format);
		void TransitionImageLayout(VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout);
		void CreateVertexBuffer();
		void CreateBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VmaMemoryUsage properties, VkBuffer& buffer, VmaAllocation& allocation);
		void CopyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size);
		void CreateImage(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VmaMemoryUsage properties, VkImage& image, VmaAllocation& allocation);
		void CopyBufferToImage(VkBuffer buffer, VkImage image, int32_t xoffset, int32_t yoffset, uint32_t width, uint32_t height);
		VkShaderModule CreateShaderModule(const unsigned char* shaderCode, uint32_t shaderSize);
		
		VkSurfaceFormatKHR ChooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats);
		VkPresentModeKHR ChooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes);
		VkExtent2D ChooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities);
		
		VkCommandBuffer BeginSingleTimeCommands();
		void EndSingleTimeCommands(VkCommandBuffer commandBuffer);
		
		void CleanupSwapChain();
		void DestroyAllTexturesToDestroy();
		
		// Objects
		VkInstance instance;
		VkPhysicalDevice physicalDevice;
		VkDevice device;
		VmaAllocator allocator;
		VkDebugReportCallbackEXT callback;
		VkSurfaceKHR surface;
		VkQueue graphicsQueue;
		VkQueue presentQueue;
		
		VkSwapchainKHR swapChain;
		std::vector<VkImage> swapChainImages;
		VkFormat swapChainImageFormat;
		VkExtent2D swapChainExtent;
		std::vector<VkImageView> swapChainImageViews;
		
		VkRenderPass renderPass;
		VkDescriptorPool uniformDescriptorPool;
		VkDescriptorPool samplerDescriptorPool;
		VkDescriptorSetLayout uniformDescriptorSetLayout;
		VkDescriptorSetLayout samplerDescriptorSetLayout;
		VkPipelineLayout pipelineLayout;
		VkPipeline graphicsPipeline;
		std::vector<VkFramebuffer> swapChainFramebuffers;
		
		VkCommandPool commandPool;
		VkCommandBuffer commandBuffer;
		VkSemaphore imageAvailableSemaphore;
		VkSemaphore renderFinishedSemaphore;
		
		VkBuffer vertexBuffer;
		VmaAllocation vertexBufferMemory;
		std::vector<TempUniformData> tempUniformData;
		
		VkSampler textureSampler;
		const VulkanTextureData* boundTexture;
		std::vector<VulkanTextureData> texturesToDestroy;
		
		uint32_t uniformBufferIndex;
		uint32_t imageIndex;
		bool swapChainCreated;
		VulkanPipelineData pipelineData;
		VkClearValue clearColor;
		GLFWwindow* window;
};

#endif
