#pragma once

#if defined(ENABLE_OPENGL_RENDERER) || defined(ENABLE_OPENGLES_RENDERER)
#include "../libs/glad/glad.h"
#endif

#ifdef ENABLE_OPENGLES_RENDERER
	#include <GLES2/gl2.h>
#endif

#ifdef ENABLE_OPENGL_RENDERER
	#ifdef __APPLE__
		#include <OpenGL/gl.h>
	#else
		#include <GL/gl.h>
	#endif
#endif

#ifdef ENABLE_VULKAN_RENDERER
	#include <vulkan/vulkan.h>
#endif

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <vector>
#include <unordered_map>
#include "Math.hpp"
#include "../UTF8String.hpp"

inline void WindowUtilFramebufferSizeCallback(GLFWwindow* window, int width, int height);
inline void WindowUtilCursorPosCallback(GLFWwindow* window, double xpos, double ypos);
inline void WindowUtilKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
inline void WindowUtilMouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
inline void WindowUtilCharCallback(GLFWwindow* window, unsigned int codepoint);
inline void WindowUtilScrollCallback(GLFWwindow* window, double xoffset, double yoffset);
inline void WindowUtilDropCallback(GLFWwindow* window, int count, const char** paths);
inline void WindowUtilJoystickCallback(int joy, int event);

enum KeyState
{
	KEYSTATE_RELEASED,
	KEYSTATE_PRESSED,
	KEYSTATE_HELD
};

enum RendererEnum
{
	RENDERER_OPENGL,
	RENDERER_OPENGLES,
	RENDERER_VULKAN
};

struct JoystickInfo
{
	JoystickInfo();
	JoystickInfo(const UTF8String& initName, int buttonCount, int axisCount);
	UTF8String name;
	std::vector<KeyState> buttons;
	std::vector<std::pair<float, bool>> axes; // pair<axisValue, hasBeenHeld>
};

class WindowUtil
{
	public:
		static WindowUtil& Get();
		friend inline void WindowUtilFramebufferSizeCallback(GLFWwindow* window, int width, int height);
		friend inline void WindowUtilCursorPosCallback(GLFWwindow* window, double xpos, double ypos);
		friend inline void WindowUtilKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
		friend inline void WindowUtilMouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
		friend inline void WindowUtilCharCallback(GLFWwindow* window, unsigned int codepoint);
		friend inline void WindowUtilScrollCallback(GLFWwindow* window, double xoffset, double yoffset);
		friend inline void WindowUtilDropCallback(GLFWwindow* window, int count, const char** paths);
		friend inline void WindowUtilJoystickCallback(int joy, int event);
		int Init(const UTF8String& windowName, uint32_t width, uint32_t height, RendererEnum rendererToUse);
		void Cleanup() const;
		void PollEvents();
		void SwapBuffers();
		void SetClearColor(float red, float green, float blue) const;
		void ClearColor() const;
		bool ShouldWindowClose() const;
		void ToggleWindowedFullscreen();
		void SetVSync(bool isVsync);
		void SetCursorHidden(bool isHidden);
		// Clipboard functions
		void SetClipboardStr(const char* str) const;
		const char* GetClipboardStr()         const;
		// Getters
		bool                                         IsWindowResized()               const;
		const Vec2&                                  GetWindowSize()                 const;
		const Vec2&                                  GetCursorPos()                  const;
		const Vec2&                                  GetScrollOffset()               const;
		KeyState                                     GetKeyState(int key)            const;
		KeyState                                     GetMouseButtonState(int button) const;
		const std::unordered_map<int, JoystickInfo>& GetJoyInfo()                    const;
		const uint32_t*                              GetMostRecentCodepoint()        const;
		bool                                         IsControlHeld()                 const;
		bool                                         IsWindowedFullscreen()          const;
		const std::vector<UTF8String>&               GetDroppedFileNames()           const;
		const char*                                  GetKeyboardKeyName(int key)     const;
		int                                          GetRecentKeyPress()             const;
		int                                          GetRecentMousePress()           const;
		RendererEnum                                 GetRenderer()                   const;
	private:
		WindowUtil();
		Vec2 setSize;
		Vec2 trueSize;
		Vec2 cursorPos;
		Vec2 scrollOffset;
		KeyState keyStates[GLFW_KEY_LAST];
		KeyState mouseButtonStates[GLFW_MOUSE_BUTTON_LAST];
		uint32_t mostRecentCodepoint;
		bool codepointJustReceived;
		int keySetToHeldNextPoll;
		int mouseButtonSetToHeldNextPoll;
		int modifierBits;
		bool vsync;
		bool isWindowedFullscreen;
		bool recentWindowResize;
		std::vector<UTF8String> droppedFileNames;
		std::unordered_map<int, JoystickInfo> joysticks;
		RendererEnum renderer;
		GLFWwindow* window;
};
