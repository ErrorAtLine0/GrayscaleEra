#pragma once
#include <unordered_map>
#include "../UTF8String.hpp"
#include "Texture.hpp"
#include "Shader.hpp"
#include "Math.hpp"
#include "../libs/stb_truetype.h"
#include <deque>

#define MAX_DRAW_LAYERS 20

struct FontLoadInfo
{
	UTF8String path;
	float pixelHeight;
	uint32_t startingCodePoint;
	uint32_t codePointRange;
};

enum SpriteTransparency
{
	WHITENESS_TRANSPARENCY,
	NORMAL_TRANSPARENCY,
	WHITE_IS_TRANSPARENT
};

class DrawUtil
{
	public:
		static DrawUtil& Get();
		bool Init(const std::vector<FontLoadInfo>& fontsToLoad, float generalFontScale, uint32_t width, uint32_t height);
		void Update();
		void AddSprite(const UTF8String& name, const UTF8String& imagePath);
		const TexturePtr& GetSprite(const UTF8String& name);
		void DrawSprite(uint32_t layer, const UTF8String& name, const Vec2& position, const Vec2& size, float rotate = 0.0f, SpriteTransparency transparencyMode = WHITENESS_TRANSPARENCY, float alpha = 1.0f, const BVec2& flip = BVec2(false, false));
		void DrawSprite(uint32_t layer, const TexturePtr& texture, const Vec2& position, const Vec2& size, float rotate = 0.0f, SpriteTransparency transparencyMode = WHITENESS_TRANSPARENCY, float alpha = 1.0f, const BVec2& flip = BVec2(false, false));
		void DrawRectangle(uint32_t layer, const Vec2& position, const Vec2& size, float white = 0.0f, float alpha = 1.0f);
		void DrawText(uint32_t layer, const UTF8String& str, Vec2 position, float scale, float white = 0.0f, float alpha = 1.0f);
		void DrawBatchedCommands();
		float CalculateTextWidth(const UTF8String& str, float scale);
		void SetColorInvert(bool isInvert);
		void SetProjectionMatrix(uint32_t width, uint32_t height);
		void SetNullTexture(const UTF8String& name);
		void Cleanup();
	private:
	#if defined(ENABLE_OPENGL_RENDERER) || defined(ENABLE_OPENGLES_RENDERER)
		void OpenGLInit();
	#endif
	#ifdef ENABLE_VULKAN_RENDERER
		void VulkanInit();
	#endif
		
		// Text Rendering members
		struct Character
		{
			Character();
			Character(const uint8_t* initData, const IVec2& initSize, const IVec2& initBearing, float initAdvance);
			Character(const Character& character);
			void operator=(const Character& character);
			~Character();
			
			uint8_t* data;
			IVec2 size;
			IVec2 bearing;
			float advance;
		};
		struct CacheTextTexture
		{
			CacheTextTexture(const TexturePtr& initTexture, const Vec2& initSize, float initPixelsBelowLine);
			TexturePtr texture;
			Vec2 size;
			float pixelsBelowLine;
		};
		struct FontData
		{
			std::shared_ptr<InMemBitStream> buffer;
			float scale;
			stbtt_fontinfo info;
			uint32_t startingCodePoint;
			uint32_t codePointRange;
		};
		
		CacheTextTexture* GetTextTextureFromStringCache(const UTF8String& str);
		FontData* GetFontFromCodepoint(uint32_t c);
		void LoadCharacter(uint32_t c);
		std::vector<FontData> fontData;
		float generalFontScale;
		std::unordered_map<uint32_t, Character> usedCharacters;
		std::deque<std::pair<UTF8String, CacheTextTexture>> prerenderedStrings; // <cached string, texture>

		struct DrawSpriteCommand
		{
			DrawSpriteCommand(const TexturePtr& initTexture, const Vec2& initPosition, const Vec2& initSize, float initRotate, SpriteTransparency initTransparencyMode, float initAlpha, const BVec2& initFlip);
			const TexturePtr& texture;
			Vec2 position;
			Vec2 size;
			float rotate;
			SpriteTransparency transparencyMode;
			float alpha;
			BVec2 flip;
		};
		struct DrawTextCommand
		{
			DrawTextCommand(const UTF8String& initStr, const Vec2& initPosition, float initScale, float initWhite, float initAlpha);
			UTF8String str;
			Vec2 position;
			float scale;
			float white;
			float alpha;
		};
		struct DrawRectCommand
		{
			DrawRectCommand(const Vec2& initPosition, const Vec2& initSize, float initWhite, float initAlpha);
			Vec2 position;
			Vec2 size;
			float white;
			float alpha;
		};
		struct DrawCommandLayer
		{
			std::vector<DrawSpriteCommand> spriteCommands;
			std::vector<DrawRectCommand> rectCommands;
			std::vector<DrawTextCommand> textCommands;
		};
	
		// Sprite rendering members
		std::unordered_map<UTF8String, TexturePtr> nameToSprite;

		struct DrawUtilUniformData {
			float model[16];
			float projection[4][4];
 			int32_t flipX;
			int32_t flipY;
			int32_t drawType;
			float alpha;
			int32_t transparencyMode;
			bool invertColors;
			float white;
		} UBO;
		
	#if defined(ENABLE_OPENGL_RENDERER) || defined(ENABLE_OPENGLES_RENDERER)
		Shader shader;
		GLuint VBO;
		// Uniform locations (speedup, finding uniforms is costly)
		GLint shaderDrawType;
		GLint shaderModel;
		GLint shaderFlip;
		GLint shaderAlpha;
		GLint shaderWhite;
		GLint shaderTransparencyMode;
		GLint shaderInvertColors;
		
	#endif
	#ifdef ENABLE_OPENGL_RENDERER
		GLuint VAO;
	#endif
	#ifdef ENABLE_VULKAN_RENDERER
		VulkanPipelineData pipelineData;
	#endif
		TexturePtr nullTexture;
		
		// Batched draw commands
		DrawCommandLayer drawCommands[MAX_DRAW_LAYERS];
};
