#pragma once
#include "../libs/glad/glad.h"
#include <memory>
#include "../UTF8String.hpp"
#include "VulkanUtil.hpp"

class Texture
{
	public:
		Texture(const UTF8String& imagePath);
		Texture(const unsigned char* data, int32_t width, int32_t height);
		void ChangeData(const unsigned char* data, int32_t xoffset, int32_t yoffset, int32_t width, int32_t height);
		void Bind() const;
		~Texture();
		
		static unsigned char* LoadImageData(const UTF8String& path, int32_t* width, int32_t* height, int32_t* nrChannels, int32_t componentsPerPixel);
		static void FreeImageData(unsigned char* data);
	private:
		void Init(const unsigned char* data, int32_t width, int32_t height);
	#if defined(ENABLE_OPENGL_RENDERER) || defined(ENABLE_OPENGLES_RENDERER)
		GLuint ID;
		static const GLuint* currentlyBoundTexture;
	#endif
	#ifdef ENABLE_VULKAN_RENDERER
		VulkanTextureData vulkanTextureData;
		static const VulkanTextureData* currentlyBoundVulkanTexture;
	#endif
};

typedef std::shared_ptr<Texture> TexturePtr;
