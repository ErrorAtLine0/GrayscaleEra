#include "ControllerUtil.hpp"
#include <cmath>
#include "../TimeUtil.hpp"

ButtonInfo::ButtonInfo() {}

ButtonInfo::ButtonInfo(InputDeviceType initInputDevice, float initAxisSetting, int32_t initButton):
inputDevice(initInputDevice),
axisSetting(initAxisSetting),
button(initButton)
{}

void ButtonInfo::Write(OutMemBitStream& outStream) const
{
	outStream.WriteBits(inputDevice, ByteUtil::GetRequiredBits<INPUTDEVICE_MAX>::Value);
	outStream.WriteBits(axisSetting);
	outStream.WriteBits(button);
}

bool ButtonInfo::Read(InMemBitStream& inStream)
{
	if(!inStream.ReadBits(inputDevice, ByteUtil::GetRequiredBits<INPUTDEVICE_MAX>::Value)) return false;
	if(!inStream.ReadBits(axisSetting)) return false;
	if(!inStream.ReadBits(button)) return false;
	return true;
}

ControllerUtil& ControllerUtil::Get()
{
	static ControllerUtil sInstance;
	return sInstance;
}

void ControllerUtil::Init()
{
	// Get any joystick
	auto it = WindowUtil::Get().GetJoyInfo().begin();
	if(it != WindowUtil::Get().GetJoyInfo().end())
		joystickID = it->first;
	else
		joystickID = -1;
	cursorSpeed = 0.0f;
	scrollSpeed = 0.0f;
	isGameMode = false;
	lastScrollInput = TimeUtil::Get().GetTimeSinceStart();
}

void ControllerUtil::Update()
{
	if(joystickID != -1)
	{
		if(WindowUtil::Get().GetJoyInfo().empty()) // No joysticks connected
			joystickID = -1;
		else if(WindowUtil::Get().GetJoyInfo().find(joystickID) == WindowUtil::Get().GetJoyInfo().end()) // Joystick is not connected anymore, use another one
			joystickID = WindowUtil::Get().GetJoyInfo().begin()->first;
	}
	else if(!WindowUtil::Get().GetJoyInfo().empty()) // Joystick connected
		joystickID = WindowUtil::Get().GetJoyInfo().begin()->first;
		
	// Update cursor
	if(lastMousePos != WindowUtil::Get().GetCursorPos())
	{
		lastMousePos = WindowUtil::Get().GetCursorPos();
		cursorPos = lastMousePos;
	}
	else if(isGameMode)
	{
		Vec2 cursorDirection(0.0f);
		cursorDirection.x += GetButtonPower(CONTROLLER_CURSOR_RIGHT);
		cursorDirection.x -= GetButtonPower(CONTROLLER_CURSOR_LEFT);
		cursorDirection.y += GetButtonPower(CONTROLLER_CURSOR_UP);
		cursorDirection.y -= GetButtonPower(CONTROLLER_CURSOR_DOWN);
		if(std::roundf(cursorDirection.x) != 0.0f || std::roundf(cursorDirection.y) != 0.0f)
		{
			Vec2 winSize = WindowUtil::Get().GetWindowSize();
			cursorDirection = Normalize(cursorDirection) * 150.0f;
			cursorPos.x = (winSize.x / 2.0f) + cursorDirection.x;
			cursorPos.y = (winSize.y / 2.0f) + cursorDirection.y;
		}
	}
	else
	{
		cursorPos.x += GetButtonPower(CONTROLLER_CURSOR_RIGHT) * cursorSpeed * TimeUtil::Get().GetDeltaTime() * 100.0f;
		cursorPos.x -= GetButtonPower(CONTROLLER_CURSOR_LEFT) * cursorSpeed * TimeUtil::Get().GetDeltaTime() * 100.0f;
		cursorPos.y += GetButtonPower(CONTROLLER_CURSOR_UP) * cursorSpeed * TimeUtil::Get().GetDeltaTime() * 100.0f;
		cursorPos.y -= GetButtonPower(CONTROLLER_CURSOR_DOWN) * cursorSpeed * TimeUtil::Get().GetDeltaTime() * 100.0f;
		Vec2 winSize = WindowUtil::Get().GetWindowSize();
		if(cursorPos.x > winSize.x)
			cursorPos.x = winSize.x;
		else if(cursorPos.x < 0.0f)
			cursorPos.x = 0.0f;
		if(cursorPos.y > winSize.y)
			cursorPos.y = winSize.y;
		else if(cursorPos.y < 0.0f)
			cursorPos.y = 0.0f;
	}
	
	// Update scroll
	scrollOffset = 0.0f;
	if(WindowUtil::Get().GetScrollOffset().y != 0.0f)
		scrollOffset = WindowUtil::Get().GetScrollOffset().y;
	else if(lastScrollInput + 0.016666667 < TimeUtil::Get().GetTimeSinceStart()) // Use bindings (sample every 1/60 of a second)
	{
		lastScrollInput = TimeUtil::Get().GetTimeSinceStart();
		scrollOffset += GetButtonPower(CONTROLLER_SCROLL_UP) * scrollSpeed;
		scrollOffset -= GetButtonPower(CONTROLLER_SCROLL_DOWN) * scrollSpeed;
	}
}

void ControllerUtil::SetBinding(const ButtonInfo& buttonInfo, ControllerButton controllerButton)
{
	if(buttonInfo.inputDevice == INPUTDEVICE_KEYBOARD)
	{
		keyboardBindings[controllerButton].first = false;
		keyboardBindings[controllerButton].second = buttonInfo.button;
	}
	else if(buttonInfo.inputDevice == INPUTDEVICE_MOUSE)
	{
		keyboardBindings[controllerButton].first = true;
		keyboardBindings[controllerButton].second = buttonInfo.button;
	}
	else
	{
		joystickBindings[controllerButton].first = buttonInfo.axisSetting;
		joystickBindings[controllerButton].second = buttonInfo.button;
	}
}

void ControllerUtil::SetCursorSpeed(float speed)
{
	cursorSpeed = speed * 0.01f;
}

void ControllerUtil::SetCursorToGameMode(bool gameMode)
{
	isGameMode = gameMode;
}

void ControllerUtil::SetScrollSpeed(float speed)
{
	scrollSpeed = speed;
}

int ControllerUtil::GetCurrentJoystickID()
{
	return joystickID;
}

const Vec2& ControllerUtil::GetCursorPos()
{
	return cursorPos;
}

float ControllerUtil::GetScrollOffset()
{
	return scrollOffset;
}

float ControllerUtil::GetButtonPower(ControllerButton button)
{
	float keyboardInput = 0.0f;
	if(keyboardBindings[button].second < 0)
		keyboardInput = 0.0f;
	else if(keyboardBindings[button].first) // Is mouse input
		keyboardInput = WindowUtil::Get().GetMouseButtonState(keyboardBindings[button].second) != KEYSTATE_RELEASED ? 1.0f : 0.0f;
	else
		keyboardInput = WindowUtil::Get().GetKeyState(keyboardBindings[button].second) != KEYSTATE_RELEASED ? 1.0f : 0.0f;
	
	float joystickInput = 0.0f;
	if(joystickID != -1)
	{
		auto joystickIt = WindowUtil::Get().GetJoyInfo().find(joystickID);
		if(joystickIt != WindowUtil::Get().GetJoyInfo().end())
		{
			const JoystickInfo& joystickInfo = joystickIt->second;
			if(joystickBindings[button].second < 0)
				joystickInput = 0.0f;
			else if(joystickBindings[button].first == 0.0f) // Is a button
			{
				if(joystickBindings[button].second < static_cast<int32_t>(joystickInfo.buttons.size()))
					joystickInput = joystickInfo.buttons[joystickBindings[button].second] != KEYSTATE_RELEASED ? 1.0f : 0.0f;
			}
			else // Is an axis
			{
				if(joystickBindings[button].second < static_cast<int32_t>(joystickInfo.axes.size()))
				{
					float axis = joystickInfo.axes[joystickBindings[button].second].first;
					// If axis is pointing opposite of intended direction, return 0
					if((axis > 0.0f && joystickBindings[button].first < 0.0f) || (axis < 0.0f && joystickBindings[button].first > 0.0f))
						joystickInput = 0.0f;
					else
						joystickInput = std::fabs(axis);
				}
			}
		}
	}
	
	return (keyboardInput > joystickInput ? keyboardInput : joystickInput);
}

KeyState ControllerUtil::GetButtonState(ControllerButton button)
{
	KeyState keyboardInput = KEYSTATE_RELEASED;
	if(keyboardBindings[button].second < 0)
		keyboardInput = KEYSTATE_RELEASED;
	else if(keyboardBindings[button].first) // Is mouse input
		keyboardInput = WindowUtil::Get().GetMouseButtonState(keyboardBindings[button].second);
	else
		keyboardInput = WindowUtil::Get().GetKeyState(keyboardBindings[button].second);
	
	KeyState joystickInput = KEYSTATE_RELEASED;
	if(joystickID != -1)
	{
		auto joystickIt = WindowUtil::Get().GetJoyInfo().find(joystickID);
		if(joystickIt != WindowUtil::Get().GetJoyInfo().end())
		{
			const JoystickInfo& joystickInfo = joystickIt->second;
			if(joystickBindings[button].second < 0)
				joystickInput = KEYSTATE_RELEASED;
			if(joystickBindings[button].first == 0.0f) // Is a button
			{
				if(joystickBindings[button].second < static_cast<int32_t>(joystickInfo.buttons.size()))
					joystickInput = joystickInfo.buttons[joystickBindings[button].second];
			}
			else // Is an axis
			{
				if(joystickBindings[button].second < static_cast<int32_t>(joystickInfo.axes.size()))
				{
					float roundedAxis = std::roundf(joystickInfo.axes[joystickBindings[button].second].first);
					bool hasBeenHeld = joystickInfo.axes[joystickBindings[button].second].second;
					if(roundedAxis == joystickBindings[button].first && !hasBeenHeld)
						joystickInput = KEYSTATE_PRESSED;
					else if(roundedAxis == joystickBindings[button].first && hasBeenHeld)
						joystickInput = KEYSTATE_HELD;
					else
						joystickInput = KEYSTATE_RELEASED;
				}
			}
		}
	}
	
	return (keyboardInput > joystickInput ? keyboardInput : joystickInput);
}
