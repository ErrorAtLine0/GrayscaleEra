#ifdef ENABLE_VULKAN_RENDERER
#include "VulkanUtil.hpp"
#include "../FileUtil.hpp"
#include "../ErrorUtil.hpp"
#include <unordered_set>
#include <cstring>
#include <limits>
#include <array>
#define VMA_IMPLEMENTATION
#include "../libs/vk_mem_alloc.h"
#ifdef GSE_DEBUG
	#include <iostream>
#endif

const std::array<const char*, 1> validationLayers =
{
	"VK_LAYER_LUNARG_standard_validation"
};

const std::array<const char*, 1> deviceExtensions =
{
	VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

VulkanUtil& VulkanUtil::Get()
{
	static VulkanUtil sInstance;
	return sInstance;
}

void VulkanUtil::Init(GLFWwindow* window)
{
	this->window = window;
	CreateInstance();
	SetupDebugCallback();
	CreateSurface();
	PickPhysicalDevice();
	CreateLogicalDevice();
	CreateAllocator();
	CreateCommandPool();
	CreateCommandBuffers();
	CreateDescriptorPools();
	CreateDescriptorSetLayouts();
	CreateTextureImageSampler();
	CreateSyncObjects();
	swapChainCreated = false;
	imageIndex = 0;
	boundTexture = nullptr;
}

void VulkanUtil::SetPipelineData(const VulkanPipelineData& pipelineData)
{
	this->pipelineData = pipelineData;
	CreateVertexBuffer();
	RefreshSwapChain();
}

void VulkanUtil::SetClearColor(float red, float green, float blue, float alpha)
{
	clearColor.color.float32[0] = red;
	clearColor.color.float32[1] = green;
	clearColor.color.float32[2] = blue;
	clearColor.color.float32[3] = alpha;
}

VulkanTextureData VulkanUtil::CreateTexture(const void* rawData, uint32_t texWidth, uint32_t texHeight)
{
	if(texWidth == 0 || texHeight == 0)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::CreateTexture Image has a width or height of 0");
		return {VK_NULL_HANDLE};
	}
	auto texturePair = CreateTextureImage(rawData, texWidth, texHeight);
	VkImageView imageView = CreateImageView(texturePair.first, VK_FORMAT_R8_UNORM);
	
	VkDescriptorSet descriptorSet;
	VkDescriptorSetLayout layouts[] = {samplerDescriptorSetLayout};
	VkDescriptorSetAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = samplerDescriptorPool;
	allocInfo.descriptorSetCount = sizeof(layouts) / sizeof(layouts[0]);
	allocInfo.pSetLayouts = layouts;
	if(vkAllocateDescriptorSets(device, &allocInfo, &descriptorSet) != VK_SUCCESS)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::CreateTexture Failed to allocate descriptor set");
		return {VK_NULL_HANDLE};
	}
	VkDescriptorImageInfo imageInfo;
	imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	imageInfo.imageView = imageView;
	imageInfo.sampler = textureSampler;
	
	VkWriteDescriptorSet descriptorWrite;
	descriptorWrite.pNext = VK_NULL_HANDLE;
	descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	descriptorWrite.dstSet = descriptorSet;
	descriptorWrite.dstBinding = 0;
	descriptorWrite.dstArrayElement = 0;
	descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	descriptorWrite.descriptorCount = 1;
	descriptorWrite.pImageInfo = &imageInfo;
	vkUpdateDescriptorSets(device, 1, &descriptorWrite, 0, nullptr);
	
	return {texturePair.first, texturePair.second, imageView, descriptorSet};
}

void VulkanUtil::DeleteTexture(const VulkanTextureData& textureData)
{
	texturesToDestroy.emplace_back(textureData);
}

void VulkanUtil::DestroyAllTexturesToDestroy()
{
	for(VulkanTextureData& textureData : texturesToDestroy)
	{
		vkDeviceWaitIdle(device);
		vkDestroyImageView(device, textureData.imageView, nullptr);
		vmaDestroyImage(allocator, textureData.image, textureData.imageMemory);
		vkFreeDescriptorSets(device, samplerDescriptorPool, 1, &textureData.descriptorSet);
	}
	texturesToDestroy.clear();
}

void VulkanUtil::BindTexture(const VulkanTextureData& textureData)
{
	boundTexture = &textureData;
}

void VulkanUtil::ChangeTextureData(const VulkanTextureData& textureData, const void* rawData, int32_t xoffset, int32_t yoffset, uint32_t width, uint32_t height)
{
	VkDeviceSize imageSize = width * height;
	if(imageSize == 0)
		return;
	VkBuffer stagingBuffer;
	VmaAllocation stagingBufferMemory;
	CreateBuffer(imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU, stagingBuffer, stagingBufferMemory);
	void* data;
	vmaMapMemory(allocator, stagingBufferMemory, &data);
	std::memcpy(data, rawData, static_cast<size_t>(imageSize));
	vmaUnmapMemory(allocator, stagingBufferMemory);
	TransitionImageLayout(textureData.image, VK_FORMAT_R8_UNORM, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
	CopyBufferToImage(stagingBuffer, textureData.image, xoffset, yoffset, width, height);
	TransitionImageLayout(textureData.image, VK_FORMAT_R8_UNORM, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
	vmaDestroyBuffer(allocator, stagingBuffer, stagingBufferMemory);
}

void VulkanUtil::UpdateUniformData(const VulkanUniformData& uniformData)
{
	TempUniformData* toAdd;
	
	if(uniformBufferIndex == tempUniformData.size())
	{
		tempUniformData.emplace_back();
		toAdd = &tempUniformData.back();
		CreateBuffer(uniformData.bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU, toAdd->buffer, toAdd->deviceMemory);
	
		VkDescriptorSetLayout layouts[] = {uniformDescriptorSetLayout};
		VkDescriptorSetAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		allocInfo.descriptorPool = uniformDescriptorPool;
		allocInfo.descriptorSetCount = sizeof(layouts) / sizeof(layouts[0]);
		allocInfo.pSetLayouts = layouts;
		if(vkAllocateDescriptorSets(device, &allocInfo, &toAdd->descriptorSet) != VK_SUCCESS)
		{
			ErrorUtil::Get().SetError("Memory", "VulkanUtil::UpdateUniformData Failed to allocate descriptor set");
			return;
		}
		VkDescriptorBufferInfo bufferInfo;
		bufferInfo.buffer = toAdd->buffer;
		bufferInfo.offset = 0;
		bufferInfo.range = uniformData.bufferSize;
		
		VkWriteDescriptorSet descriptorWrite;
		descriptorWrite.pNext = VK_NULL_HANDLE;
		descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		descriptorWrite.dstSet = toAdd->descriptorSet;
		descriptorWrite.dstBinding = 0;
		descriptorWrite.dstArrayElement = 0;
		descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		descriptorWrite.descriptorCount = 1;
		descriptorWrite.pBufferInfo = &bufferInfo;
		vkUpdateDescriptorSets(device, 1, &descriptorWrite, 0, nullptr);
	}
	else
		toAdd = &tempUniformData[uniformBufferIndex];
	
	void* data;
	vmaMapMemory(allocator, toAdd->deviceMemory, &data);
	std::memcpy(data, uniformData.data, uniformData.bufferSize);
	vmaUnmapMemory(allocator, toAdd->deviceMemory);

	uniformBufferIndex++;
}

void VulkanUtil::RefreshSwapChain()
{
	vkDeviceWaitIdle(device);
	if(swapChainCreated)
		CleanupSwapChain();
	CreateSwapChain();
	CreateImageViews();
	CreateRenderPass();
	CreateGraphicsPipeline();
	CreateFramebuffers();
	swapChainCreated = true;
}

void VulkanUtil::CreateInstance()
{
	VkApplicationInfo appInfo = {};
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pApplicationName = "Grayscale Era Game Library";
	appInfo.apiVersion = VK_API_VERSION_1_0;
	VkInstanceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pApplicationInfo = &appInfo;

	auto requiredExtensions = GetRequiredExtensions();
	createInfo.enabledExtensionCount = static_cast<uint32_t>(requiredExtensions.size());
	createInfo.ppEnabledExtensionNames = requiredExtensions.data();
	
#ifdef GSE_DEBUG
	createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
	createInfo.ppEnabledLayerNames = validationLayers.data();
#else
	createInfo.enabledLayerCount = 0;
#endif
	if(vkCreateInstance(&createInfo, nullptr, &instance) != VK_SUCCESS)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::InitInstance Failed to create instance");
		return;
	}
}

void VulkanUtil::SetupDebugCallback()
{
#ifdef GSE_DEBUG
	VkDebugReportCallbackCreateInfoEXT createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
	createInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;
	createInfo.pfnCallback = DebugCallback;
	if(CreateDebugReportCallbackEXT(instance, &createInfo, nullptr, &callback) != VK_SUCCESS)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::SetupDebugCallback Failed to setup debug callback");
		return;
	}
#endif
}

void VulkanUtil::PickPhysicalDevice()
{
	physicalDevice = VK_NULL_HANDLE;
	uint32_t deviceCount = 0;
	vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);
	if(deviceCount == 0)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::PickPhysicalDevice Failed to find GPU with Vulkan support");
		return;
	}
	std::vector<VkPhysicalDevice> devices(deviceCount);
	vkEnumeratePhysicalDevices(instance, &deviceCount, devices.data());
	for(const VkPhysicalDevice &device : devices)
		if(IsDeviceSuitable(device))
		{
			physicalDevice = device;
			break;
		}
	if(physicalDevice == VK_NULL_HANDLE)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::PickPhysicalDevice Failed to find a suitable GPU");
		return;
	}
}

bool VulkanUtil::IsDeviceSuitable(VkPhysicalDevice device)
{
	VulkanUtil::QueueFamilyIndices indices = FindQueueFamilies(device);
	bool extensionsSupported = CheckDeviceExtensionSupport(device);
	bool swapChainAdequate = false;
	if(extensionsSupported)
	{
		VulkanUtil::SwapChainSupportDetails swapChainSupport = QuerySwapChainSupport(device);
		swapChainAdequate = !swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty();
	}
	return indices.isComplete() && extensionsSupported && swapChainAdequate;
}

VulkanUtil::QueueFamilyIndices VulkanUtil::FindQueueFamilies(VkPhysicalDevice device)
{
	VulkanUtil::QueueFamilyIndices indices;
	uint32_t queueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);
	std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());
	int i = 0;
	for(const VkQueueFamilyProperties& queueFamily : queueFamilies)
	{
		if(queueFamily.queueCount > 0 && queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT)
			indices.graphicsFamily = i;
		VkBool32 presentSupport = false;
		vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentSupport);
		if(queueFamily.queueCount > 0 && presentSupport)
			indices.presentFamily = i;
		if(indices.isComplete())
			break;
		i++;
	}
	return indices;
}

bool VulkanUtil::CheckDeviceExtensionSupport(VkPhysicalDevice device)
{
	uint32_t extensionCount = 0;
	vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);
	std::vector<VkExtensionProperties> availableExtensions(extensionCount);
	vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data());
	std::unordered_set<UTF8String> requiredExtensions(deviceExtensions.begin(), deviceExtensions.end());
	for(const VkExtensionProperties& extension : availableExtensions)
		requiredExtensions.erase(extension.extensionName);
	return requiredExtensions.empty();
}

VulkanUtil::SwapChainSupportDetails VulkanUtil::QuerySwapChainSupport(VkPhysicalDevice device)
{
	VulkanUtil::SwapChainSupportDetails details;
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details.capabilities);
	
	uint32_t formatCount = 0;
	vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nullptr);
	if(formatCount != 0)
	{
		details.formats.resize(formatCount);
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, details.formats.data());
	}
	
	uint32_t presentModeCount = 0;
	vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, nullptr);
	if(presentModeCount != 0)
	{
		details.presentModes.resize(presentModeCount);
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, details.presentModes.data());
	}
	
	return details;
}

void VulkanUtil::CreateLogicalDevice()
{
	VulkanUtil::QueueFamilyIndices indices = FindQueueFamilies(physicalDevice);
	std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
	std::unordered_set<int32_t> uniqueQueueFamilies = {indices.graphicsFamily, indices.presentFamily};
	float queuePriority = 1.0f;
	for(int32_t queueFamily : uniqueQueueFamilies)
	{
		VkDeviceQueueCreateInfo queueCreateInfo = {};
		queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo.queueFamilyIndex = queueFamily;
		queueCreateInfo.queueCount = 1;
		queueCreateInfo.pQueuePriorities = &queuePriority;
		queueCreateInfos.emplace_back(queueCreateInfo);
	}

	VkPhysicalDeviceFeatures deviceFeatures = {};

	VkDeviceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
	createInfo.pQueueCreateInfos = queueCreateInfos.data();
	createInfo.pEnabledFeatures = &deviceFeatures;
	createInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());
	createInfo.ppEnabledExtensionNames = deviceExtensions.data();
#ifdef GSE_DEBUG
	createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
	createInfo.ppEnabledLayerNames = validationLayers.data();
#else
	createInfo.enabledLayerCount = 0;
#endif
	if(vkCreateDevice(physicalDevice, &createInfo, nullptr, &device) != VK_SUCCESS)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::CreateLogicalDevice Failed to create logical device");
		return;
	}
	
	vkGetDeviceQueue(device, indices.graphicsFamily, 0, &graphicsQueue);
	vkGetDeviceQueue(device, indices.presentFamily, 0, &presentQueue);
}

void VulkanUtil::CreateSurface()
{
	if(glfwCreateWindowSurface(instance, window, nullptr, &surface) != VK_SUCCESS)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::CreateSurface Failed to create window surface");
		return;
	}
}

void VulkanUtil::CreateAllocator()
{
	VmaAllocatorCreateInfo allocatorInfo = {};
	allocatorInfo.physicalDevice = physicalDevice;
	allocatorInfo.device = device;
	vmaCreateAllocator(&allocatorInfo, &allocator);
}

void VulkanUtil::CreateSwapChain()
{
	VulkanUtil::SwapChainSupportDetails swapChainSupport = QuerySwapChainSupport(physicalDevice);
	VkSurfaceFormatKHR surfaceFormat = ChooseSwapSurfaceFormat(swapChainSupport.formats);
	VkPresentModeKHR presentMode = ChooseSwapPresentMode(swapChainSupport.presentModes);
	VkExtent2D extent = ChooseSwapExtent(swapChainSupport.capabilities);
	uint32_t imageCount = swapChainSupport.capabilities.minImageCount + 1;
	if(swapChainSupport.capabilities.maxImageCount > 0 && imageCount > swapChainSupport.capabilities.maxImageCount)
		imageCount = swapChainSupport.capabilities.maxImageCount;
	VkSwapchainCreateInfoKHR createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	createInfo.surface = surface;
	createInfo.minImageCount = imageCount;
	createInfo.imageFormat = surfaceFormat.format;
	createInfo.imageColorSpace = surfaceFormat.colorSpace;
	createInfo.imageExtent = extent;
	createInfo.imageArrayLayers = 1;
	createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	
	VulkanUtil::QueueFamilyIndices indices = FindQueueFamilies(physicalDevice);
	
	if(indices.graphicsFamily != indices.presentFamily)
	{
		uint32_t queueFamilyIndices[] = {(uint32_t) indices.graphicsFamily, (uint32_t) indices.presentFamily};
		createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		createInfo.queueFamilyIndexCount = 2;
		createInfo.pQueueFamilyIndices = queueFamilyIndices;
	}
	else
	{
		createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
		createInfo.queueFamilyIndexCount = 0;
		createInfo.pQueueFamilyIndices = nullptr;
	}
	
	createInfo.preTransform = swapChainSupport.capabilities.currentTransform;
	createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	createInfo.presentMode = presentMode;
	createInfo.clipped = VK_TRUE;
	createInfo.oldSwapchain = VK_NULL_HANDLE;
	if(vkCreateSwapchainKHR(device, &createInfo, nullptr, &swapChain) != VK_SUCCESS)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::CreateSwapChain Failed to create swap chain");
		return;
	}
	
	vkGetSwapchainImagesKHR(device, swapChain, &imageCount, nullptr);
	swapChainImages.resize(imageCount);
	vkGetSwapchainImagesKHR(device, swapChain, &imageCount, swapChainImages.data());
	swapChainImageFormat = surfaceFormat.format;
	swapChainExtent = extent;
}

VkSurfaceFormatKHR VulkanUtil::ChooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats)
{
	if(availableFormats.size() == 1 && availableFormats[0].format == VK_FORMAT_UNDEFINED)
		return {VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR};
	for(const VkSurfaceFormatKHR& availableFormat : availableFormats)
		if(availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
			return availableFormat;
	return availableFormats[0];
}

VkPresentModeKHR VulkanUtil::ChooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes)
{
	VkPresentModeKHR bestMode = VK_PRESENT_MODE_FIFO_KHR;
	for(const VkPresentModeKHR& availablePresentMode : availablePresentModes)
		if(availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR)
			return availablePresentMode;
		else if(availablePresentMode == VK_PRESENT_MODE_IMMEDIATE_KHR)
			bestMode = availablePresentMode;
	return bestMode;
}

VkExtent2D VulkanUtil::ChooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities)
{
	if(capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max())
		return capabilities.currentExtent;
	else
	{
		int width, height;
		glfwGetFramebufferSize(window, &width, &height);
		VkExtent2D actualExtent = {static_cast<uint32_t>(width), static_cast<uint32_t>(height)};
		actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
		actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));
		return actualExtent;
	}
}

void VulkanUtil::CreateImageViews()
{
	swapChainImageViews.resize(swapChainImages.size());
	for(uint32_t i = 0; i < swapChainImages.size(); i++)
		swapChainImageViews[i] = CreateImageView(swapChainImages[i], swapChainImageFormat);
}

VkImageView VulkanUtil::CreateImageView(VkImage image, VkFormat format)
{
	VkImageViewCreateInfo viewInfo = {};
	viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	viewInfo.image = image;
	viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
	viewInfo.format = format;
	viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	viewInfo.subresourceRange.baseMipLevel = 0;
	viewInfo.subresourceRange.levelCount = 1;
	viewInfo.subresourceRange.baseArrayLayer = 0;
	viewInfo.subresourceRange.layerCount = 1;
	VkImageView imageView;
	if(vkCreateImageView(device, &viewInfo, nullptr, &imageView) != VK_SUCCESS)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::CreateImageView Failed to create texture image view");
		return VK_NULL_HANDLE;
	}
	return imageView;
}

void VulkanUtil::CreateRenderPass()
{
	VkAttachmentDescription colorAttachment = {};
	colorAttachment.format = swapChainImageFormat;
	colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
	colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
	
	VkAttachmentReference colorAttachmentRef = {};
	colorAttachmentRef.attachment = 0;
	colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
	
	VkSubpassDescription subpass = {};
	subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpass.colorAttachmentCount = 1;
	subpass.pColorAttachments = &colorAttachmentRef;
	
	VkRenderPassCreateInfo renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassInfo.attachmentCount = 1;
	renderPassInfo.pAttachments = &colorAttachment;
	renderPassInfo.subpassCount = 1;
	renderPassInfo.pSubpasses = &subpass;
	
	VkSubpassDependency dependency = {};
	dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
	dependency.dstSubpass = 0;
	dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.srcAccessMask = 0;
	dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	renderPassInfo.dependencyCount = 1;
	renderPassInfo.pDependencies = &dependency;
	
	if(vkCreateRenderPass(device, &renderPassInfo, nullptr, &renderPass) != VK_SUCCESS)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::CreateRenderPass Failed to create render pass");
		return;
	}
}

void VulkanUtil::CreateDescriptorSetLayouts()
{
	VkDescriptorSetLayoutBinding uboLayoutBinding = {};
	uboLayoutBinding.binding = 0;
	uboLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	uboLayoutBinding.descriptorCount = 1;
	uboLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT;
	
	VkDescriptorSetLayoutBinding samplerLayoutBinding = {};
	samplerLayoutBinding.binding = 0;
	samplerLayoutBinding.descriptorCount = 1;
	samplerLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	samplerLayoutBinding.pImmutableSamplers = nullptr;
	samplerLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

	VkDescriptorSetLayoutCreateInfo uboLayoutInfo = {};
	uboLayoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	uboLayoutInfo.bindingCount = 1;
	uboLayoutInfo.pBindings = &uboLayoutBinding;
	if(vkCreateDescriptorSetLayout(device, &uboLayoutInfo, nullptr, &uniformDescriptorSetLayout) != VK_SUCCESS)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::CreateDescriptorSetLayouts Failed to create uniform descriptor set layout");
		return;
	}
	VkDescriptorSetLayoutCreateInfo samplerLayoutInfo = {};
	samplerLayoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	samplerLayoutInfo.bindingCount = 1;
	samplerLayoutInfo.pBindings = &samplerLayoutBinding;
	if(vkCreateDescriptorSetLayout(device, &samplerLayoutInfo, nullptr, &samplerDescriptorSetLayout) != VK_SUCCESS)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::CreateDescriptorSetLayouts Failed to create sampler descriptor set layout");
		return;
	}
}

void VulkanUtil::CreateGraphicsPipeline()
{
	std::shared_ptr<InMemBitStream> vertStream = FileUtil::Get().ReadFromFile(pipelineData.vertexShaderPath);
	std::shared_ptr<InMemBitStream> fragStream =  FileUtil::Get().ReadFromFile(pipelineData.fragmentShaderPath);
	std::vector<unsigned char> vertCode(vertStream->GetRemainingBytes());
	std::vector<unsigned char> fragCode(fragStream->GetRemainingBytes());
	vertStream->ReadBits(vertCode.data(), vertCode.size() * 8);
	fragStream->ReadBits(fragCode.data(), fragCode.size() * 8);
	
	VkShaderModule vertShaderModule = CreateShaderModule(vertCode.data(), vertCode.size());
	VkShaderModule fragShaderModule = CreateShaderModule(fragCode.data(), fragCode.size());
	
	VkPipelineShaderStageCreateInfo vertShaderStageInfo = {};
	vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
	vertShaderStageInfo.module = vertShaderModule;
	vertShaderStageInfo.pName = "main";
	VkPipelineShaderStageCreateInfo fragShaderStageInfo = {};
	fragShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	fragShaderStageInfo.module = fragShaderModule;
	fragShaderStageInfo.pName = "main";
	VkPipelineShaderStageCreateInfo shaderStages[] = {vertShaderStageInfo, fragShaderStageInfo};
	
	VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
	vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertexInputInfo.vertexBindingDescriptionCount = 1;
	vertexInputInfo.pVertexBindingDescriptions = &pipelineData.vertexData.bindingDescription;
	vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(pipelineData.vertexData.attributeDescriptions.size());
	vertexInputInfo.pVertexAttributeDescriptions = pipelineData.vertexData.attributeDescriptions.data();
	
	VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
	inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
	inputAssembly.primitiveRestartEnable = VK_FALSE;
	
	VkViewport viewport = {};
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = static_cast<float>(swapChainExtent.width);
	viewport.height = static_cast<float>(swapChainExtent.height);
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;
	
	VkRect2D scissor = {};
	scissor.offset = {0, 0};
	scissor.extent = swapChainExtent;
	
	VkPipelineViewportStateCreateInfo viewportState = {};
	viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportState.viewportCount = 1;
	viewportState.pViewports = &viewport;
	viewportState.scissorCount = 1;
	viewportState.pScissors = &scissor;
	
	VkPipelineRasterizationStateCreateInfo rasterizer = {};
	rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizer.depthClampEnable = VK_FALSE;
	rasterizer.rasterizerDiscardEnable = VK_FALSE;
	rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
	rasterizer.lineWidth = 1.0f;
	rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
	rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
	rasterizer.depthBiasEnable = VK_FALSE;
	
	VkPipelineMultisampleStateCreateInfo multisampling = {};
	multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisampling.sampleShadingEnable = VK_FALSE;
	multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	
	VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
	colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT;
	colorBlendAttachment.blendEnable = VK_TRUE;
	colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
	colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
	colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
	colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
	colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;
	
	VkPipelineColorBlendStateCreateInfo colorBlending = {};
	colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlending.logicOpEnable = VK_FALSE;
	colorBlending.attachmentCount = 1;
	colorBlending.pAttachments = &colorBlendAttachment;
	
	VkDynamicState dynamicStates[] = {
		VK_DYNAMIC_STATE_VIEWPORT
	};
	
	VkPipelineDynamicStateCreateInfo dynamicState = {};
	dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	dynamicState.dynamicStateCount = 1;
	dynamicState.pDynamicStates = dynamicStates;
	
	VkDescriptorSetLayout layouts[] = {uniformDescriptorSetLayout, samplerDescriptorSetLayout};
	VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
	pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutInfo.setLayoutCount = sizeof(layouts) / sizeof(layouts[0]);
	pipelineLayoutInfo.pSetLayouts = layouts;
	
	if(vkCreatePipelineLayout(device, &pipelineLayoutInfo, nullptr, &pipelineLayout) != VK_SUCCESS)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::CreateGraphicsPipeline Failed to create pipeline layout");
		return;
	}
	
	VkGraphicsPipelineCreateInfo pipelineInfo = {};
	pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pipelineInfo.stageCount = 2;
	pipelineInfo.pStages = shaderStages;
	pipelineInfo.pVertexInputState = &vertexInputInfo;
	pipelineInfo.pInputAssemblyState = &inputAssembly;
	pipelineInfo.pViewportState = &viewportState;
	pipelineInfo.pRasterizationState = &rasterizer;
	pipelineInfo.pMultisampleState = &multisampling;
	pipelineInfo.pColorBlendState = &colorBlending;
	pipelineInfo.layout = pipelineLayout;
	pipelineInfo.renderPass = renderPass;
	pipelineInfo.subpass = 0;
	
	if(vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &graphicsPipeline) != VK_SUCCESS)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::CreateGraphicsPipeline Failed to create graphics pipeline");
		return;
	}
	
	vkDestroyShaderModule(device, vertShaderModule, nullptr);
	vkDestroyShaderModule(device, fragShaderModule, nullptr);
}

void VulkanUtil::CreateFramebuffers()
{
	swapChainFramebuffers.resize(swapChainImageViews.size());
	for(uint32_t i = 0; i < swapChainImageViews.size(); i++)
	{
		VkImageView attachments[] =
		{
			swapChainImageViews[i]
		};
		VkFramebufferCreateInfo framebufferInfo = {};
		framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		framebufferInfo.renderPass = renderPass;
		framebufferInfo.attachmentCount = 1;
		framebufferInfo.pAttachments = attachments;
		framebufferInfo.width = swapChainExtent.width;
		framebufferInfo.height = swapChainExtent.height;
		framebufferInfo.layers = 1;
		if(vkCreateFramebuffer(device, &framebufferInfo, nullptr, &swapChainFramebuffers[i]) != VK_SUCCESS)
		{
			ErrorUtil::Get().SetError("Memory", "VulkanUtil::CreateFramebuffers Failed to create framebuffer");
			return;
		}
	}
}

void VulkanUtil::CreateCommandPool()
{
	VulkanUtil::QueueFamilyIndices queueFamilyIndices = FindQueueFamilies(physicalDevice);
	VkCommandPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily;
	poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
	if(vkCreateCommandPool(device, &poolInfo, nullptr, &commandPool) != VK_SUCCESS)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::CreateCommandPool Failed to create command pool");
		return;
	}
}

std::pair<VkImage, VmaAllocation> VulkanUtil::CreateTextureImage(const void* imageRawData, uint32_t texWidth, uint32_t texHeight)
{
	VkImage textureImage;
	VmaAllocation textureImageMemory;
	
	VkDeviceSize imageSize = texWidth * texHeight;
	VkBuffer stagingBuffer;
	VmaAllocation stagingBufferMemory;
	CreateBuffer(imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU, stagingBuffer, stagingBufferMemory);
	void* data;
	vmaMapMemory(allocator, stagingBufferMemory, &data);
	std::memcpy(data, imageRawData, static_cast<size_t>(imageSize));
	vmaUnmapMemory(allocator, stagingBufferMemory);
	CreateImage(texWidth, texHeight, VK_FORMAT_R8_UNORM, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VMA_MEMORY_USAGE_GPU_ONLY, textureImage, textureImageMemory);
	TransitionImageLayout(textureImage, VK_FORMAT_R8_UNORM, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
	CopyBufferToImage(stagingBuffer, textureImage, 0, 0, texWidth, texHeight);
	TransitionImageLayout(textureImage, VK_FORMAT_R8_UNORM, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
	vmaDestroyBuffer(allocator, stagingBuffer, stagingBufferMemory);
	return std::pair<VkImage, VmaAllocation>(textureImage, textureImageMemory);
}

void VulkanUtil::TransitionImageLayout(VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout)
{
	VkCommandBuffer commandBuffer = BeginSingleTimeCommands();
	VkImageMemoryBarrier barrier = {};
	barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	barrier.oldLayout = oldLayout;
	barrier.newLayout = newLayout;
	barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.image = image;
	barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	barrier.subresourceRange.baseMipLevel = 0;
	barrier.subresourceRange.levelCount = 1;
	barrier.subresourceRange.baseArrayLayer = 0;
	barrier.subresourceRange.layerCount = 1;
	barrier.srcAccessMask = 0;
	barrier.dstAccessMask = 0;
	
	VkPipelineStageFlags sourceStage;
	VkPipelineStageFlags destinationStage;
	if(oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
	{
		barrier.srcAccessMask = 0;
		barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
	}
	else if(oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
	{
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
		sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
	}
	else if(oldLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
	{
		barrier.srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
		barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		sourceStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
		destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
	}
	else
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::TransitionImageLayout Unsupported layout transition");
		return;
	}
		
	vkCmdPipelineBarrier(commandBuffer, sourceStage, destinationStage, 0, 0, nullptr, 0, nullptr, 1, &barrier);
	EndSingleTimeCommands(commandBuffer);
}

void VulkanUtil::CreateTextureImageSampler()
{
	VkSamplerCreateInfo samplerInfo = {};
	samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerInfo.magFilter = VK_FILTER_NEAREST;
	samplerInfo.minFilter = VK_FILTER_NEAREST;
	samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerInfo.anisotropyEnable = VK_FALSE;
	samplerInfo.maxAnisotropy = 1;
	samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
	samplerInfo.unnormalizedCoordinates = VK_FALSE;
	samplerInfo.compareEnable = VK_FALSE;
	samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
	samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST;
	samplerInfo.mipLodBias = 0.0f;
	samplerInfo.minLod = 0.0f;
	samplerInfo.maxLod = 0.0f;
	if(vkCreateSampler(device, &samplerInfo, nullptr, &textureSampler) != VK_SUCCESS)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::CreateTextureSampler Failed to create texture sampler");
		return;
	}
}

void VulkanUtil::CreateDescriptorPools()
{
	VkDescriptorPoolSize uniformPoolSize;
	uniformPoolSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	uniformPoolSize.descriptorCount = 5000;
	
	VkDescriptorPoolSize samplerPoolSize;
	samplerPoolSize.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	samplerPoolSize.descriptorCount = 5000;
	
	VkDescriptorPoolCreateInfo uniformPoolInfo = {};
	uniformPoolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	uniformPoolInfo.poolSizeCount = 1;
	uniformPoolInfo.pPoolSizes = &uniformPoolSize;
	uniformPoolInfo.maxSets = 5000;
	if(vkCreateDescriptorPool(device, &uniformPoolInfo, nullptr, &uniformDescriptorPool) != VK_SUCCESS)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::CreateDescriptorPools Failed to create uniform descriptor pool");
		return;
	}
	
	VkDescriptorPoolCreateInfo samplerPoolInfo = {};
	samplerPoolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	samplerPoolInfo.poolSizeCount = 1;
	samplerPoolInfo.pPoolSizes = &samplerPoolSize;
	samplerPoolInfo.maxSets = 5000;
	samplerPoolInfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
	if(vkCreateDescriptorPool(device, &samplerPoolInfo, nullptr, &samplerDescriptorPool) != VK_SUCCESS)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::CreateDescriptorPools Failed to create sampler descriptor pool");
		return;
	}
}

#ifdef GSE_DEBUG
VKAPI_ATTR VkBool32 VKAPI_CALL VulkanUtil::DebugCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType, uint64_t obj, size_t location, int32_t code, const char* layerPrefix, const char* msg, void* userData)
{
	std::cerr << "VulkanUtil::DebugCallback " << msg << std::endl;
	return VK_FALSE;
}

VkResult VulkanUtil::CreateDebugReportCallbackEXT(VkInstance instance, const VkDebugReportCallbackCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugReportCallbackEXT* pCallback)
{
	auto func = (PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT");
	if(func != nullptr)
		return func(instance, pCreateInfo, pAllocator, pCallback);
	else
		return VK_ERROR_EXTENSION_NOT_PRESENT;
}

void VulkanUtil::DestroyDebugReportCallbackEXT(VkInstance instance, VkDebugReportCallbackEXT callback, VkAllocationCallbacks* pAllocator)
{
	auto func = (PFN_vkDestroyDebugReportCallbackEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT");
	if(func != nullptr)
		func(instance, callback, pAllocator);
}
#endif

std::vector<const char*> VulkanUtil::GetRequiredExtensions()
{
	uint32_t glfwExtensionCount;
	const char** glfwExtensionNames;
	glfwExtensionNames = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);
	std::vector<const char*> extensions(glfwExtensionNames, glfwExtensionNames + glfwExtensionCount);
#ifdef GSE_DEBUG
	extensions.emplace_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
#endif
	return extensions;
}

void VulkanUtil::CreateImage(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VmaMemoryUsage properties, VkImage& image, VmaAllocation& allocation)
{
	VkImageCreateInfo imageInfo = {};
	imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageInfo.imageType = VK_IMAGE_TYPE_2D;
	imageInfo.extent.width = static_cast<uint32_t>(width);
	imageInfo.extent.height = static_cast<uint32_t>(height);
	imageInfo.extent.depth = 1;
	imageInfo.mipLevels = 1;
	imageInfo.arrayLayers = 1;
	imageInfo.format = format;
	imageInfo.tiling = tiling;
	imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	imageInfo.usage = usage;
	imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
	
	VmaAllocationCreateInfo allocInfo = {};
	allocInfo.usage = properties;
	if(vmaCreateImage(allocator, &imageInfo, &allocInfo, &image, &allocation, nullptr) != VK_SUCCESS)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::CreateImage Failed to allocate image memory");
		return;
	}
}

void VulkanUtil::CopyBufferToImage(VkBuffer buffer, VkImage image, int32_t xoffset, int32_t yoffset,  uint32_t width, uint32_t height)
{
	VkCommandBuffer commandBuffer = BeginSingleTimeCommands();
	VkBufferImageCopy region = {};
	region.bufferOffset = 0;
	region.bufferRowLength = 0;
	region.bufferImageHeight = 0;
	region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	region.imageSubresource.mipLevel = 0;
	region.imageSubresource.baseArrayLayer = 0;
	region.imageSubresource.layerCount = 1;
	region.imageOffset = {xoffset, yoffset, 0};
	region.imageExtent = {width, height, 1};
	vkCmdCopyBufferToImage(commandBuffer, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);
	EndSingleTimeCommands(commandBuffer);
}

void VulkanUtil::CreateBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VmaMemoryUsage properties, VkBuffer& buffer, VmaAllocation& allocation)
{
	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = size;
	bufferInfo.usage = usage;
	bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	VmaAllocationCreateInfo allocInfo = {};
	allocInfo.usage = properties;
	if(vmaCreateBuffer(allocator, &bufferInfo, &allocInfo, &buffer, &allocation, nullptr) != VK_SUCCESS)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::CreateBuffer Failed to allocate buffer memory");
		return;
	}
}

void VulkanUtil::CopyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size)
{
	VkCommandBuffer commandBuffer = BeginSingleTimeCommands();
	VkBufferCopy copyRegion = {};
	copyRegion.size = size;
	vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);
	EndSingleTimeCommands(commandBuffer);
}

VkCommandBuffer VulkanUtil::BeginSingleTimeCommands()
{
	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandPool = commandPool;
	allocInfo.commandBufferCount = 1;
	VkCommandBuffer commandBuffer;
	vkAllocateCommandBuffers(device, &allocInfo, &commandBuffer);
	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
	vkBeginCommandBuffer(commandBuffer, &beginInfo);
	return commandBuffer;
}

void VulkanUtil::EndSingleTimeCommands(VkCommandBuffer commandBuffer)
{
	vkEndCommandBuffer(commandBuffer);
	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffer;
	vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
	vkQueueWaitIdle(graphicsQueue);
	vkFreeCommandBuffers(device, commandPool, 1, &commandBuffer);
}

void VulkanUtil::CreateVertexBuffer()
{
	VkBuffer stagingBuffer;
	VmaAllocation stagingBufferMemory;
	CreateBuffer(pipelineData.vertexData.bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU, stagingBuffer, stagingBufferMemory);
	void* data;
	vmaMapMemory(allocator, stagingBufferMemory, &data);
	std::memcpy(data, pipelineData.vertexData.data, static_cast<size_t>(pipelineData.vertexData.bufferSize));
	vmaUnmapMemory(allocator, stagingBufferMemory);
	CreateBuffer(pipelineData.vertexData.bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VMA_MEMORY_USAGE_GPU_ONLY, vertexBuffer, vertexBufferMemory);
	CopyBuffer(stagingBuffer, vertexBuffer, pipelineData.vertexData.bufferSize);
	vmaDestroyBuffer(allocator, stagingBuffer, stagingBufferMemory);
}

void VulkanUtil::CreateCommandBuffers()
{
	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool = commandPool;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount = 1;
	if(vkAllocateCommandBuffers(device, &allocInfo, &commandBuffer) != VK_SUCCESS)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::CreateCommandBuffers Failed to allocate draw command buffer");
		return;
	}
}

void VulkanUtil::CreateSyncObjects()
{
	VkSemaphoreCreateInfo semaphoreInfo = {};
	semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	if(vkCreateSemaphore(device, &semaphoreInfo, nullptr, &imageAvailableSemaphore) != VK_SUCCESS || vkCreateSemaphore(device, &semaphoreInfo, nullptr, &renderFinishedSemaphore) != VK_SUCCESS)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::CreateSyncObjects Failed to create sync objects for frame");
		return;
	}
}

VkShaderModule VulkanUtil::CreateShaderModule(const unsigned char* shaderCode, uint32_t shaderSize)
{
	VkShaderModuleCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	createInfo.codeSize = shaderSize;
	createInfo.pCode = reinterpret_cast<const uint32_t*>(shaderCode);
	VkShaderModule shaderModule;
	if(vkCreateShaderModule(device, &createInfo, nullptr, &shaderModule) != VK_SUCCESS)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::CreateShaderModule Failed to create shader module");
		return VK_NULL_HANDLE;
	}
	return shaderModule;
}

void VulkanUtil::StartImage()
{
	for(;;)
	{
		VkResult result = vkAcquireNextImageKHR(device, swapChain, std::numeric_limits<uint64_t>::max(), imageAvailableSemaphore, VK_NULL_HANDLE, &imageIndex);
		if(result == VK_ERROR_OUT_OF_DATE_KHR)
		{
			RefreshSwapChain();
			continue;
		}
		else if(result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR)
		{
			ErrorUtil::Get().SetError("Memory", "VulkanUtil::StartImage Failed to acquire swap chain image");
			return;
		}
		break;
	}
	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
	if(vkBeginCommandBuffer(commandBuffer, &beginInfo) != VK_SUCCESS)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::StartImage Failed to begin recording command buffer");
		return;
	}
	VkRenderPassBeginInfo renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassInfo.renderPass = renderPass;
	renderPassInfo.framebuffer = swapChainFramebuffers[imageIndex];
	renderPassInfo.renderArea.offset = {0, 0};
	renderPassInfo.renderArea.extent = swapChainExtent;
	renderPassInfo.clearValueCount = 1;
	renderPassInfo.pClearValues = &clearColor;
	vkCmdBeginRenderPass(commandBuffer, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
	vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);
	VkBuffer vertexBuffers[] = {vertexBuffer};
	VkDeviceSize offsets[] = {0};
	vkCmdBindVertexBuffers(commandBuffer, 0, 1, vertexBuffers, offsets);
}

void VulkanUtil::EndImage()
{
	vkCmdEndRenderPass(commandBuffer);
	if(vkEndCommandBuffer(commandBuffer) != VK_SUCCESS)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::EndImage Failed to record command buffer");
		return;
	}
	
	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
	submitInfo.waitSemaphoreCount = 1;
	submitInfo.pWaitSemaphores = &imageAvailableSemaphore;
	submitInfo.pWaitDstStageMask = waitStages;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffer;
	submitInfo.signalSemaphoreCount = 1;
	submitInfo.pSignalSemaphores = &renderFinishedSemaphore;
	if(vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE) != VK_SUCCESS)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::EndImage Failed to submit command buffer");
		return;
	}
	
	VkPresentInfoKHR presentInfo = {};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = &renderFinishedSemaphore;
	VkSwapchainKHR swapChains[] = {swapChain};
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = swapChains;
	presentInfo.pImageIndices = &imageIndex;
	VkResult result = vkQueuePresentKHR(presentQueue, &presentInfo);
	if(result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR)
		RefreshSwapChain();
	else if(result != VK_SUCCESS)
	{
		ErrorUtil::Get().SetError("Memory", "VulkanUtil::EndImage Failed to present swap chain image");
		return;
	}
		
	// Use fences later
	vkQueueWaitIdle(presentQueue);
	uniformBufferIndex = 0;
	DestroyAllTexturesToDestroy();
}

void VulkanUtil::Draw()
{
	VkDescriptorSet descriptorSets[] = {tempUniformData[uniformBufferIndex - 1].descriptorSet, boundTexture->descriptorSet};
	vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, sizeof(descriptorSets) / sizeof(descriptorSets[0]), descriptorSets, 0, nullptr);
	vkCmdDraw(commandBuffer, pipelineData.vertexData.vertexCount, 1, 0, 0);
}

void VulkanUtil::CleanupSwapChain()
{
	if(swapChainCreated)
	{
		for(VkFramebuffer framebuffer : swapChainFramebuffers)
			vkDestroyFramebuffer(device, framebuffer, nullptr);
		vkDestroyPipeline(device, graphicsPipeline, nullptr);
		vkDestroyPipelineLayout(device, pipelineLayout, nullptr);
		vkDestroyRenderPass(device, renderPass, nullptr);
		for(VkImageView imageView : swapChainImageViews)
			vkDestroyImageView(device, imageView, nullptr);
		vkDestroySwapchainKHR(device, swapChain, nullptr);
		swapChainCreated = false;
	}
}

void VulkanUtil::Cleanup()
{
	vkDeviceWaitIdle(device);
	vkDestroySemaphore(device, imageAvailableSemaphore, nullptr);
	vkDestroySemaphore(device, renderFinishedSemaphore, nullptr);
	vkFreeCommandBuffers(device, commandPool, 1, &commandBuffer);
	CleanupSwapChain();
	DestroyAllTexturesToDestroy();
	vkDestroySampler(device, textureSampler, nullptr);
	for(TempUniformData& tempUniform : tempUniformData)
		vmaDestroyBuffer(allocator, tempUniform.buffer, tempUniform.deviceMemory);
	vkDestroyDescriptorPool(device, uniformDescriptorPool, nullptr);
	vkDestroyDescriptorSetLayout(device, uniformDescriptorSetLayout, nullptr);
	vkDestroyDescriptorPool(device, samplerDescriptorPool, nullptr);
	vkDestroyDescriptorSetLayout(device, samplerDescriptorSetLayout, nullptr);
	vmaDestroyBuffer(allocator, vertexBuffer, vertexBufferMemory);
	vkDestroyCommandPool(device, commandPool, nullptr);
	vmaDestroyAllocator(allocator);
	vkDestroyDevice(device, nullptr);
#ifdef GSE_DEBUG
	DestroyDebugReportCallbackEXT(instance, callback, nullptr);
#endif
	vkDestroySurfaceKHR(instance, surface, nullptr);
	vkDestroyInstance(instance, nullptr);
}

#endif
