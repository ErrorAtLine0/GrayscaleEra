#include "DrawUtil.hpp"
#include "WindowUtil.hpp"
#include "../FileUtil.hpp"
#include <cstring>
#include "../ErrorUtil.hpp"
#include <algorithm>
#include "VulkanUtil.hpp"
#define STB_TRUETYPE_IMPLEMENTATION
#include "../libs/stb_truetype.h"
#define MAX_STRINGS_IN_CACHE 40

#define VERTEX_COUNT 4
float opengl_vertices[] = {
	// Pos     TexCoords
	0.0f, 1.0f,  0.0f, 1.0f,
	0.0f, 0.0f,  0.0f, 0.0f,
	1.0f, 1.0f,  1.0f, 1.0f,
	1.0f, 0.0f,  1.0f, 0.0f
};

float vulkan_vertices[] = {
	// Pos     TexCoords
	0.0f, 1.0f,  0.0f, 0.0f,
	0.0f, 0.0f,  0.0f, 1.0f,
	1.0f, 1.0f,  1.0f, 0.0f,
	1.0f, 0.0f,  1.0f, 1.0f
};

DrawUtil::Character::Character():
data(nullptr),
size(),
bearing(),
advance(0)
{}

DrawUtil::Character::Character(const uint8_t* initData, const IVec2& initSize, const IVec2& initBearing, float initAdvance):
data(static_cast<uint8_t*>(std::malloc(initSize.x * initSize.y))),
size(initSize),
bearing(initBearing),
advance(initAdvance)
{
	std::memcpy(data, initData, size.x*size.y);
}

DrawUtil::Character::Character(const Character& character):
data(static_cast<uint8_t*>(std::malloc(character.size.x * character.size.y))),
size(character.size),
bearing(character.bearing),
advance(character.advance)
{
	std::memcpy(data, character.data, size.x*size.y);
}

void DrawUtil::Character::operator=(const Character& character)
{
	uint8_t* tempPtr = static_cast<uint8_t*>(std::realloc(data, character.size.x * character.size.y));
	if(!tempPtr)
	{
		ErrorUtil::Get().SetError("Memory", "DrawUtil::Character::operator= Failed to realloc data");
		return;
	}
	data = tempPtr;
	std::memcpy(data, character.data, character.size.x * character.size.y);
	size = character.size;
	bearing = character.bearing;
	advance = character.advance;
}

DrawUtil::Character::~Character()
{
	std::free(data);
}

DrawUtil::CacheTextTexture::CacheTextTexture(const TexturePtr& initTexture, const Vec2& initSize, float initPixelsBelowLine):
texture(initTexture),
size(initSize),
pixelsBelowLine(initPixelsBelowLine)
{}

DrawUtil::DrawSpriteCommand::DrawSpriteCommand(const TexturePtr& initTexture, const Vec2& initPosition, const Vec2& initSize, float initRotate, SpriteTransparency initTransparencyMode, float initAlpha, const BVec2& initFlip):
texture(initTexture),
position(initPosition),
size(initSize),
rotate(initRotate),
transparencyMode(initTransparencyMode),
alpha(initAlpha),
flip(initFlip)
{}

DrawUtil::DrawTextCommand::DrawTextCommand(const UTF8String& initStr, const Vec2& initPosition, float initScale, float initWhite, float initAlpha):
str(initStr),
position(initPosition),
scale(initScale),
white(initWhite),
alpha(initAlpha)
{}

DrawUtil::DrawRectCommand::DrawRectCommand(const Vec2& initPosition, const Vec2& initSize, float initWhite, float initAlpha):
position(initPosition),
size(initSize),
white(initWhite),
alpha(initAlpha)
{}

DrawUtil& DrawUtil::Get()
{
	static DrawUtil sInstance;
	return sInstance;
}

bool DrawUtil::Init(const std::vector<FontLoadInfo>& fontsToLoad, float generalFontScale, uint32_t width, uint32_t height)
{
	for(const FontLoadInfo& fontToLoad : fontsToLoad)
	{
		fontData.emplace_back();
		fontData.back().buffer = FileUtil::Get().ReadFromFile(fontToLoad.path);
		if(!stbtt_InitFont(&fontData.back().info, fontData.back().buffer->GetBufferPtr(), 0))
		{
			ErrorUtil::Get().SetError("Memory", "DrawUtil::Init Could not initialize font info");
			return false;
		}
		fontData.back().scale = stbtt_ScaleForPixelHeight(&fontData.back().info, fontToLoad.pixelHeight);
		fontData.back().startingCodePoint = fontToLoad.startingCodePoint;
		fontData.back().codePointRange = fontToLoad.codePointRange;
	}
	this->generalFontScale = generalFontScale;
	
	if(WindowUtil::Get().GetRenderer() == RENDERER_OPENGL || WindowUtil::Get().GetRenderer() == RENDERER_OPENGLES)
		orthoMat(UBO.projection, static_cast<float>(width), 0.0f, static_cast<float>(height), 0.0f);
	else if(WindowUtil::Get().GetRenderer() == RENDERER_VULKAN)
		orthoMat(UBO.projection, static_cast<float>(width), 0.0f, 0.0f, static_cast<float>(height));
	UBO.invertColors = false;
	// Get model matrix ready
	for(uint8_t i = 0; i < 16; i++)
		UBO.model[i] = 0.0f;
	UBO.model[10] = 1.0f;
	UBO.model[15] = 1.0f;
	
#if defined(ENABLE_OPENGL_RENDERER) || defined(ENABLE_OPENGLES_RENDERER)
	if(WindowUtil::Get().GetRenderer() == RENDERER_OPENGLES || WindowUtil::Get().GetRenderer() == RENDERER_OPENGL)
		OpenGLInit();
#endif
#ifdef ENABLE_VULKAN_RENDERER
	if(WindowUtil::Get().GetRenderer() == RENDERER_VULKAN)
		VulkanInit();
#endif
	return true;
}

#if defined(ENABLE_OPENGL_RENDERER) || defined(ENABLE_OPENGLES_RENDERER)
void DrawUtil::OpenGLInit()
{
	if(WindowUtil::Get().GetRenderer() == RENDERER_OPENGLES)
		shader.Init("shaders/drawES.vert", "shaders/drawES.frag");
	else if(WindowUtil::Get().GetRenderer() == RENDERER_OPENGL)
		shader.Init("shaders/draw.vert", "shaders/draw.frag");
	shader.BindAttribLoc(0, "vertex");
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(opengl_vertices), opengl_vertices, GL_STATIC_DRAW);
	
#ifdef ENABLE_OPENGL_RENDERER
	if(WindowUtil::Get().GetRenderer() == RENDERER_OPENGL)
	{
		glGenVertexArrays(1, &VAO);

		glBindVertexArray(VAO);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), reinterpret_cast<GLvoid*>(0));
		glEnableVertexAttribArray(0);
		
		glBindVertexArray(0);
	}
#endif
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	shader.Use();
	glUniformMatrix4fv(shader.GetUniLoc("projection"), 1, GL_FALSE, (float*)UBO.projection);
	glUniform1i(shader.GetUniLoc("texLocation"), 0);

	shaderModel = shader.GetUniLoc("model");
	shaderFlip = shader.GetUniLoc("flip");
	shaderAlpha = shader.GetUniLoc("alpha");
	shaderTransparencyMode = shader.GetUniLoc("transparencyMode");
	shaderInvertColors = shader.GetUniLoc("invertColors");
	shaderWhite = shader.GetUniLoc("white");
	shaderDrawType = shader.GetUniLoc("drawType");

// Do it in initialization, as there is only one texture, and one shader
	shader.Use();
	glActiveTexture(GL_TEXTURE0);
#ifdef ENABLE_OPENGLES_RENDERER
	if(WindowUtil::Get().GetRenderer() == RENDERER_OPENGLES)
	{
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), reinterpret_cast<GLvoid*>(0));
		glEnableVertexAttribArray(0);
	}
#endif
#ifdef ENABLE_OPENGL_RENDERER
	if(WindowUtil::Get().GetRenderer() == RENDERER_OPENGL)
		glBindVertexArray(VAO);
#endif
}
#endif

#ifdef ENABLE_VULKAN_RENDERER
void DrawUtil::VulkanInit()
{
	VulkanPipelineData pipelineData;
	pipelineData.vertexShaderPath = "shaders/drawVulkanVert.spv";
	pipelineData.fragmentShaderPath = "shaders/drawVulkanFrag.spv";
	pipelineData.vertexData.data = &vulkan_vertices;
	pipelineData.vertexData.bufferSize = sizeof(vulkan_vertices);
	pipelineData.vertexData.vertexCount = VERTEX_COUNT;
	pipelineData.vertexData.bindingDescription.binding = 0;
	pipelineData.vertexData.bindingDescription.stride = sizeof(vulkan_vertices) / VERTEX_COUNT;
	pipelineData.vertexData.bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
	pipelineData.vertexData.attributeDescriptions.emplace_back();
	pipelineData.vertexData.attributeDescriptions.back().binding = 0;
	pipelineData.vertexData.attributeDescriptions.back().location = 0;
	pipelineData.vertexData.attributeDescriptions.back().format = VK_FORMAT_R32G32B32A32_SFLOAT;
	pipelineData.vertexData.attributeDescriptions.back().offset = 0;
	VulkanUtil::Get().SetPipelineData(pipelineData);
	VulkanUtil::Get().StartImage();
}
#endif

void DrawUtil::Update()
{
	if(WindowUtil::Get().IsWindowResized())
		SetProjectionMatrix(WindowUtil::Get().GetWindowSize().x, WindowUtil::Get().GetWindowSize().y);
}

void DrawUtil::AddSprite(const UTF8String& name, const UTF8String& imagePath)
{
	nameToSprite.emplace(name, std::make_shared<Texture>(imagePath));
}

const TexturePtr& DrawUtil::GetSprite(const UTF8String& name)
{
	return nameToSprite[name];
}

void DrawUtil::DrawSprite(uint32_t layer, const UTF8String& name, const Vec2& position, const Vec2& size, float rotate, SpriteTransparency transparencyMode, float alpha, const BVec2& flip)
{
	if(position.x + size.x >= 0.0f && position.x <= WindowUtil::Get().GetWindowSize().x && position.y + size.y >= 0.0f && position.y <= WindowUtil::Get().GetWindowSize().y)
	{
		auto textureIt = nameToSprite.find(name);
		if(textureIt != nameToSprite.end())
			drawCommands[layer].spriteCommands.emplace_back(textureIt->second, position, size, rotate, transparencyMode, alpha, flip);
	}
}

void DrawUtil::DrawSprite(uint32_t layer, const TexturePtr& texture, const Vec2& position, const Vec2& size, float rotate, SpriteTransparency transparencyMode, float alpha, const BVec2& flip)
{
	if(position.x + size.x >= 0.0f && position.x <= WindowUtil::Get().GetWindowSize().x && position.y + size.y >= 0.0f && position.y <= WindowUtil::Get().GetWindowSize().y && alpha != 0.0f)
		drawCommands[layer].spriteCommands.emplace_back(!texture ? nullTexture : texture, position, size, rotate, transparencyMode, alpha, flip);
}

void DrawUtil::DrawRectangle(uint32_t layer, const Vec2& position, const Vec2& size, float white, float alpha)
{
	if(position.x + size.x >= 0.0f && position.x <= WindowUtil::Get().GetWindowSize().x && position.y + size.y >= 0.0f && position.y <= WindowUtil::Get().GetWindowSize().y && alpha != 0.0f)
		drawCommands[layer].rectCommands.emplace_back(position, size, white, alpha);
}

void DrawUtil::DrawText(uint32_t layer, const UTF8String& str, Vec2 position, float scale, float white, float alpha)
{
	if(alpha != 0.0f)
		drawCommands[layer].textCommands.emplace_back(str, position, scale * generalFontScale, white, alpha);
}

DrawUtil::FontData* DrawUtil::GetFontFromCodepoint(uint32_t c)
{
	for(FontData& font : fontData)
	{
		if(c >= font.startingCodePoint &&
		  (font.codePointRange == 0 || c < font.startingCodePoint + font.codePointRange))
			return &font;
	}
	return &fontData[0];
}

void DrawUtil::LoadCharacter(uint32_t c)
{
	int width, height, xoff, yoff;
	DrawUtil::FontData* font = GetFontFromCodepoint(c);
	uint8_t* bitmapChar = stbtt_GetCodepointBitmap(&font->info, font->scale, font->scale, c, &width, &height, &xoff, &yoff);
	int advance;
	stbtt_GetGlyphHMetrics(&font->info, c, &advance, 0);
	usedCharacters.emplace(std::piecewise_construct, std::forward_as_tuple(c), std::forward_as_tuple
	(bitmapChar,
	 IVec2(width, height),
	 IVec2(xoff, -yoff),
	 c == ' ' ? 512 * font->scale : advance * font->scale) // Spaces are a bit too large
	);
}

DrawUtil::CacheTextTexture* DrawUtil::GetTextTextureFromStringCache(const UTF8String& str)
{
	auto it = std::find_if(prerenderedStrings.begin(), prerenderedStrings.end(), [&str](const std::pair<UTF8String, DrawUtil::CacheTextTexture>& p){
		return p.first == str;
	});
	if(it != prerenderedStrings.end())
		return &it->second;
	// Not found in cache, generate
	TexturePtr strTex;
	Vec2 sizeTex;
	float yBelowLine = 0.0f, yAboveLine = 0.0f;
	// Calculate width and height for new texture
	UTF8String::codepoint_iterator c;
	for(c = str.codepoint_begin(); c != str.codepoint_end(); ++c)
	{
		uint32_t codepoint = *c;
		auto chPairIt = usedCharacters.find(codepoint);
		Character ch;
		if(chPairIt != usedCharacters.end())
			ch = chPairIt->second;
		else
		{
			LoadCharacter(codepoint);
			ch = usedCharacters[codepoint];
		}
		sizeTex.x += ch.advance;
		if(ch.size.y - ch.bearing.y > yBelowLine)
			yBelowLine = ch.size.y - ch.bearing.y;
		if(ch.bearing.y > yAboveLine)
			yAboveLine = ch.bearing.y;
	}
	sizeTex.y = yBelowLine + yAboveLine;
	// Create a white texture
	
	if(sizeTex.x == 0 || sizeTex.y == 0)
		return nullptr;
	
	uint8_t* whiteData = static_cast<uint8_t*>(std::malloc(sizeTex.x * sizeTex.y));
	for(uint32_t i = 0; i < sizeTex.x * sizeTex.y; i++)
		whiteData[i] = 0;
	strTex = std::make_shared<Texture>(whiteData, sizeTex.x, sizeTex.y);
	std::free(whiteData);
	// Start rendering string to texture
	Vec2 pos;
	for(c = str.codepoint_begin(); c != str.codepoint_end(); ++c)
	{
		uint32_t codepoint = *c;
		Character ch = usedCharacters[codepoint]; // Already loaded beforehand, should be found
		
		float xpos = pos.x + ch.bearing.x;
		float ypos = (sizeTex.y - ch.size.y) - (yBelowLine - (ch.size.y - ch.bearing.y));
		strTex->ChangeData(ch.data, xpos, ypos, ch.size.x, ch.size.y);
		pos.x += ch.advance;
	}
	if(prerenderedStrings.size() == MAX_STRINGS_IN_CACHE)
		prerenderedStrings.pop_front();
	prerenderedStrings.emplace_back(str, DrawUtil::CacheTextTexture(strTex, sizeTex, yBelowLine));
	return &prerenderedStrings.back().second;
}

float DrawUtil::CalculateTextWidth(const UTF8String& str, float scale)
{
	DrawUtil::CacheTextTexture* cacheTex = GetTextTextureFromStringCache(str);
	if(cacheTex)
		return cacheTex->size.x * scale * generalFontScale;
	return 0.0f;
}

void DrawUtil::DrawBatchedCommands()
{
	for(int32_t i = MAX_DRAW_LAYERS - 1; i >= 0; i--)
	{
		DrawCommandLayer& dcl = drawCommands[i];
		// Render sprites
		if(!dcl.spriteCommands.empty())
		{
		#if defined(ENABLE_OPENGL_RENDERER) || defined(ENABLE_OPENGLES_RENDERER)
			if(WindowUtil::Get().GetRenderer() == RENDERER_OPENGL || WindowUtil::Get().GetRenderer() == RENDERER_OPENGLES)
				glUniform1i(shaderDrawType, 0);
		#endif
		#ifdef ENABLE_VULKAN_RENDERER
			if(WindowUtil::Get().GetRenderer() == RENDERER_VULKAN)
				UBO.drawType = 0;
		#endif
			for(DrawSpriteCommand& dsc : dcl.spriteCommands)
			{
				// Translate
				UBO.model[12] = dsc.position.x;
				UBO.model[13] = dsc.position.y;
				
				// Rotate (do later, not useful rn) (and yes, i used to use GLM)
				//~ if(dsc.rotate != 0.0f)
				//~ {
					//~ model = glm::translate(model, glm::vec3(0.5f * dsc.size.x, 0.5f * dsc.size.y, 0.0f));
					//~ model = glm::rotate(model, static_cast<float>(glm::radians(dsc.rotate)), glm::vec3(0.0f, 0.0f, 1.0f));
					//~ model = glm::translate(model, glm::vec3(-0.5f * dsc.size.x, -0.5f * dsc.size.y, 0.0f));
				//~ }
				
				// Scale
				UBO.model[0] = dsc.size.x;
				UBO.model[5] = dsc.size.y;
				dsc.texture->Bind();
			#if defined(ENABLE_OPENGL_RENDERER) || defined(ENABLE_OPENGLES_RENDERER)
				if(WindowUtil::Get().GetRenderer() == RENDERER_OPENGL || WindowUtil::Get().GetRenderer() == RENDERER_OPENGLES)
				{
					glUniformMatrix4fv(shaderModel, 1, GL_FALSE, UBO.model);
					glUniform2f(shaderFlip, dsc.flip.x, dsc.flip.y);
					glUniform1f(shaderAlpha, dsc.alpha);
					glUniform1i(shaderTransparencyMode, dsc.transparencyMode);
					glDrawArrays(GL_TRIANGLE_STRIP, 0, VERTEX_COUNT);
				}
			#endif
			#ifdef ENABLE_VULKAN_RENDERER
				if(WindowUtil::Get().GetRenderer() == RENDERER_VULKAN)
				{
 					UBO.flipX = dsc.flip.x ? 1 : 0;
					UBO.flipY = dsc.flip.y ? 1 : 0;
					UBO.alpha = dsc.alpha;
					UBO.transparencyMode = dsc.transparencyMode;
					VulkanUtil::Get().UpdateUniformData({&UBO, sizeof(UBO)});
					VulkanUtil::Get().Draw();
				}
			#endif
			}
			dcl.spriteCommands.clear();
		}
		// Render rectangles
		if(!dcl.rectCommands.empty())
		{
		#if defined(ENABLE_OPENGL_RENDERER) || defined(ENABLE_OPENGLES_RENDERER)
			if(WindowUtil::Get().GetRenderer() == RENDERER_OPENGL || WindowUtil::Get().GetRenderer() == RENDERER_OPENGLES)
				glUniform1i(shaderDrawType, 1);
		#endif
		#ifdef ENABLE_VULKAN_RENDERER
			if(WindowUtil::Get().GetRenderer() == RENDERER_VULKAN)
				UBO.drawType = 1;
		#endif
			for(DrawRectCommand& drc : dcl.rectCommands)
			{
				// Translate
				UBO.model[12] = drc.position.x;
				UBO.model[13] = drc.position.y;
				
				// Scale
				UBO.model[0] = drc.size.x;
				UBO.model[5] = drc.size.y;
				
			#if defined(ENABLE_OPENGL_RENDERER) || defined(ENABLE_OPENGLES_RENDERER)
				if(WindowUtil::Get().GetRenderer() == RENDERER_OPENGL || WindowUtil::Get().GetRenderer() == RENDERER_OPENGLES)
				{
					glUniformMatrix4fv(shaderModel, 1, GL_FALSE, UBO.model);
					glUniform1f(shaderWhite, drc.white);
					glUniform1f(shaderAlpha, drc.alpha);
					glDrawArrays(GL_TRIANGLE_STRIP, 0, VERTEX_COUNT);
				}
			#endif
			#ifdef ENABLE_VULKAN_RENDERER
				if(WindowUtil::Get().GetRenderer() == RENDERER_VULKAN)
				{
					UBO.white = drc.white;
					UBO.alpha = drc.alpha;
					VulkanUtil::Get().UpdateUniformData({&UBO, sizeof(UBO)});
					VulkanUtil::Get().Draw();
				}
			#endif
			}
			dcl.rectCommands.clear();
		}
		// Render text
		if(!dcl.textCommands.empty())
		{
		#if defined(ENABLE_OPENGL_RENDERER) || defined(ENABLE_OPENGLES_RENDERER)
			if(WindowUtil::Get().GetRenderer() == RENDERER_OPENGL || WindowUtil::Get().GetRenderer() == RENDERER_OPENGLES)
				glUniform1i(shaderDrawType, 2);
		#endif
		#ifdef ENABLE_VULKAN_RENDERER
			if(WindowUtil::Get().GetRenderer() == RENDERER_VULKAN)
				UBO.drawType = 2;
		#endif
			for(DrawTextCommand& dtc : dcl.textCommands)
			{
				DrawUtil::CacheTextTexture* cacheTexture = GetTextTextureFromStringCache(dtc.str);
				if(cacheTexture)
				{
					// Translate
					UBO.model[12] = dtc.position.x;
					UBO.model[13] = dtc.position.y - cacheTexture->pixelsBelowLine * dtc.scale;
					
					// Scale
					UBO.model[0] = cacheTexture->size.x * dtc.scale;
					UBO.model[5] = cacheTexture->size.y * dtc.scale;
					cacheTexture->texture->Bind();

				#if defined(ENABLE_OPENGL_RENDERER) || defined(ENABLE_OPENGLES_RENDERER)
					if(WindowUtil::Get().GetRenderer() == RENDERER_OPENGL || WindowUtil::Get().GetRenderer() == RENDERER_OPENGLES)
					{
						glUniformMatrix4fv(shaderModel, 1, GL_FALSE, UBO.model);
						glUniform1f(shaderWhite, dtc.white);
						glUniform1f(shaderAlpha, dtc.alpha);
						glDrawArrays(GL_TRIANGLE_STRIP, 0, VERTEX_COUNT);
					}
				#endif
				#ifdef ENABLE_VULKAN_RENDERER
					if(WindowUtil::Get().GetRenderer() == RENDERER_VULKAN)
					{
						UBO.white = dtc.white;
						UBO.alpha = dtc.alpha;
						VulkanUtil::Get().UpdateUniformData({&UBO, sizeof(UBO)});
						VulkanUtil::Get().Draw();
					}
				#endif
				}
			}
			dcl.textCommands.clear();
		}
	}
}

void DrawUtil::SetColorInvert(bool isInvert)
{
	UBO.invertColors = isInvert;
#if defined(ENABLE_OPENGL_RENDERER) || defined(ENABLE_OPENGLES_RENDERER)
	if(WindowUtil::Get().GetRenderer() == RENDERER_OPENGL || WindowUtil::Get().GetRenderer() == RENDERER_OPENGLES)
		glUniform1i(shaderInvertColors, UBO.invertColors);
#endif
}

void DrawUtil::SetProjectionMatrix(uint32_t width, uint32_t height)
{
	if(WindowUtil::Get().GetRenderer() == RENDERER_OPENGL || WindowUtil::Get().GetRenderer() == RENDERER_OPENGLES)
	{
		orthoMat(UBO.projection, static_cast<float>(width), 0.0f, static_cast<float>(height), 0.0f);
#if defined(ENABLE_OPENGL_RENDERER) || defined(ENABLE_OPENGLES_RENDERER)
		glUniformMatrix4fv(shader.GetUniLoc("projection"), 1, GL_FALSE, (float*)UBO.projection);
#endif
	}
	else if(WindowUtil::Get().GetRenderer() == RENDERER_VULKAN)
		orthoMat(UBO.projection, static_cast<float>(width), 0.0f, 0.0f, static_cast<float>(height));

}

void DrawUtil::SetNullTexture(const UTF8String& name)
{
	nullTexture = GetSprite(name);
}

void DrawUtil::Cleanup()
{
	// Clear to ensure textures are cleaned up properly
	usedCharacters.clear();
	prerenderedStrings.clear();
	nameToSprite.clear();
	nullTexture = nullptr;
}
