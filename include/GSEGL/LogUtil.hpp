#pragma once
#include <queue>
#include <unordered_map>
#include "UTF8String.hpp"

class LogUtil
{
	public:
		static LogUtil& Get();
		bool ContainsLoggedMessages(const UTF8String& logName);
		void CreateLog(const UTF8String& logName);
		void LogMessage(const UTF8String& logName, const UTF8String& message);
		UTF8String GetLoggedMessage(const UTF8String& logName);
	private:
		std::unordered_map<UTF8String, std::queue<UTF8String>> messages;
};
