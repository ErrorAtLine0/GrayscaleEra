#include "LogUtil.hpp"
#include <iostream>

LogUtil& LogUtil::Get()
{
	static LogUtil sInstance;
	return sInstance;
}

bool LogUtil::ContainsLoggedMessages(const UTF8String& logName)
{
	auto it = messages.find(logName);
	if(it != messages.end())
		return !it->second.empty();
	return false;
}

void LogUtil::CreateLog(const UTF8String& logName)
{
	messages.emplace(logName, std::queue<UTF8String>());
}

void LogUtil::LogMessage(const UTF8String& logName, const UTF8String& message)
{
	auto it = messages.find(logName);
	if(it != messages.end())
	{
		it->second.emplace(message);
		std::cerr << '[' << logName.GetCString() << "] " << message.GetCString() << '\n';
	}
	else
		std::cerr << "Log not available\n";
}

UTF8String LogUtil::GetLoggedMessage(const UTF8String& logName)
{
	auto it = messages.find(logName);
	if(it != messages.end())
	{
		UTF8String message = it->second.front();
		it->second.pop();
		return message;
	}
	else
		return "";
}
