#include "TimeUtil.hpp"

TimeUtil& TimeUtil::Get()
{
	static TimeUtil sInstance;
	return sInstance;
}

void TimeUtil::Init()
{
	#ifdef _WIN32
		QueryPerformanceFrequency(&frequency);
	#endif
		Update();
		deltaTime = 0;
		timeSinceStart = 0;
}

void TimeUtil::Update()
{
#ifdef _WIN32
	LARGE_INTEGER newTime;
	QueryPerformanceCounter(&newTime);
	deltaTime = static_cast<double>(newTime.QuadPart - lastTime.QuadPart) / frequency.QuadPart;
	lastTime = newTime;
	timeSinceStart = static_cast<double>(newTime.QuadPart) / frequency.QuadPart;
#else
	struct timeval newTime;
	gettimeofday(&newTime, nullptr);
	deltaTime = (newTime.tv_sec - lastSeconds);
	deltaTime += (newTime.tv_usec - lastMicroseconds) / 1000000.0;
	lastSeconds = newTime.tv_sec;
	lastMicroseconds = newTime.tv_usec;
	timeSinceStart = newTime.tv_sec;
	timeSinceStart += newTime.tv_usec / 1000000.0;
#endif
}

double TimeUtil::GetTimeSinceStart() const
{
	return timeSinceStart;
}

double TimeUtil::GetDeltaTime() const
{
	return deltaTime;
}
