#pragma once
#include "Network/OutMemBitStream.hpp"
#include "Network/InMemBitStream.hpp"
#include "UTF8String.hpp"
#include <vector>
#include <memory>
class FileUtil
{
	public:
		static FileUtil& Get();
		void Init();
		bool RenameFile(const UTF8String& filePath, const UTF8String& newFilePath);
		bool WriteToFile(const UTF8String& filePath, const OutMemBitStream& outStream, bool binaryMode = true);
		std::shared_ptr<InMemBitStream> ReadFromFile(const UTF8String& filePath, bool binaryMode = true);
		std::shared_ptr<std::vector<UTF8String>> GetFilesInPath(const UTF8String& path);
		bool CreateDir(const UTF8String& path);
		bool RemoveFile(const UTF8String& path);
		const UTF8String& GetExecutableDir();
		UTF8String GetTruePath(const UTF8String& path);
	private:
		UTF8String executableDir;
};
