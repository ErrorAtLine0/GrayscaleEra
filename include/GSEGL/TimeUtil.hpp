#pragma once
#ifdef _WIN32
	#include <windows.h>
#else
	#include <sys/time.h>
#endif

class TimeUtil
{
	public:
		static TimeUtil& Get();
		void Init();
		void Update();
		double GetTimeSinceStart() const;
		double GetDeltaTime() const;
	private:
	#ifdef _WIN32
		LARGE_INTEGER frequency;
		LARGE_INTEGER lastTime;
	#else
		long lastSeconds;
		long lastMicroseconds;
	#endif
		double timeSinceStart;
		double deltaTime;
};
