#pragma once
#include <unordered_map>
#include <tuple>
#include "UTF8String.hpp"

class ErrorUtil
{
	public:
		static ErrorUtil& Get();
		void SetError(const UTF8String& type, const UTF8String& error);
		int GetErrorCode();
		bool HasError(const UTF8String& type);
		void ClearErrors();
		const UTF8String* GetError(const UTF8String& type);
	private:
		std::unordered_map<UTF8String, UTF8String> lastErrors;
};
