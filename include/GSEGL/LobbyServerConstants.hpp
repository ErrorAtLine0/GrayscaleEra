#pragma once
#include <cstdint>
#include "Network/SocketAddress.hpp"
#define MAX_PACKET_SIZE 1200
#define LOBBYSTART_TIMEOUT 900.0 // 15 minutes
#define FIRST_VERSION 9000000000000000000 // Number for first version. DO NOT CHANGE
#define CURRENT_VERSION 9000000000000000000 // Increment after every major release
#define DEFAULT_PORT 38694
#define DEFAULT_PORT_STR "38694"

enum LobbyPacketType : uint32_t
{
	// Client requests
	
	// From hosting client
	LOBBYPT_SetupLobby = 0,
	LOBBYPT_ExplicitlyCloseLobby,
	
	// From connecting client
	LOBBYPT_ConnectTo,
	
	// From updating client
	LOBBYPT_IsSameVersion,
	
	// Server replies
	
	// To hosting client
	LOBBYPT_IncorrectString, // Either it was empty, or above max
	LOBBYPT_NameTaken,
	LOBBYPT_AlreadyHosting,
	LOBBYPT_SuccessfulCreation,
	LOBBYPT_IncomingConnection, // Used for UDP hole punching, when client connects to hosting client from external network
	
	// To connecting client
	LOBBYPT_LobbyNotAvailable,
	LOBBYPT_LobbyAvailable,
	
	// To updating client
	LOBBYPT_SameVersion,
	
	// To everyone
	LOBBYPT_DifferentVersion
};

struct Packet
{
	int size;
	SocketAddress from;
	uint8_t data[MAX_PACKET_SIZE];
};
