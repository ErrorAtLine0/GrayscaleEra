#include "FileUtil.hpp"
#include "ErrorUtil.hpp"
#include <cstdio>
#include <cstring>
#ifdef _WIN32
	#include <windows.h>
	#include <tchar.h>
	#include <shlwapi.h>
#else
	#include <unistd.h>
	#include <sys/stat.h>
	#include <sys/types.h>
	#include <dirent.h>
#endif
#ifdef __APPLE__
	#include <mach-o/dyld.h>
#endif

FileUtil& FileUtil::Get()
{
	static FileUtil sInstance;
	return sInstance;
}

void FileUtil::Init()
{
	// Get executable path (if possible)
	#ifdef _WIN32
		wchar_t exeRawPath[2048];
		HMODULE hModule = GetModuleHandle(nullptr);
		if(hModule != nullptr)
		{
			GetModuleFileNameW(hModule, exeRawPath, sizeof(exeRawPath));
	        *(StrRChrW(exeRawPath, nullptr, '\\') + 1) = '\0';
	        executableDir = UTF8String(static_cast<const wchar_t*>(exeRawPath));
		}
	#elif __linux__
		char exeRawPath[2048];
		ssize_t pathSize = readlink("/proc/self/exe", exeRawPath, sizeof(exeRawPath));
		if(pathSize >= 0)
		{
			exeRawPath[pathSize] = '\0';
			*(strrchr(exeRawPath, '/') + 1) = '\0';
			executableDir = exeRawPath;
		}
	#elif __APPLE__
		char exeBadPath[2048];
		uint32_t exeBadPathSize = sizeof(exeBadPath);
		if(_NSGetExecutablePath(exeBadPath, &exeBadPathSize) == 0)
		{
			// Now clean all symlinks
			char exeRawPath[2048];
			char* successful = realpath(exeBadPath, exeRawPath);
			if(successful)
			{
				*(strrchr(exeRawPath, '/') + 1) = '\0';
				executableDir = exeRawPath;
			}
		}
	#elif __FreeBSD__
		char exeRawPath[2048];
		ssize_t pathSize = readlink("/proc/curproc/file", exeRawPath, sizeof(exeRawPath));
		if(pathSize >= 0)
		{
			exeRawPath[pathSize] = '\0';
			*(strrchr(exeRawPath, '/') + 1) = '\0';
			executableDir = exeRawPath;
		}
	#endif
}

bool FileUtil::RenameFile(const UTF8String& filePath, const UTF8String& newFilePath)
{
	UTF8String truePath = GetTruePath(filePath);
	UTF8String newTruePath = GetTruePath(newFilePath);
#ifdef _WIN32
	if(!_wrename(truePath.GetUTF16String().data(), newTruePath.GetUTF16String().data()))
#else
	if(!std::rename(truePath.GetCString(), newTruePath.GetCString()))
#endif
		return true;
	else
	{
		ErrorUtil::Get().SetError("Memory", "FileUtil::RenameFile Could not rename file");
		return false;
	}
}

bool FileUtil::WriteToFile(const UTF8String& filePath, const OutMemBitStream& outStream, bool binaryMode)
{
	UTF8String truePath = GetTruePath(filePath);
#ifdef _WIN32
	FILE* file = _wfopen(truePath.GetUTF16String().data(), binaryMode ? L"wb" : L"w");
#else
	FILE* file = std::fopen(truePath.GetCString(), binaryMode ? "wb" : "w");
#endif
	if(file == nullptr)
	{
		ErrorUtil::Get().SetError("Memory", "FileUtil::WriteToFile Could not open file for writing");
		return false;
	}
	std::fwrite(outStream.GetBufferPtr(), outStream.GetByteLength(), 1, file);
	std::fclose(file);
	return true;
}

std::shared_ptr<InMemBitStream> FileUtil::ReadFromFile(const UTF8String& filePath, bool binaryMode)
{
	UTF8String truePath = GetTruePath(filePath);
#ifdef _WIN32
	FILE* file = _wfopen(truePath.GetUTF16String().data(), binaryMode ? L"rb" : L"r");
#else
	FILE* file = std::fopen(truePath.GetCString(), binaryMode ? "rb" : "r");
#endif
	if(file == nullptr)
	{
		ErrorUtil::Get().SetError("Memory", "FileUtil::ReadFromFile Could not open file for reading");
		return nullptr;
	}
	std::fseek(file, 0, SEEK_END);
	uint32_t fileSize = std::ftell(file);
	std::fseek(file, 0, SEEK_SET);
	uint8_t* rawData = static_cast<uint8_t*>(std::malloc(fileSize));
	std::size_t elementsRead = std::fread(rawData, fileSize, 1, file);
	std::fclose(file);
	if(elementsRead < 1)
	{
		ErrorUtil::Get().SetError("Memory", "FileUtil::ReadFromFile Error occurred while reading file");
		std::free(rawData);
		return nullptr;
	}
	
	std::shared_ptr<InMemBitStream> toRet = std::make_shared<InMemBitStream>(rawData, fileSize);
	std::free(rawData);
	return toRet;
}

std::shared_ptr<std::vector<UTF8String>> FileUtil::GetFilesInPath(const UTF8String& path)
{
	UTF8String truePath = GetTruePath(path);
#ifdef _WIN32
	WIN32_FIND_DATAW findFileData;
	HANDLE hFind;
	hFind = FindFirstFileW((truePath + "/*.*").GetUTF16String().data(), &findFileData);
	if(hFind == INVALID_HANDLE_VALUE)
	{
		ErrorUtil::Get().SetError("Memory", "FileUtil::GetFilesInPath Could not open directory");
		return nullptr;
	}
	std::shared_ptr<std::vector<UTF8String>> fileNames = std::make_shared<std::vector<UTF8String>>();
	do
	{
		if(std::wcscmp(findFileData.cFileName, L".") && std::wcscmp(findFileData.cFileName, L".."))
			fileNames->emplace_back(findFileData.cFileName);
	} while(FindNextFileW(hFind, &findFileData));
	FindClose(hFind);
	return fileNames;
#else
	DIR* dirPtr = opendir(truePath.GetCString());
	if(!dirPtr)
	{
		ErrorUtil::Get().SetError("Memory", "FileUtil::GetFilesInPath Could not open directory");
		return nullptr;
	}
	std::shared_ptr<std::vector<UTF8String>> fileNames = std::make_shared<std::vector<UTF8String>>();
	struct dirent* fileInPath = nullptr;
	while((fileInPath = readdir(dirPtr)))
		if(std::strcmp(fileInPath->d_name, ".") && std::strcmp(fileInPath->d_name, ".."))
			fileNames->emplace_back(fileInPath->d_name);
	closedir(dirPtr);
	return fileNames;
#endif
}

bool FileUtil::CreateDir(const UTF8String& path)
{
	UTF8String truePath = GetTruePath(path);
#ifdef _WIN32
	if(CreateDirectoryW(truePath.GetUTF16String().data(), nullptr))
#else
	if(!mkdir(truePath.GetCString(), 0777))
#endif
		return true;
	else
	{
		ErrorUtil::Get().SetError("Memory", "FileUtil::CreateDirectory Could not create directory");
		return false;
	}
}

bool FileUtil::RemoveFile(const UTF8String& path)
{
	UTF8String truePath = GetTruePath(path);
#ifdef _WIN32
	if(DeleteFileW(truePath.GetUTF16String().data()))
#else
	if(!std::remove(truePath.GetCString()))
#endif
		return true;
	else
	{
		ErrorUtil::Get().SetError("Memory", "FileUtil::RemoveFile Could not remove file");
		return false;
	}
}

const UTF8String& FileUtil::GetExecutableDir() { return executableDir; }

UTF8String FileUtil::GetTruePath(const UTF8String& path)
{
#ifdef _WIN32
	if(!PathIsRelativeW(path.GetUTF16String().data()))
		return path;
	else
		return executableDir + path;
#else
	if(path.GetCString()[0] == '/')
		return path;
	else
		return executableDir + path;
#endif
	return path;
}
