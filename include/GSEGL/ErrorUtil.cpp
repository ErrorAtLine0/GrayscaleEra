#include "ErrorUtil.hpp"
#include <iostream>
#include <cstring>
#ifdef _WIN32
	#ifndef WIN32_LEAN_AND_MEAN
		#define WIN32_LEAN_AND_MEAN
	#endif
	#include <windows.h>
#else
	#include <errno.h>
#endif

ErrorUtil& ErrorUtil::Get()
{
	static ErrorUtil sInstance;
	return sInstance;
}

void ErrorUtil::SetError(const UTF8String& type, const UTF8String& error)
{
	lastErrors[type] = error;
	lastErrors[type] += UTF8String(" ") + UTF8String::IntToStr(GetErrorCode());
	std::cerr << "[ERROR] " << lastErrors[type].GetCString() << '\n';
}

int ErrorUtil::GetErrorCode()
{
#ifdef _WIN32
	return GetLastError();
#else
	return errno;
#endif
}

bool ErrorUtil::HasError(const UTF8String& type)
{
	return lastErrors.find(type) != lastErrors.end();
}

void ErrorUtil::ClearErrors()
{
	lastErrors.clear();
}

const UTF8String* ErrorUtil::GetError(const UTF8String& type)
{
	auto it = lastErrors.find(type);
	if(it != lastErrors.end())
		return &it->second;
	return nullptr;
}
