#include "UTF8String.hpp"
#include "ErrorUtil.hpp"
#include <cstring>
#include <cstdio>

UTF8String::UTF8String():
buffer(nullptr),
byteCapacity(0),
byteHead(0)
{
	NullTerminate();
}

UTF8String::UTF8String(uint32_t bufferSize):
buffer(nullptr),
byteCapacity(0),
byteHead(0)
{
	ReallocBuffer(bufferSize);
	NullTerminate();
}

UTF8String::UTF8String(const UTF8String& str):
buffer(nullptr),
byteCapacity(0),
byteHead(0)
{
	*this = str;
}

UTF8String::UTF8String(const uint8_t* rawData, uint32_t size):
buffer(nullptr),
byteCapacity(0),
byteHead(size)
{
	ReallocBuffer(size+1);
	std::memcpy(buffer, rawData, size);
	NullTerminate();
}

UTF8String::UTF8String(const char* str):
buffer(nullptr),
byteCapacity(0),
byteHead(0)
{
	*this = str;
}

UTF8String::UTF8String(const wchar_t* utf16Str):
buffer(nullptr),
byteCapacity(0),
byteHead(0)
{
	*this = utf16Str;
}

UTF8String::~UTF8String()
{
	std::free(buffer);
}

void UTF8String::ReallocBuffer(uint32_t newByteCapacity)
{
	uint8_t* tempBuffer = static_cast<uint8_t*>(std::realloc(buffer, newByteCapacity));
	if(tempBuffer == nullptr)
	{
		ErrorUtil::Get().SetError("Memory", "UTF8String::ReallocBuffer");
		return;
	}
	buffer = tempBuffer;
	byteCapacity = newByteCapacity;
}

void UTF8String::AddCodepoint(uint32_t codepoint)
{
	uint32_t resultHead = byteHead;
	if(codepoint < 128)
	{
		resultHead += 1;
		if(resultHead >= byteCapacity)
			ReallocBuffer(std::max(resultHead, byteCapacity * 2));
		buffer[byteHead] = codepoint;
	}
	else if(codepoint >= 128 && codepoint < 2048)
	{
		resultHead += 2;
		if(resultHead >= byteCapacity)
			ReallocBuffer(std::max(resultHead, byteCapacity * 2));
		buffer[byteHead]     = 192 | static_cast<uint8_t>(codepoint >> 6);
		buffer[byteHead + 1] = 128 | static_cast<uint8_t>((codepoint << 26) >> 26);
	}
	else if(codepoint >= 2048 && codepoint < 65536)
	{
		resultHead += 3;
		if(resultHead >= byteCapacity)
			ReallocBuffer(std::max(resultHead, byteCapacity * 2));
		buffer[byteHead]     = 224 | static_cast<uint8_t>(codepoint >> 12);
		buffer[byteHead + 1] = 128 | static_cast<uint8_t>((codepoint << 20) >> 26);
		buffer[byteHead + 2] = 128 | static_cast<uint8_t>((codepoint << 26) >> 26);
	}
	else
	{
		resultHead += 4;
		if(resultHead >= byteCapacity)
			ReallocBuffer(std::max(resultHead, byteCapacity * 2));
		buffer[byteHead]     = 240 | static_cast<uint8_t>(codepoint >> 18);
		buffer[byteHead + 1] = 128 | static_cast<uint8_t>((codepoint << 14) >> 26);
		buffer[byteHead + 2] = 128 | static_cast<uint8_t>((codepoint << 20) >> 26);
		buffer[byteHead + 3] = 128 | static_cast<uint8_t>((codepoint << 26) >> 26);
	}
	byteHead = resultHead;
	NullTerminate();
}

uint32_t UTF8String::GetCodepoint(uint32_t charIndex) const
{
	uint32_t byteIndex = GetByteIndexFromCharIndex(charIndex);
	if((buffer[byteIndex] & 128) == 0)
		return static_cast<uint32_t>(buffer[byteIndex]) & 255;
	else if((buffer[byteIndex] & 224) == 192)
	{
		uint32_t toRet = 0;
		toRet |= (static_cast<uint32_t>(buffer[byteIndex]) & 31) << 6;
		toRet |= (static_cast<uint32_t>(buffer[byteIndex+1]) & 63);
		return toRet;
	}
	else if((buffer[byteIndex] & 240) == 224)
	{
		uint32_t toRet = 0;
		toRet |= (static_cast<uint32_t>(buffer[byteIndex]) & 15) << 12;
		toRet |= (static_cast<uint32_t>(buffer[byteIndex+1]) & 63) << 6;
		toRet |= (static_cast<uint32_t>(buffer[byteIndex+2]) & 63);
		return toRet;
	}
	else
	{
		uint32_t toRet = 0;
		toRet |= (static_cast<uint32_t>(buffer[byteIndex]) & 7) << 18;
		toRet |= (static_cast<uint32_t>(buffer[byteIndex+1]) & 63) << 12;
		toRet |= (static_cast<uint32_t>(buffer[byteIndex+2]) & 63) << 6;
		toRet |= (static_cast<uint32_t>(buffer[byteIndex+3]) & 63);
		return toRet;
	}
}

void UTF8String::PopBack()
{
	if(byteHead > 0)
		for(uint32_t i = byteHead-1;i < byteHead;i--)
		{
			if((buffer[i] & 192) != 128) // Is a head of a character. Pop from here
			{
				byteHead -= byteHead - i;
				if(byteHead < byteCapacity/2)
					ReallocBuffer(byteCapacity/2);
				NullTerminate();
				break;
			}
		}
}

UTF8String UTF8String::SubStr(uint32_t indexStart, uint32_t indexEnd)
{
	uint32_t strLength = GetStrLength();
	if((indexStart < strLength && indexEnd <= strLength)&&
		indexStart < indexEnd)
	{
		uint32_t byteIndexStart = GetByteIndexFromCharIndex(indexStart);
		uint32_t byteIndexEnd = GetByteIndexFromCharIndex(indexEnd);
		uint32_t byteIndexDiff = byteIndexEnd - byteIndexStart;
		UTF8String toRet(byteIndexDiff + 1);
		std::memcpy(toRet.buffer, buffer + byteIndexStart, byteIndexDiff);
		toRet.byteHead = byteIndexDiff;
		toRet.NullTerminate();
		return toRet;
	}
	else
		return UTF8String();
}

void UTF8String::operator=(const UTF8String& utf8String)
{
	byteHead = utf8String.byteHead;
	ReallocBuffer(byteHead + 1);
	std::memcpy(buffer, utf8String.buffer, byteHead);
	NullTerminate();
}

void UTF8String::operator=(const char* cString)
{
	ReallocBuffer(std::strlen(cString) + 1);
	std::memcpy(buffer, cString, byteCapacity);
	byteHead = byteCapacity - 1;
	NullTerminate();
}

void UTF8String::operator=(const wchar_t* utf16String)
{
	for(uint16_t i = 0; i < wcslen(utf16String); i++)
	{
		if(utf16String[i] <= 0xFFFF)
			AddCodepoint(utf16String[i]);
		else
		{
			uint32_t codepoint = utf16String[i] - 0xD800;
			codepoint = codepoint << 10;
			++i;
			codepoint |= utf16String[i] - 0xDC00;
			codepoint += 0x10000;
			AddCodepoint(codepoint);
		}
	}
}

void UTF8String::operator+=(const UTF8String& utf8String)
{
	if(!utf8String.IsEmpty())
	{
		uint32_t resultHead = byteHead + utf8String.byteHead;
		if(resultHead > byteCapacity)
			ReallocBuffer(std::max(resultHead + 1, byteCapacity * 2));
		std::memcpy(buffer+byteHead, utf8String.buffer, utf8String.byteHead);
		byteHead = resultHead;
		NullTerminate();
	}
}

void UTF8String::operator+=(const char* cString)
{
	if(cString)
	{
		uint32_t cStrLen = std::strlen(cString);
		uint32_t resultHead = byteHead + cStrLen;
		if(resultHead > byteCapacity)
			ReallocBuffer(std::max(resultHead + 1, byteCapacity * 2));
		std::memcpy(buffer+byteHead, cString, cStrLen);
		byteHead = resultHead;
		NullTerminate();
	}
}

bool UTF8String::operator==(const UTF8String& utf8String) const
{
	if(byteHead != utf8String.byteHead)
		return false;
	else if(byteHead == 0) // Thus, both would be empty strings, and this would be true
		return true;
	return !(std::memcmp(buffer, utf8String.buffer, byteHead));
}

bool UTF8String::operator!=(const UTF8String& utf8String) const
{
	return !(*this == utf8String);
}

UTF8String UTF8String::operator+(const UTF8String& utf8String) const
{
	UTF8String toRet = *this;
	toRet += utf8String;
	return toRet;
}

UTF8String UTF8String::operator+(const char* cString) const
{
	UTF8String toRet = *this;
	toRet += cString;
	return toRet;
}

bool UTF8String::Read(InMemBitStream& inStream)
{
	uint16_t receivedByteHead = 0;
	if(!inStream.ReadBits(receivedByteHead)) return false;
	if(inStream.GetRemainingBytes() < receivedByteHead) return false;
	ReallocBuffer(static_cast<uint32_t>(receivedByteHead) + 1);
	inStream.ReadBits(static_cast<void*>(buffer), static_cast<uint32_t>(receivedByteHead)*8);
	byteHead = static_cast<uint32_t>(receivedByteHead);
	NullTerminate();
	return true;
}

void UTF8String::Write(OutMemBitStream& outStream) const
{
	uint16_t byteHeadToWrite = static_cast<uint16_t>(byteHead);
	outStream.WriteBits(byteHeadToWrite);
	outStream.WriteBits(static_cast<const void*>(buffer), byteHead*8);
}

bool UTF8String::ReadText(InMemBitStream& inStream)
{
	char charRead;
	Clear();
	if(!inStream.ReadBits(charRead) && charRead != '"') return false;
	for(;;)
	{
		char codepoint[4];
		uint32_t intCodepoint = 0;
		if(!inStream.ReadBits(codepoint[0])) return false;
		if((codepoint[0] & 128) == 0)
		{
			if(codepoint[0] == '"')
				break;
			std::memcpy(&intCodepoint, codepoint, 1);
			AddCodepoint(intCodepoint);
		}
		else if((codepoint[0] & 224) == 192)
		{
			if(!inStream.ReadBits(codepoint[1])) return false;
			std::memcpy(&intCodepoint, codepoint, 2);
			AddCodepoint(intCodepoint);
		}
		else if((codepoint[0] & 240) == 224)
		{
			if(!inStream.ReadBits(codepoint[1])) return false;
			if(!inStream.ReadBits(codepoint[2])) return false;
			std::memcpy(&intCodepoint, codepoint, 3);
			AddCodepoint(intCodepoint);
		}
		else
		{
			if(!inStream.ReadBits(codepoint[1])) return false;
			if(!inStream.ReadBits(codepoint[2])) return false;
			if(!inStream.ReadBits(codepoint[3])) return false;
			std::memcpy(&intCodepoint, codepoint, 4);
			AddCodepoint(intCodepoint);
		}
	}
	return true;
}

void UTF8String::WriteText(OutMemBitStream& outStream) const
{
	outStream.WriteBits('"');
	outStream.WriteBits(static_cast<const void*>(buffer), byteHead*8);
	outStream.WriteBits('"');
}

const char* UTF8String::GetCString() const
{
	return reinterpret_cast<char*>(buffer);
}

uint32_t UTF8String::GetByteLength() const
{
	return byteHead;
}

uint32_t UTF8String::GetStrLength() const
{
	return CalculateStrLength();
}

bool UTF8String::IsEmpty() const
{
	return byteHead == 0;
}

std::vector<wchar_t> UTF8String::GetUTF16String() const
{
	std::vector<wchar_t> utf16Str;
	for(codepoint_iterator c = codepoint_begin(); c != codepoint_end(); ++c)
	{
		uint32_t codepoint = *c;
		if(codepoint <= 0xFFFF)
			utf16Str.emplace_back(static_cast<wchar_t>(codepoint));
		else
		{
			codepoint -= 0x10000;
			utf16Str.emplace_back(static_cast<wchar_t>((codepoint >> 10) + 0xD800));
			utf16Str.emplace_back(static_cast<wchar_t>((codepoint & 0x3FF) + 0xDC00));
		}
	}
	utf16Str.emplace_back('\0');
	return utf16Str;
}

void UTF8String::Clear()
{
	byteHead = 0;
	ReallocBuffer(1);
	buffer[0] = '\0';
}

UTF8String UTF8String::DoubleToStr(double val, const char* formatStr)
{
	char buffer[50];
	std::snprintf(buffer, 50, formatStr, val);
	return buffer;
}

UTF8String UTF8String::IntToStr(int32_t val, const char* formatStr)
{
	char buffer[12];
	std::snprintf(buffer, 12, formatStr, val);
	return buffer;
}

int32_t UTF8String::StrToInt(const UTF8String& val)
{
	char* end;
	int32_t num = strtol((char*)val.buffer, &end, 0);
	if(*end != '\0')
	{
		ErrorUtil::Get().SetError("Memory", "UTF8String::StrToInt Unable to convert string to number");
		return 0;
	}
	else
		return num;
}

UTF8String::codepoint_iterator::codepoint_iterator():
str(nullptr),
byteIndex(0)
{}

UTF8String::codepoint_iterator::codepoint_iterator(const UTF8String* pointStr):
str(pointStr),
byteIndex(0)
{}

UTF8String::codepoint_iterator::codepoint_iterator(const UTF8String* pointStr, uint32_t pointIndex):
str(pointStr),
byteIndex(pointIndex)
{}

uint32_t UTF8String::codepoint_iterator::operator*() const
{
	if((str->buffer[byteIndex] & 128) == 0)
		return static_cast<uint32_t>(str->buffer[byteIndex]) & 255;
	else if((str->buffer[byteIndex] & 224) == 192)
	{
		uint32_t toRet = 0;
		toRet |= (static_cast<uint32_t>(str->buffer[byteIndex]) & 31) << 6;
		toRet |= (static_cast<uint32_t>(str->buffer[byteIndex+1]) & 63);
		return toRet;
	}
	else if((str->buffer[byteIndex] & 240) == 224)
	{
		uint32_t toRet = 0;
		toRet |= (static_cast<uint32_t>(str->buffer[byteIndex]) & 15) << 12;
		toRet |= (static_cast<uint32_t>(str->buffer[byteIndex+1]) & 63) << 6;
		toRet |= (static_cast<uint32_t>(str->buffer[byteIndex+2]) & 63);
		return toRet;
	}
	else
	{
		uint32_t toRet = 0;
		toRet |= (static_cast<uint32_t>(str->buffer[byteIndex]) & 7) << 18;
		toRet |= (static_cast<uint32_t>(str->buffer[byteIndex+1]) & 63) << 12;
		toRet |= (static_cast<uint32_t>(str->buffer[byteIndex+2]) & 63) << 6;
		toRet |= (static_cast<uint32_t>(str->buffer[byteIndex+3]) & 63);
		return toRet;
	}
}

void UTF8String::codepoint_iterator::operator++()
{
	if(byteIndex == str->byteHead)
		return;
	if((str->buffer[byteIndex] & 128) == 0)
		byteIndex += 1;
	else if((str->buffer[byteIndex] & 224) == 192)
		byteIndex += 2;
	else if((str->buffer[byteIndex] & 240) == 224)
		byteIndex += 3;
	else
		byteIndex += 4;
}

bool UTF8String::codepoint_iterator::operator!=(const codepoint_iterator& it) const
{
	return !((str == it.str) && (byteIndex == it.byteIndex));
}

bool UTF8String::codepoint_iterator::operator==(const codepoint_iterator& it) const
{
	return (str == it.str) && (byteIndex == it.byteIndex);
}

void UTF8String::codepoint_iterator::operator=(const codepoint_iterator& it)
{
	str = it.str;
	byteIndex = it.byteIndex;
}
		
UTF8String::codepoint_iterator UTF8String::codepoint_begin() const
{
	return codepoint_iterator(this);
}

UTF8String::codepoint_iterator UTF8String::codepoint_end() const
{
	return codepoint_iterator(this, byteHead);
}

uint32_t UTF8String::GetByteIndexFromCharIndex(uint32_t charIndex) const
{
	uint32_t byteIndex = 0;
	for(uint32_t i = 0; i < charIndex; i++)
	{
		if((buffer[byteIndex] & 128) == 0)
			byteIndex += 1;
		else if((buffer[byteIndex] & 224) == 192)
			byteIndex += 2;
		else if((buffer[byteIndex] & 240) == 224)
			byteIndex += 3;
		else
			byteIndex += 4;
	}
	return byteIndex;
}

void UTF8String::NullTerminate()
{
	if(byteHead == byteCapacity)
		ReallocBuffer(byteCapacity + 1);
	buffer[byteHead] = '\0';
}

uint32_t UTF8String::CalculateStrLength() const
{
	uint32_t toRet = 0;
	for(uint32_t byteIndex = 0; byteIndex < byteHead;)
	{
		if((buffer[byteIndex] & 128) == 0)
			byteIndex += 1;
		else if((buffer[byteIndex] & 224) == 192)
			byteIndex += 2;
		else if((buffer[byteIndex] & 240) == 224)
			byteIndex += 3;
		else
			byteIndex += 4;
		++toRet;
	}
	return toRet;
}
