#pragma once
#include "Network/InMemBitStream.hpp"
#include "Network/OutMemBitStream.hpp"
#include <cstdlib>
#include <functional>
#include <vector>

class UTF8String
{
	public:
		UTF8String();
		UTF8String(uint32_t bufferSize);
		UTF8String(const UTF8String& str);
		UTF8String(const char* str);
		UTF8String(const uint8_t* rawData, uint32_t size);
		UTF8String(const wchar_t* utf16Str);
		~UTF8String();
		void ReallocBuffer(uint32_t newByteCapacity);
		void AddCodepoint(uint32_t codepoint);
		uint32_t GetCodepoint(uint32_t charIndex) const;
		void PopBack();
		UTF8String SubStr(uint32_t indexStart, uint32_t indexEnd);
		void operator=(const UTF8String& utf8String);
		void operator=(const char* cString);
		void operator=(const wchar_t* utf16String);
		void operator+=(const UTF8String& utf8String);
		void operator+=(const char* cString);
		bool operator==(const UTF8String& utf8String) const;
		bool operator!=(const UTF8String& utf8String) const;
		UTF8String operator+(const UTF8String& utf8String) const;
		UTF8String operator+(const char* cString) const;
		bool Read(InMemBitStream& inStream);
		void Write(OutMemBitStream& outStream) const;
		bool ReadText(InMemBitStream& inStream);
		void WriteText(OutMemBitStream& outStream) const;
		const char* GetCString() const;
		uint32_t GetByteLength() const;
		uint32_t GetStrLength() const;
		bool IsEmpty() const;
		std::vector<wchar_t> GetUTF16String() const;
		void Clear();
		static UTF8String DoubleToStr(double val, const char* formatStr = "%g");
		static UTF8String IntToStr(int32_t val, const char* formatStr = "%d");
		static int32_t StrToInt(const UTF8String& val);
		
		class codepoint_iterator
		{
			public:
				codepoint_iterator();
				codepoint_iterator(const UTF8String* pointStr);
				codepoint_iterator(const UTF8String* pointStr, uint32_t pointIndex);
				uint32_t operator*() const;
				void operator++();
				bool operator!=(const codepoint_iterator& it) const;
				bool operator==(const codepoint_iterator& it) const;
				void operator=(const codepoint_iterator& it);
			private:
				const UTF8String* str;
				uint32_t byteIndex;
		};
		codepoint_iterator codepoint_begin() const;
		codepoint_iterator codepoint_end() const;
	private:
		friend struct std::hash<UTF8String>;
		friend struct std::hash<const UTF8String>;
		uint32_t GetByteIndexFromCharIndex(uint32_t charIndex) const;
		void NullTerminate();
		uint32_t CalculateStrLength() const;
		
		uint8_t* buffer;
		uint32_t byteCapacity;
		uint32_t byteHead;
};

namespace std
{
	// djb2 algorithm by Dan Bernstein
	template<> struct hash<UTF8String>
	{
		uint32_t operator()(const UTF8String& str) const
		{
		    uint32_t toRet = 5381;
		    uint32_t c;
		    uint8_t* buffPtr = str.buffer;
		    while((c = *buffPtr++))
		        toRet = ((toRet << 5) + toRet) + c;
		    return toRet;
		}
	};
	template<> struct hash<const UTF8String>
	{
		uint32_t operator()(const UTF8String& str) const
		{
		    uint32_t toRet = 5381;
		    uint32_t c;
		    uint8_t* buffPtr = str.buffer;
		    while((c = *buffPtr++))
		        toRet = ((toRet << 5) + toRet) + c;
		    return toRet;
		}
	};
}
