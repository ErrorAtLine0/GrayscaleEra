#include "RandGenUtil.hpp"
#ifdef __MINGW32__
	#include <ctime>
#endif

RandGenUtil& RandGenUtil::Get()
{
	static RandGenUtil sInstance;
	return sInstance;
}

void RandGenUtil::Init()
{
#ifdef __MINGW32__
	numberGenerator.seed(time(NULL)); // Mingw gives the same random number sequence when seeding using random_device
#else
	std::random_device rd;
	numberGenerator.seed(rd());
#endif
}

uint64_t RandGenUtil::GetRandomUInt64(uint64_t inMin, uint64_t inMax)
{
	std::uniform_int_distribution<uint64_t> dist(inMin, inMax);
	return dist(numberGenerator);
}

int64_t RandGenUtil::GetRandomInt64(int64_t inMin, int64_t inMax)
{
	std::uniform_int_distribution<int64_t> dist(inMin, inMax);
	return dist(numberGenerator);
}

double RandGenUtil::GetRandomDouble(double inMin, double inMax)
{
	std::uniform_real_distribution<double> dist(inMin, inMax);
	return dist(numberGenerator);
}
