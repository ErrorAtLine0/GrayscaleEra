# Grayscale Era
A multiplayer 2D fighting/adventure game where people can create their own characters, arenas, and adventures from scratch.

### NOTICE
This game gives your public and private IP address to a master server, where it is shared with your clients,
when creating a lobby/playing in public play. The game has also not been tested heavily on macOS.
Use at your own risk.

### Licensing
This project uses third party libraries and fonts that are licensed under other terms. The licenses are located in the "licenses" folder.
This project's license is located in [LICENSE.txt](LICENSE.txt)

### Direct contributions to the game
- Yousef Khadadeh (ErrorAtLine0): Programmer, Visual Artist
  - Email: erroratline0@gmail.com

### Website
Visit [https://erroratline0.com/grayscaleera.html](https://erroratline0.com/grayscaleera.html), where you can find a tutorial and an FAQ.

### Compilers used to compile binary distributions of this project
- [GCC](https://gcc.gnu.org/): Linux version
- [Mingw-w64](https://mingw-w64.org/): Windows version
- [Clang](https://clang.llvm.org/): macOS version

### Libraries used in the project
- [GLFW](http://www.glfw.org/): Windowing and input
- [stb_truetype](https://github.com/nothings/stb): Loading truetype fonts
- [stb_image](https://github.com/nothings/stb): Loading images
- [GLAD](https://github.com/Dav1dde/glad): OpenGL Loader
- [miniz](https://github.com/richgel999/miniz): Compression
- [Vulkan Memory Allocator](https://github.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator): Easier, more organized memory allocation for Vulkan
- [LunarG Vulkan SDK](https://www.lunarg.com/): Used for developing and debugging Vulkan
- [MoltenVK](https://moltengl.com/moltenvk/): A Vulkan implementation built on Metal
Some libraries in this list depend on other libraries, or the compiler
might depend on separate libraries not listed here.

### Other elements used in the project
- [GNU Unifont](http://unifoundry.com/unifont.html): The unicode font that is used for this game

### Thank you to
- Joshua Glazer and Sanjay Madhav for writing the book "Multiplayer Game Programming".
  The book taught me a great deal in networking in general, and a lot of the network code
  for Grayscale Era looks very similar to the examples given in the book.
- Joey De Vries, writer of [Learn OpenGL](https://learnopengl.com/). This tutorial was
  the first I ever read of OpenGL and graphics, and thanks to it, I was able to completely
  grasp the basics of OpenGL without any hassle. I'm personally looking forward to what
  [Learn Vulkan](https://learnvulkan.com/) is going to be :D.
- Alexander Overvoorde, writer of [Vulkan Tutorial](https://vulkan-tutorial.com/). This
  tutorial was a great way to start with a graphics API which has very, very little beginner
  material to begin with (hopefully, that changes in the future). With this tutorial, and the
  knowledge from Learn OpenGL, I was able to write a port of the game library for Vulkan.
