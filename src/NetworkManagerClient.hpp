#pragma once
#include "NetworkManager.hpp"
#include <GSEGL/Network/ReliableDataManager.hpp>

class NetworkManagerClient : public NetworkManager
{
	public:
		NetworkManagerClient();
		bool Init(const UTF8String& initUserName, const UTF8String& initLobbyName, WorldType initWorldType);
		bool Init(const UTF8String& initUserName, const UTF8String& initIP, uint16_t initPort, WorldType initWorldType);
		bool Update();
		bool IsServer() { return false; }
		double GetEstimatedSendRate();
		double GetRTT();
		void CreateObject(GameObject* gameObj); // Used to preload level blocks
		void SendChatMessage(const UTF8String& chatMessage);
	private:
		SocketAddressPtr serverAddr;
		bool playerDataQueued;
		DeliveryNotificationManager deliveryNotifier;
		ReliableDataManager reliableDataManager;
		OutMemBitStream worldDataOutStream;
		double timeLastPacketSent;
		PrivateID privateID;
		LinkingContext linkingContext;
		ReplicationManager replicationManager;
};
