#include <GSEGL/Graphics/WindowUtil.hpp>
#include <GSEGL/TimeUtil.hpp>
#include <GSEGL/Graphics/DrawUtil.hpp>
#include <GSEGL/Graphics/GUI/GUIUtil.hpp>
#include <GSEGL/RandGenUtil.hpp>
#include <GSEGL/Graphics/ControllerUtil.hpp>
#include "ScreenNavigator.hpp"
#include "NetworkManagerServer.hpp"
#include "Screens/MainMenuScreen.hpp"
#include "Screens/PaintScreen.hpp"
#include <GSEGL/LogUtil.hpp>
#include "LogViewer.hpp"
#include "AbilityData.hpp"
#include "GameObjects/Block.hpp"
#include "GameObjects/RealPlayer.hpp"
#include "GameObjects/NPC.hpp"
#include "GameObjects/Projectile.hpp"
#include "GameObjects/SummonedMinion.hpp"
#include <GSEGL/FileUtil.hpp>
#include "GlobalSettingsUtil.hpp"

int main()
{
	LogUtil::Get().CreateLog("NORMAL");
	FileUtil::Get().Init();
	RandGenUtil::Get().Init();
	TimeUtil::Get().Init();
	GlobalSettingsUtil::Get().InitMain();
	GlobalSettingsUtil::Get().WriteMainToFile();
	uint32_t rendererType = GlobalSettingsUtil::Get().mainSettings.GetChoice(0);
	RendererEnum rendererEnum = RENDERER_OPENGL;
	switch(rendererType)
	{
		case 0: { rendererEnum = RENDERER_OPENGL; LogUtil::Get().LogMessage("NORMAL", "Using OpenGL Renderer"); break; }
		case 1: { rendererEnum = RENDERER_OPENGLES; LogUtil::Get().LogMessage("NORMAL", "Using OpenGL ES Renderer"); break; }
		case 2: { rendererEnum = RENDERER_VULKAN; LogUtil::Get().LogMessage("NORMAL", "Using Vulkan Renderer"); break; }
	}
	WindowUtil::Get().Init("Grayscale Era", 800, 600, rendererEnum);
	ControllerUtil::Get().Init();
	
	std::vector<FontLoadInfo> fontsToLoad;
	fontsToLoad.emplace_back();
	fontsToLoad.back().path = "fonts/unifont.ttf";
	fontsToLoad.back().pixelHeight = 48.0f;
	fontsToLoad.back().startingCodePoint = 0x0;
	fontsToLoad.back().codePointRange = 0x10000;
	fontsToLoad.emplace_back();
	fontsToLoad.back().path = "fonts/unifont_upper.ttf";
	fontsToLoad.back().pixelHeight = 48.0f;
	fontsToLoad.back().startingCodePoint = 0x10000;
	fontsToLoad.back().codePointRange = 0x0;
	DrawUtil::Get().Init(fontsToLoad, 0.032f, 800, 600);
	
	WindowUtil::Get().SetClearColor(1.0f, 1.0f, 1.0f);
	WindowUtil::Get().SetCursorHidden(true);
	GUIUtil::Get().Init();
	AbilityData::Init();
	
	ScreenNavigator::Get().SetScreen(std::make_shared<MainMenuScreen>());
	
	ObjectCreationRegistry::Get().RegisterCreationFunction<Block>();
	ObjectCreationRegistry::Get().RegisterCreationFunction<RealPlayer>();
	ObjectCreationRegistry::Get().RegisterCreationFunction<NPC>();
	ObjectCreationRegistry::Get().RegisterCreationFunction<Projectile>();
	ObjectCreationRegistry::Get().RegisterCreationFunction<SummonedMinion>();
	DrawUtil::Get().AddSprite("ArenaButton", "textures/ArenaButton.png");
	DrawUtil::Get().AddSprite("AdventureButton", "textures/AdventureButton.png");
	DrawUtil::Get().AddSprite("CancelButton", "textures/CancelButton.png");
	DrawUtil::Get().AddSprite("DoneButton", "textures/DoneButton.png");
	DrawUtil::Get().AddSprite("CopyButton", "textures/CopyButton.png");
	DrawUtil::Get().AddSprite("PasteButton", "textures/PasteButton.png");
	DrawUtil::Get().AddSprite("UndoButton", "textures/UndoButton.png");
	DrawUtil::Get().AddSprite("ArrowButton", "textures/ArrowButton.png");
	DrawUtil::Get().AddSprite("PencilIcon", "textures/PencilIcon.png");
	DrawUtil::Get().AddSprite("RemoveIcon", "textures/RemoveIcon.png");
	DrawUtil::Get().AddSprite("AddIcon", "textures/AddIcon.png");
	DrawUtil::Get().AddSprite("MinusIcon", "textures/MinusIcon.png");
	DrawUtil::Get().AddSprite("SettingsIcon", "textures/SettingsIcon.png");
	DrawUtil::Get().AddSprite("CheckBox", "textures/CheckBox.png");
	DrawUtil::Get().AddSprite("CheckedBox", "textures/CheckedBox.png");
	DrawUtil::Get().AddSprite("SaveIcon", "textures/SaveIcon.png");
	DrawUtil::Get().AddSprite("BackIcon", "textures/BackIcon.png");
	DrawUtil::Get().AddSprite("ListIcon", "textures/ListIcon.png");
	DrawUtil::Get().AddSprite("RadioButtonEmpty", "textures/RadioButtonEmpty.png");
	DrawUtil::Get().AddSprite("RadioButtonFull", "textures/RadioButtonFull.png");
	DrawUtil::Get().AddSprite("ShieldImage", "textures/ShieldImage.png");
	DrawUtil::Get().AddSprite("HealImage", "textures/HealImage.png");
	DrawUtil::Get().AddSprite("SpeedImage", "textures/SpeedImage.png");
	DrawUtil::Get().AddSprite("GravityImage", "textures/GravityImage.png");
	DrawUtil::Get().AddSprite("FIcon", "textures/FIcon.png");
	DrawUtil::Get().AddSprite("BIcon", "textures/BIcon.png");
	DrawUtil::Get().AddSprite("SIcon", "textures/SIcon.png");
	DrawUtil::Get().AddSprite("PIcon", "textures/PIcon.png");
	DrawUtil::Get().AddSprite("NIcon", "textures/NIcon.png");
	DrawUtil::Get().AddSprite("MenuBackground", "textures/MenuBackground.png");
	DrawUtil::Get().AddSprite("GUITextEntry", "textures/GUITextEntry.png");
	DrawUtil::Get().AddSprite("Cursor", "textures/Cursor.png");
	DrawUtil::Get().AddSprite("InputIcon", "textures/InputIcon.png");
	DrawUtil::Get().AddSprite("0Ability", "textures/0Ability.png");
	DrawUtil::Get().AddSprite("1Ability", "textures/1Ability.png");
	DrawUtil::Get().AddSprite("2Ability", "textures/2Ability.png");
	DrawUtil::Get().AddSprite("3Ability", "textures/3Ability.png");
	DrawUtil::Get().AddSprite("4Ability", "textures/4Ability.png");
	DrawUtil::Get().AddSprite("5Ability", "textures/5Ability.png");
	DrawUtil::Get().AddSprite("RuleListIcon", "textures/RuleListIcon.png");
	DrawUtil::Get().AddSprite("CopyIcon", "textures/CopyIcon.png");
	DrawUtil::Get().AddSprite("ZoomIcon", "textures/ZoomIcon.png");
	DrawUtil::Get().AddSprite("UpIcon", "textures/UpIcon.png");
	DrawUtil::Get().SetNullTexture("RemoveIcon");
	FileUtil::Get().CreateDir("players");
	FileUtil::Get().CreateDir("arenas");
	FileUtil::Get().CreateDir("adventures");
	NetworkManagerServer::InitSettingsList();
	WindowUtil::Get().SetClearColor(1.0f, 1.0f, 1.0f);
	GlobalSettingsUtil::Get().Init();
	SocketUtil::Get().Init();
	
	while(!WindowUtil::Get().ShouldWindowClose())
	{
		TimeUtil::Get().Update();
		WindowUtil::Get().PollEvents();
		WindowUtil::Get().ClearColor();
		DrawUtil::Get().Update();
		ControllerUtil::Get().Update();
		std::shared_ptr<Screen> screenBeingUpdated = ScreenNavigator::Get().GetScreen();
		if(!ScreenNavigator::Get().Update())
			break;
		if(GlobalSettingsUtil::Get().constantUpdate)
			GlobalSettingsUtil::Get().SetupSettings();
		GUIUtil::Get().Update();
		LogViewer::Get().Update();
		if(screenBeingUpdated == ScreenNavigator::Get().GetScreen()) // Check if the screen changed before drawing. Dont draw if it did change
			ScreenNavigator::Get().Draw();
		GUIUtil::Get().Draw();
		LogViewer::Get().Draw();
		DrawUtil::Get().DrawSprite(0, "Cursor", ControllerUtil::Get().GetCursorPos() - 20.0f, Vec2(40.0f), 0.0f, WHITE_IS_TRANSPARENT);
		DrawUtil::Get().DrawBatchedCommands();
		WindowUtil::Get().SwapBuffers();
	}
	SocketUtil::Get().Cleanup();
	ScreenNavigator::Get().SetScreen(nullptr);
	GUIUtil::Get().Cleanup();
	DrawUtil::Get().Cleanup();
	WindowUtil::Get().Cleanup();
	PaintScreen::ClearClipboard();
	return 0;
}
