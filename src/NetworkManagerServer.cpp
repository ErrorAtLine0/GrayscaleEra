#include "NetworkManagerServer.hpp"
#include "Screens/InLobbyScreen.hpp"
#include "Screens/GameScreen.hpp"
#include "Screens/MainMenuScreen.hpp"
#include "PropertiesList.hpp"
#include <GSEGL/Network/SocketUtil.hpp>
#include <GSEGL/Network/OutMemBitStream.hpp>
#include <GSEGL/RandGenUtil.hpp>
#include <GSEGL/LogUtil.hpp>
#include <GSEGL/FileUtil.hpp>
#include <memory>
#include "GameObjects/RealPlayer.hpp"

PropertiesList NetworkManagerServer::serverSettingsList("Server Settings");

NetworkManagerServer::NetworkManagerServer():
waitingForSignalToStart(false)
{}

void NetworkManagerServer::InitSettingsList()
{
	serverSettingsList.AddInt("Player Count", 1, 8, 1, 2);
	serverSettingsList.AddInt("Max Player Points Allowed (0 = ∞)", 0, 100000, 2000, DEFAULT_PLAYER_POINTS);
	serverSettingsList.AddInt("Game Length (min)", 0, 99, 1, 5);
}

bool NetworkManagerServer::Init(const UTF8String& initUserName, const UTF8String& initLobbyName, const WorldDataPtr& initWorldData, const PropertiesList& initServerSettings, bool initIsSeparateLobby)
{
	if(!NetworkManager::Init(initUserName, initLobbyName, DEFAULT_PORT)) return false;
	internalAddr = SocketUtil::Get().GetLocalIPv4Addr(lobbyServerAddr);
	if(!internalAddr) return false;
	internalAddr->GetAsSockAddrIn()->sin_port = htons(DEFAULT_PORT);
	if(!ServerInit(initWorldData, initServerSettings, initIsSeparateLobby)) return false;
	state = NETMAN_GETLOBBY;
	return true;
}

bool NetworkManagerServer::Init(const UTF8String& initUserName, uint16_t initPort, const WorldDataPtr& initWorldData, const PropertiesList& initServerSettings)
{
	if(!NetworkManager::Init(initUserName, initPort)) return false;
	if(!ServerInit(initWorldData, initServerSettings, true)) return false;
	state = NETMAN_INLOBBY;
	return true;
}

bool NetworkManagerServer::ServerInit(const WorldDataPtr& initWorldData, const PropertiesList& initServerSettings, bool initIsSeparateLobby)
{
	PlayerIDGenerator::Get().Init();
	playerID = PlayerIDGenerator::Get().GeneratePlayerID();
	requiredPlayerCount = initServerSettings.GetInt(0);
	worldData = initWorldData;
	if(worldData->GetType() == WORLD_ADVENTURE)
	{
		const PropertiesList& adventureSettings = std::static_pointer_cast<AdventureData>(worldData)->settings;
		gameProperties.gameTime = adventureSettings.GetInt(1);
		gameProperties.respawnTime = adventureSettings.GetInt(2);
		requestPlayer = adventureSettings.GetNPC(3) == 0;
		playerMaxPoints = adventureSettings.GetInt(4);
	}
	else
	{
		gameProperties.gameTime = initServerSettings.GetInt(2) * 60;
		gameProperties.respawnTime = ARENA_RESPAWN_TIME;
		requestPlayer = true;
		playerMaxPoints = initServerSettings.GetInt(1);
	}
	players.emplace_back(std::make_shared<PlayerNetData>());
	players.back()->userName = userName;
	players.back()->playerID = playerID;
	isSeparateLobby = initIsSeparateLobby;
	return true;
}

bool NetworkManagerServer::Update()
{
	/////////////////////
	// Receiving Stage //
	/////////////////////
	ReceivePackets();
	for(ClientProxyPtr& client : clients)
		client->deliveryNotifier->ProcessTimedOutPackets();
	while(!packets.empty())
	{
		Packet& packet = packets.front();
		InMemBitStream inStream(packet.data, packet.size, false);
		switch(state)
		{
			case NETMAN_GETLOBBY:
			{
				LobbyPacketType lobbyPacketType = LOBBYPT_SetupLobby;
				if(!inStream.ReadBits(lobbyPacketType)) return false;
				switch(lobbyPacketType)
				{
					case LOBBYPT_IncorrectString:
					{
						ErrorUtil::Get().SetError("Network", "NetworkManagerServer::Update String was too short or too long");
						return false;
					}
					case LOBBYPT_NameTaken:
					{
						ErrorUtil::Get().SetError("Network", "NetworkManagerServer::Update Lobby name already taken");
						LogUtil::Get().LogMessage("NORMAL", "Lobby name already taken");
						return false;
					}
					case LOBBYPT_AlreadyHosting:
					{
						ErrorUtil::Get().SetError("Network", "NetworkManagerServer::Update You are already hosting a server");
						LogUtil::Get().LogMessage("NORMAL", "You are already hosting a server");
						LogUtil::Get().LogMessage("NORMAL", "If you aren't, wait for 15 minutes");
						return false;
					}
					case LOBBYPT_SuccessfulCreation:
					{
						LogUtil::Get().LogMessage("NORMAL", "Lobby setup!");
						*lobbyServerAddr = packet.from;
						ScreenNavigator::Get().SetScreen(std::make_shared<InLobbyScreen>(std::make_shared<MainMenuScreen>(), shared_from_this()));
						state = NETMAN_INLOBBY;
						break;
					}
					case LOBBYPT_DifferentVersion:
					{
						ErrorUtil::Get().SetError("Network", "NetworkManagerServer::Update Playing a different version than master server");
						LogUtil::Get().LogMessage("NORMAL", "Outdated version! Please update your game.");
						return false;
					}
					default:
						break;
				}
				break;
			}
			case NETMAN_INLOBBY:
			{
				if(lobbyServerAddr && *lobbyServerAddr == packet.from)
				{
					LobbyPacketType lobbyPacketType = LOBBYPT_SetupLobby;
					if(!inStream.ReadBits(lobbyPacketType)) return false;
					if(lobbyPacketType == LOBBYPT_IncomingConnection)
					{
						SocketAddress addr;
						if(!addr.Read(inStream)) return false; 
						OutMemBitStream outStream; // Send empty packet for UDP hole punching
						outStream.WriteBits(TRANSMISSIONDATA_UNRELIABLE_HEADERLESS, ByteUtil::GetRequiredBits<TRANSMISSIONDATA_MAX>::Value);
						SendPacket(outStream, addr);
					}
				}
				else
				{
					TransmissionDataType transmissionDataType = TRANSMISSIONDATA_UNRELIABLE;
					PrivateID receivedPrivateID = 0;
					if(!inStream.ReadBits(transmissionDataType, ByteUtil::GetRequiredBits<TRANSMISSIONDATA_MAX>::Value)) return false;
					if(!inStream.ReadBits(receivedPrivateID)) return false;
					if(receivedPrivateID == CURRENT_VERSION) // New Client
					{
						if(players.size() == requiredPlayerCount) break;
						ClientProxyPtr newClient = std::make_shared<ClientProxy>(&linkingContext);
						int returnVal = newClient->deliveryNotifier->ProcessPacketHeader(inStream);
						if(returnVal == 0) return false; else if(returnVal == 1) break; 
						if(transmissionDataType == TRANSMISSIONDATA_RELIABLE)
						{
							returnVal = newClient->reliableDataManager->CheckIfNewData(inStream);
							if(returnVal == 0) return false; else if(returnVal == 1) break; 
						}
						PacketType packetType = PACKET_HELLO;
						if(!inStream.ReadBits(packetType, ByteUtil::GetRequiredBits<PACKET_MAX>::Value)) return false;
						if(packetType == PACKET_HELLO)
						{
							newClient->playerNetData = std::make_shared<PlayerNetData>();
							if(!newClient->playerNetData->userName.Read(inStream)) return false;
							// No two players may have the same name
							if(std::find_if(players.begin(), players.end(), [&newClient](const PlayerNetDataPtr& playerNetData){ return newClient->playerNetData->userName == playerNetData->userName; }) != players.end())
								break;
							newClient->playerNetData->playerID = PlayerIDGenerator::Get().GeneratePlayerID();
							newClient->privateID = GenerateUniquePrivateID();
							players.push_back(newClient->playerNetData);
							clients.push_back(newClient);
							// Packet to other clients, indicating new client arrival
							OutMemBitStream outStreamToOtherClients;
							outStreamToOtherClients.WriteBits(PACKET_PLAYERJOINED, ByteUtil::GetRequiredBits<PACKET_MAX>::Value);
							newClient->playerNetData->WriteInitInfo(outStreamToOtherClients);
							for(ClientProxyPtr& client : clients)
								client->reliableDataManager->QueueDataToSend(outStreamToOtherClients);
							// Welcome new client
							OutMemBitStream outStream;
							outStream.WriteBits(PACKET_WELCOME, ByteUtil::GetRequiredBits<PACKET_MAX>::Value);
							outStream.WriteBits(requiredPlayerCount);
							outStream.WriteBits(newClient->playerNetData->playerID);
							outStream.WriteBits(static_cast<uint8_t>(players.size()));
							bool requestPlayer = worldData->GetType() != WORLD_ADVENTURE || std::static_pointer_cast<AdventureData>(worldData)->settings.GetNPC(3) == 0;
							outStream.WriteBits(requestPlayer);
							outStream.WriteBits(playerMaxPoints);
							outStream.WriteBits(worldData->GetType(), ByteUtil::GetRequiredBits<WORLD_MAX>::Value);
							for(PlayerNetDataPtr& player : players)
								player->WriteInitInfo(outStream);
							newClient->addr = packet.from;
							newClient->reliableDataManager->QueueDataToSend(outStream);
							LogUtil::Get().LogMessage("NORMAL", "Player Joined");
						}
					}
					else if(receivedPrivateID < FIRST_VERSION)
					{
						ClientProxyPtr client = FindClientByPrivateID(receivedPrivateID);
						if(client)
						{
							if(transmissionDataType == TRANSMISSIONDATA_RELIABLE)
							{
								int returnVal = client->deliveryNotifier->ProcessPacketHeader(inStream);
								if(returnVal == 0) return false; else if(returnVal == 1) break;
								returnVal = client->reliableDataManager->CheckIfNewData(inStream);
								if(returnVal == 0) return false; else if(returnVal == 1) break;
								PacketType packetType = PACKET_HELLO;
								if(!inStream.ReadBits(packetType, ByteUtil::GetRequiredBits<PACKET_MAX>::Value)) return false;
								if(packetType == PACKET_CHATMESSAGE)
								{
									UTF8String chatMessage;
									if(chatMessage.Read(inStream))
										BroadcastMessage(0, GetFormattedChatMessage(client->playerNetData->userName, chatMessage));
								}
							}
							else if(transmissionDataType == TRANSMISSIONDATA_UNRELIABLE)
							{
								int returnVal = client->deliveryNotifier->ProcessPacketHeader(inStream);
								if(returnVal == 0) return false; else if(returnVal == 1) break;
							}
						}
					}
				}
				break;
			}
			case NETMAN_DATAEXCHANGE:
			{
				if(lobbyServerAddr && *lobbyServerAddr == packet.from)
					break;
				TransmissionDataType transmissionDataType = TRANSMISSIONDATA_UNRELIABLE;
				PrivateID receivedPrivateID = 0;
				if(!inStream.ReadBits(transmissionDataType, ByteUtil::GetRequiredBits<TRANSMISSIONDATA_MAX>::Value)) return false;
				if(!inStream.ReadBits(receivedPrivateID)) return false;
				ClientProxyPtr client = FindClientByPrivateID(receivedPrivateID);
				if(client)
				{
					int returnVal = client->deliveryNotifier->ProcessPacketHeader(inStream);
					if(returnVal == 0) return false; else if(returnVal == 1) break;
					if(transmissionDataType == TRANSMISSIONDATA_RELIABLE)
					{
						returnVal = client->reliableDataManager->CheckIfNewData(inStream);
						if(returnVal == 0) return false; else if(returnVal == 1) break;
					}
					PacketType packetType = PACKET_HELLO;
					if(!inStream.ReadBits(packetType, ByteUtil::GetRequiredBits<PACKET_MAX>::Value)) return false;
					if(packetType == PACKET_CHATMESSAGE)
					{
						UTF8String chatMessage;
						if(chatMessage.Read(inStream))
							BroadcastMessage(0, GetFormattedChatMessage(client->playerNetData->userName, chatMessage));
					}
					else if(packetType == PACKET_PLAYERDATA)
					{
						uint32_t bitsToRead = 0;
						uint8_t dataReceived[1000];
						if(!inStream.ReadBits(bitsToRead, 32)) return false;
						if(!inStream.ReadBits(static_cast<void*>(dataReceived), bitsToRead)) return false;
						playerIDToPlayerDataStream[client->playerNetData->playerID].WriteBits(static_cast<const void*>(dataReceived), bitsToRead);
					}
					else if(packetType == PACKET_PLAYERDATAFIN)
						SendPlayerDataToClients(client);
					else if(packetType == PACKET_HASALLDATA)
						client->hasAllData = true;
				}
				break;
			}
			case NETMAN_INGAME:
			{
				if(lobbyServerAddr && *lobbyServerAddr == packet.from)
					break;
				TransmissionDataType transmissionDataType = TRANSMISSIONDATA_UNRELIABLE;
				PrivateID receivedPrivateID = 0;
				if(!inStream.ReadBits(transmissionDataType, ByteUtil::GetRequiredBits<TRANSMISSIONDATA_MAX>::Value)) return false;
				if(!inStream.ReadBits(receivedPrivateID)) return false;
				ClientProxyPtr client = FindClientByPrivateID(receivedPrivateID);
				if(client)
				{
					int returnVal = client->deliveryNotifier->ProcessPacketHeader(inStream);
					if(returnVal == 0) return false; else if(returnVal == 1) break;
					if(transmissionDataType == TRANSMISSIONDATA_RELIABLE)
					{
						returnVal = client->reliableDataManager->CheckIfNewData(inStream);
						if(returnVal == 0) return false; else if(returnVal == 1) break;
					}
					PacketType packetType = PACKET_HELLO;
					if(!inStream.ReadBits(packetType, ByteUtil::GetRequiredBits<PACKET_MAX>::Value)) return false;
					if(packetType == PACKET_CHATMESSAGE)
					{
						UTF8String chatMessage;
						if(chatMessage.Read(inStream))
							BroadcastMessage(0, GetFormattedChatMessage(client->playerNetData->userName, chatMessage));
					}
					else if(packetType == PACKET_INPUT)
					{
						if(!GameSandbox::Get().GetRealPlayerInfoByPlayerID(client->playerNetData->playerID)->latestInputState.Read(inStream)) return false;
					}
				}
				break;
			}
			case NETMAN_ENDGAME:
			{
				if(lobbyServerAddr && *lobbyServerAddr == packet.from)
					break;
				TransmissionDataType transmissionDataType = TRANSMISSIONDATA_UNRELIABLE;
				PrivateID receivedPrivateID = 0;
				if(!inStream.ReadBits(transmissionDataType, ByteUtil::GetRequiredBits<TRANSMISSIONDATA_MAX>::Value)) return false;
				if(!inStream.ReadBits(receivedPrivateID)) return false;
				ClientProxyPtr client = FindClientByPrivateID(receivedPrivateID);
				if(client)
				{
					int returnVal = client->deliveryNotifier->ProcessPacketHeader(inStream);
					if(returnVal == 0) return false; else if(returnVal == 1) break;
					if(transmissionDataType == TRANSMISSIONDATA_RELIABLE)
					{
						returnVal = client->reliableDataManager->CheckIfNewData(inStream);
						if(returnVal == 0) return false; else if(returnVal == 1) break;
					}
					PacketType packetType = PACKET_HELLO;
					if(!inStream.ReadBits(packetType, ByteUtil::GetRequiredBits<PACKET_MAX>::Value)) return false;
					if(packetType == PACKET_CHATMESSAGE)
					{
						UTF8String chatMessage;
						if(chatMessage.Read(inStream))
							BroadcastMessage(0, GetFormattedChatMessage(client->playerNetData->userName, chatMessage));
					}
				}
				break;
			}
			default:
				break;
		}
		packets.pop_front();
	}
	
	///////////////////
	// Sending Stage //
	///////////////////
	// Check for any disconnecting clients
	std::vector<ClientProxyPtr> disconnectedClients;
	for(ClientProxyPtr& client : clients)
		if(!client->deliveryNotifier->IsConnected())
			disconnectedClients.emplace_back(client);
	switch(state)
	{
		case NETMAN_GETLOBBY:
		{
			if(timeLastPacketToLobbyServer + LOBBYSERVER_PACKET_TIMEOUT < TimeUtil::Get().GetTimeSinceStart())
			{
				if(lobbyServerSendAttempts > 5)
				{
					ErrorUtil::Get().SetError("Network", "NetworkManagerServer::Update Lobby server timed out");
					return false;
				}
				OutMemBitStream outStream;
				outStream.WriteBits(LOBBYPT_SetupLobby);
				outStream.WriteBits(CURRENT_VERSION, 64);
				lobbyName.Write(outStream);
				outStream.WriteBits(isSeparateLobby);
				internalAddr->Write(outStream);
				SendPacket(outStream, *lobbyServerAddr);
				++lobbyServerSendAttempts;
				timeLastPacketToLobbyServer = TimeUtil::Get().GetTimeSinceStart();
			}
			break;
		}
		case NETMAN_INLOBBY:
		{
			if(players.size() == requiredPlayerCount && !NeedsPlayerFromFile())
			{
				OutMemBitStream outStream;
				outStream.WriteBits(PACKET_GAMESTART, ByteUtil::GetRequiredBits<PACKET_MAX>::Value);
				for(ClientProxyPtr& client : clients)
					client->reliableDataManager->QueueDataToSend(outStream);
				// Start sending level data to every client
				SendWorldDataToClients();
				// Send own player data as well
				SendPlayerDataToClients(nullptr);
				if(!requestPlayer) // Send empty player datas
					for(ClientProxyPtr& client : clients)
						SendPlayerDataToClients(client);
				state = NETMAN_DATAEXCHANGE;
			}
			break;
		}
		case NETMAN_DATAEXCHANGE:
		{
			if(!waitingForSignalToStart && std::find_if(clients.begin(), clients.end(), [](const ClientProxyPtr& client){ return !client->gotPlayerData; }) == clients.end())
			{
				OutMemBitStream outStream;
				outStream.WriteBits(PACKET_HASALLDATA, ByteUtil::GetRequiredBits<PACKET_MAX>::Value);
				for(ClientProxyPtr& client : clients)
					client->reliableDataManager->QueueDataToSend(outStream);
				waitingForSignalToStart = true;
			}
			if(!disconnectedClients.empty())
			{
				waitingForSignalToStart = false;
				playerIDToPlayerDataStream.clear();
				state = NETMAN_INLOBBY;
				break;
			}
			else if(std::find_if(clients.begin(), clients.end(), [](const ClientProxyPtr& client){ return !client->hasAllData; }) == clients.end())
			{
				if(requestPlayer)
				{
					for(auto& playerDataPair : playerIDToPlayerDataStream)
					{
						InMemBitStream playerDataInStream(playerDataPair.second.GetBufferPtr(), playerDataPair.second.GetByteLength(), false);
						PlayerNetDataPtr playerNetData = GetPlayerNetDataByPlayerID(playerDataPair.first);
						if(!playerNetData->playerData->ReadFile(playerDataInStream)) return false;
						if(IsPlayerOP(playerNetData->playerData))
						{
							LogUtil::Get().LogMessage("NORMAL", "Overpowered player detected");
							OutMemBitStream outStream;
							outStream.WriteBits(PACKET_CHATMESSAGE, ByteUtil::GetRequiredBits<PACKET_MAX>::Value);
							UTF8String("Overpowered player detected").Write(outStream);
							disconnectedClients.emplace_back(FindClientByPlayerID(playerDataPair.first));
							for(ClientProxyPtr& client : clients)
								client->reliableDataManager->QueueDataToSend(outStream);
							break;
						}
					}
					if(IsPlayerOP(playerDataFromFile))
					{
						LogUtil::Get().LogMessage("NORMAL", "You chose an overpowered character!");
						ErrorUtil::Get().SetError("Game", "NetworkManagerServer::Update Overpowered player detected");
						return false;
					}
					GetPlayerNetDataByPlayerID(playerID)->playerData = playerDataFromFile;
				}
				
				if(!disconnectedClients.empty())
				{
					waitingForSignalToStart = false;
					playerIDToPlayerDataStream.clear();
					state = NETMAN_INLOBBY;
					break;
				}
				ScreenNavigator::Get().SetScreen(std::make_shared<GameScreen>(std::make_shared<MainMenuScreen>(), gameProperties, worldData, playerID, shared_from_this()));
				for(const PlayerNetDataPtr& player : players)
				{
					// Currently, adventures simply override chosen players.
					NPCID playerNPC = worldData->GetType() == WORLD_ADVENTURE ? std::static_pointer_cast<AdventureData>(worldData)->settings.GetNPC(3) : 0;
					if(playerNPC)
						GameSandbox::Get().AddPlayer(player->userName, playerNPC, player->playerID);
					else
						GameSandbox::Get().AddPlayer(player->userName, player->playerData, player->playerID);
				}
				
				OutMemBitStream outStream;
				outStream.WriteBits(PACKET_GAMESTART, ByteUtil::GetRequiredBits<PACKET_MAX>::Value);
				outStream.WriteFixedFloat(GameSandbox::Get().GetTimeLeftInSeconds(), 0.0f, 0.1f);
				for(ClientProxyPtr& client : clients)
					client->reliableDataManager->QueueDataToSend(outStream);
				
				ClearAllPlayerRawData();
				state = NETMAN_INGAME;
			}
			break;
		}
		case NETMAN_INGAME:
		{
			GameSandbox::Get().GetRealPlayerInfoByPlayerID(playerID)->latestInputState = InputManager::Get().GetCurrentInputState();
			
			if(GameSandbox::Get().HasGameEnded())
			{
				OutMemBitStream outStream;
				outStream.WriteBits(PACKET_ENDGAME, ByteUtil::GetRequiredBits<PACKET_MAX>::Value);
				for(ClientProxyPtr& client : clients)
					client->reliableDataManager->QueueDataToSend(outStream);
				state = NETMAN_ENDGAME;
			}
			for(ClientProxyPtr& disconnectedClient : disconnectedClients)
			{
				LogUtil::Get().LogMessage("NORMAL", disconnectedClient->playerNetData->userName + " left the game");
				GameSandbox::Get().RemovePlayer(disconnectedClient->playerNetData->playerID);
			}
			break;
		}
		case NETMAN_ENDGAME:
		{
			for(ClientProxyPtr& disconnectedClient : disconnectedClients)
				LogUtil::Get().LogMessage("NORMAL", disconnectedClient->playerNetData->userName + " left the game");
			break;
		}
		default:
			break;
	}
	// Remove disconnected clients, if any
	for(ClientProxyPtr& disconnectedClient : disconnectedClients)
		clients.erase(std::remove(clients.begin(), clients.end(), disconnectedClient));
	for(ClientProxyPtr& disconnectedClient : disconnectedClients)
	{
		OutMemBitStream outStream;
		outStream.WriteBits(PACKET_PLAYERDISCONNECTED, ByteUtil::GetRequiredBits<PACKET_MAX>::Value);
		outStream.WriteBits(disconnectedClient->playerNetData->playerID);
		for(ClientProxyPtr& client : clients)
			client->reliableDataManager->QueueDataToSend(outStream);
		players.erase(std::remove(players.begin(), players.end(), disconnectedClient->playerNetData), players.end());
	}
	
	for(ClientProxyPtr& client : clients)
	{
		if(client->timeLastPacketSent + client->deliveryNotifier->GetPacketSendRate(state == NETMAN_DATAEXCHANGE ? EXCHANGE_PACKET_RATE : MIN_PACKET_RATE) < TimeUtil::Get().GetTimeSinceStart())
		{
			OutMemBitStream outStream;
			if(client->reliableDataManager->ContainsDataToWrite())
			{
				outStream.WriteBits(TRANSMISSIONDATA_RELIABLE, ByteUtil::GetRequiredBits<TRANSMISSIONDATA_MAX>::Value);
				outStream.WriteBits(client->privateID);
				InFlightPacket* inFlightPacket = client->deliveryNotifier->WritePacketHeader(outStream);
				client->reliableDataManager->WriteDataToPacket(outStream, inFlightPacket);
			}
			else if(state != NETMAN_INGAME)
			{
				outStream.WriteBits(TRANSMISSIONDATA_UNRELIABLE, ByteUtil::GetRequiredBits<TRANSMISSIONDATA_MAX>::Value);
				outStream.WriteBits(client->privateID);
				client->deliveryNotifier->WritePacketHeader(outStream);
				outStream.WriteBits(PACKET_ALIVE, ByteUtil::GetRequiredBits<PACKET_MAX>::Value);
			}
			else
			{
				outStream.WriteBits(TRANSMISSIONDATA_REPLICATION, ByteUtil::GetRequiredBits<TRANSMISSIONDATA_MAX>::Value);
				outStream.WriteBits(client->privateID);
				InFlightPacket* inFlightPacket = client->deliveryNotifier->WritePacketHeader(outStream);
				client->replicationManager->WriteBatchedCommands(outStream, inFlightPacket);
			}
			SendPacket(outStream, client->addr);
			client->timeLastPacketSent = TimeUtil::Get().GetTimeSinceStart();
		}
	}
	return true;
}

UTF8String NetworkManagerServer::GetFormattedChatMessage(const UTF8String& userName, const UTF8String& chatMessage)
{
	return userName + UTF8String(": ") + chatMessage;
}

void NetworkManagerServer::SendChatMessage(const UTF8String& chatMessage)
{
	BroadcastMessage(0, GetFormattedChatMessage(userName, chatMessage));
}

void NetworkManagerServer::SendWorldDataToClients()
{
	OutMemBitStream outStream;
	worldData->Write(outStream);
	for(uint32_t i = 0; i < outStream.GetBitLength(); i += 8000)
	{
		OutMemBitStream singlePacket;
		singlePacket.WriteBits(PACKET_WORLDDATA, ByteUtil::GetRequiredBits<PACKET_MAX>::Value);
		if(i + 8000 > outStream.GetBitLength())
		{
			singlePacket.WriteBits(outStream.GetBitLength() - i, 32);
			singlePacket.WriteBits(static_cast<const void*>(outStream.GetBufferPtr()+(i/8)), outStream.GetBitLength() - i);
		}
		else
		{
			singlePacket.WriteBits(8000, 32);
			singlePacket.WriteBits(static_cast<const void*>(outStream.GetBufferPtr()+(i/8)), 8000);
		}
		for(ClientProxyPtr& client : clients)
			client->reliableDataManager->QueueDataToSend(singlePacket);
	}
}

void NetworkManagerServer::SendPlayerDataToClients(const ClientProxyPtr& playerOwner)
{
	if(!requestPlayer)
	{
		if(playerOwner)
			playerOwner->gotPlayerData = true;
		return;
	}
	OutMemBitStream outStream;
	if(playerOwner)
		outStream = playerIDToPlayerDataStream[playerOwner->playerNetData->playerID];
	else // Then this is the host's playerdata
		playerDataFromFile->WriteFile(outStream);
	for(uint32_t i = 0; i < outStream.GetBitLength(); i += 8000)
	{
		OutMemBitStream singlePacket;
		singlePacket.WriteBits(PACKET_PLAYERDATA, ByteUtil::GetRequiredBits<PACKET_MAX>::Value);
		singlePacket.WriteBits(playerOwner ? playerOwner->playerNetData->playerID : playerID);
		if(i + 8000 > outStream.GetBitLength())
		{
			singlePacket.WriteBits(outStream.GetBitLength() - i, 32);
			singlePacket.WriteBits(static_cast<const void*>(outStream.GetBufferPtr()+(i/8)), outStream.GetBitLength() - i);
		}
		else
		{
			singlePacket.WriteBits(8000, 32);
			singlePacket.WriteBits(static_cast<const void*>(outStream.GetBufferPtr()+(i/8)), 8000);
		}
		for(ClientProxyPtr& client : clients)
			if(client != playerOwner)
				client->reliableDataManager->QueueDataToSend(singlePacket);
	}
	if(playerOwner)
		playerOwner->gotPlayerData = true;
}

void NetworkManagerServer::BroadcastMessage(PlayerID playerIDToSendTo, const UTF8String& message)
{
	OutMemBitStream outStream;
	outStream.WriteBits(PACKET_CHATMESSAGE, ByteUtil::GetRequiredBits<PACKET_MAX>::Value);
	message.Write(outStream);
	for(ClientProxyPtr& client : clients)
	{
		if(playerIDToSendTo == 0 || client->playerNetData->playerID == playerIDToSendTo)
			client->reliableDataManager->QueueDataToSend(outStream);
	}
	if(playerIDToSendTo == 0 || playerID == playerIDToSendTo)
		LogUtil::Get().LogMessage("NORMAL", message);
}

void NetworkManagerServer::BroadcastAnnouncement(PlayerID playerIDToSendTo, const UTF8String& message)
{
	OutMemBitStream outStream;
	outStream.WriteBits(PACKET_ANNOUNCEMENT, ByteUtil::GetRequiredBits<PACKET_MAX>::Value);
	message.Write(outStream);
	for(ClientProxyPtr& client : clients)
		if(playerIDToSendTo == 0 || client->playerNetData->playerID == playerIDToSendTo)
			client->reliableDataManager->QueueDataToSend(outStream);
}

PrivateID NetworkManagerServer::GenerateUniquePrivateID()
{
	PrivateID toRet = 0;
	do
	{
		toRet = RandGenUtil::Get().GetRandomUInt64(1, FIRST_VERSION-1);
	} while(FindClientByPrivateID(toRet));
	return toRet;
}

void NetworkManagerServer::CreateObject(GameObject* gameObj, bool sendToClients)
{
	NetworkID netID = linkingContext.GetNetID(gameObj, true);
	if(sendToClients)
		for(ClientProxyPtr& client : clients)
			client->replicationManager->BatchCreate(netID, gameObj->GetChangedProperties());
	gameObj->ClearChangedProperties();
}

void NetworkManagerServer::UpdateObject(GameObject* gameObj, bool sendToClients)
{
	NetworkID netID = linkingContext.GetNetID(gameObj, false);
	if(netID && sendToClients)
		for(ClientProxyPtr& client : clients)
			client->replicationManager->BatchUpdate(netID, gameObj->GetChangedProperties());
	gameObj->ClearChangedProperties();
}

void NetworkManagerServer::DestroyObject(GameObject* gameObj, bool sendToClients)
{
	NetworkID netID = linkingContext.GetNetID(gameObj, false);
	if(netID)
	{
		if(gameObj->GetClassID() == RealPlayer::classID && GetPlayerNetDataByPlayerID(static_cast<Player*>(gameObj)->GetPlayerID()))  // Record player death
		{
			Player* playerPtr = static_cast<Player*>(gameObj);
			OutMemBitStream outStream;
			outStream.WriteBits(PACKET_KILL, ByteUtil::GetRequiredBits<PACKET_MAX>::Value);
			outStream.WriteBits(playerPtr->GetLastHitter());
			outStream.WriteBits(playerPtr->GetPlayerID());
			for(ClientProxyPtr& client : clients)
				client->reliableDataManager->QueueDataToSend(outStream);
			RecordDeath(playerPtr->GetLastHitter(), playerPtr->GetPlayerID());
		}
		if(sendToClients)
			for(ClientProxyPtr& client : clients)
				client->replicationManager->BatchDestroy(netID);
		linkingContext.RemoveGameObject(gameObj);
	}
}

double NetworkManagerServer::GetPlayerEstimatedSendRate(PlayerID playerID)
{
	ClientProxyPtr client = FindClientByPlayerID(playerID);
	if(client)
		return client->deliveryNotifier->GetEstimatedSendRate();
	else
		return 0.0;
}

double NetworkManagerServer::GetPlayerRTT(PlayerID playerID)
{
	ClientProxyPtr client = FindClientByPlayerID(playerID);
	if(client)
		return client->deliveryNotifier->GetRTT();
	else
		return 0.0;
}

ClientProxyPtr NetworkManagerServer::FindClientByAddress(const SocketAddress& addr)
{
	auto it = std::find_if(clients.begin(), clients.end(), [&addr](const ClientProxyPtr& client){return client->addr == addr;});
	if(it != clients.end())
		return (*it);
	return nullptr;
}

ClientProxyPtr NetworkManagerServer::FindClientByPlayerID(PlayerID playerID)
{
	auto it = std::find_if(clients.begin(), clients.end(), [&playerID](const ClientProxyPtr& client){return client->playerNetData->playerID == playerID;});
	if(it != clients.end())
		return (*it);
	return nullptr;
}

ClientProxyPtr NetworkManagerServer::FindClientByPrivateID(PrivateID privateID)
{
	auto it = std::find_if(clients.begin(), clients.end(), [&privateID](const ClientProxyPtr& client){return client->privateID == privateID;});
	if(it != clients.end())
		return (*it);
	return nullptr;
}

NetworkManagerServer::~NetworkManagerServer()
{
	if(lobbyServerAddr) // Attempt to explicitly close server
	{
		OutMemBitStream outStream;
		outStream.WriteBits(LOBBYPT_ExplicitlyCloseLobby);
		outStream.WriteBits(CURRENT_VERSION, 64);
		SendPacket(outStream, *lobbyServerAddr);
	}
}
