#pragma once
#include <GSEGL/Network/InMemBitStream.hpp>
#include <GSEGL/Network/OutMemBitStream.hpp>
#include <GSEGL/Graphics/Texture.hpp>
#include <vector>
#include <GSEGL/Graphics/Math.hpp>

class Frame
{
	public:
		Frame();
		Frame(uint32_t initSize, uint16_t initMsLength);
		Frame(const Frame& frame);
		void operator=(const Frame& frame);
		~Frame();
		void Write(OutMemBitStream& outStream) const;
		bool Read(InMemBitStream& inStream);
		static void WriteAnimation(OutMemBitStream& outStream, const std::vector<Frame>& animation);
		static bool ReadAnimation(InMemBitStream& inStream, std::vector<Frame>& animation);
		
		static void InitAnimationTextures(std::vector<TexturePtr>& vectorToInitTo, const std::vector<Frame>& animation, const UVec2& textureSize);
		
		uint32_t GetSize() const;
		const uint8_t* GetData() const;
		uint8_t* GetMutableDataPtr();
		uint16_t GetMsLength() const;
		
		void SetData(const uint8_t* data);
		void SetMsLength(uint16_t msLength);
	private:
		uint32_t size;
		uint8_t* data;
		uint16_t msLength;
};
