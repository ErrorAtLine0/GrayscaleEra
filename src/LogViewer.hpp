#pragma once
#include <GSEGL/UTF8String.hpp>
#include <deque>

class LogViewer
{
	public:
		static LogViewer& Get();
		void Update();
		void Draw();
		void DisplayAllMessages();
		void DisplayRecentMessages();
		
	private:
		class LoggedMessage
		{
			public:
				LoggedMessage(const UTF8String& initMessage);
				const UTF8String& GetMess() const;
				float GetAlpha() const;
			private:
				UTF8String message;
				double timeLogged;
		};
		bool displayAllMessages;
		std::deque<LoggedMessage> loggedMessages;
};
