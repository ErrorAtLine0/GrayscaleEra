#pragma once
#include "WorldData.hpp"
#include "LevelData.hpp"
#include "BlockData.hpp"
#include "PlayerData.hpp"
#include <vector>

#define ADVENTURE_TEXTURE_SIZE 100
#define ADVENTURE_ROOM_MAX 20

struct AdventureData : public WorldData
{
	AdventureData();

	std::vector<LevelData> rooms;
	std::vector<BlockData> blocks;
	std::vector<PlayerData> npcs;
	std::vector<PointData> points;
	PropertiesList settings;
	Frame image;
	
	WorldType GetType() const { return WORLD_ADVENTURE; }
	void Write(OutMemBitStream& outStream) const;
	bool Read(InMemBitStream& inStream);
	static std::shared_ptr<Frame> ReadImage(InMemBitStream& inStream);
	
	void RemoveBlock(BlockID blockToRemove);
	void RemoveNPC(NPCID npcToRemove);
	void RemovePoint(PointID pointToRemove);
};

typedef std::shared_ptr<AdventureData> AdventureDataPtr;
