#pragma once
#include <GSEGL/Graphics/Math.hpp>
#include <GSEGL/Network/InMemBitStream.hpp>
#include <GSEGL/Network/OutMemBitStream.hpp>

struct InputState
{
	InputState();
	float HorizontalMovement;
	bool WPressed;
	bool QPressed;
	bool EPressed;
	bool LeftClickPressed;
	bool RightClickPressed;
	bool TriggerPressed;
	Vec2 cursorDirection;
	void Write(OutMemBitStream& outStream) const;
	bool Read(InMemBitStream& inStream);
};

class InputManager
{
	public:
		static InputManager& Get();
		void Update();
		const InputState& GetCurrentInputState() const;
		const InputState& GetAverageInputState() const;
		void ClearAveragedInputState();
		void ClearCurrentInputState();
	private:
		InputState averagedInputState;
		InputState currentInputState;
};
