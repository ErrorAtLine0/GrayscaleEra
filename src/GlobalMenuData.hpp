#pragma once
#include <GSEGL/TimeUtil.hpp>
#include <GSEGL/Graphics/DrawUtil.hpp>
#include <GSEGL/Graphics/WindowUtil.hpp>

class GlobalMenuData
{
	public:
		static GlobalMenuData& Get()
		{
			static GlobalMenuData sInstance;
			return sInstance;
		}
		void DrawBackground()
		{
			backgroundScroll -= TimeUtil::Get().GetDeltaTime() * 0.03f;
			if(backgroundScroll <= -1.0f)
				backgroundScroll = 0.0f;
			for(float i = backgroundScroll * 2400.0f; i <= WindowUtil::Get().GetWindowSize().x; i += 2400.0f)
				DrawUtil::Get().DrawSprite(11, "MenuBackground", Vec2(i, 0.0f), Vec2(2400.0f, 600.0f));
		}
	private:
		GlobalMenuData():
		backgroundScroll(0.0f)
		{}
		float backgroundScroll;
};
