#pragma once
#include "PropertiesList.hpp"

class PointData
{
	public:
		PointData();
		
		void Write(OutMemBitStream& outStream) const;
		bool Read(InMemBitStream& inStream);
		
		PropertiesList propList;
};
