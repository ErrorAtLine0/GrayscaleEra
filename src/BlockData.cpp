#include "BlockData.hpp"
#define BLOCK_HEADER_INT 3236413783
#include <GSEGL/ErrorUtil.hpp>
#include "PropertyDecoderUtil.hpp"

BlockData::BlockData():
propList("Block Properties")
{
	SetupPropertiesList();
}

BlockData::BlockData(const std::vector<Frame>& initAnimation):
propList("Block Properties"),
animation(initAnimation)
{
	SetupPropertiesList();
}

void BlockData::Write(OutMemBitStream& outStream) const
{
	Frame::WriteAnimation(outStream, animation);
	propList.Write(outStream);
}

bool BlockData::Read(InMemBitStream& inStream)
{
	if(!Frame::ReadAnimation(inStream, animation)) return false;
	if(!propList.Read(inStream)) return false;
	return true;
}

void BlockData::WriteFile(OutMemBitStream& outStream) const
{
	OutMemBitStream firstStream;
	firstStream.WriteBits(BLOCK_HEADER_INT, 32);
	Frame::WriteAnimation(firstStream, animation);
	propList.Write(firstStream);
	outStream.WriteCompressedData(firstStream.GetBufferPtr(), firstStream.GetByteLength());
}

bool BlockData::ReadFile(InMemBitStream& inStream)
{
	std::shared_ptr<InMemBitStream> firstStream = inStream.ReadCompressedData();
	if(!firstStream) return false;
	uint32_t blockHeader = 0;
	if(!firstStream->ReadBits(blockHeader)) return false;
	if(blockHeader != BLOCK_HEADER_INT)
	{
		ErrorUtil::Get().SetError("Memory", "BlockData::ReadCompressed Data doesn't have a block header");
		return false;
	}
	if(!Frame::ReadAnimation(*firstStream, animation)) return false;
	if(!propList.Read(*firstStream)) return false;
	return true;
}

std::shared_ptr<Frame> BlockData::ReadFileImage(InMemBitStream& inStream)
{
	std::shared_ptr<InMemBitStream> firstStream = inStream.ReadCompressedData();
	if(!firstStream) return nullptr;
	uint32_t blockHeader = 0;
	if(!firstStream->ReadBits(blockHeader)) return nullptr;
	if(blockHeader != BLOCK_HEADER_INT)
	{
		ErrorUtil::Get().SetError("Memory", "BlockData::ReadFileImage Data doesn't have a block header");
		return nullptr;
	}
	std::vector<Frame> readAnimation;
	if(!Frame::ReadAnimation(*firstStream, readAnimation)) return nullptr;
	return readAnimation.empty() ? nullptr : std::make_shared<Frame>(readAnimation[0]);
}

void BlockData::SetupPropertiesList()
{
	PropertiesList movementProperties("Movement");
	movementProperties.AddInt("Range X", -100, 100, 1);
	movementProperties.AddInt("Range Y", -100, 100, 1);
	movementProperties.AddInt("Speed", 0, 500, 2);
	movementProperties.AddBool("Teleport After Movement");
	propList.AddList("Movement", movementProperties);
	propList.AddList("Status Effects", PropertyDecoderUtil::GetStatusEffectList());
}
