#include "../GameSandbox.hpp"
#include "Point.hpp"
#include "RealPlayer.hpp"
#include "Block.hpp"
#include "NPC.hpp"
#include "../PropertyDecoderUtil.hpp"

Point::Point(Vec2 initPos, int32_t initRoom, const PointData* initData):
pos(initPos),
room(initRoom),
block(nullptr),
data(initData)
{}

Vec2 Point::GetPos() const { return pos; }
int32_t Point::GetRoom() const { return room; }
const PointData* Point::GetData() const { return data; }

void Point::SetNumber(const UTF8String& name, uint32_t val)
{
	return PropertyDecoderUtil::SetNumberInMap(numbers, name, val);
}

uint32_t Point::GetNumber(const UTF8String& name) const
{
	return PropertyDecoderUtil::GetNumberInMap(numbers, name);
}

uint32_t* Point::GetNumberPtr(const UTF8String& name)
{
	return PropertyDecoderUtil::GetNumberPtrInMap(numbers, name);
}

void Point::ServerUpdate()
{
	const RuleList& ruleList = data->propList.GetRuleList(1);
	for(uint32_t i = 0; i < ruleList.GetListSize(); i++)
	{
		const Rule& rule = ruleList.GetRuleAt(i);
		bool executeResults = true;
		for(uint32_t i = 0; i < rule.conditionIndices.size(); i++)
		{
			executeResults = IsConditionTrue(rule.conditionIndices[i], rule.conditionProperties[i]);
			if(!executeResults)
				break;
		}
		if(executeResults)
			for(uint32_t i = 0; i < rule.resultIndices.size(); i++)
				ExecuteResult(rule.resultIndices[i], rule.resultProperties[i]);
	}
}

bool Point::IsConditionTrue(uint32_t condIndex, const HeadlineProperty* condProp)
{
	switch(condIndex)
	{
		case 0: // Adventure Just Started
		{
			return GameSandbox::Get().IsFirstUpdate();
		}
		case 1: // Check Global Number
		{
			return PropertyDecoderUtil::CheckGlobalNumber(condProp, PROPERTYDECODEROBJECT_POINT, this);
		}
		case 2: // Check Private Number
		{
			return PropertyDecoderUtil::CheckPrivateNumber(condProp, PROPERTYDECODEROBJECT_POINT, this, PROPERTYDECODEROBJECT_POINT, this);
		}
		case 3: // Check For Player
		{
			return PropertyDecoderUtil::CheckForPlayer(condProp, pos, room, PROPERTYDECODEROBJECT_POINT, this);
		}
		case 4: // Every X Milliseconds
		{
			return PropertyDecoderUtil::CheckTimer(condProp, propTimers);
		}
		case 5: // Random Percentage
		{
			return PropertyDecoderUtil::CheckRandomPercentage(condProp);
		}
	}
	return false;
}

void Point::ExecuteResult(uint32_t resIndex, const HeadlineProperty* resProp)
{
	switch(resIndex)
	{
		case 0: // Set Global Number
		{
			PropertyDecoderUtil::SetGlobalNumber(resProp, PROPERTYDECODEROBJECT_POINT, this);
			break;
		}
		case 1: // Set Private Number
		{
			PropertyDecoderUtil::SetPrivateNumber(resProp, PROPERTYDECODEROBJECT_POINT, this, PROPERTYDECODEROBJECT_POINT, this);
			break;
		}
		case 2: // Set Player Private Number
		{
			PropertyDecoderUtil::SetPlayerPrivateNumber(resProp, pos, room, PROPERTYDECODEROBJECT_POINT, this);
			break;
		}
		case 3: // Place Block
		{
			const PropertiesList& blockData = static_cast<const ListProperty*>(resProp)->data;
			if(blockData.GetBlock(0) == 0)
				break;
			if(block)
			{
				if(static_cast<Block*>(block)->GetBlockID() == blockData.GetBlock(0)-1) // Dont bother respawning the same block
					break;
				else
					block->ForceDestroy();
			}
			block = new Block(pos, blockData.GetBlock(0)-1, blockData.GetBool(1), room);
			GameSandbox::Get().SummonObject(block);
			break;
		}
		case 4: // Remove placed block
		{
			if(block)
			{
				block->ForceDestroy();
				block = nullptr;
			}
			break;
		}
		case 5: // Summon NPC
		{
			const PropertiesList& npcData = static_cast<const ListProperty*>(resProp)->data;
			if(npcData.GetNPC(0))
			{
				NPC* newNPC = new NPC(pos, GameSandbox::Get().GetNPCPropListIDByNPCPropList(&npcData), &npcData, room);
				newNPC->SetNumber(TEAM_NUMBER_NAME, GetNumber(TEAM_NUMBER_NAME));
				GameSandbox::Get().SummonObject(newNPC);
			}
			break;
		}
		case 6: // TP Player To Point
		{
			PropertyDecoderUtil::TPPlayerToPoint(resProp, pos, room, PROPERTYDECODEROBJECT_POINT, this);
			break;
		}
		case 7: // Set Player's Spawn
		{
			PropertyDecoderUtil::SetPlayerSpawnPoint(resProp, pos, room, PROPERTYDECODEROBJECT_POINT, this);
			break;
		}
		case 8: // Print Message
		{
			PropertyDecoderUtil::SendMessage(resProp, pos, room, PROPERTYDECODEROBJECT_POINT, this);
			break;
		}
		case 9: // Print Announcement
		{
			PropertyDecoderUtil::SetAnnouncement(resProp, pos, room, PROPERTYDECODEROBJECT_POINT, this);
			break;
		}
		case 10: // Set Player Effects
		{
			const PropertiesList& propList = static_cast<const ListProperty*>(resProp)->data;
			std::vector<RealPlayer*> players;
			PropertyDecoderUtil::GetPlayers(&players, propList.GetList(0), pos, room, PROPERTYDECODEROBJECT_POINT, this);
			for(RealPlayer* player : players)
				player->DealEffect(propList.GetList(1));
			break;
		}
		case 11: // End Game
		{
			GameSandbox::Get().EndGame();
			break;
		}
	}
}
