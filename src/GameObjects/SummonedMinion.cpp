#include "../GameSandbox.hpp"
#include "SummonedMinion.hpp"
#include "Player.hpp"
#include "RealPlayer.hpp"
#include "NPC.hpp"
#include "Block.hpp"
#include "Projectile.hpp"

ClassID SummonedMinion::classID;

SummonedMinion::SummonedMinion():
Entity::Entity(Vec2(0.0f), Vec2(100.0f), Vec2(0.0f), Vec2(0.0f), Vec2(100.0f), -1)
{}

SummonedMinion::SummonedMinion(const Vec2& initPosition, PlayerID initOwnerID, uint32_t initTeam, const PropertiesList& initPropList, const GraphicsData& initGraphicsData, const GraphicsData& initProjGraphicsData, int32_t initRoom):
Entity::Entity(initPosition, Vec2(100.0f), Vec2(0.0f), Vec2(0.0f), Vec2(100.0f), initRoom),
ownerID(initOwnerID),
team(initTeam),
timeSummoned(TimeUtil::Get().GetTimeSinceStart()),
lastBuff(TimeUtil::Get().GetTimeSinceStart()),
lastShot(TimeUtil::Get().GetTimeSinceStart()),
propList(&initPropList),
projGraphicsData(initProjGraphicsData)
{
	SetAnimation(initGraphicsData);
}

bool SummonedMinion::Read(InMemBitStream& inStream)
{
	if(!Entity::Read(inStream)) return false;
	if(changedProperties & ENTITY_CONSTRUCT)
	{
		if(!inStream.ReadBits(ownerID)) return false;
		if(!inStream.ReadBits(team)) return false;
	}
	return true;
}

void SummonedMinion::Write(OutMemBitStream& outStream, PropertyBitField propertyBitField) const
{
	Entity::Write(outStream, propertyBitField);
	if(propertyBitField & ENTITY_CONSTRUCT)
	{
		outStream.WriteBits(ownerID);
		outStream.WriteBits(team);
	}
}

void SummonedMinion::ClientUpdate()
{
	Entity::ClientUpdate();
}

void SummonedMinion::ServerUpdate()
{
	changedProperties = 0;
	bool applyBuffThisFrame = false;
	int32_t buffPer5Seconds = propList->GetInt(4);
	if(buffPer5Seconds != 0 && lastBuff + 5.0/buffPer5Seconds <= TimeUtil::Get().GetTimeSinceStart())
	{
		applyBuffThisFrame = true;
		lastBuff = TimeUtil::Get().GetTimeSinceStart();
	}
	for(Player* playerPtr : GameSandbox::Get().GetAllPlayerObjects())
	{
		if(playerPtr->GetRoom() == room)
		{
			if(applyBuffThisFrame)
			{
				float buffRange = propList->GetInt(1) * 40.0f;
				if(BoxToBoxCollision(playerPtr->GetCurrentPosition(), playerPtr->GetSize(), currentPosition - Vec2(buffRange), Vec2(100.0f) + Vec2(buffRange * 2.0f)))
				{
					if(playerPtr->GetPlayerID() == ownerID || playerPtr->GetTeam() == team) // Buff your owner/team
						playerPtr->DealBuff(propList->GetList(2), true, ownerID);
					else
						playerPtr->DealDebuff(propList->GetList(3), true, ownerID);
				}
			}
			if(playerPtr->GetPlayerID() != ownerID && playerPtr->GetTeam() != team && propList->GetBool(6))
			{
				int32_t shotPer5Seconds = propList->GetInt(11);
				if(shotPer5Seconds != 0 && lastShot + 5.0/shotPer5Seconds <= TimeUtil::Get().GetTimeSinceStart())
				{
					float detectRange = propList->GetInt(8) * 40.0f;
					if(BoxToBoxCollision(playerPtr->GetCurrentPosition(), playerPtr->GetSize(), currentPosition - Vec2(detectRange), Vec2(100.0f) + Vec2(detectRange * 2.0f)))
					{
						ProjectileProperties projProperties;
						projProperties.debuffOnHit = &propList->GetList(7);
						projProperties.size = 1;
						projProperties.speed = propList->GetInt(9);
						projProperties.lifetime = propList->GetInt(10);
						Vec2 projDirection = Normalize(playerPtr->GetCurrentPosition() - currentPosition);
						GameSandbox::Get().SummonObject(new Projectile(currentPosition+Vec2(1.0f), projDirection, ownerID, team, projProperties, projGraphicsData, room));
						lastShot = TimeUtil::Get().GetTimeSinceStart();
					}
				}
			}
		}
	}
	if(timeSummoned + propList->GetInt(5) <= TimeUtil::Get().GetTimeSinceStart())
		isSetToDestroy = true;
	Entity::ServerUpdate();
}

void SummonedMinion::SimpleColRes(Entity* entity, const Vec2& penetration)
{}

void SummonedMinion::VelocityColRes(Entity* entity, const Vec2& intersectionFractions)
{}
