#include "../GameSandbox.hpp"
#include "RealPlayer.hpp"
#include "../PropertyDecoderUtil.hpp"
#include "NPC.hpp"

ClassID RealPlayer::classID;

RealPlayer::RealPlayer()
{ }

RealPlayer::RealPlayer(const Vec2& initPosition, const PlayerInfoPtr& initPlayerInfo, const PlayerData* initPlayerData, int32_t initRoom):
Player::Player(initPosition, initPlayerInfo->playerID, &initPlayerInfo->latestInputState, &initPlayerInfo->userName, initPlayerData, initRoom),
playerInfoWeak(initPlayerInfo)
{
	SetAnimation(GetPlayerGraphicsData(0, 0, true, false));
}

void RealPlayer::ClientUpdate()
{
	Player::ClientUpdate();
}

void RealPlayer::ServerUpdate()
{
	if(playerInfoWeak.expired())
	{
		isSetToDestroy = true;
		return;
	}
	PlayerInfoPtr playerInfo = playerInfoWeak.lock();
	if(playerInfo)
	{
		playerInfo->ping = playerID != GameSandbox::Get().GetOwnPlayerID() ? GameSandbox::Get().GetRTTServer(playerID) : 0.0f;
		if(playerInfo->ping > 2.0f)
			playerInfo->ping = 2.0f;
	}
	Player::ServerUpdate();
}

void RealPlayer::Write(OutMemBitStream& outStream, PropertyBitField propertyBitField) const
{
	Player::Write(outStream, propertyBitField);
	float pingToWrite = playerInfoWeak.expired() ? 0.0f : playerInfoWeak.lock()->ping;
	outStream.WriteFixedFloat(pingToWrite, 0.0f, 0.001f, 11);
}

bool RealPlayer::Read(InMemBitStream& inStream)
{
	if(!Player::Read(inStream)) return false;
	if(changedProperties & ENTITY_CONSTRUCT)
	{
		playerInfoWeak = GameSandbox::Get().GetRealPlayerInfoByPlayerID(playerID);
		playerData = GameSandbox::Get().GetPlayerDataByPlayerID(playerID);
		userName = &playerInfoWeak.lock()->userName;
		latestInputState = &playerInfoWeak.lock()->latestInputState;
		InitPlayerSize();
	}
	float readPing = 0.0f;
	if(!inStream.ReadFixedFloat(readPing, 0.0f, 0.001f, 11)) return false;
	if(!playerInfoWeak.expired())
		playerInfoWeak.lock()->ping = readPing;
	return true;
}

void RealPlayer::ServerRuleUpdate()
{
	const RuleList& ruleList = std::static_pointer_cast<AdventureData>(GameSandbox::Get().GetWorldData())->settings.GetRuleList(5);
	for(uint32_t i = 0; i < ruleList.GetListSize(); i++)
	{
		const Rule& rule = ruleList.GetRuleAt(i);
		bool executeResults = true;
		for(uint32_t i = 0; i < rule.conditionIndices.size(); i++)
		{
			executeResults = IsConditionTrue(rule.conditionIndices[i], rule.conditionProperties[i]);
			if(!executeResults)
				break;
		}
		if(executeResults)
			for(uint32_t i = 0; i < rule.resultIndices.size(); i++)
				ExecuteResult(rule.resultIndices[i], rule.resultProperties[i]);
	}
}

bool RealPlayer::IsConditionTrue(uint32_t condIndex, const HeadlineProperty* condProp)
{
	switch(condIndex)
	{
		case 0: // Adventure Just Started
		{
			return GameSandbox::Get().IsFirstUpdate();
		}
		case 1: // Check Global Number
		{
			return PropertyDecoderUtil::CheckGlobalNumber(condProp, PROPERTYDECODEROBJECT_PLAYER, this);
		}
		case 2: // Check For Player
		{
			return PropertyDecoderUtil::CheckForPlayer(condProp, currentPosition, room, PROPERTYDECODEROBJECT_PLAYER, this);
		}
		case 3: // Distance From Point
		{
			const PropertiesList& pointDistanceData = static_cast<const ListProperty*>(condProp)->data;
			int32_t blockDistance = pointDistanceData.GetInt(1);
			const auto& pointMap = GameSandbox::Get().GetTagToPointMap();
			auto its = pointMap.equal_range(pointDistanceData.GetString(0));
			for(auto it = its.first; it != its.second; ++it)
			{
				if(Entity::BoxToBoxCollision(currentPosition - Vec2((blockDistance - 1) * 20.0f), Vec2(blockDistance * 40.0f), it->second->GetPos(), Vec2(40.0f)))
					return true;
			}
			return false;
		}
		case 4: // Distance Away From Point
		{
			const PropertiesList& pointDistanceData = static_cast<const ListProperty*>(condProp)->data;
			int32_t blockDistance = pointDistanceData.GetInt(1);
			const auto& pointMap = GameSandbox::Get().GetTagToPointMap();
			auto its = pointMap.equal_range(pointDistanceData.GetString(0));
			for(auto it = its.first; it != its.second; ++it)
			{
				if(Entity::BoxToBoxCollision(currentPosition - Vec2((blockDistance - 1) * 20.0f), Vec2(blockDistance * 40.0f), it->second->GetPos(), Vec2(40.0f)))
					return false;
			}
			return true;
		}
		case 5: // Health Above
		{
			return currentHealth > static_cast<const IntProperty*>(condProp)->data;
		}
		case 6: // Health Below
		{
			return currentHealth < static_cast<const IntProperty*>(condProp)->data;
		}
		case 7: // Every X Milliseconds
		{
			return PropertyDecoderUtil::CheckTimer(condProp, propTimers);
		}
		case 8: // Random Percentage
		{
			return PropertyDecoderUtil::CheckRandomPercentage(condProp);
		}
		case 9: // Just Died
		{
			return IsSetToDestroy();
		}
	}
	return false;
}

void RealPlayer::ExecuteResult(uint32_t resIndex, const HeadlineProperty* resProp)
{
	switch(resIndex)
	{
		case 0: // Set Global Number
		{
			PropertyDecoderUtil::SetGlobalNumber(resProp, PROPERTYDECODEROBJECT_PLAYER, this);
			break;
		}
		case 1: // Set Player Private Number
		{
			PropertyDecoderUtil::SetPlayerPrivateNumber(resProp, currentPosition, room, PROPERTYDECODEROBJECT_PLAYER, this);
			break;
		}
		case 2: // Die
		{
			isSetToDestroy = true;
			break;
		}
		case 3: // Print Message
		{
			PropertyDecoderUtil::SendMessage(resProp, currentPosition, room, PROPERTYDECODEROBJECT_PLAYER, this);
			break;
		}
		case 4: // Print Announcement
		{
			PropertyDecoderUtil::SetAnnouncement(resProp, currentPosition, room, PROPERTYDECODEROBJECT_PLAYER, this);
			break;
		}
		case 5: // Give Status Effect
		{
			DealEffect(static_cast<const ListProperty*>(resProp)->data);
			break;
		}
		case 6: // Summon NPC
		{
			const PropertiesList& npcData = static_cast<const ListProperty*>(resProp)->data;
			if(npcData.GetNPC(0))
			{
				NPC* newNPC = new NPC(currentPosition, GameSandbox::Get().GetNPCPropListIDByNPCPropList(&npcData), &npcData, room);
				newNPC->SetNumber(TEAM_NUMBER_NAME, GetTeam());
				GameSandbox::Get().SummonObject(newNPC);
			}
			break;
		}
		case 7: // TP Player To Point
		{
			PropertyDecoderUtil::TPPlayerToPoint(resProp, currentPosition, room, PROPERTYDECODEROBJECT_PLAYER, this);
			break;
		}
		case 8: // Set Player Spawn Point
		{
			PropertyDecoderUtil::SetPlayerSpawnPoint(resProp, currentPosition, room, PROPERTYDECODEROBJECT_PLAYER, this);
			break;
		}
		case 9: // End Game
		{
			GameSandbox::Get().EndGame();
			break;
		}
	}
}

void RealPlayer::SetNumber(const UTF8String& name, uint32_t val)
{
	PlayerInfoPtr playerInfo = playerInfoWeak.lock();
	if(playerInfo)
		PropertyDecoderUtil::SetNumberInMap(playerInfo->numbers, name, val);
}

uint32_t RealPlayer::GetNumber(const UTF8String& name) const
{
	PlayerInfoPtr playerInfo = playerInfoWeak.lock();
	if(playerInfo)
		return PropertyDecoderUtil::GetNumberInMap(playerInfo->numbers, name);
	return 0;
}

uint32_t* RealPlayer::GetNumberPtr(const UTF8String& name)
{
	PlayerInfoPtr playerInfo = playerInfoWeak.lock();
	if(playerInfo)
		return PropertyDecoderUtil::GetNumberPtrInMap(playerInfo->numbers, name);
	return nullptr;
}

GraphicsData RealPlayer::GetPlayerGraphicsData(uint8_t level2, uint8_t level3, bool loop, bool flip)
{
	return GraphicsData(GRAPHICS_PLAYER, playerInfoWeak.lock()->playerID, level2, level3, loop, flip);
}

const PlayerInfoWeakPtr& RealPlayer::GetPlayerInfoWeakPtr() const
{
	return playerInfoWeak;
}
