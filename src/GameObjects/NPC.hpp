#pragma once
#include "Player.hpp"
#include <unordered_map>

typedef uint32_t NPCPropListID;

class NPC : public Player
{
	public:
		CLASS_IDENTIFICATION(NPC)
		NPC();
		NPC(const Vec2& initPosition, NPCPropListID initPropListID, const PropertiesList* initPropList, int32_t initRoom);
		virtual void Write(OutMemBitStream& outStream, PropertyBitField propertyBitField) const;
		virtual bool Read(InMemBitStream& inStream);
		virtual void ServerUpdate();
		virtual void SetNumber(const UTF8String& name, uint32_t val);
		virtual uint32_t GetNumber(const UTF8String& name) const;
		virtual uint32_t* GetNumberPtr(const UTF8String& name);
		virtual void ServerRuleUpdate();
		virtual GraphicsData GetPlayerGraphicsData(uint8_t level2, uint8_t level3, bool loop, bool flip);
	protected:
		bool IsConditionTrue(uint32_t condIndex, const HeadlineProperty* condProp);
		void ExecuteResult(uint32_t resIndex, const HeadlineProperty* resProp);
		
		NPCPropListID propListID;
		const PropertiesList* propList;
		InputState npcInputState;
		NPCID npcID;
		bool firstUpdate;
		
		std::unordered_map<UTF8String, uint32_t> numbers;
};
