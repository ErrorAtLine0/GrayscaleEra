#include "../GameSandbox.hpp"
#include "Projectile.hpp"
#include "Player.hpp"
#include "RealPlayer.hpp"
#include "NPC.hpp"
#include "Block.hpp"

ClassID Projectile::classID;

ProjectileProperties::ProjectileProperties():
debuffOnHit(nullptr),
speed(0),
lifetime(0),
size(0),
homing(false),
playerPen(false),
blockPen(false)
{}

ProjectileProperties::ProjectileProperties(const PropertiesList& propList):
debuffOnHit(&propList.GetList(1)),
speed(propList.GetInt(3)),
lifetime(propList.GetInt(4)),
size(propList.GetChoice(5)),
homing(propList.GetBool(6)),
playerPen(propList.GetBool(7)),
blockPen(propList.GetBool(8))
{}

Projectile::Projectile():
Entity::Entity(Vec2(0.0f), Vec2(100.0f), Vec2(0.0f), Vec2(0.0f), Vec2(100.0f), -1)
{}

Projectile::Projectile(const Vec2& initPosition, const Vec2& initDirection, PlayerID initOwnerID, uint32_t initTeam, const ProjectileProperties& initProperties, const GraphicsData& initGraphicsData, int32_t initRoom):
Entity::Entity(initPosition, Vec2(0.0f), initDirection, Vec2(0.0f), Vec2(100.0f), initRoom),
ownerID(initOwnerID),
team(initTeam),
timeSummoned(TimeUtil::Get().GetTimeSinceStart()),
properties(initProperties)
{
	velocity *= properties.speed;
	if(properties.size == 0)
	{
		nextPosition.y += 37.5f;
		currentPosition.y += 37.5f;
		drawOffset = Vec2(-37.5f);
		size = Vec2(25.0f);
	}
	else if(properties.size == 1)
	{
		nextPosition.y += 25.0f;
		currentPosition.y += 25.0f;
		drawOffset = Vec2(-25.0f);
		size = Vec2(50.0f);
	}
	else if(properties.size == 2)
	{
		drawOffset = Vec2(0.0f);
		size = Vec2(100.0f);
	}
	SetAnimation(initGraphicsData);
}

bool Projectile::Read(InMemBitStream& inStream)
{
	if(!Entity::Read(inStream)) return false;
	if(changedProperties & ENTITY_CONSTRUCT)
	{
		if(!inStream.ReadBits(ownerID)) return false;
		if(!inStream.ReadBits(team)) return false;
	}
	return true;
}

void Projectile::Write(OutMemBitStream& outStream, PropertyBitField propertyBitField) const
{
	Entity::Write(outStream, propertyBitField);
	if(propertyBitField & ENTITY_CONSTRUCT)
	{
		outStream.WriteBits(ownerID);
		outStream.WriteBits(team);
	}
}

void Projectile::ClientUpdate()
{
	Entity::ClientUpdate();
}

void Projectile::ServerUpdate()
{
	changedProperties = 0;
	if(properties.homing) // Homing
	{
		Vec2 relativePos(250.0f);
		for(Player* playerPtr : GameSandbox::Get().GetAllPlayerObjects())
		{
			if(playerPtr->GetRoom() == room)
			{
				if(playerPtr->GetPlayerID() != ownerID && playerPtr->GetTeam() != team && entitiesHit.find(playerPtr) == entitiesHit.end())
				{
					Vec2 newRelativePos = playerPtr->GetCurrentPosition() - currentPosition;
					if(newRelativePos.x * newRelativePos.x + newRelativePos.y * newRelativePos.y < relativePos.x * relativePos.x + relativePos.y * relativePos.y)
						relativePos = newRelativePos;
				}
			}
		}
		if(relativePos != Vec2(250.0f))
		{
			velocity = Normalize(relativePos);
			velocity *= properties.speed;
		}
	}
	if(timeSummoned + properties.lifetime <= TimeUtil::Get().GetTimeSinceStart())
		isSetToDestroy = true;
	changedProperties |= ENTITY_POSITION;
	Entity::ServerUpdate();
}

void Projectile::SimpleColRes(Entity* entity, const Vec2& penetration)
{
	if(entity->GetClassID() == RealPlayer::classID || entity->GetClassID() == NPC::classID)
	{
		Player* playerPtr = static_cast<Player*>(entity);
		if(playerPtr->GetPlayerID() != ownerID && playerPtr->GetTeam() != team && entitiesHit.find(entity) == entitiesHit.end())
		{
			playerPtr->DealDebuff(*properties.debuffOnHit, true, ownerID);
			entitiesHit.emplace(entity);
			if(!properties.playerPen)
				isSetToDestroy = true;
		}
	}
	else if(entity->GetClassID() == Block::classID)
	{
		if(!properties.blockPen && !static_cast<Block*>(entity)->IsBackground())
			isSetToDestroy = true;
	}
}

void Projectile::VelocityColRes(Entity* entity, const Vec2& intersectionFractions)
{}
