#pragma once
#include "Player.hpp"

class RealPlayer : public Player
{
	public:
		CLASS_IDENTIFICATION(RealPlayer)
		RealPlayer();
		RealPlayer(const Vec2& initPosition, const PlayerInfoPtr& initPlayerInfo, const PlayerData* initPlayerData, int32_t initRoom);
		virtual void ClientUpdate();
		virtual void ServerUpdate();
		virtual void Write(OutMemBitStream& outStream, PropertyBitField propertyBitField) const;
		virtual bool Read(InMemBitStream& inStream);
		virtual void ServerRuleUpdate();
		virtual void SetNumber(const UTF8String& name, uint32_t val);
		virtual uint32_t GetNumber(const UTF8String& name) const;
		virtual uint32_t* GetNumberPtr(const UTF8String& name);
		virtual GraphicsData GetPlayerGraphicsData(uint8_t level2, uint8_t level3, bool loop, bool flip);
		const PlayerInfoWeakPtr& GetPlayerInfoWeakPtr() const;
	protected:
		bool IsConditionTrue(uint32_t condIndex, const HeadlineProperty* condProp);
		void ExecuteResult(uint32_t resIndex, const HeadlineProperty* resProp);
		PlayerInfoWeakPtr playerInfoWeak;
};
