#pragma once
#include "Entity.hpp"
#include <GSEGL/Graphics/Math.hpp>
#include "../PlayerData.hpp"
#include "Point.hpp"
#include "../InputManager.hpp"
#include <unordered_map>

#define PLAYER_BASE_SPEED 3.0f
#define BASE_GRAVITY 2000.0f
#define MIN_GRAVITY_FROM_EFFECTS -2.0f
#define MAX_GRAVITY_FROM_EFFECTS 2.0f
#define MIN_SPEED_FROM_EFFECTS 0.0f
#define MAX_SPEED_FROM_EFFECTS 2.0f
#define MOVE_RESIST 1000.0f

typedef uint8_t StatusEffectField;

enum StatusEffectType
{
	STATUS_INSTANT_HEALTH = 0,
	STATUS_HEALTH = 1 << 0,
	STATUS_SPEED = 1 << 2,
	STATUS_GRAVITY_INCREASE = 1 << 4,
	STATUS_DEFENSE_INCREASE = 1 << 6
}; 

class PlayerInfo
{
	public:
		PlayerInfo():
		nextSpawnPoint(nullptr),
		playerID(0),
		points(0),
		ping(0.1f)
		{}
		
		Point* nextSpawnPoint; // Used for adventures
		UTF8String userName;
		PlayerID playerID;
		InputState latestInputState;
		int32_t points; // Used in arenas (points meaning score, should probably change name later)
		std::unordered_map<UTF8String, uint32_t> numbers;
		float ping;
};

typedef std::shared_ptr<PlayerInfo> PlayerInfoPtr;
typedef std::weak_ptr<PlayerInfo> PlayerInfoWeakPtr;

class StatusEffect
{
	public:
		StatusEffect(StatusEffectType initType, float initValue, float initTimeLeft, bool initStackable, PlayerID initInflicter);
		StatusEffectType type;
		float value;
		float timeLeft;
		bool stackable;
		PlayerID inflicter;
};

class Player : public Entity
{
	public:
		Player();
		Player(const Vec2& initPosition, PlayerID initPlayerID, InputState* initLatestInputState, const UTF8String* initUserName, const PlayerData* initPlayerData, int32_t initRoom);
		virtual bool Read(InMemBitStream& inStream);
		virtual void Write(OutMemBitStream& outStream, PropertyBitField propertyBitField) const;
		virtual void ClientUpdate();
		virtual void ServerUpdate();
		virtual void ServerRuleUpdate() = 0;
		virtual void SimpleColRes(Entity* entity, const Vec2& penetration);
		virtual void VelocityColRes(Entity* entity, const Vec2& intersectionFractions);
		virtual void Draw(const Vec2& cameraPos, float cameraZoom);
		
		virtual GraphicsData GetPlayerGraphicsData(uint8_t level2, uint8_t level3, bool loop, bool flip) = 0;
		
		virtual void SetNumber(const UTF8String& name, uint32_t val) = 0;
		virtual uint32_t GetNumber(const UTF8String& name) const = 0;
		virtual uint32_t* GetNumberPtr(const UTF8String& name) = 0;
		
		void GiveHealth(double healthGiven, PlayerID healthGiver);
		void DealDebuff(const PropertiesList& debuffProperties, bool stackable = true, PlayerID inflicter = 0);
		void DealBuff(const PropertiesList& buffProperties, bool stackable = true, PlayerID inflicter = 0);
		void DealEffect(const PropertiesList& effectProperties, bool stackable = true, PlayerID inflicter = 0);
		double GetCooldown(uint32_t ability) const;
		float GetHealthLeft() const;
		float GetHealthLeftPercentage() const;
		const UTF8String* GetUserName() const;
		PlayerID GetPlayerID() const;
		PlayerID GetLastHitter() const;
		uint32_t GetTeam() const;
		bool PressedTriggerButton() const;
	protected:
		bool GravityUpdate(); // Return value: Is mid air
		void UniversalUpdate();
		void AbilityCollisionResolution(Entity* entity, const Vec2& penetration);
		void ExecuteAbility(uint32_t abilityNumber);
		void UpdateAbilities();
		void UpdateEffects();
		void SetLeftAnimation();
		void SetRightAnimation();
		void SetIdleAnimation();
		void InitPlayerSize();
		void AddStatusEffect(StatusEffectType type, double value, double timeLeft, bool stackable, PlayerID inflicter = 0);
		enum PlayerProperties
		{
			PLAYER_HEALTH = 1 << 4,
			PLAYER_COOLDOWN = 1 << 5,
			PLAYER_GRAVITY = 1 << 6,
			PLAYER_SPEED = 1 << 7,
			PLAYER_STATUSEFFECTS = 1 << 8
		};
		PlayerID playerID;
		PlayerID lastHitter;
		
		const PlayerData* playerData;
		InputState* latestInputState;
		const UTF8String* userName;
		bool showName;
		bool showHealthAndEffects;
		
		bool facingLeft;
		float cooldowns[5];
		float currentHealth;
		float speedFromEffects;
		float gravityFromEffects;
		float defenseIncreaseFromEffects;
		StatusEffectField currentStatusEffects;
		std::vector<StatusEffect> statusEffects;
		
		std::unordered_map<const HeadlineProperty*, double> propTimers;
		
		float buildingUpGravity;
		enum PlayerJumpState
		{
			PLAYERJUMPSTATE_FALLING,
			PLAYERJUMPSTATE_MIDAIRJUMP,
			PLAYERJUMPSTATE_ONGROUND
		} jumpState;
		double timeToSetToFalling;
};
