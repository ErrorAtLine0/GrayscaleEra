#pragma once
#include "Entity.hpp"
#include <unordered_set>
#include "../PropertiesList.hpp"
#include "../PlayerData.hpp"

class ProjectileProperties
{
	public:
		ProjectileProperties();
		ProjectileProperties(const PropertiesList& propList);
		const PropertiesList* debuffOnHit;
		int32_t speed;
		int32_t lifetime;
		uint32_t size;
		bool homing;
		bool playerPen;
		bool blockPen;
};

class Projectile : public Entity
{
	public:
		CLASS_IDENTIFICATION(Projectile)
		Projectile();
		Projectile(const Vec2& initPosition, const Vec2& initDirection, PlayerID initOwnerID, uint32_t initTeam, const ProjectileProperties& initProperties, const GraphicsData& initGraphicsData, int32_t initRoom);
		virtual bool Read(InMemBitStream& inStream);
		virtual void Write(OutMemBitStream& outStream, PropertyBitField propertyBitField) const;
		virtual void ClientUpdate();
		virtual void ServerUpdate();
		virtual void SimpleColRes(Entity* entity, const Vec2& penetration);
		virtual void VelocityColRes(Entity* entity, const Vec2& intersectionFractions);
	protected:
		PlayerID ownerID;
		uint32_t team;
		double timeSummoned;
		std::unordered_set<GameObject*> entitiesHit;
		ProjectileProperties properties;
};
