#include "../GameSandbox.hpp"
#include "NPC.hpp"
#include "RealPlayer.hpp"
#include <GSEGL/RandGenUtil.hpp>
#include "../PropertyDecoderUtil.hpp"

ClassID NPC::classID;

NPC::NPC():
propListID(0),
propList(nullptr),
npcID(0),
firstUpdate(true)
{
	latestInputState = &npcInputState;
}

NPC::NPC(const Vec2& initPosition, NPCPropListID initPropListID, const PropertiesList* initPropList, int32_t initRoom):
Player::Player(initPosition, PlayerIDGenerator::Get().GeneratePlayerID(), &npcInputState, &initPropList->GetString(1), GameSandbox::Get().GetPlayerDataByNPCID(initPropList->GetNPC(0)), initRoom),
propListID(initPropListID),
propList(initPropList),
npcID(propList->GetNPC(0)),
firstUpdate(true)
{
	SetAnimation(GetPlayerGraphicsData(0, 0, true, false));
	showName = propList->GetBool(2);
	showHealthAndEffects = propList->GetBool(3);
}

void NPC::Write(OutMemBitStream& outStream, PropertyBitField propertyBitField) const
{
	Player::Write(outStream, propertyBitField);
	if(propertyBitField & ENTITY_CONSTRUCT)
		outStream.WriteBits(propListID);
}

bool NPC::Read(InMemBitStream& inStream)
{
	if(!Player::Read(inStream)) return false;
	if(changedProperties & ENTITY_CONSTRUCT)
	{
		inStream.ReadBits(propListID);
		propList = GameSandbox::Get().GetNPCPropListByNPCPropListID(propListID);
		showName = propList->GetBool(2);
		showHealthAndEffects = propList->GetBool(3);
		npcID = propList->GetNPC(0);
		playerData = GameSandbox::Get().GetPlayerDataByNPCID(npcID);
		userName = &propList->GetString(1);
		InitPlayerSize();
	}
	return true;
}

void NPC::ServerUpdate()
{
	Player::ServerUpdate();
}

void NPC::SetNumber(const UTF8String& name, uint32_t val)
{
	PropertyDecoderUtil::SetNumberInMap(numbers, name, val);
}

uint32_t NPC::GetNumber(const UTF8String& name) const
{
	return PropertyDecoderUtil::GetNumberInMap(numbers, name);
}

uint32_t* NPC::GetNumberPtr(const UTF8String& name)
{
	return PropertyDecoderUtil::GetNumberPtrInMap(numbers, name);
}

void NPC::ServerRuleUpdate()
{
	npcInputState = InputState();
	const RuleList& ruleList = propList->GetRuleList(4);
	for(uint32_t i = 0; i < ruleList.GetListSize(); i++)
	{
		const Rule& rule = ruleList.GetRuleAt(i);
		bool executeResults = true;
		for(uint32_t i = 0; i < rule.conditionIndices.size(); i++)
		{
			executeResults = IsConditionTrue(rule.conditionIndices[i], rule.conditionProperties[i]);
			if(!executeResults)
				break;
		}
		if(executeResults)
			for(uint32_t i = 0; i < rule.resultIndices.size(); i++)
				ExecuteResult(rule.resultIndices[i], rule.resultProperties[i]);
	}
	firstUpdate = false;
}

bool NPC::IsConditionTrue(uint32_t condIndex, const HeadlineProperty* condProp)
{
	switch(condIndex)
	{
		case 0: // Just Summoned
		{
			return firstUpdate;
		}
		case 1: // Check Global Number
		{
			return PropertyDecoderUtil::CheckGlobalNumber(condProp, PROPERTYDECODEROBJECT_PLAYER, this);
		}
		case 2: // Check Private Number
		{
			return PropertyDecoderUtil::CheckPrivateNumber(condProp, PROPERTYDECODEROBJECT_PLAYER, this, PROPERTYDECODEROBJECT_PLAYER, this);
		}
		case 3: // Check For Player
		{
			return PropertyDecoderUtil::CheckForPlayer(condProp, currentPosition, room, PROPERTYDECODEROBJECT_PLAYER, this);
		}
		case 4: // Distance From Point
		{
			const PropertiesList& pointDistanceData = static_cast<const ListProperty*>(condProp)->data;
			int32_t blockDistance = pointDistanceData.GetInt(1);
			const auto& pointMap = GameSandbox::Get().GetTagToPointMap();
			auto its = pointMap.equal_range(pointDistanceData.GetString(0));
			for(auto it = its.first; it != its.second; ++it)
			{
				if(Entity::BoxToBoxCollision(currentPosition - Vec2((blockDistance - 1) * 20.0f), Vec2(blockDistance * 40.0f), it->second->GetPos(), Vec2(40.0f)))
					return true;
			}
			return false;
		}
		case 5: // Distance Away From Point
		{
			const PropertiesList& pointDistanceData = static_cast<const ListProperty*>(condProp)->data;
			int32_t blockDistance = pointDistanceData.GetInt(1);
			const auto& pointMap = GameSandbox::Get().GetTagToPointMap();
			auto its = pointMap.equal_range(pointDistanceData.GetString(0));
			for(auto it = its.first; it != its.second; ++it)
			{
				if(Entity::BoxToBoxCollision(currentPosition - Vec2((blockDistance - 1) * 20.0f), Vec2(blockDistance * 40.0f), it->second->GetPos(), Vec2(40.0f)))
					return false;
			}
			return true;
		}
		case 6: // Health Above
		{
			return currentHealth > static_cast<const IntProperty*>(condProp)->data;
		}
		case 7: // Health Below
		{
			return currentHealth < static_cast<const IntProperty*>(condProp)->data;
		}
		case 8: // Every X Milliseconds
		{
			return PropertyDecoderUtil::CheckTimer(condProp, propTimers);
		}
		case 9: // Random Percentage
		{
			return PropertyDecoderUtil::CheckRandomPercentage(condProp);
		}
		case 10: // Just Died
		{
			return IsSetToDestroy();
		}
	}
	return false;
}

void NPC::ExecuteResult(uint32_t resIndex, const HeadlineProperty* resProp)
{
	switch(resIndex)
	{
		case 0: // Set Global Number
		{
			PropertyDecoderUtil::SetGlobalNumber(resProp, PROPERTYDECODEROBJECT_PLAYER, this);
			break;
		}
		case 1: // Set Private Number
		{
			PropertyDecoderUtil::SetPrivateNumber(resProp, PROPERTYDECODEROBJECT_PLAYER, this, PROPERTYDECODEROBJECT_PLAYER, this);
			break;
		}
		case 2: // Set Player Private Number
		{
			PropertyDecoderUtil::SetPlayerPrivateNumber(resProp, currentPosition, room, PROPERTYDECODEROBJECT_PLAYER, this);
			break;
		}
		case 3: // Die
		{
			isSetToDestroy = true;
			break;
		}
		case 4: // Use Move
		{
			const PropertiesList& movePropList = static_cast<const ListProperty*>(resProp)->data;
			uint32_t moveToUse = movePropList.GetChoice(0);
			uint32_t directionToAim = movePropList.GetChoice(1);
			const PropertiesList& playerPropList = movePropList.GetList(2);
			const UTF8String& pointTag = movePropList.GetString(3);
			switch(directionToAim)
			{
				case 0: // Up
				{
					npcInputState.cursorDirection = Vec2(0.0f, 1.0f);
					break;
				}
				case 1: // Down
				{
					npcInputState.cursorDirection = Vec2(0.0f, -1.0f);
					break;
				}
				case 2: // Left
				{
					npcInputState.cursorDirection = Vec2(-1.0f, 0.0f);
					SetLeftAnimation();
					break;
				}
				case 3: // Right
				{
					npcInputState.cursorDirection = Vec2(1.0f, 0.0f);
					SetRightAnimation();
					break;
				}
				case 4: // Towards Closest Specified Player
				{
					std::vector<RealPlayer*> specifiedPlayers;
					PropertyDecoderUtil::GetPlayers(&specifiedPlayers, playerPropList, currentPosition, room, PROPERTYDECODEROBJECT_PLAYER, this);
					RealPlayer* closestSpecifiedPlayer = PropertyDecoderUtil::GetClosestPlayer(specifiedPlayers, currentPosition, room);
					if(closestSpecifiedPlayer)
					{
						npcInputState.cursorDirection = Normalize(closestSpecifiedPlayer->GetCurrentPosition() - currentPosition);
						if(npcInputState.cursorDirection.x > 0.0f)
							SetRightAnimation();
						else
							SetLeftAnimation();
					}
					break;
				}
				case 5: // Away From Closest Specified Player
				{
					std::vector<RealPlayer*> specifiedPlayers;
					PropertyDecoderUtil::GetPlayers(&specifiedPlayers, playerPropList, currentPosition, room, PROPERTYDECODEROBJECT_PLAYER, this);
					RealPlayer* closestSpecifiedPlayer = PropertyDecoderUtil::GetClosestPlayer(specifiedPlayers, currentPosition, room);
					if(closestSpecifiedPlayer)
					{
						npcInputState.cursorDirection = Normalize(currentPosition - closestSpecifiedPlayer->GetCurrentPosition());
						if(npcInputState.cursorDirection.x > 0.0f)
							SetRightAnimation();
						else
							SetLeftAnimation();
					}
					break;
				}
				case 6: // Towards Specified Point
				{
					Point* point = GameSandbox::Get().GetPointByTag(pointTag);
					if(point)
					{
						npcInputState.cursorDirection = Normalize(point->GetPos() - currentPosition);
						if(npcInputState.cursorDirection.x > 0.0f)
							SetRightAnimation();
						else
							SetLeftAnimation();
					}
					break;
				}
				case 7: // Away From Specified Point
				{
					Point* point = GameSandbox::Get().GetPointByTag(pointTag);
					if(point)
					{
						npcInputState.cursorDirection = Normalize(currentPosition - point->GetPos());
						if(npcInputState.cursorDirection.x > 0.0f)
							SetRightAnimation();
						else
							SetLeftAnimation();
					}
					break;
				}
				case 8: // Wherever they're facing
				{
					break;
				}
				case 9: // Random
				{
					npcInputState.cursorDirection = Normalize(Vec2(RandGenUtil::Get().GetRandomDouble(-1.0, 1.0), RandGenUtil::Get().GetRandomDouble(-1.0, 1.0)));
					if(npcInputState.cursorDirection.x > 0.0f)
						SetRightAnimation();
					else
						SetLeftAnimation();
					break;
				}
			}
			switch(moveToUse)
			{
				case 0: { latestInputState->HorizontalMovement = facingLeft ? -TimeUtil::Get().GetDeltaTime() : TimeUtil::Get().GetDeltaTime(); break; }
				case 1: { latestInputState->WPressed = true; break; }
				case 2: { latestInputState->LeftClickPressed = true; break; }
				case 3: { latestInputState->RightClickPressed = true; break; }
				case 4: { latestInputState->QPressed = true; break; }
				case 5: { latestInputState->EPressed = true; break; }
			}
			break;
		}
		case 5: // Print Message
		{
			PropertyDecoderUtil::SendMessage(resProp, currentPosition, room, PROPERTYDECODEROBJECT_PLAYER, this);
			break;
		}
		case 6: // Print Announcement
		{
			PropertyDecoderUtil::SetAnnouncement(resProp, currentPosition, room, PROPERTYDECODEROBJECT_PLAYER, this);
			break;
		}
		case 7: // Give Status Effect
		{
			DealEffect(static_cast<const ListProperty*>(resProp)->data);
			break;
		}
		case 8: // End Game
		{
			GameSandbox::Get().EndGame();
			break;
		}
	}
}

GraphicsData NPC::GetPlayerGraphicsData(uint8_t level2, uint8_t level3, bool loop, bool flip)
{
	return GraphicsData(GRAPHICS_NPC, npcID, level2, level3, loop, flip);
}

