#pragma once
#include "Entity.hpp"
#include "../PlayerData.hpp"
#include "../PropertiesList.hpp"

class SummonedMinion : public Entity
{
	public:
		CLASS_IDENTIFICATION(SummonedMinion)
		SummonedMinion();
		SummonedMinion(const Vec2& initPosition, PlayerID initOwnerID, uint32_t initTeam, const PropertiesList& initPropList, const GraphicsData& initGraphicsData, const GraphicsData& initProjGraphicsData, int32_t initRoom);
		virtual bool Read(InMemBitStream& inStream);
		virtual void Write(OutMemBitStream& outStream, PropertyBitField propertyBitField) const;
		virtual void ClientUpdate();
		virtual void ServerUpdate();
		virtual void SimpleColRes(Entity* entity, const Vec2& penetration);
		virtual void VelocityColRes(Entity* entity, const Vec2& intersectionFractions);
	protected:
		PlayerID ownerID;
		uint32_t team;
		double timeSummoned;
		double lastBuff;
		double lastShot;
		const PropertiesList* propList;
		GraphicsData projGraphicsData;
};
