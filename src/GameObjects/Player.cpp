#include "../GameSandbox.hpp"
#include "Player.hpp"
#include "RealPlayer.hpp"
#include "NPC.hpp"
#include "Block.hpp"
#include "Projectile.hpp"
#include "SummonedMinion.hpp"
#include "../PropertyDecoderUtil.hpp"
#include <GSEGL/Graphics/DrawUtil.hpp>

#define EXTRA_JUMP_TIME 0.2

Player::Player():
Entity::Entity(Vec2(0.0f), Vec2(0.0f), Vec2(0.0f), Vec2(0.0f), Vec2(100.0f), -1),
playerID(0),
lastHitter(0),
playerData(nullptr),
latestInputState(nullptr),
userName(nullptr),
showName(true),
showHealthAndEffects(true),
facingLeft(false),
currentStatusEffects(0),
buildingUpGravity(0.0f),
jumpState(PLAYERJUMPSTATE_ONGROUND),
timeToSetToFalling(TimeUtil::Get().GetTimeSinceStart())
{
	facingLeft = false;
}

Player::Player(const Vec2& initPosition, PlayerID initPlayerID, InputState* initLatestInputState, const UTF8String* initUserName, const PlayerData* initPlayerData, int32_t initRoom):
Entity::Entity(initPosition, Vec2(0.0f), Vec2(0.0f), Vec2(0.0f), Vec2(100.0f), initRoom),
playerID(initPlayerID),
lastHitter(initPlayerID),
playerData(initPlayerData),
latestInputState(initLatestInputState),
userName(initUserName),
showName(true),
showHealthAndEffects(true),
facingLeft(false),
currentHealth(playerData->statistics.GetPropListRef().GetInt(0)),
speedFromEffects(1.0f),
gravityFromEffects(1.0f),
defenseIncreaseFromEffects(0.0f),
currentStatusEffects(0),
buildingUpGravity(0.0f),
jumpState(PLAYERJUMPSTATE_ONGROUND),
timeToSetToFalling(TimeUtil::Get().GetTimeSinceStart())
{
	for(uint8_t i = 0; i < 5; i++)
		cooldowns[i] = playerData->abilities[i].GetCooldown();
	InitPlayerSize();
	if(GameSandbox::Get().GetWorldData()->GetType() == WORLD_ARENA)
		statusEffects.emplace_back(STATUS_DEFENSE_INCREASE, 1000.0f, 5.0f, true, 0);
	changedProperties |= PLAYER_HEALTH;
}

StatusEffect::StatusEffect(StatusEffectType initType, float initValue, float initTimeLeft, bool initStackable, PlayerID initInflicter):
type(initType),
value(initValue),
timeLeft(initTimeLeft),
stackable(initStackable),
inflicter(initInflicter)
{}

bool Player::Read(InMemBitStream& inStream)
{
	if(!Entity::Read(inStream)) return false;
	if(changedProperties & ENTITY_CONSTRUCT)
		if(!inStream.ReadBits(playerID)) return false;
	if(changedProperties & PLAYER_HEALTH)
		if(!inStream.ReadFixedFloat(currentHealth, 0.0f, 1.0f, 12)) return false;
	if(changedProperties & PLAYER_COOLDOWN)
		for(uint8_t i = 0; i < 5; i++)
		{
			if(!inStream.ReadFixedFloat(cooldowns[i], 0.0f, 0.1f, 8)) return false;
			if(GetClassID() == RealPlayer::classID)
				cooldowns[i] -= GameSandbox::Get().GetRTTClient();
			if(cooldowns[i] <= 0.0f)
				cooldowns[i] = 0.0f;
		}
	if(changedProperties & PLAYER_SPEED)
	{
		if(!inStream.ReadFixedFloat(speedFromEffects, MIN_SPEED_FROM_EFFECTS, 0.01f, 8)) return false;
	}
	else
		speedFromEffects = 1.0f;
	if(changedProperties & PLAYER_GRAVITY)
	{
		if(!inStream.ReadFixedFloat(gravityFromEffects, MIN_GRAVITY_FROM_EFFECTS, 0.01f, 9)) return false;
	}
	else
		gravityFromEffects = 0.0f;
	if(changedProperties & PLAYER_STATUSEFFECTS)
	{
		currentStatusEffects = 0;
		if(!inStream.ReadBits(currentStatusEffects)) return false;
	}
	return true;
}

void Player::Write(OutMemBitStream& outStream, PropertyBitField propertyBitField) const
{
	Entity::Write(outStream, propertyBitField);
	if(propertyBitField & ENTITY_CONSTRUCT)
		outStream.WriteBits(playerID);
	if(propertyBitField & PLAYER_HEALTH)
		outStream.WriteFixedFloat(currentHealth, 0.0f, 1.0f, 12);
	if(propertyBitField & PLAYER_COOLDOWN)
		for(uint8_t i = 0; i < 5; i++)
			outStream.WriteFixedFloat(cooldowns[i], 0.0f, 0.1f, 8);
	if(propertyBitField & PLAYER_SPEED)
		outStream.WriteFixedFloat(speedFromEffects, MIN_SPEED_FROM_EFFECTS, 0.01f, 8);
	if(propertyBitField & PLAYER_GRAVITY)
		outStream.WriteFixedFloat(gravityFromEffects, MIN_GRAVITY_FROM_EFFECTS, 0.01f, 9);
	if(propertyBitField & PLAYER_STATUSEFFECTS)
		outStream.WriteBits(currentStatusEffects);
}

void Player::ClientUpdate()
{
	if(!playerData)
		return;
	if(playerID == GameSandbox::Get().GetOwnPlayerID())
		UniversalUpdate();
	else
		GravityUpdate();
	Entity::ClientUpdate();
}

void Player::ServerUpdate()
{
	changedProperties = 0;
	UniversalUpdate();
	changedProperties |= ENTITY_POSITION;
	Entity::ServerUpdate();
}

void Player::UniversalUpdate()
{
	float movementResist = GravityUpdate() ? MOVE_RESIST : MOVE_RESIST * 4.0f;
	if(velocity.x < 0.0f)
	{
		velocity.x += movementResist * TimeUtil::Get().GetDeltaTime();
		if(velocity.x > 0.0f)
			velocity.x = 0.0f;
	}
	else if(velocity.x > 0.0f)
	{
		velocity.x -= movementResist * TimeUtil::Get().GetDeltaTime();
		if(velocity.x < 0.0f)
			velocity.x = 0.0f;
	}
	
	if(latestInputState->HorizontalMovement < 0.0f)
	{
		SetLeftAnimation();
		if(!DoesBoxContainBlock(Vec2(currentPosition.x - 0.01f, currentPosition.y + 10.0f), Vec2(size.x, size.y - 20.0f), room))
		{
			float walkVel = (-(playerData->statistics.GetPropListRef().GetInt(1)) * speedFromEffects) * PLAYER_BASE_SPEED;
			if(std::fabs(velocity.x) <= std::fabs(walkVel))
				velocity.x = walkVel;
			else
				velocity.x += walkVel * TimeUtil::Get().GetDeltaTime();
		}
		latestInputState->HorizontalMovement += TimeUtil::Get().GetDeltaTime();
		if(latestInputState->HorizontalMovement > 0.0f)
			latestInputState->HorizontalMovement = 0.0f;
	}
	else if(latestInputState->HorizontalMovement > 0.0f)
	{
		SetRightAnimation();
		if(!DoesBoxContainBlock(Vec2(currentPosition.x + 0.01f, currentPosition.y + 10.0f), Vec2(size.x, size.y - 20.0f), room))
		{
			float walkVel = (playerData->statistics.GetPropListRef().GetInt(1) * speedFromEffects) * PLAYER_BASE_SPEED;
			if(std::fabs(velocity.x) <= walkVel)
				velocity.x = walkVel;
			else
				velocity.x += walkVel * TimeUtil::Get().GetDeltaTime();
		}
		latestInputState->HorizontalMovement -= TimeUtil::Get().GetDeltaTime();
		if(latestInputState->HorizontalMovement < 0.0f)
			latestInputState->HorizontalMovement = 0.0f;
	}
	else
		SetIdleAnimation();
	if(latestInputState->WPressed && cooldowns[0] <= 0.0f)
		ExecuteAbility(0);
	if(latestInputState->LeftClickPressed && cooldowns[1] <= 0.0f)
		ExecuteAbility(1);
	if(latestInputState->RightClickPressed && cooldowns[2] <= 0.0f)
		ExecuteAbility(2);
	if(latestInputState->QPressed && cooldowns[3] <= 0.0f)
		ExecuteAbility(3);
	if(latestInputState->EPressed && cooldowns[4] <= 0.0f)
		ExecuteAbility(4);
	UpdateAbilities();
	if(!GameSandbox::Get().IsClient())
		UpdateEffects();
	
	if(currentHealth == 0)
		isSetToDestroy = true;
	if(currentPosition.y < -300.0f) // Fell into void
		isSetToDestroy = true;
}

bool Player::GravityUpdate()
{
	if(!DoesBoxContainBlock(Vec2(currentPosition.x + 1.0f, currentPosition.y), Vec2(size.x - 2.0f, size.y), room))
	{
		float gravityTotalEffect = (1.0f - (playerData->statistics.GetPropListRef().GetInt(2) / 100.0f)) + gravityFromEffects;
		buildingUpGravity += BASE_GRAVITY * TimeUtil::Get().GetDeltaTime();
		velocity.y -= (BASE_GRAVITY + buildingUpGravity) * gravityTotalEffect * TimeUtil::Get().GetDeltaTime();
		if(jumpState == PLAYERJUMPSTATE_ONGROUND && timeToSetToFalling < TimeUtil::Get().GetTimeSinceStart())
			jumpState = PLAYERJUMPSTATE_FALLING;
		return true;
	}
	else
	{
		timeToSetToFalling = TimeUtil::Get().GetTimeSinceStart() + EXTRA_JUMP_TIME;
		buildingUpGravity = 0.0f;
		if(DoesBoxContainBlock(Vec2(currentPosition.x + 1.0f - velocity.x * TimeUtil::Get().GetDeltaTime(), currentPosition.y - velocity.y * TimeUtil::Get().GetDeltaTime()), Vec2(size.x - 2.0f, size.y), room))
		{
			if(jumpState == PLAYERJUMPSTATE_MIDAIRJUMP)
				SetAnimation(GetPlayerGraphicsData(0, 0, true, facingLeft));
			jumpState = PLAYERJUMPSTATE_ONGROUND;
		}
		return false;
	}
}

void Player::SimpleColRes(Entity* entity, const Vec2& penetration)
{
	bool isNotClient = !GameSandbox::Get().IsClient();
	if(entity->GetClassID() == Block::classID)
	{
		Block* blockPtr = static_cast<Block*>(entity);
		if(isNotClient)
			DealEffect(blockPtr->GetEffectProperties(), false);
		if(!blockPtr->IsBackground())
		{
			if(penetration.x > 0.0f)
			{
				nextPosition.x = blockPtr->GetCurrentPosition().x - size.x;
				velocity.x = blockPtr->GetVelocity().x > 0.0f ? 0.0f : blockPtr->GetVelocity().x;
			}
			else if(penetration.x < 0.0f)
			{
				nextPosition.x = blockPtr->GetCurrentPosition().x + blockPtr->GetSize().x;
				velocity.x = blockPtr->GetVelocity().x < 0.0f ? 0.0f : blockPtr->GetVelocity().x;
			}
			if(penetration.y > 0.0f)
			{
				nextPosition.y = blockPtr->GetCurrentPosition().y - size.y;
				velocity.y = -(std::fabs(velocity.y / 4.0f));
				if(blockPtr->GetVelocity().y < 0.0f)
					velocity.y += blockPtr->GetVelocity().y;
			}
			else if(penetration.y < 0.0f)
			{
				nextPosition.y = blockPtr->GetCurrentPosition().y + blockPtr->GetSize().y;
				if(velocity.y <= 0.0f)
					velocity.y = blockPtr->GetVelocity().y;
			}
		}
	}
}

void Player::VelocityColRes(Entity* entity, const Vec2& intersectionFractions)
{
	if(entity->GetClassID() == Block::classID && !static_cast<Block*>(entity)->IsBackground())
	{
		velocity.x *= intersectionFractions.x;
		velocity.y *= intersectionFractions.y;
	}
}

void Player::SetLeftAnimation()
{
	if(graphicsQueue.front().level2 == 0 && graphicsQueue.front().level3 == 0) // If currently playing idle animation
		SetAnimation(GetPlayerGraphicsData(0, 1, true, true));
	if(!facingLeft)
	{
		facingLeft = true;
		for(GraphicsData& graphicsData : graphicsQueue)
			graphicsData.flip = true;
		changedProperties |= ENTITY_ANIMATION;
	}
}

void Player::SetRightAnimation()
{
	if(graphicsQueue.front().level2 == 0 && graphicsQueue.front().level3 == 0) // If currently playing idle animation
		SetAnimation(GetPlayerGraphicsData(0, 1, true, false));
	if(facingLeft)
	{
		facingLeft = false;
		for(GraphicsData& graphicsData : graphicsQueue)
			graphicsData.flip = false;
		changedProperties |= ENTITY_ANIMATION;
	}
}

void Player::SetIdleAnimation()
{
	if(graphicsQueue.front().level2 == 0 && graphicsQueue.front().level3 == 1) // If currently playing walking animation
		SetAnimation(GetPlayerGraphicsData(0, 0, true, facingLeft));
}

void Player::InitPlayerSize()
{
	uint32_t playerSize = playerData->statistics.GetPropListRef().GetChoice(3);
	if(playerSize == 0)
	{
		drawOffset = Vec2(0.0f);
		size = Vec2(100.0f);
	}
	else if(playerSize == 1)
	{
		nextPosition.y += 25.0f;
		currentPosition.y += 25.0f;
		drawOffset = Vec2(-25.0f);
		size = Vec2(50.0f);
	}
	else if(playerSize == 2)
	{
		nextPosition.y += 37.5f;
		currentPosition.y += 37.5f;
		drawOffset = Vec2(-37.5f);
		size = Vec2(25.0f);
	}
}

void Player::ExecuteAbility(uint32_t abilityNumber)
{
	if(!playerData)
		return;
	const AbilityData& abilityData = playerData->abilities[abilityNumber];
	uint32_t abilityType = abilityData.GetAbilityType();
	const PropertiesList& propList = abilityData.GetPropListRef();
	if(abilityType == 1) // Jump ability
	{
		if(jumpState == PLAYERJUMPSTATE_ONGROUND)
		{
			currentPosition.y += propList.GetInt(1) * TimeUtil::Get().GetDeltaTime();
			velocity.y = propList.GetInt(1);
			SetAnimation(GetPlayerGraphicsData(abilityNumber+1, 0, false, facingLeft));
			AddAnimation(GetPlayerGraphicsData(abilityNumber+1, 1, true, facingLeft));
			DealBuff(propList.GetList(0));
			jumpState = PLAYERJUMPSTATE_MIDAIRJUMP;
			buildingUpGravity = 0.0f;
			if(!GameSandbox::Get().IsClient())
				cooldowns[abilityNumber] = abilityData.GetCooldown();
		}
	}
	else if(abilityType == 4) // Hop Towards Cursor Ability
	{
		SetAnimation(GetPlayerGraphicsData(abilityNumber+1, 0, false, facingLeft));
		AddAnimation(GetPlayerGraphicsData(0, 0, true, facingLeft));
		Vec2 hopVector = latestInputState->cursorDirection;
		float range = propList.GetInt(1) * 400.0f;
		hopVector *= range;
		velocity += hopVector;
		DealBuff(propList.GetList(0));
		if(!GameSandbox::Get().IsClient())
			cooldowns[abilityNumber] = abilityData.GetCooldown();
	}
	if(GameSandbox::Get().IsClient()) // Client can only predict jumping and teleporting
		return;
	if(abilityType == 0) // Self buff ability
	{
		SetAnimation(GetPlayerGraphicsData(abilityNumber+1, 0, false, facingLeft));
		AddAnimation(GetPlayerGraphicsData(0, 0, true, facingLeft));
		DealBuff(propList.GetList(0));
		cooldowns[abilityNumber] = abilityData.GetCooldown();
	}
	else if(abilityType == 2) // Projectile ability
	{
		SetAnimation(GetPlayerGraphicsData(abilityNumber+1, 0, false, facingLeft));
		AddAnimation(GetPlayerGraphicsData(0, 0, true, facingLeft));
		Vec2 projDirection = Vec2(facingLeft ? -1.0f : 1.0f, 0.0f);
		if(propList.GetBool(2)) // Cursor Aim
			projDirection = latestInputState->cursorDirection;
		GameSandbox::Get().SummonObject(new Projectile(currentPosition+Vec2(1.0f), projDirection, playerID, GetTeam(), ProjectileProperties(propList), GetPlayerGraphicsData(abilityNumber+1, 1, true, facingLeft), room));
		DealBuff(propList.GetList(0));
		cooldowns[abilityNumber] = abilityData.GetCooldown();
	}
	else if(abilityType == 3) // Melee Ability
	{
		SetAnimation(GetPlayerGraphicsData(abilityNumber+1, 0, false, facingLeft));
		AddAnimation(GetPlayerGraphicsData(0, 0, true, facingLeft));
		float range = 40.0f * propList.GetInt(2);
		Vec2 damageBoxPosition, damageBoxSize;
		if(propList.GetBool(3)) // Hits in a circle
		{
			damageBoxPosition = Vec2(currentPosition.x - range, currentPosition.y - range);
			damageBoxSize = Vec2(range*2 + size.x, range*2 + size.y);
		}
		else
		{
			damageBoxPosition = Vec2(facingLeft ? currentPosition.x - range : currentPosition.x, currentPosition.y);
			damageBoxSize = Vec2(range + size.x, size.y);
		}
		for(Player* playerPtr : GameSandbox::Get().GetAllPlayerObjects())
			if(playerPtr != this && playerPtr->GetTeam() != GetTeam() && playerPtr->GetRoom() == room)
			{
				Vec2 playerCollisionPos = playerPtr->GetCurrentPosition();
				if(BoxToBoxCollision(playerCollisionPos, playerPtr->GetSize(), damageBoxPosition, damageBoxSize))
					playerPtr->DealDebuff(propList.GetList(1), true, playerID);
			}
		DealBuff(propList.GetList(0));
		cooldowns[abilityNumber] = abilityData.GetCooldown();
	}
	else if(abilityType == 5) // Summon Minion
	{
		SetAnimation(GetPlayerGraphicsData(abilityNumber+1, 0, false, facingLeft));
		AddAnimation(GetPlayerGraphicsData(0, 0, true, facingLeft));
		GameSandbox::Get().SummonObject(new SummonedMinion(currentPosition, playerID, GetTeam(), propList, GetPlayerGraphicsData(abilityNumber+1, 1, true, facingLeft), GetPlayerGraphicsData(abilityNumber+1, 2, true, facingLeft), room));
		DealBuff(propList.GetList(0));
		cooldowns[abilityNumber] = abilityData.GetCooldown();
	}
}

void Player::UpdateAbilities()
{
	for(uint8_t abilityNumber = 0; abilityNumber < 5; abilityNumber++)
	{	
		cooldowns[abilityNumber] -= TimeUtil::Get().GetDeltaTime();
		if(cooldowns[abilityNumber] < 0.0)
			cooldowns[abilityNumber] = 0.0;
		changedProperties |= PLAYER_COOLDOWN;
	}
}

void Player::UpdateEffects()
{
	StatusEffectField lastStatusEffects = currentStatusEffects;
	currentStatusEffects = 0;
	speedFromEffects = 1.0f;
	gravityFromEffects = 0.0f;
	defenseIncreaseFromEffects = 0.0f;
	float healthBeforeEffects = currentHealth;
	for(uint32_t i = 0; i < statusEffects.size(); i++)
	{
		switch(statusEffects[i].type)
		{
			case STATUS_INSTANT_HEALTH:
			{
				GiveHealth(statusEffects[i].value, statusEffects[i].inflicter);
				break;
			}
			case STATUS_HEALTH:
			{
				GiveHealth(statusEffects[i].value * TimeUtil::Get().GetDeltaTime(), statusEffects[i].inflicter);
				break;
			}
			case STATUS_SPEED:
			{
				speedFromEffects += statusEffects[i].value / 100.0f;
				break;
			}
			case STATUS_GRAVITY_INCREASE:
			{
				gravityFromEffects += statusEffects[i].value / 100.0f;
				break;
			}
			case STATUS_DEFENSE_INCREASE:
			{
				defenseIncreaseFromEffects += statusEffects[i].value / 100.0f;
				break;
			}
		}
		if(statusEffects[i].value != 0.0f)
			currentStatusEffects |= statusEffects[i].type;
		statusEffects[i].timeLeft -= TimeUtil::Get().GetDeltaTime();
		if(statusEffects[i].timeLeft <= 0.0f)
		{
			statusEffects.erase(statusEffects.begin()+i);
			--i;
		}
	}
	if(speedFromEffects < MIN_SPEED_FROM_EFFECTS)
		speedFromEffects = MIN_SPEED_FROM_EFFECTS;
	else if(speedFromEffects > MAX_SPEED_FROM_EFFECTS)
		speedFromEffects = MAX_SPEED_FROM_EFFECTS;
	if(speedFromEffects != 1.0f)
		changedProperties |= PLAYER_SPEED;
	if(speedFromEffects > 1.0f)
		currentStatusEffects |= STATUS_SPEED << 1;
	
	if(gravityFromEffects < MIN_GRAVITY_FROM_EFFECTS)
		gravityFromEffects = MIN_GRAVITY_FROM_EFFECTS;
	if(gravityFromEffects > MAX_GRAVITY_FROM_EFFECTS)
		gravityFromEffects = MAX_GRAVITY_FROM_EFFECTS;
	if(gravityFromEffects != 0.0f)
		changedProperties |= PLAYER_GRAVITY;
	if(gravityFromEffects > 0.0f)
		currentStatusEffects |= STATUS_GRAVITY_INCREASE << 1;
	
	if(defenseIncreaseFromEffects < 0.0f)
		currentStatusEffects |= STATUS_DEFENSE_INCREASE << 1;
	
	if((currentStatusEffects & STATUS_HEALTH) && healthBeforeEffects > currentHealth)
		currentStatusEffects |= STATUS_HEALTH << 1;
		
	if(currentStatusEffects != lastStatusEffects)
		changedProperties |= PLAYER_STATUSEFFECTS;
}

void Player::GiveHealth(double healthGiven, PlayerID healthGiver)
{
	if(defenseIncreaseFromEffects < 1.0f && currentHealth != 0)
	{
		float maxHealth = playerData->statistics.GetPropListRef().GetInt(0);
		currentHealth += healthGiven * (1.0f - defenseIncreaseFromEffects);
		if(currentHealth < 0)
			currentHealth = 0;
		else if(currentHealth > maxHealth)
			currentHealth = maxHealth;
		if(healthGiven < 0 && healthGiver != 0)
			lastHitter = healthGiver;
		changedProperties |= PLAYER_HEALTH;
	}
}

void Player::DealDebuff(const PropertiesList& debuffProperties, bool stackable, PlayerID inflicter)
{
	AddStatusEffect(STATUS_INSTANT_HEALTH, -debuffProperties.GetInt(0), 0.0, stackable, inflicter);
	AddStatusEffect(STATUS_HEALTH, -debuffProperties.GetInt(1), debuffProperties.GetInt(2), stackable, inflicter);
	AddStatusEffect(STATUS_SPEED, -debuffProperties.GetInt(3), debuffProperties.GetInt(4), stackable, inflicter);
	AddStatusEffect(STATUS_GRAVITY_INCREASE, debuffProperties.GetInt(5), debuffProperties.GetInt(6), stackable, inflicter);
	AddStatusEffect(STATUS_DEFENSE_INCREASE, debuffProperties.GetInt(7), debuffProperties.GetInt(8), stackable, inflicter);	
}

void Player::DealBuff(const PropertiesList& buffProperties, bool stackable, PlayerID inflicter)
{
	AddStatusEffect(STATUS_INSTANT_HEALTH, buffProperties.GetInt(0), 0.0, stackable, inflicter);
	AddStatusEffect(STATUS_HEALTH, buffProperties.GetInt(1), buffProperties.GetInt(2), stackable, inflicter);
	AddStatusEffect(STATUS_SPEED, buffProperties.GetInt(3), buffProperties.GetInt(4), stackable, inflicter);
	AddStatusEffect(STATUS_GRAVITY_INCREASE, -buffProperties.GetInt(5), buffProperties.GetInt(6), stackable, inflicter);
	AddStatusEffect(STATUS_DEFENSE_INCREASE, buffProperties.GetInt(7), buffProperties.GetInt(8), stackable, inflicter);
}

void Player::DealEffect(const PropertiesList& effectProperties, bool stackable, PlayerID inflicter)
{
	AddStatusEffect(STATUS_INSTANT_HEALTH, effectProperties.GetInt(0), 0.0, stackable, inflicter);
	AddStatusEffect(STATUS_HEALTH, effectProperties.GetInt(1), effectProperties.GetInt(2), stackable, inflicter);
	AddStatusEffect(STATUS_SPEED, effectProperties.GetInt(3), effectProperties.GetInt(4), stackable, inflicter);
	AddStatusEffect(STATUS_GRAVITY_INCREASE, effectProperties.GetInt(5), effectProperties.GetInt(6), stackable, inflicter);
	AddStatusEffect(STATUS_DEFENSE_INCREASE, -effectProperties.GetInt(7), effectProperties.GetInt(8), stackable, inflicter);
}

void Player::AddStatusEffect(StatusEffectType type, double value, double timeLeft, bool stackable, PlayerID inflicter)
{
	if(!GameSandbox::Get().IsClient())
	{
		if(stackable)
			statusEffects.emplace_back(type, value, timeLeft, stackable, inflicter);
		else
		{
			auto it = std::find_if(statusEffects.begin(), statusEffects.end(), [&type](const StatusEffect& sE){ return (!sE.stackable && sE.type == type);});
			if(it != statusEffects.end())
			{
				if(std::abs(it->value) < std::abs(value))
				{
					it->value = value;
					it->timeLeft = timeLeft;
				}
			}
			else
				statusEffects.emplace_back(type, value, timeLeft, stackable, inflicter);
		}
	}
}

double Player::GetCooldown(uint32_t ability) const { return cooldowns[ability]; }
float Player::GetHealthLeft() const { return currentHealth; }
float Player::GetHealthLeftPercentage() const { return static_cast<float>(currentHealth) / playerData->statistics.GetPropListRef().GetInt(0); }
const UTF8String* Player::GetUserName() const { return userName; }
PlayerID Player::GetPlayerID() const { return playerID; }
PlayerID Player::GetLastHitter() const { return lastHitter; }
uint32_t Player::GetTeam() const { return GetNumber(TEAM_NUMBER_NAME); }
bool Player::PressedTriggerButton() const { return latestInputState && latestInputState->TriggerPressed; }

void Player::Draw(const Vec2& cameraPos, float cameraZoom)
{
	if(!playerData)
		return;
	if(showHealthAndEffects)
		DrawUtil::Get().DrawRectangle(ENTITY_DRAW_LAYER - 1, (Vec2(currentPosition.x, currentPosition.y + 110.0f)- cameraPos + drawOffset) * cameraZoom, Vec2(100.0f * GetHealthLeftPercentage(), 10.0f) * cameraZoom);
	if(showName && userName)
	{
		DrawUtil::Get().DrawRectangle(ENTITY_DRAW_LAYER - 1, (Vec2(currentPosition.x - 60.0f, currentPosition.y + 125.0f) - cameraPos + drawOffset) * cameraZoom, Vec2(220.0f, 20.0f) * cameraZoom, 0.5f, 0.7f);
		DrawUtil::Get().DrawText(ENTITY_DRAW_LAYER - 1, *userName, (Vec2(currentPosition.x - 55.0f, currentPosition.y + 127.0f) - cameraPos + drawOffset) * cameraZoom, 17.0f * cameraZoom, 0.9f, 1.0f);
	}
	if(showHealthAndEffects)
	{
		if(currentStatusEffects & STATUS_HEALTH)
		{
			DrawUtil::Get().DrawSprite(ENTITY_DRAW_LAYER - 1, "HealImage", (Vec2(currentPosition.x, currentPosition.y + 150.0f) - cameraPos + drawOffset) * cameraZoom, Vec2(20.0f) * cameraZoom);
			if(currentStatusEffects & STATUS_HEALTH << 1)
				DrawUtil::Get().DrawSprite(ENTITY_DRAW_LAYER - 1, "RemoveIcon", (Vec2(currentPosition.x, currentPosition.y + 150.0f) - cameraPos + drawOffset) * cameraZoom, Vec2(20.0f) * cameraZoom);
		}
		if(currentStatusEffects & STATUS_SPEED)
		{
			DrawUtil::Get().DrawSprite(ENTITY_DRAW_LAYER - 1, "SpeedImage", (Vec2(currentPosition.x + 25.0f, currentPosition.y + 150.0f) - cameraPos + drawOffset) * cameraZoom, Vec2(20.0f) * cameraZoom);
			if(!(currentStatusEffects & STATUS_SPEED << 1))
				DrawUtil::Get().DrawSprite(ENTITY_DRAW_LAYER - 1, "RemoveIcon", (Vec2(currentPosition.x + 25.0f, currentPosition.y + 150.0f) - cameraPos + drawOffset) * cameraZoom, Vec2(20.0f) * cameraZoom);
		}
		if(currentStatusEffects & STATUS_GRAVITY_INCREASE)
		{
			DrawUtil::Get().DrawSprite(ENTITY_DRAW_LAYER - 1, "GravityImage", (Vec2(currentPosition.x + 50.0f, currentPosition.y + 150.0f) - cameraPos + drawOffset) * cameraZoom, Vec2(20.0f) * cameraZoom);
			if(currentStatusEffects & STATUS_GRAVITY_INCREASE << 1)
				DrawUtil::Get().DrawSprite(ENTITY_DRAW_LAYER - 1, "RemoveIcon", (Vec2(currentPosition.x + 50.0f, currentPosition.y + 150.0f) - cameraPos + drawOffset) * cameraZoom, Vec2(20.0f) * cameraZoom);
		}
		if(currentStatusEffects & STATUS_DEFENSE_INCREASE)
		{
			DrawUtil::Get().DrawSprite(ENTITY_DRAW_LAYER - 1, "ShieldImage", (Vec2(currentPosition.x + 75.0f, currentPosition.y + 150.0f) - cameraPos + drawOffset) * cameraZoom, Vec2(20.0f) * cameraZoom);
			if(currentStatusEffects & STATUS_DEFENSE_INCREASE << 1)
				DrawUtil::Get().DrawSprite(ENTITY_DRAW_LAYER - 1, "RemoveIcon", (Vec2(currentPosition.x + 75.0f, currentPosition.y + 150.0f) - cameraPos + drawOffset) * cameraZoom, Vec2(20.0f) * cameraZoom);
		}
	}
	Entity::Draw(cameraPos, cameraZoom);
}
