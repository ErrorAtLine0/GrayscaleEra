#pragma once
#include "Entity.hpp"
#include "../PropertiesList.hpp"

// Block ID given to this class should be negated by one, since air isn't an actual block

class Block : public Entity
{
	public:
		CLASS_IDENTIFICATION(Block)
		Block();
		Block(const Vec2& initPosition, BlockID initBlockID, bool initIsBackground, int32_t initRoom);
		virtual bool Read(InMemBitStream& inStream);
		virtual void Write(OutMemBitStream& outStream, PropertyBitField propertyBitField) const;
		virtual void ClientUpdate();
		virtual void ServerUpdate();
		virtual void CollisionCheck();
		virtual void SimpleColRes(Entity* entity, const Vec2& penetration);
		virtual void VelocityColRes(Entity* entity, const Vec2& intersectionFractions);
		bool ReadConstructionData(InMemBitStream& inStream);
		void WriteConstructionData(OutMemBitStream& outStream) const;
		bool IsBackground() const;
		const PropertiesList& GetEffectProperties() const;
		BlockID GetBlockID() const;
		bool IsStatic() const;
	protected:
		BlockID blockID;
		bool isBackground;
		bool flipMovement;
		Vec2 startPosition;
};
