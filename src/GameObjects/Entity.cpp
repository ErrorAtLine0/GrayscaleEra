#include "../GameSandbox.hpp"
#include "Entity.hpp"
#include <GSEGL/Graphics/DrawUtil.hpp>
#include "Block.hpp"
#include "RealPlayer.hpp"
#include "NPC.hpp"
#include "../LevelData.hpp"
#define MAX_VELOCITY 2000.0f
#define CLIENT_ERROR_MARGIN_PER_SECOND 250.0f
#define SERVER_ERROR_MARGIN_PER_SECOND 150.0f
#define CLIENT_VELOCITY_FACTOR 1.2f
#define CLIENT_TIME_MARGIN 0.2f

GraphicsData::GraphicsData():
graphicsType(GRAPHICS_BLOCK),
level1(0),
level2(0),
level3(0),
loop(false),
flip(false)
{}

GraphicsData::GraphicsData(GraphicsType initGraphicsType, uint32_t initLevel1, uint8_t initLevel2, uint8_t initLevel3, bool initLoop, bool initFlip):
graphicsType(initGraphicsType),
level1(initLevel1),
level2(initLevel2),
level3(initLevel3),
loop(initLoop),
flip(initFlip)
{}

void GraphicsData::Write(OutMemBitStream& outStream) const
{
	outStream.WriteBits(graphicsType, ByteUtil::GetRequiredBits<GRAPHICS_MAX>::Value);
	outStream.WriteBits(level1);
	outStream.WriteBits(level2);
	outStream.WriteBits(level3);
	outStream.WriteBits(loop);
	outStream.WriteBits(flip);
}

bool GraphicsData::Read(InMemBitStream& inStream)
{
	if(!inStream.ReadBits(graphicsType, ByteUtil::GetRequiredBits<GRAPHICS_MAX>::Value)) return false;
	if(!inStream.ReadBits(level1)) return false;
	if(!inStream.ReadBits(level2)) return false;
	if(!inStream.ReadBits(level3)) return false;
	if(!inStream.ReadBits(loop)) return false;
	if(!inStream.ReadBits(flip)) return false;
	return true;
}

bool GraphicsData::operator==(const GraphicsData& graphicsData)
{
	return (graphicsData.graphicsType == graphicsType && graphicsData.level1 == level1 && graphicsData.level2 == level2 && graphicsData.level3 == level3 && graphicsData.loop == loop && graphicsData.flip == flip);
}

Entity::Entity():
GameObject::GameObject(),
room(-1),
lastFrames(nullptr),
lastTextures(nullptr)
{}

Entity::Entity(const Vec2& initPosition, const Vec2& initSize, const Vec2& initVelocity, const Vec2& initDrawOffset, const Vec2& initDrawSize, int32_t initRoom):
GameObject::GameObject(),
currentPosition(initPosition),
nextPosition(initPosition),
velocity(initVelocity),
size(initSize),
drawOffset(initDrawOffset),
drawSize(initDrawSize),
room(initRoom),
lastFrames(nullptr),
lastTextures(nullptr)
{
	changedProperties |= ENTITY_CONSTRUCT | ENTITY_POSITION | ENTITY_TELEPORT;
}

bool Entity::Read(InMemBitStream& inStream)
{
	if(!inStream.ReadBits(changedProperties)) return false;
	if(changedProperties & ENTITY_CONSTRUCT && GetClassID() == Block::classID)
	{
		if(!static_cast<Block*>(this)->ReadConstructionData(inStream)) return false;
	}
	if(changedProperties & ENTITY_POSITION)
	{
		Vec2 newPos;
		if(!inStream.ReadFixedFloat(newPos.x, GameSandbox::Get().GetWorldMinSize(), 1.0f, 15)) return false;
		if(!inStream.ReadFixedFloat(newPos.y, GameSandbox::Get().GetWorldMinSize(), 1.0f, 15)) return false;
		Vec2 newVel;
		if(!inStream.ReadFixedFloat(newVel.x, -MAX_VELOCITY, 1.0f, 12)) return false;
		if(!inStream.ReadFixedFloat(newVel.y, -MAX_VELOCITY, 1.0f, 12)) return false;
		
		/* For the client, the player is going through client prediction. Only use the server values
		 * if the client values are *EXTREMELY* off */
		if(GetClassID() == RealPlayer::classID && static_cast<Player*>(this)->GetPlayerID() == GameSandbox::Get().GetOwnPlayerID())
		{
			double RTT = GameSandbox::Get().GetRTTClient();
			if(RTT < 0.1)
				RTT = 0.1;
			else if(RTT > 2.0)
				RTT = 2.0;
			float clientErrorMargin = CLIENT_ERROR_MARGIN_PER_SECOND * RTT;
			bool goingInOneDirection = (newVel.y == 0.0f && velocity.y == 0.0f) || (newVel.x == 0.0f && newVel.y == 0.0f);
			if(goingInOneDirection)
				clientErrorMargin /= 3.0;
			if(clientErrorMargin < 5.0f)
				clientErrorMargin = 5.0f;
			if(clientErrorMargin > 300.0f)
				clientErrorMargin = 300.0f;
			Vec2 updateBoxSize = Vec2((std::fabs(velocity.x + newVel.x)) * RTT * CLIENT_VELOCITY_FACTOR + clientErrorMargin * 2.0f, (std::fabs(velocity.y + newVel.y)) * RTT * CLIENT_VELOCITY_FACTOR + clientErrorMargin * 2.0f);
			Vec2 updateBoxPos = Vec2(nextPosition.x < newPos.x ? nextPosition.x - clientErrorMargin : newPos.x - clientErrorMargin, nextPosition.y < newPos.y ? nextPosition.y - clientErrorMargin : newPos.y - clientErrorMargin);
				
			bool outOfBox =
			// Check if nextPosition is out of update range
			(nextPosition.x < updateBoxPos.x || nextPosition.x > updateBoxPos.x + updateBoxSize.x ||
			nextPosition.y < updateBoxPos.y || nextPosition.y > updateBoxPos.y + updateBoxSize.y) ||
			// Check if newPos is out of update range
			(newPos.x < updateBoxPos.x || newPos.x > updateBoxPos.x + updateBoxSize.x ||
			newPos.y < updateBoxPos.y || newPos.y > updateBoxPos.y + updateBoxSize.y);
			
			// Debugging client prediction
// 			if(outOfBox)
// 			{
// 				std::cerr << "Incorrect read!\n";
// 				std::cerr << "Next Pos: " << nextPosition.x << ' ' << nextPosition.y << '\n';
// 				std::cerr << "Velocity: " << velocity.x << ' ' << velocity.y << '\n';
// 				std::cerr << "New Pos: " << newPos.x << ' ' << newPos.y << '\n';
// 				std::cerr << "New Vel: " << newVel.x << ' ' << newVel.y << '\n';
// 				std::cerr << "Client Error Margin: " << clientErrorMargin << '\n';
// 				std::cerr << "Update Box Pos: " << updateBoxPos.x << ' ' << updateBoxPos.y << '\n';
// 				std::cerr << "Update Box Size: " << updateBoxSize.x << ' ' << updateBoxSize.y << '\n';
// 				std::cerr << "Packet RTT: " << GameSandbox::Get().GetRTTClient() << 
// 				'\n';
// 			}
			if(!outOfBox)
				incorrectReadTime = TimeUtil::Get().GetTimeSinceStart();
			
			if(changedProperties & (ENTITY_CONSTRUCT | ENTITY_TELEPORT) || (velocity == Vec2(0.0f) && newVel == Vec2(0.0f)) || (incorrectReadTime + RTT + CLIENT_TIME_MARGIN < TimeUtil::Get().GetTimeSinceStart()))
			{
				nextPosition = newPos;
				currentPosition = nextPosition;
				velocity = newVel;
			}
		}
		else // Any other object just gets its values set normally
		{
			double serverErrorMargin = GetClassID() == Block::classID ? 10.0f : SERVER_ERROR_MARGIN_PER_SECOND * GameSandbox::Get().GetRTTClient();
			
			if(changedProperties & (ENTITY_CONSTRUCT | ENTITY_TELEPORT) || (velocity == Vec2(0.0f) && newVel == Vec2(0.0f)) || (std::fabs(nextPosition.x - newPos.x) > serverErrorMargin || std::fabs(nextPosition.y - newPos.y) > serverErrorMargin))
			{
				nextPosition = newPos;
				currentPosition = nextPosition;
			}
			velocity = newVel;
		}
		int8_t newRoom = 0;
		if(!inStream.ReadBits(newRoom)) return false;
		SetRoomPosition(nextPosition, newRoom);
	}
	if(changedProperties & ENTITY_ANIMATION)
	{
		graphicsQueue.clear();
		uint8_t queueSize = 0;
		if(!inStream.ReadBits(queueSize)) return false;
		for(uint8_t i = 0; i < queueSize; i++)
		{
			graphicsQueue.emplace_back();
			if(!graphicsQueue.back().Read(inStream)) return false;
		}
		timeLastFramePlayed = TimeUtil::Get().GetTimeSinceStart();
		currentFrame = 0;
	}
	return true;
}

void Entity::Write(OutMemBitStream& outStream, PropertyBitField propertyBitField) const
{
	outStream.WriteBits(propertyBitField);
	if(propertyBitField & ENTITY_CONSTRUCT && GetClassID() == Block::classID)
		static_cast<const Block*>(this)->WriteConstructionData(outStream);
	if(propertyBitField & ENTITY_POSITION)
	{
		outStream.WriteFixedFloat(nextPosition.x, GameSandbox::Get().GetWorldMinSize(), 1.0f, 15);
		outStream.WriteFixedFloat(nextPosition.y, GameSandbox::Get().GetWorldMinSize(), 1.0f, 15);
		outStream.WriteFixedFloat(velocity.x, -MAX_VELOCITY, 1.0f, 12);
		outStream.WriteFixedFloat(velocity.y, -MAX_VELOCITY, 1.0f, 12);
		outStream.WriteBits(static_cast<int8_t>(room));
	}
	if(propertyBitField & ENTITY_ANIMATION)
	{
		uint8_t queueSize = graphicsQueue.size();
		outStream.WriteBits(queueSize);
		for(uint8_t i = 0; i < queueSize; i++)
			graphicsQueue[i].Write(outStream);
	}
}

void Entity::ClientUpdate()
{
	if(currentPosition.x < GameSandbox::Get().GetWorldMinSize())
		currentPosition.x = GameSandbox::Get().GetWorldMinSize();
	if(currentPosition.x > GameSandbox::Get().GetWorldMaxSize())
		currentPosition.x = GameSandbox::Get().GetWorldMaxSize();
	if(currentPosition.y < GameSandbox::Get().GetWorldMinSize())
		currentPosition.y = GameSandbox::Get().GetWorldMinSize();
	if(currentPosition.y > GameSandbox::Get().GetWorldMaxSize())
		currentPosition.y = GameSandbox::Get().GetWorldMaxSize();
	if(velocity.x > MAX_VELOCITY)
		velocity.x = MAX_VELOCITY;
	if(velocity.x < -MAX_VELOCITY)
		velocity.x = -MAX_VELOCITY;
	if(velocity.y > MAX_VELOCITY)
		velocity.y = MAX_VELOCITY;
	if(velocity.y < -MAX_VELOCITY)
		velocity.y = -MAX_VELOCITY;
}

void Entity::ServerUpdate()
{
	if(currentPosition.x < GameSandbox::Get().GetWorldMinSize())
		currentPosition.x = GameSandbox::Get().GetWorldMinSize();
	if(currentPosition.x > GameSandbox::Get().GetWorldMaxSize())
		currentPosition.x = GameSandbox::Get().GetWorldMaxSize();
	if(currentPosition.y < GameSandbox::Get().GetWorldMinSize())
		currentPosition.y = GameSandbox::Get().GetWorldMinSize();
	if(currentPosition.y > GameSandbox::Get().GetWorldMaxSize())
		currentPosition.y = GameSandbox::Get().GetWorldMaxSize();
	if(velocity.x > MAX_VELOCITY)
		velocity.x = MAX_VELOCITY;
	if(velocity.x < -MAX_VELOCITY)
		velocity.x = -MAX_VELOCITY;
	if(velocity.y > MAX_VELOCITY)
		velocity.y = MAX_VELOCITY;
	if(velocity.y < -MAX_VELOCITY)
		velocity.y = -MAX_VELOCITY;
}

void Entity::CollisionCheck()
{
	if(room < 0 || room >= static_cast<int32_t>(GameSandbox::Get().GetRoomCount())) return;
	std::vector<std::vector<GameObject*>*> chunksToCheck;
	GameSandbox::Get().GetFourNearestChunksToPos(&chunksToCheck, Vec2((currentPosition.x + currentPosition.x + size.x) / 2.0f, (currentPosition.y + currentPosition.y + size.y) / 2.0f), room);
	for(std::vector<GameObject*>* chunk : chunksToCheck)
		for(GameObject* gameObject : *chunk)
			ObjectCollisionCheck(gameObject);
	for(GameObject* gameObject : GameSandbox::Get().GetMovingObjects(room))
		ObjectCollisionCheck(gameObject);
}

void Entity::ObjectCollisionCheck(GameObject* gameObject)
{
	// Thanks to https://hamaluik.com/posts/swept-aabb-collision-using-minkowski-difference/
	Entity* gameEntity = static_cast<Entity*>(gameObject);
	if(this == gameEntity)
		return;
	
	// First check: Inaccurate check
	if(currentPosition.x - gameEntity->currentPosition.x > ((velocity.x * TimeUtil::Get().GetDeltaTime()) + size.x - (gameEntity->velocity.x * TimeUtil::Get().GetDeltaTime()) + gameEntity->size.x + 40.0f))
		return;
	if(currentPosition.y - gameEntity->currentPosition.y > ((velocity.y * TimeUtil::Get().GetDeltaTime()) + size.y - (gameEntity->velocity.y * TimeUtil::Get().GetDeltaTime()) + gameEntity->size.y + 40.0f))
		return;
	
	// Second check: Simple AABB
	Vec2 minkowskiPos = currentPosition - ((gameEntity->currentPosition) + gameEntity->size);
	Vec2 minkowskiSize = size + gameEntity->size;
	if(minkowskiPos.x <= 0.0f && minkowskiPos.x + minkowskiSize.x >= 0.0f &&
		minkowskiPos.y <= 0.0f && minkowskiPos.y + minkowskiSize.y >= 0.0f)
	{
		SimpleColRes(gameEntity, ClosestPointOnBoundsToPoint(minkowskiPos, minkowskiSize, Vec2(0.0f)));
		return;
	}
	
	// Third Check: Swept AABB
	Vec2 relativeVel = (gameEntity->velocity - velocity) * static_cast<float>(TimeUtil::Get().GetDeltaTime());
	Vec2 intersectionFractions = Vec2(std::numeric_limits<float>::infinity());
	if(relativeVel.x || relativeVel.y)
		intersectionFractions = GetRayToBoxIntersectionFractions(Vec2(0.0f), relativeVel, minkowskiPos, minkowskiSize);
	if(intersectionFractions.x < std::numeric_limits<float>::infinity())
		VelocityColRes(gameEntity, intersectionFractions);
}

Vec2 Entity::ClosestPointOnBoundsToPoint(const Vec2& boxPos, const Vec2& boxSize, const Vec2& point)
{
	float minDist = std::fabs(point.x - boxPos.x);
	Vec2 boundsPoint(boxPos.x, point.y);
	if(std::fabs((boxPos.x + boxSize.x) - point.x) < minDist)
	{
		minDist = std::fabs((boxPos.x + boxSize.x) - point.x);
		boundsPoint = Vec2((boxPos.x + boxSize.x), point.y);
	}
	if(std::fabs((boxPos.y + boxSize.y) - point.y) < minDist)
	{
		minDist = std::fabs((boxPos.y + boxSize.y) - point.y);
		boundsPoint = Vec2(point.x, (boxPos.y + boxSize.y));
	}
	if(std::fabs(boxPos.y - point.y) < minDist)
	{
		minDist = std::fabs(boxPos.y - point.y);
		boundsPoint = Vec2(point.x, boxPos.y);
	}
	return boundsPoint;
}

void Entity::SetToNextPosition()
{
	nextPosition = Vec2(nextPosition.x + velocity.x * TimeUtil::Get().GetDeltaTime(), nextPosition.y + velocity.y * TimeUtil::Get().GetDeltaTime());
	if(currentPosition != nextPosition)
		changedProperties |= ENTITY_POSITION;
	currentPosition = nextPosition;
}

Vec2 Entity::GetRayToBoxIntersectionFractions(const Vec2& origin, const Vec2& direction, const Vec2& boxPos, const Vec2& boxSize)
{
	Vec2 end = origin + direction;
	Vec2 minT = GetRayToRayIntersectionFractions(origin, end, Vec2(boxPos.x, boxPos.y), Vec2(boxPos.x, boxPos.y + boxSize.y));
	Vec2 x = GetRayToRayIntersectionFractions(origin, end, Vec2(boxPos.x, boxPos.y + boxSize.y), Vec2(boxPos.x + boxSize.x, boxPos.y + boxSize.y));
	if(x.x*x.x+x.y*x.y < minT.x*minT.x+minT.y*minT.y)
		minT = x;
	x = GetRayToRayIntersectionFractions(origin, end, Vec2(boxPos.x + boxSize.x, boxPos.y + boxSize.y), Vec2(boxPos.x + boxSize.x, boxPos.y));
	if(x.x*x.x+x.y*x.y < minT.x*minT.x+minT.y*minT.y)
		minT = x;
	x = GetRayToRayIntersectionFractions(origin, end, Vec2(boxPos.x + boxSize.x, boxPos.y), Vec2(boxPos.x, boxPos.y));
	if(x.x*x.x+x.y*x.y < minT.x*minT.x+minT.y*minT.y)
		minT = x;
	return minT;
}

Vec2 Entity::GetRayToRayIntersectionFractions(const Vec2& originA, const Vec2& endA, const Vec2& originB, const Vec2& endB)
{
	Vec2 r = endA - originA;
	Vec2 s = endB - originB;
	
	float numerator = CrossMultiply2DVectors(originB - originA, r);
	float denominator = CrossMultiply2DVectors(r, s);
	if(denominator == 0)
		return Vec2(std::numeric_limits<float>::infinity());
	float u = numerator/denominator;
	float t = CrossMultiply2DVectors(originB - originA, s) / denominator;
	if(t > 0.0f && t < 1.0f && u > 0.0f && u < 1.0f)
		return Vec2(u, t);
	return Vec2(std::numeric_limits<float>::infinity());
}

float Entity::CrossMultiply2DVectors(const Vec2& vecA, const Vec2& vecB)
{
	return vecA.x * vecB.y - vecA.y * vecB.x;
}

bool Entity::DoesBoxContainBlock(const Vec2& boxPos, const Vec2& boxSize, int32_t room)
{
	if(room < 0 || room >= static_cast<int32_t>(GameSandbox::Get().GetRoomCount())) return false;
	std::vector<std::vector<GameObject*>*> chunksToCheck;
	GameSandbox::Get().GetFourNearestChunksToPos(&chunksToCheck, Vec2((boxPos.x + boxPos.x + boxSize.x) / 2.0f, (boxPos.y + boxPos.y + boxSize.y) / 2.0f), room);
	for(std::vector<GameObject*>* chunk : chunksToCheck)
		for(GameObject* gameObj : *chunk)
		{
			if(gameObj && gameObj->GetClassID() == Block::classID)
			{
				Block* blockPtr = static_cast<Block*>(gameObj);
				if(!blockPtr->IsBackground())
				{
					if(BoxToBoxCollision(boxPos, boxSize, blockPtr->GetCurrentPosition(), blockPtr->GetSize()))
						return true;
				}
			}
		}
	for(GameObject* gameObj : GameSandbox::Get().GetMovingObjects(room))
	{
		if(gameObj && gameObj->GetClassID() == Block::classID)
		{
			Block* blockPtr = static_cast<Block*>(gameObj);
			if(!blockPtr->IsBackground())
			{
				if(BoxToBoxCollision(boxPos, boxSize, blockPtr->GetCurrentPosition(), blockPtr->GetSize()))
					return true;
			}
		}
	}
	return false;
}

bool Entity::BoxToBoxCollision(const Vec2& box1Pos, const Vec2& box1Size, const Vec2& box2Pos, const Vec2& box2Size)
{
	if(box1Pos.x <= box2Pos.x + box2Size.x && box1Pos.x + box1Size.x >= box2Pos.x)
		if(box1Pos.y <= box2Pos.y + box2Size.y && box1Pos.y + box1Size.y >= box2Pos.y)
			return true;
	return false;
}

const Vec2& Entity::GetCurrentPosition() const { return currentPosition; }
const Vec2& Entity::GetSize() const { return size; }
const Vec2& Entity::GetVelocity() const { return velocity; }
int32_t Entity::GetRoom() const { return room; }

void Entity::Draw(const Vec2& cameraPos, float cameraZoom)
{
	for(;;)
	{
		Vec2 winSize = WindowUtil::Get().GetWindowSize() / cameraZoom;
		if((currentPosition.x + drawOffset.x > cameraPos.x + winSize.x || currentPosition.x + drawOffset.x + drawSize.x < cameraPos.x) ||
			currentPosition.y + drawOffset.x > cameraPos.y + winSize.y || currentPosition.y + drawOffset.y + drawSize.y < cameraPos.y)
			break;
		Vec2 drawPos = (currentPosition - cameraPos + drawOffset) * cameraZoom;
		Vec2 zoomedDrawSize = drawSize * cameraZoom;
		if(!graphicsQueue.empty())
		{
			GraphicsData& graphicsToPlay = graphicsQueue.front();
			const std::vector<Frame>* animation = GetAnimationFromGraphicsData(graphicsToPlay);
			const std::vector<TexturePtr>* textures = GetTexturesFromGraphicsData(graphicsToPlay);
			lastGraphicsData = graphicsToPlay;
			if((animation && textures) && (!animation->empty() && !textures->empty()))
			{
				if(TimeUtil::Get().GetTimeSinceStart() > timeLastFramePlayed + (*animation)[currentFrame].GetMsLength()/1000.0)
				{
					++currentFrame;
					if(currentFrame == animation->size())
					{
						currentFrame = 0;
						if(!graphicsToPlay.loop)
						{
							graphicsQueue.pop_front();
							continue;
						}
					}
					timeLastFramePlayed = TimeUtil::Get().GetTimeSinceStart();
				}
				DrawUtil::Get().DrawSprite(ENTITY_DRAW_LAYER, (*textures)[currentFrame], drawPos, zoomedDrawSize, 0.0f, WHITENESS_TRANSPARENCY, 1.0f, BVec2(graphicsToPlay.flip, false));
			}
			else if(!graphicsToPlay.loop)
				graphicsQueue.pop_front();
			else
				DrawUtil::Get().DrawSprite(ENTITY_DRAW_LAYER, static_cast<TexturePtr>(nullptr), drawPos, zoomedDrawSize);
		}
		else
			DrawUtil::Get().DrawSprite(ENTITY_DRAW_LAYER, static_cast<TexturePtr>(nullptr), drawPos, zoomedDrawSize);
		break;
	}
}

const std::vector<Frame>* Entity::GetAnimationFromGraphicsData(const GraphicsData& graphicsData)
{
	if(lastFrames && lastGraphicsData == graphicsData)
		return lastFrames;
	switch(graphicsData.graphicsType)
	{
		case GRAPHICS_BLOCK:
		{
			lastFrames = &(GameSandbox::Get().GetBlockData()[graphicsData.level1].animation);
			break;
		}
		case GRAPHICS_PLAYER:
		{
			if(graphicsData.level2 == 0)
			{
				if(graphicsData.level3 == 0)
					lastFrames = &(GameSandbox::Get().GetPlayerDataByPlayerID(graphicsData.level1)->idleAnimation);
				else if(graphicsData.level3 == 1)
					lastFrames = &(GameSandbox::Get().GetPlayerDataByPlayerID(graphicsData.level1)->walkAnimation);
			}
			else
				lastFrames = &(GameSandbox::Get().GetPlayerDataByPlayerID(graphicsData.level1)->abilities[graphicsData.level2-1].GetAnimationListRef()[graphicsData.level3].second);
			break;
		}
		case GRAPHICS_NPC:
		{
			if(graphicsData.level2 == 0)
			{
				if(graphicsData.level3 == 0)
					lastFrames = &(GameSandbox::Get().GetPlayerDataByNPCID(graphicsData.level1)->idleAnimation);
				else if(graphicsData.level3 == 1)
					lastFrames = &(GameSandbox::Get().GetPlayerDataByNPCID(graphicsData.level1)->walkAnimation);
			}
			else
				lastFrames = &(GameSandbox::Get().GetPlayerDataByNPCID(graphicsData.level1)->abilities[graphicsData.level2-1].GetAnimationListRef()[graphicsData.level3].second);
			break;
		}
		case GRAPHICS_MAX: break;
	}
	return lastFrames;
}

const std::vector<TexturePtr>* Entity::GetTexturesFromGraphicsData(const GraphicsData& graphicsData)
{
	if(lastTextures && lastGraphicsData == graphicsData)
		return lastTextures;
	switch(graphicsData.graphicsType)
	{
		case GRAPHICS_BLOCK:
		{
			lastTextures = GameSandbox::Get().GetBlockTexturesFromID(graphicsData.level1);
			break;
		}
		case GRAPHICS_PLAYER:
		{
			lastTextures = (&(*GameSandbox::Get().GetPlayerTexturesFromID(graphicsData.level1))[graphicsData.level2][graphicsData.level3]);
			break;
		}
		case GRAPHICS_NPC:
		{
			lastTextures = (&(*GameSandbox::Get().GetNPCTexturesFromID(graphicsData.level1))[graphicsData.level2][graphicsData.level3]);
			break;
		}
		case GRAPHICS_MAX: break;
	}
	return lastTextures;
}

void Entity::SetAnimation(const GraphicsData& graphicsData)
{
	graphicsQueue.clear();
	AddAnimation(graphicsData);
}

void Entity::AddAnimation(const GraphicsData& graphicsData)
{
	changedProperties |= ENTITY_ANIMATION;
	graphicsQueue.emplace_back(graphicsData);
	timeLastFramePlayed = TimeUtil::Get().GetTimeSinceStart();
	currentFrame = 0;
}

void Entity::SetRoomPosition(const Vec2& newPos, int32_t newRoom)
{
	changedProperties |= ENTITY_POSITION;
	currentPosition = newPos;
	nextPosition = newPos;
	if(newRoom != room)
	{
		changedProperties |= ENTITY_TELEPORT;
		GameSandbox::Get().MoveObject(this, room, newRoom);
		room = newRoom;
	}
}

void Entity::ServerMoveTo(const Vec2& moveDirection, double timeSkip)
{
	Vec2 oldVelocity = velocity;
	velocity = (moveDirection / TimeUtil::Get().GetDeltaTime()) * timeSkip;
	SetToNextPosition();
	velocity = oldVelocity;
}
