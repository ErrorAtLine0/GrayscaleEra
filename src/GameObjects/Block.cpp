#include "../GameSandbox.hpp"
#include "Block.hpp"
#include "Player.hpp"

ClassID Block::classID;

Block::Block():
Entity::Entity(Vec2(0.0f), Vec2(40.0f), Vec2(0.0f), Vec2(0.0f), Vec2(40.0f), -1)
{}

Block::Block(const Vec2& initPosition, BlockID initBlockID, bool initIsBackground, int32_t initRoom):
Entity::Entity(initPosition, Vec2(40.0f), Vec2(0.0f), Vec2(0.0f), Vec2(40.0f), initRoom),
blockID(initBlockID),
isBackground(initIsBackground),
flipMovement(false),
startPosition(initPosition)
{
	SetAnimation(GraphicsData(GRAPHICS_BLOCK, blockID, 0, 0, true, false));
	const PropertiesList& propList = GameSandbox::Get().GetBlockData()[blockID].propList;
	const PropertiesList& movementProperties = propList.GetList(0);
	float XDistance = movementProperties.GetInt(0) * 40.0f;
	float YDistance = movementProperties.GetInt(1) * 40.0f;
	float speed = movementProperties.GetInt(2);
	if((XDistance != 0.0f || YDistance != 0.0f) && speed != 0.0f)
	{
		float totalDistance = std::fabs(XDistance) + std::fabs(YDistance);
		velocity.x = (XDistance / totalDistance) * speed;
		velocity.y = (YDistance / totalDistance) * speed;
	}
}

bool Block::Read(InMemBitStream& inStream)
{
	if(!Entity::Read(inStream)) return false;
	return true;
}

void Block::Write(OutMemBitStream& outStream, PropertyBitField propertyBitField) const
{
	Entity::Write(outStream, propertyBitField);
}

bool Block::ReadConstructionData(InMemBitStream& inStream)
{
	if(!inStream.ReadBits(blockID)) return false;
	if(!inStream.ReadBits(isBackground)) return false;
	return true;
}

void Block::WriteConstructionData(OutMemBitStream& outStream) const
{
	outStream.WriteBits(blockID);
	outStream.WriteBits(isBackground);
}

void Block::ClientUpdate()
{
	Entity::ClientUpdate();
}

void Block::ServerUpdate()
{
	changedProperties = 0;
	if(velocity.x || velocity.y)
	{
		const PropertiesList& movementProperties = GameSandbox::Get().GetBlockData()[blockID].propList.GetList(0);
		float rangeX = movementProperties.GetInt(0) * 40.0f;
		float rangeY = movementProperties.GetInt(1) * 40.0f;
		float range = rangeX * rangeX + rangeY * rangeY;
		float diffX = currentPosition.x - startPosition.x;
		float diffY = currentPosition.y - startPosition.y;
		float distanceFromBlockToStart = diffX * diffX + diffY * diffY;
		if(flipMovement)
		{
			if((diffX <= 0.0f && velocity.x <= 0.0f) || (diffX >= 0.0f && velocity.x >= 0.0f))
				if((diffY <= 0.0f && velocity.y <= 0.0f) || (diffY >= 0.0f && velocity.y >= 0.0f))
				{
					flipMovement = false;
					velocity.x = -velocity.x;
					velocity.y = -velocity.y;
					currentPosition = startPosition;
					nextPosition = startPosition;
				}
		}
		else if(distanceFromBlockToStart >= range)
		{
			if(movementProperties.GetBool(3))
			{
				currentPosition = startPosition;
				nextPosition = startPosition;
			}
			else
			{
				flipMovement = true;
				velocity.x = -velocity.x;
				velocity.y = -velocity.y;
			}
		}
		changedProperties |= ENTITY_POSITION;
	}
	Entity::ServerUpdate();
}

void Block::CollisionCheck() {}

void Block::SimpleColRes(Entity* entity, const Vec2& penetration) {}

void Block::VelocityColRes(Entity* entity, const Vec2& intersectionFractions) {}

bool Block::IsBackground() const { return isBackground; }

const PropertiesList& Block::GetEffectProperties() const
{
	return GameSandbox::Get().GetBlockData()[blockID].propList.GetList(1);
}

BlockID Block::GetBlockID() const { return blockID; }

bool Block::IsStatic() const
{
	const PropertiesList& movementProperties = GameSandbox::Get().GetBlockData()[blockID].propList.GetList(0);
	return ((movementProperties.GetInt(0) == 0 && movementProperties.GetInt(1) == 0) || movementProperties.GetInt(2) == 0);
}
