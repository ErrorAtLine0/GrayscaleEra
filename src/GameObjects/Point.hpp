#pragma once
#include "../PointData.hpp"
#include <GSEGL/Network/ReplicationManager.hpp>
#include <unordered_map>

/* These are meant to be only summoned on the server side.*
 * Clients don't have to know where points are placed in the level */

class Point
{
	public:
		Point(Vec2 initPos, int32_t initRoom, const PointData* initData);
		void ServerUpdate();
		Vec2 GetPos() const;
		int32_t GetRoom() const;
		const PointData* GetData() const;
		void SetNumber(const UTF8String& name, uint32_t val);
		uint32_t GetNumber(const UTF8String& name) const;
		uint32_t* GetNumberPtr(const UTF8String& name);
	private:
		bool IsConditionTrue(uint32_t condIndex, const HeadlineProperty* condProp);
		void ExecuteResult(uint32_t resIndex, const HeadlineProperty* resProp);
		Vec2 pos;
		int32_t room;
		GameObject* block;
		const PointData* data;
		std::unordered_map<UTF8String, uint32_t> numbers;
		std::unordered_map<const HeadlineProperty*, double> propTimers;
};
