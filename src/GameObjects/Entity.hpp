#pragma once
#include <GSEGL/Network/ReplicationManager.hpp>
#include "../Frame.hpp"
#include <GSEGL/Graphics/Texture.hpp>
#include <deque>
#define ENTITY_DRAW_LAYER 16

enum GraphicsType
{
	GRAPHICS_BLOCK,
	GRAPHICS_PLAYER,
	GRAPHICS_NPC,
	GRAPHICS_MAX
};

struct GraphicsData
{
	GraphicsData();
	GraphicsData(GraphicsType initGraphicsType, uint32_t initLevel1, uint8_t initLevel2, uint8_t initLevel3, bool initLoop, bool initFlip);
	void Write(OutMemBitStream& outStream) const;
	bool Read(InMemBitStream& inStream);
	bool operator==(const GraphicsData& graphicsData);
	GraphicsType graphicsType;
	uint32_t level1;
	uint8_t level2;
	uint8_t level3;
	bool loop;
	bool flip;
};

class Entity : public GameObject
{
	public:
		Entity();
		Entity(const Vec2& initPosition, const Vec2& initSize, const Vec2& initVelocity, const Vec2& DrawOffset, const Vec2& initDrawSize, int32_t initRoom);
		virtual bool Read(InMemBitStream& inStream);
		virtual void Write(OutMemBitStream& outStream, PropertyBitField propertyBitField) const;
		virtual void ClientUpdate();
		virtual void ServerUpdate();
		virtual void CollisionCheck();
		virtual void SimpleColRes(Entity* entity, const Vec2& penetration) = 0;
		virtual void VelocityColRes(Entity* entity, const Vec2& intersectionFractions) = 0;
		virtual void Draw(const Vec2& cameraPos, float cameraZoom);
		void SetToNextPosition();
		
		void SetRoomPosition(const Vec2& newPos, int32_t newRoom);
		void ServerMoveTo(const Vec2& moveDirection, double timeSkip);
		
		void SetAnimation(const GraphicsData& graphicsData);
		void AddAnimation(const GraphicsData& graphicsData);
		
		const std::vector<Frame>* GetAnimationFromGraphicsData(const GraphicsData& graphicsData);
		const std::vector<TexturePtr>* GetTexturesFromGraphicsData(const GraphicsData& graphicsData);
		
		const Vec2& GetCurrentPosition() const;
		const Vec2& GetSize() const;
		const Vec2& GetVelocity() const;
		int32_t GetRoom() const;
		
		static Vec2 ClosestPointOnBoundsToPoint(const Vec2& boxPos, const Vec2& boxSize, const Vec2& point);
		static Vec2 GetRayToBoxIntersectionFractions(const Vec2& origin, const Vec2& direction, const Vec2& boxPos, const Vec2& boxSize);
		static Vec2 GetRayToRayIntersectionFractions(const Vec2& originA, const Vec2& endA, const Vec2& originB, const Vec2& endB);
		static float CrossMultiply2DVectors(const Vec2& vecA, const Vec2& vecB);
		static bool DoesBoxContainBlock(const Vec2& boxPos, const Vec2& boxSize, int32_t room);
		static bool BoxToBoxCollision(const Vec2& box1Pos, const Vec2& box1Size, const Vec2& box2Pos, const Vec2& box2Size);
	protected:
		void ObjectCollisionCheck(GameObject* gameObject);
		enum EntityProperties
		{
			ENTITY_CONSTRUCT = 1 << 0,
			ENTITY_POSITION = 1 << 1,
			ENTITY_ANIMATION = 1 << 2,
			ENTITY_TELEPORT = 1 << 3
		};
		Vec2 currentPosition;
		Vec2 nextPosition;
		Vec2 velocity;
		Vec2 size;
		
		Vec2 drawOffset;
		Vec2 drawSize;
		
		int32_t room;
		
		// Keep last graphics data stored (speedup)
		GraphicsData lastGraphicsData;
		const std::vector<Frame>* lastFrames;
		const std::vector<TexturePtr>* lastTextures;
		
		std::deque<GraphicsData> graphicsQueue;
		double incorrectReadTime;
		double timeLastFramePlayed;
		uint32_t currentFrame;
};
