#include "Frame.hpp"
#include <GSEGL/ErrorUtil.hpp>
#include <cstring>

Frame::Frame():
size(0),
data(nullptr),
msLength(0)
{}

Frame::Frame(uint32_t initSize, uint16_t initMsLength):
size(initSize),
data(static_cast<uint8_t*>(std::malloc(initSize))),
msLength(initMsLength)
{
	for(uint32_t i = 0; i < size; i++)
		data[i] = 0;
}

Frame::Frame(const Frame& frame):
size(frame.size),
data(static_cast<uint8_t*>(std::malloc(frame.size))),
msLength(frame.msLength)
{
	std::memcpy(data, frame.data, size);
}

void Frame::operator=(const Frame& frame)
{
	size = frame.size;
	msLength = frame.msLength;
	uint8_t* tempData = static_cast<uint8_t*>(std::realloc(data, size));
	if(tempData)
	{
		data = tempData;
		std::memcpy(data, frame.data, size);
	}
}

Frame::~Frame() { std::free(data); }

void Frame::Write(OutMemBitStream& outStream) const
{
	outStream.WriteBits(size);
	outStream.WriteBits(static_cast<const void*>(data), size*8);
	outStream.WriteBits(msLength);
}

bool Frame::Read(InMemBitStream& inStream)
{
	if(!inStream.ReadBits(size)) return false;
	uint8_t* tempData = static_cast<uint8_t*>(std::realloc(data, size));
	if(!tempData)
	{
		ErrorUtil::Get().SetError("Memory", "Frame::Read Could not allocate data");
		return false;
	}
	data = tempData;
	if(!inStream.ReadBits(static_cast<void*>(data), size*8)) return false;
	if(!inStream.ReadBits(msLength)) return false;
	return true;
}

void Frame::WriteAnimation(OutMemBitStream& outStream, const std::vector<Frame>& animation)
{
	outStream.WriteBits(animation.size(), 16);
	for(const Frame& frame : animation)
		frame.Write(outStream);
}

bool Frame::ReadAnimation(InMemBitStream& inStream, std::vector<Frame>& animation)
{
	animation.clear();
	uint16_t framesInAnimation = 0;
	if(!inStream.ReadBits(framesInAnimation, 16)) return false;
	for(uint8_t i = 0; i < framesInAnimation; i++)
	{
		animation.emplace_back();
		if(!animation.back().Read(inStream)) return false;
	}
	return true;
}

void Frame::InitAnimationTextures(std::vector<TexturePtr>& vectorToInitTo, const std::vector<Frame>& animation, const UVec2& textureSize)
{
	for(uint8_t i = 0; i < animation.size(); i++)
		vectorToInitTo.emplace_back(std::make_shared<Texture>(animation[i].GetData(), textureSize.x, textureSize.y));
}

uint32_t Frame::GetSize() const { return size; }
const uint8_t* Frame::GetData() const { return data; }
uint8_t* Frame::GetMutableDataPtr() { return data; }
uint16_t Frame::GetMsLength() const { return msLength; }

void Frame::SetData(const uint8_t* data) { std::memcpy(this->data, data, size); }
void Frame::SetMsLength(uint16_t msLength) { this->msLength = msLength; }
