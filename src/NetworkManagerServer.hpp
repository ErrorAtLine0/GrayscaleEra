#pragma once
#include "NetworkManager.hpp"
#include <GSEGL/Network/ReliableDataManager.hpp>
#include <GSEGL/Network/ReplicationManager.hpp>
#include <vector>
#include "LevelData.hpp"
#include "GameSandbox.hpp"

#define COUNTDOWN_TIME 5.0
class ClientProxy
{
	public:
		ClientProxy(LinkingContext* initLinkingContext):
		deliveryNotifier(new DeliveryNotificationManager),
		reliableDataManager(new ReliableDataManager),
		replicationManager(new ReplicationManager(initLinkingContext)),
		timeLastPacketSent(0.0),
		hasAllData(false),
		gotPlayerData(false),
		playerNetData(std::make_shared<PlayerNetData>())
		{}
		~ClientProxy()
		{
			delete deliveryNotifier;
			delete reliableDataManager;
			delete replicationManager;
		}
		DeliveryNotificationManager* deliveryNotifier;
		ReliableDataManager* reliableDataManager;
		ReplicationManager* replicationManager;
		SocketAddress addr;
		double timeLastPacketSent;
		bool hasAllData;
		bool gotPlayerData;
		PrivateID privateID;
		PlayerNetDataPtr playerNetData;
};

typedef std::shared_ptr<ClientProxy> ClientProxyPtr;

class NetworkManagerServer : public NetworkManager
{
	public:
		NetworkManagerServer();
		bool Init(const UTF8String& initUserName, const UTF8String& initLobbyName, const WorldDataPtr& initWorldData, const PropertiesList& initServerSettings, bool initIsSeparateLobby);
		bool Init(const UTF8String& initUserName, uint16_t initPort, const WorldDataPtr& initWorldData, const PropertiesList& initServerSettings);
		bool Update();
		bool IsServer() { return true; }
		void SendChatMessage(const UTF8String& chatMessage);
		void BroadcastMessage(PlayerID playerIDToSendTo, const UTF8String& message);
		void BroadcastAnnouncement(PlayerID playerIDToSendTo, const UTF8String& message);
		void CreateObject(GameObject* gameObj, bool sendToClients = true);
		void UpdateObject(GameObject* gameObj, bool sendToClients = true);
		void DestroyObject(GameObject* gameObj, bool sendToClients = true);
		double GetPlayerEstimatedSendRate(PlayerID playerID);
		double GetPlayerRTT(PlayerID playerID);
		static void InitSettingsList();
		static PropertiesList serverSettingsList;
		~NetworkManagerServer();
	private:
		bool ServerInit(const WorldDataPtr& initWorldData, const PropertiesList& initServerSettings, bool initIsSeparateLobby);
		UTF8String GetFormattedChatMessage(const UTF8String& userName, const UTF8String& chatMessage);
		void SendWorldDataToClients();
		void SendPlayerDataToClients(const ClientProxyPtr& playerOwner);
		PrivateID GenerateUniquePrivateID();
		ClientProxyPtr FindClientByAddress(const SocketAddress& addr);
		ClientProxyPtr FindClientByPlayerID(PlayerID playerID);
		ClientProxyPtr FindClientByPrivateID(PrivateID privateID);
		std::vector<ClientProxyPtr> clients;
		SocketAddressPtr internalAddr;
		bool isSeparateLobby;
		bool waitingForSignalToStart;
		GameSandboxProperties gameProperties;
};
