#include "GameSandbox.hpp"
#include <GSEGL/RandGenUtil.hpp>
#include <GSEGL/FileUtil.hpp>
#include <GSEGL/LogUtil.hpp>
#include "NetworkManagerServer.hpp"
#include "NetworkManagerClient.hpp"
#include "GameObjects/Block.hpp"
#include "GameObjects/Player.hpp"
#include "GameObjects/NPC.hpp"
#include "PropertyDecoderUtil.hpp"
#include <cmath>
#define CHUNKSIZE 10
#define CHUNKSIZEINPIXELS (CHUNKSIZE * 40.0f)

PlayerIDGenerator& PlayerIDGenerator::Get()
{
	static PlayerIDGenerator sInstance;
	return sInstance;
}

void PlayerIDGenerator::Init()
{
	playerID = 1;
}

PlayerID PlayerIDGenerator::GeneratePlayerID()
{
	return playerID++;
}

GameSandbox& GameSandbox::Get()
{
	static GameSandbox sInstance;
	return sInstance;
}

void GameSandbox::Init(const GameSandboxProperties& initGameProperties, const WorldDataPtr& initWorldData, const std::shared_ptr<NetworkManager>& initNetMan, PlayerID initOwnPlayerID)
{
	gameProperties = initGameProperties;
	netMan = initNetMan;
	isNotClient = !netMan || netMan->IsServer();
	isServer = netMan ? netMan->IsServer() : false;
	timeLeft = gameProperties.gameTime;
	gameEnded = false;
	worldDataPtr = initWorldData;
	announcements.clear();
	announcementsSent.clear();
	isFirstUpdate = true;
	
	// If there is no network manager, the Game Sandbox handles generating all player IDs on its own
	if(!netMan)
		PlayerIDGenerator::Get().Init();
	
	switch(worldDataPtr->GetType())
	{
		case WORLD_ADVENTURE:
		{
			adventureDataPtr = std::static_pointer_cast<AdventureData>(worldDataPtr);
			blocks = &adventureDataPtr->blocks;
			npcs = &adventureDataPtr->npcs;
			for(uint32_t roomNumber = 0; roomNumber < adventureDataPtr->rooms.size(); roomNumber++)
			{
				LevelData& level = adventureDataPtr->rooms[roomNumber];
				levels.emplace_back(&level);
				roomObjects.emplace_back();
				movingObjects.emplace_back();
				staticBlockChunks.emplace_back();
				uint32_t chunkCountOnAxis = level.levelSize % CHUNKSIZE == 0 ? level.levelSize / CHUNKSIZE : level.levelSize / CHUNKSIZE + 1;
				for(uint32_t i = 0; i < chunkCountOnAxis; i++)
				{
					staticBlockChunks[roomNumber].emplace_back();
					for(uint32_t j = 0; j < chunkCountOnAxis; j++)
						staticBlockChunks[roomNumber][i].emplace_back();
				}
			}
			LoadNPCPropLists(adventureDataPtr->settings.GetRuleList(5));
			break;
		}
		case WORLD_ARENA:
		{
			arenaDataPtr = std::static_pointer_cast<ArenaData>(worldDataPtr);
			blocks = &arenaDataPtr->blocks;
			levels.emplace_back(&arenaDataPtr->level);
			roomObjects.emplace_back();
			movingObjects.emplace_back();
			staticBlockChunks.emplace_back();
			uint32_t chunkCountOnAxis = arenaDataPtr->level.levelSize % CHUNKSIZE == 0 ? arenaDataPtr->level.levelSize / CHUNKSIZE : arenaDataPtr->level.levelSize / CHUNKSIZE + 1;
			for(uint32_t i = 0; i < chunkCountOnAxis; i++)
			{
				staticBlockChunks[0].emplace_back();
				for(uint32_t j = 0; j < chunkCountOnAxis; j++)
					staticBlockChunks[0][i].emplace_back();
			}
			break;
		}
		case WORLD_MAX: break;
	}
	
	ownPlayerID = initOwnPlayerID;
	
	for(const BlockData& block : *blocks)
	{
		blockTextures.emplace_back();
		Frame::InitAnimationTextures(blockTextures.back(), block.animation, UVec2(BLOCK_TEXTURE_SIZE));
	}
	if(npcs)
	{
		for(const PlayerData& npcData : *npcs)
		{
			npcTextures.emplace_back();
			InitPlayerTextures(npcTextures.back(), npcData);
		}
	}
	
	/* Quick Load: Allows the client to create the blocks in the level based on the level data in hand, 
	* without waiting for the server to send the data. */
	for(uint32_t room = 0; room < levels.size(); room++)
	{
		LevelData* level = levels[room];
		for(uint32_t i = 0; i < level->levelSize; i++)
			for(uint32_t j = 0; j < level->levelSize; j++)
			{
				if(level->BlockAt(j, i) != 0)
				{
					GameObject* newBlock = new Block(Vec2(j*40.0f, i*40.0f), level->BlockAt(j, i)-1, false, room);
					CreateObject(newBlock, false);
				}
				if(level->BackBlockAt(j, i) != 0)
				{
					GameObject* newBlock = new Block(Vec2(j*40.0f, i*40.0f), level->BackBlockAt(j, i)-1, true, room);
					CreateObject(newBlock, false);
				}
				if(level->PointAt(j, i) != 0)
				{
					if(isNotClient)
						points.emplace(arenaDataPtr ? "" : adventureDataPtr->points[level->PointAt(j, i) - 1].propList.GetString(0), new Point(Vec2(j*40.0f, i*40.0f), room, arenaDataPtr ? nullptr : &adventureDataPtr->points[level->PointAt(j, i) - 1]));
					if(adventureDataPtr)
						LoadNPCPropLists(adventureDataPtr->points[level->PointAt(j, i) - 1].propList.GetRuleList(1));
				}
			}
	}
}

void GameSandbox::LoadNPCPropLists(const RuleList& ruleList)
{
	for(uint32_t i = 0; i < ruleList.GetListSize(); i++)
		for(const HeadlineProperty* headProp : ruleList.GetRuleAt(i).resultProperties)
		{
			if(headProp->GetPropertyType() == PROPERTY_LIST && static_cast<const ListProperty*>(headProp)->data.GetListName() == "NPC")
				npcPropLists.emplace_back(&static_cast<const ListProperty*>(headProp)->data);
		}
}

void GameSandbox::InitPlayerTextures(std::vector<std::vector<std::vector<TexturePtr>>>& thisPlayerTextures, const PlayerData& playerData)
{
	thisPlayerTextures.emplace_back(); // Main animations
	thisPlayerTextures.back().emplace_back(); // Idle textures
	Frame::InitAnimationTextures(thisPlayerTextures.back().back(), playerData.idleAnimation, UVec2(PLAYER_TEXTURE_SIZE));
	thisPlayerTextures.back().emplace_back(); // Walk textures
	Frame::InitAnimationTextures(thisPlayerTextures.back().back(), playerData.walkAnimation, UVec2(PLAYER_TEXTURE_SIZE));
	for(uint32_t i = 0; i < 5; i++) // Ability animations
	{
		thisPlayerTextures.emplace_back();
		for(const auto& namedPair : playerData.abilities[i].GetAnimationListRef())
		{
			thisPlayerTextures.back().emplace_back();
			Frame::InitAnimationTextures(thisPlayerTextures.back().back(), namedPair.second, UVec2(PLAYER_TEXTURE_SIZE));
		}
	}
}

bool GameSandbox::Update()
{
	timeLeft -= TimeUtil::Get().GetDeltaTime();
	if(timeLeft < 0.0)
		timeLeft = 0.0;
	if(isNotClient)
	{
		for(std::vector<GameObject*>& movingObjectRoom : movingObjects)
			for(GameObject* gameObj : movingObjectRoom)
				gameObj->ServerUpdate();
		for(std::vector<GameObject*>& movingObjectRoom : movingObjects)
			for(GameObject* gameObj : movingObjectRoom)
				if(!gameObj->IsSetToDestroy())
					gameObj->CollisionCheck();
		for(std::vector<GameObject*>& movingObjectRoom : movingObjects)
			for(GameObject* gameObj : movingObjectRoom)
				if(!gameObj->IsSetToDestroy())
					gameObj->SetToNextPosition();
		if(worldDataPtr->GetType() == WORLD_ADVENTURE)
		{
			for(auto& pointPair : points)
				pointPair.second->ServerUpdate();
			for(Player* player : playerObjects)
				player->ServerRuleUpdate();
		}
		if(isServer)
		{
			std::shared_ptr<NetworkManagerServer> netManServer = std::static_pointer_cast<NetworkManagerServer>(netMan);
			for(GameObject* gameObj : objects)
				netManServer->UpdateObject(gameObj);
		}
		for(GameObject* objectToSummon : objectsToSummon)
			CreateObject(objectToSummon);
		objectsToSummon.clear();
		for(auto it = objects.begin(); it != objects.end();)
		{
			GameObject* gameObj = *it;
			if(gameObj->IsSetToDestroy())
			{
				if(gameObj->GetClassID() == RealPlayer::classID)
				{
					RealPlayer* playerPtr = static_cast<RealPlayer*>(gameObj);
					PlayerInfoWeakPtr playerInfoWeakPtr = playerPtr->GetPlayerInfoWeakPtr();
					if(!playerInfoWeakPtr.expired())
						playerRespawnTimers.emplace_back(gameProperties.respawnTime, playerInfoWeakPtr.lock());
				}
				it = DestroyObject(gameObj);
			}
			else
				++it;
		}
		for(uint32_t i = 0; i < playerRespawnTimers.size(); i++) // Number simply wraps back to 0 if it ever goes to UINT_MAX
		{
			playerRespawnTimers[i].first -= TimeUtil::Get().GetDeltaTime();
			if(playerRespawnTimers[i].first <= 0.0)
			{
				RespawnPlayer(playerRespawnTimers[i].second);
				playerRespawnTimers.erase(playerRespawnTimers.begin()+i);
				--i;
			}
		}
		// Send announcements to clients
		if(isServer)
		{
			std::shared_ptr<NetworkManagerServer> netManServer = std::static_pointer_cast<NetworkManagerServer>(netMan);
			for(PlayerInfoPtr& realPlayerInfo : realPlayerInfos)
			{
				if(realPlayerInfo->playerID != ownPlayerID)
				{
					UTF8String& announcementSent = announcementsSent[realPlayerInfo->playerID];
					UTF8String& announcement = announcements[realPlayerInfo->playerID];
					if(announcementSent != announcement)
					{
						netManServer->BroadcastAnnouncement(realPlayerInfo->playerID, announcement);
						announcementSent = announcement;
					}
				}
			}
		}
		if(gameProperties.gameTime != 0 && GetTimeLeftInSeconds() == 0.0)
			gameEnded = true;
	}
	else
	{
		for(std::vector<GameObject*>& movingObjectRoom : movingObjects)
			for(GameObject* gameObj : movingObjectRoom)
				gameObj->ClientUpdate();
		for(std::vector<GameObject*>& movingObjectRoom : movingObjects)
			for(GameObject* gameObj : movingObjectRoom)
			{
				if((gameObj->GetClassID() == RealPlayer::classID || gameObj->GetClassID() == NPC::classID))
					gameObj->CollisionCheck();
				gameObj->SetToNextPosition();
			}
	}
	isFirstUpdate = false;
	if(gameEnded)
		return false;
	return true;
}

void GameSandbox::DrawObjects()
{
	if(cameraRoom >= 0 && cameraRoom < roomObjects.size())
		for(GameObject* object : roomObjects[cameraRoom])
			object->Draw(cameraPos, cameraZoom);
}

void GameSandbox::SetCamera(const Vec2& cameraPos, float cameraZoom, uint32_t cameraRoom)
{
	this->cameraPos = cameraPos;
	this->cameraZoom = cameraZoom;
	this->cameraRoom = cameraRoom;
}

PlayerID GameSandbox::AddPlayer(const UTF8String& userName, const PlayerDataPtr& playerData, PlayerID playerID)
{
	playerIDToPlayerData.emplace(playerID, playerData);
	InitPlayerTextures(playerTextures[playerID], *playerData);
	return AddPlayerGeneral(userName, playerID);
}

PlayerID GameSandbox::AddPlayer(const UTF8String& userName, NPCID npcIDForData, PlayerID playerID)
{
	playerIDToNPCID.emplace(playerID, npcIDForData);
	return AddPlayerGeneral(userName, playerID);
}

PlayerID GameSandbox::AddPlayerGeneral(const UTF8String& userName, PlayerID playerID)
{
	realPlayerInfos.emplace_back(std::make_shared<PlayerInfo>());
	realPlayerInfos.back()->userName = userName;
	realPlayerInfos.back()->playerID = playerID;
	realPlayerInfos.back()->numbers.emplace(TEAM_NUMBER_NAME, realPlayerInfos.size());
	if(isNotClient)
	{
		if(worldDataPtr->GetType() == WORLD_ARENA)
			RespawnPlayer(realPlayerInfos.back());
		else
		{
			Point* spawnPoint = GetPointByTag(std::static_pointer_cast<AdventureData>(worldDataPtr)->settings.GetString(0));
			if(spawnPoint)
				realPlayerInfos.back()->nextSpawnPoint = spawnPoint;
			RespawnPlayer(realPlayerInfos.back());
		}
	}
	return playerID;
}

void GameSandbox::RemovePlayer(PlayerID playerID)
{
	auto it = std::find_if(realPlayerObjects.begin(), realPlayerObjects.end(), [&](const RealPlayer* realPlayer){ return realPlayer->GetPlayerID() == playerID;});
	if(it != realPlayerObjects.end())
		DestroyObject(*it);
	playerRespawnTimers.erase(std::remove_if(playerRespawnTimers.begin(), playerRespawnTimers.end(), [&playerID](const std::pair<double, PlayerInfoPtr>& pr){ return pr.second->playerID == playerID; }), playerRespawnTimers.end());
}

void GameSandbox::ClientAddObject(GameObject* gameObj)
{
	objects.emplace_back(gameObj);
	if(gameObj->GetClassID() == NPC::classID || gameObj->GetClassID() == RealPlayer::classID)
	{
		playerObjects.emplace_back(static_cast<Player*>(gameObj));
		if(gameObj->GetClassID() == RealPlayer::classID)
			realPlayerObjects.emplace_back(static_cast<RealPlayer*>(gameObj));
	}
	MoveObject(gameObj, -1, static_cast<Entity*>(gameObj)->GetRoom());
}

void GameSandbox::MoveObject(GameObject* gameObj, int32_t oldRoom, int32_t newRoom)
{
	if(std::find(objects.begin(), objects.end(), gameObj) == objects.end()) return;
	if(oldRoom >= 0 && oldRoom < static_cast<int32_t>(roomObjects.size()))
	{
		roomObjects[oldRoom].erase(std::remove(roomObjects[oldRoom].begin(), roomObjects[oldRoom].end(), gameObj), roomObjects[oldRoom].end());
		movingObjects[oldRoom].erase(std::remove(movingObjects[oldRoom].begin(), movingObjects[oldRoom].end(), gameObj), movingObjects[oldRoom].end());
		for(uint32_t i = 0; i < staticBlockChunks[oldRoom].size(); i++)
			for(uint32_t j = 0; j < staticBlockChunks[oldRoom][i].size(); j++)
				staticBlockChunks[oldRoom][i][j].erase(std::remove(staticBlockChunks[oldRoom][i][j].begin(), staticBlockChunks[oldRoom][i][j].end(), gameObj), staticBlockChunks[oldRoom][i][j].end());
	}
	if(newRoom >= 0 && newRoom < static_cast<int32_t>(roomObjects.size()) && std::find(roomObjects[newRoom].begin(), roomObjects[newRoom].end(), gameObj) == roomObjects[newRoom].end())
	{
		roomObjects[newRoom].emplace_back(gameObj);
		if(gameObj->GetClassID() == Block::classID && static_cast<Block*>(gameObj)->IsStatic())
			AddBlockToChunk(gameObj, newRoom);
		else
			movingObjects[newRoom].emplace_back(gameObj);
	}
}

PlayerID GameSandbox::GetOwnPlayerID()
{
	return ownPlayerID;
}

uint32_t GameSandbox::GetRoomCount()
{
	return roomObjects.size();
}

void GameSandbox::SummonObject(GameObject* object)
{
	objectsToSummon.emplace_back(object);
}

bool GameSandbox::HasGameEnded()
{
	return gameEnded;
}

void GameSandbox::EndGame()
{
	gameEnded = true;
}

void GameSandbox::CreateObject(GameObject* object, bool sendToClients)
{
	objects.emplace_back(object);
	if(object->GetClassID() == NPC::classID || object->GetClassID() == RealPlayer::classID)
	{
		playerObjects.emplace_back(static_cast<Player*>(object));
		if(object->GetClassID() == RealPlayer::classID)
			realPlayerObjects.emplace_back(static_cast<RealPlayer*>(object));
	}
	uint32_t room = static_cast<Entity*>(object)->GetRoom();
	if(room >= 0 && room < roomObjects.size())
	{
		roomObjects[room].emplace_back(object);
		if(object->GetClassID() == Block::classID && static_cast<Block*>(object)->IsStatic())
			AddBlockToChunk(object, room);
		else
			movingObjects[room].emplace_back(object);
	}
	if(isServer)
	{
		std::shared_ptr<NetworkManagerServer> netManServer = std::static_pointer_cast<NetworkManagerServer>(netMan);
		netManServer->CreateObject(object, sendToClients);
	}
	else if(!isNotClient) // is client o-o
	{
		std::shared_ptr<NetworkManagerClient> netManClient = std::static_pointer_cast<NetworkManagerClient>(netMan);
		netManClient->CreateObject(object);
	}
}

std::vector<GameObject*>::iterator GameSandbox::DestroyObject(GameObject* object)
{
	object->ServerDestroy();
	if(isServer)
	{
		std::shared_ptr<NetworkManagerServer> netManServer = std::static_pointer_cast<NetworkManagerServer>(netMan);
		netManServer->DestroyObject(object);
	}
	if(object->GetClassID() == NPC::classID || object->GetClassID() == RealPlayer::classID)
	{
		playerObjects.erase(std::remove(playerObjects.begin(), playerObjects.end(), object), playerObjects.end());
		if(object->GetClassID() == RealPlayer::classID)
			realPlayerObjects.erase(std::remove(realPlayerObjects.begin(), realPlayerObjects.end(), object), realPlayerObjects.end());
	}
	int32_t room = static_cast<Entity*>(object)->GetRoom();
	if(room >= 0 && room < static_cast<int32_t>(roomObjects.size()))
	{
		roomObjects[room].erase(std::remove(roomObjects[room].begin(), roomObjects[room].end(), object), roomObjects[room].end());
		movingObjects[room].erase(std::remove(movingObjects[room].begin(), movingObjects[room].end(), object), movingObjects[room].end());
		for(uint32_t i = 0; i < staticBlockChunks[room].size(); i++)
			for(uint32_t j = 0; j < staticBlockChunks[room][i].size(); j++)
				staticBlockChunks[room][i][j].erase(std::remove(staticBlockChunks[room][i][j].begin(), staticBlockChunks[room][i][j].end(), object), staticBlockChunks[room][i][j].end());
	}
	delete object;
	return objects.erase(std::remove(objects.begin(), objects.end(), object), objects.end());
}

void GameSandbox::RespawnPlayer(const PlayerInfoPtr& playerToRespawn)
{
	if(worldDataPtr->GetType() == WORLD_ARENA)
	{
		Point* randomSpawnPoint = GetRandSpawnPoint();
		CreateObject(new RealPlayer(randomSpawnPoint ? randomSpawnPoint->GetPos() : Vec2(0.0f), playerToRespawn, GetPlayerDataByPlayerID(playerToRespawn->playerID), randomSpawnPoint ? randomSpawnPoint->GetRoom() : 0));
	}
	else
		CreateObject(new RealPlayer(playerToRespawn->nextSpawnPoint ? playerToRespawn->nextSpawnPoint->GetPos() : Vec2(0.0f), playerToRespawn, GetPlayerDataByPlayerID(playerToRespawn->playerID), playerToRespawn->nextSpawnPoint ? playerToRespawn->nextSpawnPoint->GetRoom() : 0));
}

std::vector<GameObject*>& GameSandbox::GetObjects(uint32_t roomNumber)
{
	return roomObjects[roomNumber];
}

std::vector<GameObject*>& GameSandbox::GetAllObjects()
{
	return objects;
}

std::vector<Player*>& GameSandbox::GetAllPlayerObjects()
{
	return playerObjects;
}

std::vector<RealPlayer*>& GameSandbox::GetAllRealPlayerObjects()
{
	return realPlayerObjects;
}

std::vector<GameObject*>::iterator GameSandbox::ClientEraseObject(const std::vector<GameObject*>::iterator& it)
{
	const GameObject* object = *it;
	if(object->GetClassID() == NPC::classID || object->GetClassID() == RealPlayer::classID)
	{
		playerObjects.erase(std::remove(playerObjects.begin(), playerObjects.end(), object), playerObjects.end());
		if(object->GetClassID() == RealPlayer::classID)
		{
			realPlayerObjects.erase(std::remove(realPlayerObjects.begin(), realPlayerObjects.end(), object), realPlayerObjects.end());
		}
	}
	uint32_t room = static_cast<const Entity*>(object)->GetRoom();
	if(room >= 0 && room < roomObjects.size())
	{
		roomObjects[room].erase(std::remove(roomObjects[room].begin(), roomObjects[room].end(), object), roomObjects[room].end());
		movingObjects[room].erase(std::remove(movingObjects[room].begin(), movingObjects[room].end(), object), movingObjects[room].end());
		for(uint32_t i = 0; i < staticBlockChunks[room].size(); i++)
			for(uint32_t j = 0; j < staticBlockChunks[room][i].size(); j++)
				staticBlockChunks[room][i][j].erase(std::remove(staticBlockChunks[room][i][j].begin(), staticBlockChunks[room][i][j].end(), object), staticBlockChunks[room][i][j].end());
	}
	delete object;
	return objects.erase(it);
}

const std::vector<TexturePtr>* GameSandbox::GetBlockTexturesFromID(uint8_t blockID)
{
	return &blockTextures[blockID];
}

const std::vector<std::vector<std::vector<TexturePtr>>>* GameSandbox::GetPlayerTexturesFromID(PlayerID playerID)
{
	auto it = playerTextures.find(playerID);
	if(it == playerTextures.end())
	{
		auto it = playerIDToNPCID.find(playerID);
		if(it == playerIDToNPCID.end())
			return nullptr;
		else
			return GetNPCTexturesFromID(it->second);
	}
	else
		return &it->second;
}

const std::vector<std::vector<std::vector<TexturePtr>>>* GameSandbox::GetNPCTexturesFromID(NPCID npcID)
{
	return npcID != 0 ? &npcTextures[npcID - 1] : nullptr;
}

std::vector<PlayerInfoPtr>& GameSandbox::GetAllRealPlayerInfos()
{
	return realPlayerInfos;
}

PlayerInfoPtr GameSandbox::GetRealPlayerInfoByPlayerID(PlayerID playerID)
{
	auto it = std::find_if(realPlayerInfos.begin(), realPlayerInfos.end(), [&playerID](const PlayerInfoPtr& player){return player->playerID == playerID;});
	if(it == realPlayerInfos.end())
		return nullptr;
	else
		return *it;
}

const UTF8String* GameSandbox::GetUserNameByPlayerID(PlayerID playerID)
{
	auto it = std::find_if(playerObjects.begin(), playerObjects.end(), [&playerID](const Player* playerObj){
		return playerObj->GetPlayerID() == playerID;
	});
	if(it == playerObjects.end())
		return nullptr;
	else
		return (*it)->GetUserName();
}

const PlayerData* GameSandbox::GetPlayerDataByPlayerID(PlayerID playerID)
{
	auto it = playerIDToPlayerData.find(playerID);
	if(it == playerIDToPlayerData.end())
	{
		auto it = playerIDToNPCID.find(playerID);
		if(it == playerIDToNPCID.end())
			return nullptr;
		else
			return GetPlayerDataByNPCID(it->second);
	}
	else
		return it->second.get();
}

const PlayerData* GameSandbox::GetPlayerDataByNPCID(NPCID npcID)
{
	return npcID != 0 ? &(*npcs)[npcID - 1] : nullptr;
}

NPCPropListID GameSandbox::GetNPCPropListIDByNPCPropList(const PropertiesList* npcPropList)
{
	for(NPCPropListID i = 0; i < npcPropLists.size(); i++)
		if(npcPropList == npcPropLists[i])
			return i;
	return 0;
}

const PropertiesList* GameSandbox::GetNPCPropListByNPCPropListID(NPCPropListID npcPropListID)
{
	return npcPropLists[npcPropListID];
}

WorldDataPtr GameSandbox::GetWorldData()
{
	return worldDataPtr;
}

const LevelData* GameSandbox::GetLevelData(uint32_t levelNumber)
{
	return levels[levelNumber];
}

std::vector<BlockData>& GameSandbox::GetBlockData()
{
	return *blocks;
}

const std::unordered_multimap<UTF8String, Point*>& GameSandbox::GetTagToPointMap()
{
	return points;
}

Point* GameSandbox::GetPointByTag(const UTF8String& tag)
{
	auto it = points.find(tag);
	return it != points.end() ? it->second : nullptr;
}

void GameSandbox::SetNumber(const UTF8String& name, uint32_t val)
{
	PropertyDecoderUtil::SetNumberInMap(numbers, name, val);
}

uint32_t GameSandbox::GetNumber(const UTF8String& name)
{
	return PropertyDecoderUtil::GetNumberInMap(numbers, name);
}

uint32_t* GameSandbox::GetNumberPtr(const UTF8String& name)
{
	return PropertyDecoderUtil::GetNumberPtrInMap(numbers, name);
}

void GameSandbox::SetRealPlayerSpawnPoint(const PlayerInfoPtr& playerInfo, Point* spawnPoint)
{
	playerInfo->nextSpawnPoint = spawnPoint;
}

Vec2 GameSandbox::GetCursorDirection()
{
	auto it = std::find_if(realPlayerObjects.begin(), realPlayerObjects.end(), [&](const RealPlayer* realPlayerObj){ return realPlayerObj->GetPlayerID() == ownPlayerID; });
	if(it != realPlayerObjects.end())
	{
		RealPlayer* playerPtr = *it;
		Vec2 cursorPosInWorld = cameraPos + (ControllerUtil::Get().GetCursorPos() / cameraZoom);
		Vec2 finalDirection = Normalize(cursorPosInWorld - (playerPtr->GetCurrentPosition() + Vec2(50.0f)));
		return finalDirection;
	}
	return Vec2(0.0f);
}

float GameSandbox::GetWorldMaxSize()
{
	if(worldDataPtr->GetType() == WORLD_ADVENTURE)
		return MAX_WORLD_BORDER_ADVENTURE;
	else
		return MAX_WORLD_BORDER_ARENA;
}

float GameSandbox::GetWorldMinSize()
{
	return MIN_WORLD_BORDER;
}

void GameSandbox::SendMessage(PlayerID playerID, const UTF8String& message)
{
	if(isServer)
		std::static_pointer_cast<NetworkManagerServer>(netMan)->BroadcastMessage(playerID, message);
	else if(isNotClient && (playerID == 0 || playerID == ownPlayerID)) // No network, just print it
		LogUtil::Get().LogMessage("NORMAL", message);
}

bool GameSandbox::IsClient()
{
	return !isNotClient;
}

bool GameSandbox::IsServer()
{
	return isServer;
}

double GameSandbox::GetEstimatedSendRateClient()
{
	if(!isNotClient)
		return std::static_pointer_cast<NetworkManagerClient>(netMan)->GetEstimatedSendRate();
	return 0.0;
}

double GameSandbox::GetEstimatedSendRateServer(PlayerID playerID)
{
	if(isServer)
		return std::static_pointer_cast<NetworkManagerServer>(netMan)->GetPlayerEstimatedSendRate(playerID);
	return 0.0;
}

double GameSandbox::GetRTTClient()
{
	if(!isNotClient)
		return std::static_pointer_cast<NetworkManagerClient>(netMan)->GetRTT();
	return 0.0;
}

double GameSandbox::GetRTTServer(PlayerID playerID)
{
	if(isServer)
		return std::static_pointer_cast<NetworkManagerServer>(netMan)->GetPlayerRTT(playerID);
	return 0.0;
}

void GameSandbox::SetAnnouncement(PlayerID playerID, const UTF8String& message)
{
	if(playerID == 0 || playerID == ownPlayerID)
		announcements[ownPlayerID] = message;
	if(isServer)
		for(const PlayerInfoPtr& realPlayerInfo : realPlayerInfos)
		{
			if(realPlayerInfo->playerID != ownPlayerID && (playerID == 0 || playerID == realPlayerInfo->playerID))
				announcements[realPlayerInfo->playerID] = message;
		}
}

const UTF8String& GameSandbox::GetAnnouncement()
{
	return announcements[ownPlayerID];
}

Point* GameSandbox::GetRandSpawnPoint()
{
	if(points.empty())
		return nullptr;
	auto it = points.begin();
	std::advance(it, RandGenUtil::Get().GetRandomUInt64(0, points.size()-1));
	return it->second;
}

double GameSandbox::GetTimeLeftInSeconds()
{
	return timeLeft;
}

bool GameSandbox::IsFirstUpdate()
{
	return isFirstUpdate;
}

void GameSandbox::AddBlockToChunk(GameObject* object, uint32_t room)
{
	if(room >= 0 && room < staticBlockChunks.size())
	{
		Vec2 pos = static_cast<Block*>(object)->GetCurrentPosition();
		staticBlockChunks[room][pos.x / CHUNKSIZEINPIXELS][pos.y / CHUNKSIZEINPIXELS].emplace_back(object);
	}
}

void GameSandbox::GetFourNearestChunksToPos(std::vector<std::vector<GameObject*>*>* chunkVec, const Vec2& pos, uint32_t room)
{
	UVec2 chunkPosIsIn(pos.x / CHUNKSIZEINPIXELS, pos.y / CHUNKSIZEINPIXELS);
	// First, get the chunk the position is in
	AddChunkPtrToVec(chunkPosIsIn.x, chunkPosIsIn.y, room, chunkVec);
	// Then, get the closest chunk on the X axis
	bool closeToRight;
	bool closeToUp;
	if(std::fmod(pos.x, CHUNKSIZEINPIXELS) > CHUNKSIZEINPIXELS / 2.0f)
	{
		closeToRight = true;
		AddChunkPtrToVec(chunkPosIsIn.x + 1, chunkPosIsIn.y, room, chunkVec);
	}
	else
	{
		closeToRight = false;
		AddChunkPtrToVec(chunkPosIsIn.x - 1, chunkPosIsIn.y, room, chunkVec);
	}
	// Next, closest chunk on the y axis
	if(std::fmod(pos.y, CHUNKSIZEINPIXELS) > CHUNKSIZEINPIXELS / 2.0f)
	{
		closeToUp = true;
		AddChunkPtrToVec(chunkPosIsIn.x, chunkPosIsIn.y + 1, room, chunkVec);
	}
	else
	{
		closeToUp = false;
		AddChunkPtrToVec(chunkPosIsIn.x, chunkPosIsIn.y - 1, room, chunkVec);
	}
	// Finally, get the chunk that is closest diagonally from the chunk the position is in
	AddChunkPtrToVec(chunkPosIsIn.x + (closeToRight ? 1 : -1), chunkPosIsIn.y + (closeToUp ? 1 : -1), room, chunkVec);
}

void GameSandbox::AddChunkPtrToVec(uint32_t x, uint32_t y, uint32_t room, std::vector<std::vector<GameObject*>*>* chunkVec)
{
	if(x >= 0 && x < staticBlockChunks[room].size() &&
	   y >= 0 && y < staticBlockChunks[room][x].size())
		chunkVec->emplace_back(&staticBlockChunks[room][x][y]);
}

std::vector<GameObject*>& GameSandbox::GetMovingObjects(uint32_t roomNumber)
{
	return movingObjects[roomNumber];
}

void GameSandbox::Cleanup()
{
	for(GameObject* gameObj : objects)
		delete gameObj;
	for(auto& pointPair : points)
		delete pointPair.second;
	objects.clear();
	roomObjects.clear();
	playerObjects.clear();
	realPlayerObjects.clear();
	staticBlockChunks.clear();
	movingObjects.clear();
	points.clear();
	blockTextures.clear();
	playerTextures.clear();
	npcTextures.clear();
	objectsToSummon.clear();
	realPlayerInfos.clear();
	playerIDToPlayerData.clear();
	playerIDToNPCID.clear();
	playerRespawnTimers.clear();
	npcPropLists.clear();
	levels.clear();
	numbers.clear();
	netMan = nullptr;
	worldDataPtr = nullptr;
	adventureDataPtr = nullptr;
	arenaDataPtr = nullptr;
	blocks = nullptr;
	npcs = nullptr;
	gameEnded = false;
	gameProperties = GameSandboxProperties();
	announcements.clear();
	announcementsSent.clear();
	ownPlayerID = 0;
}
