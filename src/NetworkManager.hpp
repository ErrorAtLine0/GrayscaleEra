#pragma once
#include <GSEGL/LobbyServerConstants.hpp>
#include <deque>
#include <GSEGL/UTF8String.hpp>
#include <GSEGL/Network/SocketUtil.hpp>
#include <GSEGL/Network/ReplicationManager.hpp>
#include "PlayerData.hpp"
#include "ArenaData.hpp"
#include "AdventureData.hpp"
#include "InputManager.hpp"
#define LOBBYSERVER_PACKET_TIMEOUT 3.0
#define EXCHANGE_PACKET_RATE 0.01
#define MAX_MESSAGE_LENGTH 40

// To simulate lag
// #define SIMULATE_REAL_LIFE_NET

typedef uint64_t PrivateID;

struct PlayerNetData
{
	PlayerNetData():
	playerData(std::make_shared<PlayerData>())
	{}
	void WriteInitInfo(OutMemBitStream& outStream) const
	{
		outStream.WriteBits(playerID);
		userName.Write(outStream);
	}
	bool ReadInitInfo(InMemBitStream& inStream)
	{
		if(!inStream.ReadBits(playerID)) return false;
		if(!userName.Read(inStream)) return false;
		return true;
	}
	UTF8String userName;
	PlayerDataPtr playerData;
	PlayerID playerID;
};

typedef std::shared_ptr<PlayerNetData> PlayerNetDataPtr;

class NetworkManager : public std::enable_shared_from_this<NetworkManager>
{
	public:
		bool Init(const UTF8String& initUserName, const UTF8String& initLobbyName, uint16_t initPort);
		bool Init(const UTF8String& initUserName, uint16_t initPort);
		virtual bool Update() = 0;
		virtual bool IsServer() = 0;
		virtual void SendChatMessage(const UTF8String& chatMessage) = 0;
		bool SetPlayerData(const PlayerDataPtr& playerDataToLoad); // Returns false if not allowed (overpowered)
		void SendPacket(const OutMemBitStream& outStream, const SocketAddress& addr);
		uint8_t GetRequiredPlayerCount() const;
		const std::vector<PlayerNetDataPtr>& GetAllPlayerNetData() const;
		const UTF8String& GetUsername() const;
		LinkingContext* GetLinkingContext();
		PlayerID GetOwnPlayerID() const;
		uint64_t GetMaxPlayerPoints() const;
		bool IsPlayerOP(const PlayerDataPtr& playerDataToCheck) const;
		bool NeedsPlayerFromFile() const;
		virtual ~NetworkManager();
	protected:
		void ClearAllPlayerRawData();
		std::vector<PlayerNetDataPtr>::iterator GetPlayerNetDataItByPlayerID(PlayerID playerID);
		PlayerNetDataPtr GetPlayerNetDataByPlayerID(PlayerID playerID);
		bool GeneralInit(const UTF8String& initUserName, uint16_t initPort);
		void RecordDeath(PlayerID killer, PlayerID victim);
		void ReceivePackets();
		enum NetworkManagerState
		{
			NETMAN_GETLOBBY,
			NETMAN_INLOBBY,
			NETMAN_DATAEXCHANGE,
			NETMAN_INGAME,
			NETMAN_ENDGAME
		} state;
		enum PacketType
		{
			PACKET_HELLO,
			PACKET_WELCOME,
			PACKET_NAMEEXISTS,
			PACKET_CHATMESSAGE,
			PACKET_ANNOUNCEMENT,
			PACKET_PLAYERJOINED,
			PACKET_PLAYERDISCONNECTED,
			PACKET_HASALLDATA,
			PACKET_GAMESTART,
			PACKET_WORLDDATA,
			PACKET_PLAYERDATA,
			PACKET_PLAYERDATAFIN,
			PACKET_ALIVE,
			PACKET_INPUT,
			PACKET_KILL,
			PACKET_ENDGAME,
			PACKET_MAX
		};
		UTF8String userName;
		std::deque<Packet> packets;
		UDPSocketPtr sock;
		
		// Lobby server data
		UTF8String lobbyName;
		double timeLastPacketToLobbyServer;
		uint8_t lobbyServerSendAttempts;
		SocketAddressPtr lobbyServerAddr;
		
		WorldDataPtr worldData;
		PlayerDataPtr playerDataFromFile;
		uint64_t playerMaxPoints;
		std::unordered_map<PlayerID, OutMemBitStream> playerIDToPlayerDataStream;
		uint8_t requiredPlayerCount;
		LinkingContext linkingContext;
		PlayerID playerID;
		std::vector<PlayerNetDataPtr> players;
		bool requestPlayer;
		
	#ifdef SIMULATE_REAL_LIFE_NET
		bool ReceiveLaggingPackets();
		void UpdateLaggingPackets();
		std::vector<std::pair<double, Packet>> laggingPackets;
	#endif
};

bool BasicNetworkWaitLoop(const std::shared_ptr<void>& netMan);
