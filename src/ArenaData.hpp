#pragma once
#include "WorldData.hpp"
#include "LevelData.hpp"
#include "BlockData.hpp"
#include "Frame.hpp"

#define ARENA_TEXTURE_SIZE 100
#define ARENA_RESPAWN_TIME 5.0

struct ArenaData : public WorldData
{
	ArenaData();
	
	LevelData level;
	std::vector<BlockData> blocks;
	Frame image;
	
	void RemoveBlock(BlockID blockToRemove);
	
	WorldType GetType() const { return WORLD_ARENA; }
	void Write(OutMemBitStream& outStream) const;
	bool Read(InMemBitStream& inStream);
	static std::shared_ptr<Frame> ReadImage(InMemBitStream& inStream);
};

typedef std::shared_ptr<ArenaData> ArenaDataPtr;
