#include "PlayerData.hpp"
#include <GSEGL/ErrorUtil.hpp>
#include <GSEGL/FileUtil.hpp>
#define PLAYER_HEADER_INT 3236413780

PlayerData::PlayerData():
statistics(PlayerData::GetStatistics())
{
}

void PlayerData::Write(OutMemBitStream& outStream) const
{
	statistics.GetPropListRef().Write(outStream);
	Frame::WriteAnimation(outStream, idleAnimation);
	Frame::WriteAnimation(outStream, walkAnimation);
	for(uint8_t i = 0; i < 5; i++)
		abilities[i].Write(outStream);
}

bool PlayerData::Read(InMemBitStream& inStream)
{
	if(!statistics.GetPropListRef().Read(inStream)) return false;
	if(!Frame::ReadAnimation(inStream, idleAnimation)) return false;
	if(!Frame::ReadAnimation(inStream, walkAnimation)) return false;
	for(uint8_t i = 0; i < 5; i++)
		if(!abilities[i].Read(inStream)) return false;
	return true;
}

void PlayerData::WriteFile(OutMemBitStream& outStream) const
{
	OutMemBitStream firstStream;
	firstStream.WriteBits(PLAYER_HEADER_INT, 32);
	statistics.GetPropListRef().Write(firstStream);
	Frame::WriteAnimation(firstStream, idleAnimation);
	Frame::WriteAnimation(firstStream, walkAnimation);
	for(uint8_t i = 0; i < 5; i++)
		abilities[i].Write(firstStream);
	outStream.WriteCompressedData(firstStream.GetBufferPtr(), firstStream.GetByteLength());
}

bool PlayerData::ReadFile(InMemBitStream& inStream)
{
	std::shared_ptr<InMemBitStream> firstStream = inStream.ReadCompressedData();
	if(!firstStream) return false;
	uint32_t playerHeader = 0;
	if(!firstStream->ReadBits(playerHeader)) return false;
	if(playerHeader != PLAYER_HEADER_INT)
	{
		ErrorUtil::Get().SetError("Memory", "PlayerData::ReadCompressed Data doesn't have a player header");
		return false;
	}
	if(!statistics.GetPropListRef().Read(*firstStream)) return false;
	if(!Frame::ReadAnimation(*firstStream, idleAnimation)) return false;
	if(!Frame::ReadAnimation(*firstStream, walkAnimation)) return false;
	for(uint8_t i = 0; i < 5; i++)
		if(!abilities[i].Read(*firstStream)) return false;
	return true;
}

std::shared_ptr<Frame> PlayerData::ReadFileImage(InMemBitStream& inStream)
{
	std::shared_ptr<InMemBitStream> firstStream = inStream.ReadCompressedData();
	if(!firstStream) return nullptr;
	uint32_t playerHeader = 0;
	if(!firstStream->ReadBits(playerHeader)) return nullptr;
	if(playerHeader != PLAYER_HEADER_INT)
	{
		ErrorUtil::Get().SetError("Memory", "PlayerData::ReadFileImage Data doesn't have a player header");
		return nullptr;
	}
	PricedPropertiesList statistics = GetStatistics();
	if(!statistics.GetPropListRef().Read(*firstStream)) return nullptr;
	std::vector<Frame> idleAnimation;
	if(!Frame::ReadAnimation(*firstStream, idleAnimation)) return nullptr;
	return idleAnimation.empty() ? nullptr : std::make_shared<Frame>(idleAnimation[0]);
}

std::shared_ptr<PlayerData> PlayerData::ReadPlayerFile(const UTF8String& playerName)
{
	std::shared_ptr<InMemBitStream> inStream = FileUtil::Get().ReadFromFile(UTF8String("players/") + playerName);
	if(inStream)
	{
		PlayerDataPtr playerData(std::make_shared<PlayerData>());
		if(playerData->ReadFile(*inStream))
			return playerData;
	}
	return nullptr;
}

uint64_t PlayerData::GetPoints() const
{
	double priceViewed = 0.0;
	for(uint8_t i = 0; i < 5; i++)
		priceViewed += abilities[i].GetCost();
	priceViewed += statistics.GetCost();
	return priceViewed;
}

PricedPropertiesList PlayerData::GetStatistics()
{
	PricedPropertiesList statistics("Player Stats");
	statistics.AddInt("Health", 100, PLAYER_MAX_HEALTH, 200);
	statistics.AddInt("Speed", 20, 400, 40);
	statistics.AddInt("Gravity Decrease (%)", 10, 50, 1000);
	PricedChoiceList sizeChoiceList;
	sizeChoiceList.emplace_back("Full (100x100)", 0);
	sizeChoiceList.emplace_back("Half (50x50)", 400);
	sizeChoiceList.emplace_back("Quarter (25x25)", 800);
	statistics.AddChoice("Player Collision Size", sizeChoiceList);
	return statistics;
}
