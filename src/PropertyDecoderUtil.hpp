#pragma once
#include "GameObjects/RealPlayer.hpp"
#include <vector>

#define TEAM_NUMBER_NAME "__TEAM"

enum PropertyDecoderObjectType
{
	PROPERTYDECODEROBJECT_PLAYER,
	PROPERTYDECODEROBJECT_POINT
};
class PropertyDecoderUtil
{
	public:
		static bool CheckForPlayer(const HeadlineProperty* prop, const Vec2& pos, int32_t room, PropertyDecoderObjectType checkerObjType, const void* checkerObj);
		static RealPlayer* GetClosestPlayer(const std::vector<RealPlayer*>& playerVec, const Vec2& pos, int32_t room);
		static void GetPlayers(std::vector<RealPlayer*>* toRet, const PropertiesList& playerSelectorPropList, const Vec2& pos, int32_t room, PropertyDecoderObjectType checkerObjType, const void* checkerObj);
		static PropertiesList GetAnnouncementList();
		static PropertiesList GetMessageList();
		static PropertiesList GetStatusEffectList();
		static PropertiesList GetPlayerSelectorList();
		static PropertiesList GetSetPlayerPrivateNumberList();
		static PropertiesList GetTimerList();
		static PropertiesList GetNPCList();
		static PropertiesList GetRealPlayerConditions();
		static PropertiesList GetRealPlayerResults();
		static PropertiesList GetCheckNumberList();
		static PropertiesList GetSetNumberList();
		static PropertiesList GetPlayerToPointList();
		static void SendMessage(const HeadlineProperty* prop, const Vec2& pos, int32_t room, PropertyDecoderObjectType checkerObjType, const void* checkerObj);
		static void SetAnnouncement(const HeadlineProperty* prop, const Vec2& pos, int32_t room, PropertyDecoderObjectType checkerObjType, const void* checkerObj);
		static void SetGlobalNumber(const HeadlineProperty* prop, PropertyDecoderObjectType setterObjType, const void* setterObj);
		static bool CheckGlobalNumber(const HeadlineProperty* prop, PropertyDecoderObjectType checkerObjType, const void* checkerObj);
		static void SetPrivateNumber(const HeadlineProperty* prop, PropertyDecoderObjectType objType, void* obj, PropertyDecoderObjectType setterObjType, const void* setterObj);
		static bool CheckPrivateNumber(const HeadlineProperty* prop, PropertyDecoderObjectType objType, const void* obj, PropertyDecoderObjectType checkerObjType, const void* checkerObj);
		static void SetPlayerPrivateNumber(const HeadlineProperty* prop, const Vec2& pos, int32_t room, PropertyDecoderObjectType setterObjType, void* setterObj);
		static bool CheckTimer(const HeadlineProperty* prop, std::unordered_map<const HeadlineProperty*, double>& propTimers);
		static bool CheckRandomPercentage(const HeadlineProperty* prop);
		static void SetNumberInMap(std::unordered_map<UTF8String, uint32_t>& numbers, const UTF8String& name, uint32_t val);
		static uint32_t GetNumberInMap(const std::unordered_map<UTF8String, uint32_t>& numbers, const UTF8String& name);
		static uint32_t* GetNumberPtrInMap(std::unordered_map<UTF8String, uint32_t>& numbers, const UTF8String& name);
		static void TPPlayerToPoint(const HeadlineProperty* prop, const Vec2& pos, int32_t room, PropertyDecoderObjectType checkerObjType, const void* checkerObj);
		static void SetPlayerSpawnPoint(const HeadlineProperty* prop, const Vec2& pos, int32_t room, PropertyDecoderObjectType checkerObjType, const void* checkerObj);
	private:
		static void SetNumber(uint32_t* number, uint32_t operation, uint32_t operand);
		static bool CheckNumber(uint32_t number, uint32_t operation, uint32_t operand);
		static bool IsPlayerSelected(RealPlayer* player, const PropertiesList& playerSelectorPropList, const Vec2& pos, int32_t room, PropertyDecoderObjectType checkerObjType, const void* checkerObj);
		static bool PlayerWithinDistance(RealPlayer* player, const Vec2& pos, int32_t room, int32_t blockDistance);
};
