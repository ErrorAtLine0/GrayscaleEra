#pragma once
#include "PropertiesList.hpp"

class GlobalSettingsUtil
{
	public:
		static GlobalSettingsUtil& Get();
		GlobalSettingsUtil();
		void Init();
		void InitMain();
		void WriteMainToFile();
		bool ReadMainFromFile();
		bool ReadFromFile();
		void WriteToFile();
		void SetupSettings();
		uint32_t GetDefaultRenderer();
		bool constantUpdate;
		PropertiesList settings;
		PropertiesList mainSettings;
};
