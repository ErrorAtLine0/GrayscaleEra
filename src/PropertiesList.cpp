#include "PropertiesList.hpp"
#include <GSEGL/ErrorUtil.hpp>
#include <algorithm>

HeadlineProperty* HeadlineProperty::CreateProperty(PropertyType propertyType, const UTF8String& propertyName)
{
	switch(propertyType)
	{
		case PROPERTY_HEADLINE:
			return new HeadlineProperty(propertyName);
		case PROPERTY_STRING:
			return new StringProperty(propertyName);
		case PROPERTY_INT:
			return new IntProperty(propertyName);
		case PROPERTY_BOOL:
			return new BoolProperty(propertyName);
		case PROPERTY_CHOICE:
			return new ChoiceProperty(propertyName);
		case PROPERTY_LIST:
			return new ListProperty(propertyName);
		case PROPERTY_BUTTON:
			return new ButtonProperty(propertyName);
		case PROPERTY_BLOCK:
			return new BlockProperty(propertyName);
		case PROPERTY_NPC:
			return new NPCProperty(propertyName);
		case PROPERTY_RULELIST:
			return new RuleListProperty(propertyName);
	}
	return nullptr;
}

HeadlineProperty::HeadlineProperty(const UTF8String& initPropertyName):
	propertyName(initPropertyName)
{}

HeadlineProperty::HeadlineProperty(const HeadlineProperty& initProperty)
{
	*this = initProperty;
}

HeadlineProperty::~HeadlineProperty() { }

void HeadlineProperty::operator=(const HeadlineProperty& property)
{
	propertyName = property.propertyName;
	TransferInfo(&property);
}

void HeadlineProperty::Write(OutMemBitStream& outStream) const { }
bool HeadlineProperty::Read(InMemBitStream& inStream) { return true; }
void HeadlineProperty::WriteText(OutMemBitStream& outStream) const { }
bool HeadlineProperty::ReadText(InMemBitStream& inStream) { return true; }

PropertyType HeadlineProperty::GetPropertyType() const { return PROPERTY_HEADLINE; }
void HeadlineProperty::TransferInfo(const HeadlineProperty* property) {}
const UTF8String& HeadlineProperty::GetPropertyName() const { return propertyName; }

StringProperty::StringProperty(const UTF8String& initPropertyName):
HeadlineProperty::HeadlineProperty(initPropertyName),
maxLength(0)
{}
PropertyType StringProperty::GetPropertyType() const { return PROPERTY_STRING; }
void StringProperty::TransferInfo(const HeadlineProperty* property)
{
	const StringProperty* castedProperty = static_cast<const StringProperty*>(property);
	maxLength = castedProperty->maxLength;
	data = castedProperty->data;
}
void StringProperty::Write(OutMemBitStream& outStream) const
{
	data.Write(outStream);
}
bool StringProperty::Read(InMemBitStream& inStream)
{
	return data.Read(inStream);
}
void StringProperty::WriteText(OutMemBitStream& outStream) const
{
	propertyName.WriteText(outStream);
	outStream.WriteBits(':');
	data.WriteText(outStream);
	outStream.WriteBits('\n');
}
bool StringProperty::ReadText(InMemBitStream& inStream)
{
	UTF8String stringHolder;
	char charHolder;
	if(!stringHolder.ReadText(inStream)) return false;
	if(!inStream.ReadBits(charHolder)) return false;
	if(!data.ReadText(inStream)) return false;
	if(!inStream.ReadBits(charHolder)) return false;
	return true;
}

IntProperty::IntProperty(const UTF8String& initPropertyName):
HeadlineProperty::HeadlineProperty(initPropertyName),
minVal(0),
maxVal(0),
diff(0),
data(0)
{}
PropertyType IntProperty::GetPropertyType() const { return PROPERTY_INT; }
void IntProperty::TransferInfo(const HeadlineProperty* property)
{
	const IntProperty* castedProperty = static_cast<const IntProperty*>(property);
	minVal = castedProperty->minVal;
	maxVal = castedProperty->maxVal;
	diff = castedProperty->diff;
	data = castedProperty->data;
}
void IntProperty::Write(OutMemBitStream& outStream) const
{
	outStream.WriteBits(data/diff);
}
bool IntProperty::Read(InMemBitStream& inStream)
{
	if(!inStream.ReadBits(data)) return false;
	data *= diff;
	return true;
}
void IntProperty::WriteText(OutMemBitStream& outStream) const
{
	propertyName.WriteText(outStream);
	outStream.WriteBits(':');
	UTF8String::IntToStr(data).WriteText(outStream);
	outStream.WriteBits('\n');
}
bool IntProperty::ReadText(InMemBitStream& inStream)
{
	UTF8String stringHolder;
	char charHolder;
	if(!stringHolder.ReadText(inStream)) return false;
	if(!inStream.ReadBits(charHolder)) return false;
	if(!stringHolder.ReadText(inStream)) return false;
	if(!inStream.ReadBits(charHolder)) return false;
	data = UTF8String::StrToInt(stringHolder);
	return true;
}

BoolProperty::BoolProperty(const UTF8String& initPropertyName):
HeadlineProperty::HeadlineProperty(initPropertyName),
data(false)
{}
PropertyType BoolProperty::GetPropertyType() const { return PROPERTY_BOOL; }
void BoolProperty::TransferInfo(const HeadlineProperty* property)
{
	const BoolProperty* castedProperty = static_cast<const BoolProperty*>(property);
	data = castedProperty->data;
}
void BoolProperty::Write(OutMemBitStream& outStream) const
{
	outStream.WriteBits(data);
}
bool BoolProperty::Read(InMemBitStream& inStream)
{
	return inStream.ReadBits(data);
}
void BoolProperty::WriteText(OutMemBitStream& outStream) const
{
	propertyName.WriteText(outStream);
	outStream.WriteBits(':');
	UTF8String(data ? "TRUE" : "FALSE").WriteText(outStream);
	outStream.WriteBits('\n');
}
bool BoolProperty::ReadText(InMemBitStream& inStream)
{
	UTF8String stringHolder;
	char charHolder;
	if(!stringHolder.ReadText(inStream)) return false;
	if(!inStream.ReadBits(charHolder)) return false;
	if(!stringHolder.ReadText(inStream)) return false;
	if(!inStream.ReadBits(charHolder)) return false;
	data = stringHolder == "TRUE" ? true : false;
	return true;
}

ChoiceProperty::ChoiceProperty(const UTF8String& initPropertyName):
HeadlineProperty::HeadlineProperty(initPropertyName),
data(0)
{}
PropertyType ChoiceProperty::GetPropertyType() const { return PROPERTY_CHOICE; }
void ChoiceProperty::TransferInfo(const HeadlineProperty* property)
{
	const ChoiceProperty* castedProperty = static_cast<const ChoiceProperty*>(property);
	choices = castedProperty->choices;
	data = castedProperty->data;
}
void ChoiceProperty::Write(OutMemBitStream& outStream) const
{
	outStream.WriteBits(data);
}
bool ChoiceProperty::Read(InMemBitStream& inStream)
{
	return inStream.ReadBits(data);
}
void ChoiceProperty::WriteText(OutMemBitStream& outStream) const
{
	propertyName.WriteText(outStream);
	outStream.WriteBits(':');
	choices[data].WriteText(outStream);
	outStream.WriteBits('\n');
}
bool ChoiceProperty::ReadText(InMemBitStream& inStream)
{
	UTF8String stringHolder;
	char charHolder;
	if(!stringHolder.ReadText(inStream)) return false;
	if(!inStream.ReadBits(charHolder)) return false;
	if(!stringHolder.ReadText(inStream)) return false;
	if(!inStream.ReadBits(charHolder)) return false;
	auto it = std::find(choices.begin(), choices.end(), stringHolder);
	if(it == choices.end()) return false;
	data = it - choices.begin();
	return true;
}

ListProperty::ListProperty(const UTF8String& initPropertyName):
HeadlineProperty::HeadlineProperty(initPropertyName),
data(initPropertyName)
{}

PropertyType ListProperty::GetPropertyType() const { return PROPERTY_LIST; }

void ListProperty::TransferInfo(const HeadlineProperty* property)
{
	data = static_cast<const ListProperty*>(property)->data;
}

void ListProperty::Write(OutMemBitStream& outStream) const
{
	data.Write(outStream);
}

bool ListProperty::Read(InMemBitStream& inStream)
{
	return data.Read(inStream);
}
void ListProperty::WriteText(OutMemBitStream& outStream) const
{
	// Implement when needed
}
bool ListProperty::ReadText(InMemBitStream& inStream)
{
	return true; // Implement when needed
}

ButtonProperty::ButtonProperty(const UTF8String& initPropertyName):
HeadlineProperty::HeadlineProperty(initPropertyName)
{}

PropertyType ButtonProperty::GetPropertyType() const { return PROPERTY_BUTTON; }

void ButtonProperty::TransferInfo(const HeadlineProperty* property)
{
	data = static_cast<const ButtonProperty*>(property)->data;
}
void ButtonProperty::Write(OutMemBitStream& outStream) const
{
	data.Write(outStream);
}
bool ButtonProperty::Read(InMemBitStream& inStream)
{
	return data.Read(inStream);
}
void ButtonProperty::WriteText(OutMemBitStream& outStream) const
{
	// Implement when needed
}
bool ButtonProperty::ReadText(InMemBitStream& inStream)
{
	return true; // Implement when needed
}

BlockProperty::BlockProperty(const UTF8String& initPropertyName):
HeadlineProperty::HeadlineProperty(initPropertyName),
data(0)
{}

PropertyType BlockProperty::GetPropertyType() const { return PROPERTY_BLOCK; }

void BlockProperty::TransferInfo(const HeadlineProperty* property)
{
	data = static_cast<const BlockProperty*>(property)->data;
}
void BlockProperty::Write(OutMemBitStream& outStream) const
{
	outStream.WriteBits(data);
}
bool BlockProperty::Read(InMemBitStream& inStream)
{
	return inStream.ReadBits(data);
}
void BlockProperty::WriteText(OutMemBitStream& outStream) const
{
	propertyName.WriteText(outStream);
	outStream.WriteBits(':');
	UTF8String::IntToStr(data).WriteText(outStream);
	outStream.WriteBits('\n');
}
bool BlockProperty::ReadText(InMemBitStream& inStream)
{
	UTF8String stringHolder;
	char charHolder;
	if(!stringHolder.ReadText(inStream)) return false;
	if(!inStream.ReadBits(charHolder)) return false;
	if(!stringHolder.ReadText(inStream)) return false;
	if(!inStream.ReadBits(charHolder)) return false;
	data = UTF8String::StrToInt(stringHolder);
	return true;
}

NPCProperty::NPCProperty(const UTF8String& initPropertyName):
HeadlineProperty::HeadlineProperty(initPropertyName),
data(0)
{}

PropertyType NPCProperty::GetPropertyType() const { return PROPERTY_NPC; }

void NPCProperty::TransferInfo(const HeadlineProperty* property)
{
	data = static_cast<const NPCProperty*>(property)->data;
}
void NPCProperty::Write(OutMemBitStream& outStream) const
{
	outStream.WriteBits(data);
}
bool NPCProperty::Read(InMemBitStream& inStream)
{
	return inStream.ReadBits(data);
}
void NPCProperty::WriteText(OutMemBitStream& outStream) const
{
	propertyName.WriteText(outStream);
	outStream.WriteBits(':');
	UTF8String::IntToStr(data).WriteText(outStream);
	outStream.WriteBits('\n');
}
bool NPCProperty::ReadText(InMemBitStream& inStream)
{
	UTF8String stringHolder;
	char charHolder;
	if(!stringHolder.ReadText(inStream)) return false;
	if(!inStream.ReadBits(charHolder)) return false;
	if(!stringHolder.ReadText(inStream)) return false;
	if(!inStream.ReadBits(charHolder)) return false;
	data = UTF8String::StrToInt(stringHolder);
	return true;
}

RuleListProperty::RuleListProperty(const UTF8String& initPropertyName):
HeadlineProperty::HeadlineProperty(initPropertyName),
data(initPropertyName)
{}

PropertyType RuleListProperty::GetPropertyType() const { return PROPERTY_RULELIST; }

void RuleListProperty::TransferInfo(const HeadlineProperty* property)
{
	const RuleListProperty* castedProperty = static_cast<const RuleListProperty*>(property);
	data = castedProperty->data;
}

void RuleListProperty::Write(OutMemBitStream& outStream) const
{
	data.Write(outStream);
}
bool RuleListProperty::Read(InMemBitStream& inStream)
{
	return data.Read(inStream);
}
void RuleListProperty::WriteText(OutMemBitStream& outStream) const
{
	// Implement when needed
}
bool RuleListProperty::ReadText(InMemBitStream& inStream)
{
	return true; // Implement when needed
}

PropertiesList::PropertiesList():
	listName("Nameless")
{}

PropertiesList::PropertiesList(const UTF8String& initListName):
	listName(initListName)
{}

PropertiesList::PropertiesList(const PropertiesList& propertiesList)
{
	*this = propertiesList;
}

void PropertiesList::operator=(const PropertiesList& propertiesList)
{
	listName = propertiesList.listName;
	propertiesVector.clear();
	for(HeadlineProperty* prop : propertiesList.propertiesVector)
	{
		propertiesVector.emplace_back(HeadlineProperty::CreateProperty(prop->GetPropertyType(), prop->GetPropertyName()));
		(*propertiesVector.back()) = *prop;
	}
}

HeadlineProperty* PropertiesList::GetPropAt(uint32_t index) { return propertiesVector[index]; }
const HeadlineProperty* PropertiesList::GetPropAt(uint32_t index) const { return propertiesVector[index]; }

void PropertiesList::AddString(const UTF8String& propertyName, uint32_t maxLength)
{
	StringProperty* newProperty = new StringProperty(propertyName);
	newProperty->maxLength = maxLength;
	AddProperty(newProperty);
}

void PropertiesList::AddInt(const UTF8String& propertyName, int32_t minVal, int32_t maxVal, int32_t diff, int32_t defaultVal)
{
	IntProperty* newProperty = new IntProperty(propertyName);
	if(defaultVal != 0)
		newProperty->data = defaultVal;
	newProperty->minVal = minVal;
	newProperty->maxVal = maxVal;
	newProperty->diff = diff;
	AddProperty(newProperty);
}

void PropertiesList::AddBool(const UTF8String& propertyName)
{
	AddProperty(new BoolProperty(propertyName));
}

void PropertiesList::AddChoice(const UTF8String& propertyName, const std::vector<UTF8String>& choices, int32_t defaultVal)
{
	ChoiceProperty* newProperty = new ChoiceProperty(propertyName);
	newProperty->choices = choices;
	newProperty->data = defaultVal;
	AddProperty(newProperty);
}

void PropertiesList::AddList(const UTF8String& propertyName, const PropertiesList& propertiesList)
{
	ListProperty* newProperty = new ListProperty(propertyName);
	newProperty->data = propertiesList;
	AddProperty(newProperty);
}

void PropertiesList::AddButton(const UTF8String& propertyName, const ButtonInfo& buttonInfo)
{
	ButtonProperty* newProperty = new ButtonProperty(propertyName);
	newProperty->data = buttonInfo;
	AddProperty(newProperty);
}

void PropertiesList::AddBlock(const UTF8String& propertyName)
{
	AddProperty(new BlockProperty(propertyName));
}

void PropertiesList::AddNPC(const UTF8String& propertyName)
{
	AddProperty(new NPCProperty(propertyName));
}

void PropertiesList::AddRuleList(const UTF8String& propertyName, const PropertiesList& conditions, const PropertiesList& results)
{
	RuleListProperty* newProperty = new RuleListProperty(propertyName);
	newProperty->data.SetConditions(conditions);
	newProperty->data.SetResults(results);
	AddProperty(newProperty);
}

void PropertiesList::AddHeadline(const UTF8String& propertyName)
{
	AddProperty(new HeadlineProperty(propertyName));
}

void PropertiesList::AddProperty(HeadlineProperty* property)
{
	propertiesVector.emplace_back(property);
}

const UTF8String& PropertiesList::GetString(uint32_t index) const
{
	return static_cast<StringProperty*>(propertiesVector[index])->data;
}

int32_t PropertiesList::GetInt(uint32_t index) const
{
	return static_cast<IntProperty*>(propertiesVector[index])->data;
}

bool PropertiesList::GetBool(uint32_t index) const
{
	return static_cast<BoolProperty*>(propertiesVector[index])->data;
}

uint32_t PropertiesList::GetChoice(uint32_t index) const
{
	return static_cast<ChoiceProperty*>(propertiesVector[index])->data;
}

const PropertiesList& PropertiesList::GetList(uint32_t index) const
{
	return static_cast<ListProperty*>(propertiesVector[index])->data;
}

const ButtonInfo& PropertiesList::GetButton(uint32_t index) const
{
	return static_cast<ButtonProperty*>(propertiesVector[index])->data;
}

BlockID PropertiesList::GetBlock(uint32_t index) const
{
	return static_cast<BlockProperty*>(propertiesVector[index])->data;
}

NPCID PropertiesList::GetNPC(uint32_t index) const
{
	return static_cast<NPCProperty*>(propertiesVector[index])->data;
}

const RuleList& PropertiesList::GetRuleList(uint32_t index) const
{
	return static_cast<RuleListProperty*>(propertiesVector[index])->data;
}

void PropertiesList::RemoveBlock(BlockID blockToRemove)
{
	for(HeadlineProperty* prop : propertiesVector)
		RemoveBlockFromProperty(prop, blockToRemove);
}

void PropertiesList::RemoveNPC(NPCID npcToRemove)
{
	for(HeadlineProperty* prop : propertiesVector)
		RemoveNPCFromProperty(prop, npcToRemove);
}

void PropertiesList::RemoveBlockFromProperty(HeadlineProperty* prop, BlockID blockToRemove)
{
	if(prop->GetPropertyType() == PROPERTY_BLOCK)
	{
		BlockID& block = static_cast<BlockProperty*>(prop)->data;
		if(block > blockToRemove)
			--block;
		else if(block == blockToRemove)
			block = 0;
	}
	else if(prop->GetPropertyType() == PROPERTY_RULELIST)
		static_cast<RuleListProperty*>(prop)->data.RemoveBlock(blockToRemove);
	else if(prop->GetPropertyType() == PROPERTY_LIST)
		static_cast<ListProperty*>(prop)->data.RemoveBlock(blockToRemove);
}

void PropertiesList::RemoveNPCFromProperty(HeadlineProperty* prop, NPCID npcToRemove)
{
	if(prop->GetPropertyType() == PROPERTY_NPC)
	{
		NPCID& npc = static_cast<NPCProperty*>(prop)->data;
		if(npc > npcToRemove)
			--npc;
		else if(npc == npcToRemove)
			npc = 0;
	}
	else if(prop->GetPropertyType() == PROPERTY_RULELIST)
		static_cast<RuleListProperty*>(prop)->data.RemoveNPC(npcToRemove);
	else if(prop->GetPropertyType() == PROPERTY_LIST)
		static_cast<ListProperty*>(prop)->data.RemoveNPC(npcToRemove);
}

void PropertiesList::Write(OutMemBitStream& outStream) const
{
	outStream.WriteBits(propertiesVector.size(), 32); // By writing size, it makes it easier to add more features in the future
	for(uint32_t i = 0; i < propertiesVector.size(); i++)
		propertiesVector[i]->Write(outStream);
}

bool PropertiesList::Read(InMemBitStream& inStream)
{
	uint32_t listSize = 0;
	if(!inStream.ReadBits(listSize, 32)) return false;
	if(listSize > propertiesVector.size())
	{
		ErrorUtil::Get().SetError("Memory", "PropertiesList::Read List size read is larger than actual list size");
		return false;
	}
	for(uint32_t i = 0; i < listSize; i++)
		if(!propertiesVector[i]->Read(inStream)) return false;
	return true;
}

void PropertiesList::WriteText(OutMemBitStream& outStream) const
{
	listName.WriteText(outStream);
	outStream.WriteBits(':');
	UTF8String::IntToStr(propertiesVector.size()).WriteText(outStream);
	outStream.WriteBits('\n');
	for(uint32_t i = 0; i < propertiesVector.size(); i++)
		propertiesVector[i]->WriteText(outStream);
}

bool PropertiesList::ReadText(InMemBitStream& inStream)
{
	UTF8String stringHolder;
	char charHolder;
	if(!stringHolder.ReadText(inStream)) return false;
	if(!inStream.ReadBits(charHolder)) return false;
	if(!stringHolder.ReadText(inStream)) return false;
	if(!inStream.ReadBits(charHolder)) return false;
	uint32_t listSize = UTF8String::StrToInt(stringHolder);
	for(uint32_t i = 0; i < listSize; i++)
		if(!propertiesVector[i]->ReadText(inStream)) return false;
	return true;
}

const UTF8String& PropertiesList::GetListName() const { return listName; }
uint32_t PropertiesList::GetListSize() const { return propertiesVector.size(); }

PropertiesList::~PropertiesList()
{
	for(HeadlineProperty* prop : propertiesVector)
		delete prop;
}

// Rule list source

Rule::Rule() {}

Rule::Rule(const std::vector<uint32_t>& initConditionIndices, const std::vector<const HeadlineProperty*>& initConditionProperties, const std::vector<uint32_t>& initResultIndices, const std::vector<const HeadlineProperty*>& initResultProperties):
conditionIndices(initConditionIndices),
resultIndices(initResultIndices)
{
	for(const HeadlineProperty* initConditionProperty : initConditionProperties)
	{
		conditionProperties.emplace_back(HeadlineProperty::CreateProperty(initConditionProperty->GetPropertyType(), initConditionProperty->GetPropertyName()));
		*conditionProperties.back() = *initConditionProperty;
	}
	for(const HeadlineProperty* initResultProperty : initResultProperties)
	{
		resultProperties.emplace_back(HeadlineProperty::CreateProperty(initResultProperty->GetPropertyType(), initResultProperty->GetPropertyName()));
		*resultProperties.back() = *initResultProperty;
	}
}

Rule::Rule(const Rule& initRule)
{
	*this = initRule;
}

Rule::~Rule()
{
	for(HeadlineProperty* conditionProperty : conditionProperties)
		delete conditionProperty;
	for(HeadlineProperty* resultProperty : resultProperties)
		delete resultProperty;
}

void Rule::operator=(const Rule& rule)
{
	for(HeadlineProperty* conditionProperty : conditionProperties)
		delete conditionProperty;
	conditionProperties.clear();
	for(HeadlineProperty* resultProperty : resultProperties)
		delete resultProperty;
	resultProperties.clear();
	for(HeadlineProperty* ruleConditionProperty : rule.conditionProperties)
	{
		conditionProperties.emplace_back(HeadlineProperty::CreateProperty(ruleConditionProperty->GetPropertyType(), ruleConditionProperty->GetPropertyName()));
		*conditionProperties.back() = *ruleConditionProperty;
	}
	for(HeadlineProperty* ruleResultProperty : rule.resultProperties)
	{
		resultProperties.emplace_back(HeadlineProperty::CreateProperty(ruleResultProperty->GetPropertyType(), ruleResultProperty->GetPropertyName()));
		*resultProperties.back() = *ruleResultProperty;
	}
	resultIndices = rule.resultIndices;
	conditionIndices = rule.conditionIndices;
}

void Rule::Write(OutMemBitStream& outStream) const
{
	outStream.WriteBits(conditionIndices.size(), 32);
	for(uint32_t conditionIndex : conditionIndices)
		outStream.WriteBits(conditionIndex);
	outStream.WriteBits(resultIndices.size(), 32);
	for(uint32_t resultIndex : resultIndices)
		outStream.WriteBits(resultIndex);
	// Sizes of these vectors are equal to their index equivalent
	for(HeadlineProperty* conditionProperty : conditionProperties)
		conditionProperty->Write(outStream);
	for(HeadlineProperty* resultProperty : resultProperties)
		resultProperty->Write(outStream);
}

bool Rule::Read(InMemBitStream& inStream, const PropertiesList& conditions, const PropertiesList& results)
{
	uint32_t vecSize = 0;
	if(!inStream.ReadBits(vecSize)) return false;
	for(uint32_t i = 0; i < vecSize; i++)
	{
		conditionIndices.emplace_back();
		if(!inStream.ReadBits(conditionIndices.back())) return false;
		if(conditionIndices.back() >= conditions.GetListSize())
		{
			ErrorUtil::Get().SetError("Memory", "Rule::Read Condition does not exist");
			return false;
		}
	}
	
	if(!inStream.ReadBits(vecSize)) return false;
	for(uint32_t i = 0; i < vecSize; i++)
	{
		resultIndices.emplace_back();
		if(!inStream.ReadBits(resultIndices.back())) return false;
		if(resultIndices.back() >= results.GetListSize())
		{
			ErrorUtil::Get().SetError("Memory", "Rule::Read Result does not exist");
			return false;
		}
	}
	
	std::vector<const HeadlineProperty*> conditionPtrs;
	std::vector<const HeadlineProperty*> resultPtrs;
	for(uint32_t i : conditionIndices)
		conditionPtrs.emplace_back(conditions.GetPropAt(i));
	for(uint32_t i : resultIndices)
		resultPtrs.emplace_back(results.GetPropAt(i));
	*this = Rule(conditionIndices, conditionPtrs, resultIndices, resultPtrs);
	
	for(HeadlineProperty* conditionProperty : conditionProperties)
		if(!conditionProperty->Read(inStream)) return false;
	for(HeadlineProperty* resultProperty : resultProperties)
		if(!resultProperty->Read(inStream)) return false;
	return true;
}

RuleList::RuleList():
listName("Nameless")
{}

RuleList::RuleList(const UTF8String& initListName):
listName(initListName)
{}

void RuleList::AddRule(const std::vector<uint32_t>& conditionIndices, const std::vector<uint32_t>& resultIndices)
{
	std::vector<const HeadlineProperty*> conditionPtrs;
	std::vector<const HeadlineProperty*> resultPtrs;
	for(uint32_t i : conditionIndices)
		conditionPtrs.emplace_back(conditions.GetPropAt(i));
	for(uint32_t i : resultIndices)
		resultPtrs.emplace_back(results.GetPropAt(i));
	rules.emplace_back(conditionIndices, conditionPtrs, resultIndices, resultPtrs);
}

void RuleList::RemoveRule(uint32_t index)
{
	rules.erase(rules.begin() + index);
}

void RuleList::MoveRule(uint32_t index, uint32_t newIndex)
{
	Rule ruleToMove = rules[index];
	rules.erase(rules.begin() + index);
	rules.emplace(rules.begin() + newIndex, ruleToMove);
}

Rule& RuleList::GetRuleAt(uint32_t index) { return rules[index]; }
const Rule& RuleList::GetRuleAt(uint32_t index) const { return rules[index]; }

void RuleList::RemoveBlock(BlockID blockToRemove)
{
	for(Rule& rule : rules)
	{
		for(HeadlineProperty* conditionProperty : rule.conditionProperties)
			PropertiesList::RemoveBlockFromProperty(conditionProperty, blockToRemove);
		for(HeadlineProperty* resultProperty : rule.resultProperties)
			PropertiesList::RemoveBlockFromProperty(resultProperty, blockToRemove);
	}
}

void RuleList::RemoveNPC(NPCID npcToRemove)
{
	for(Rule& rule : rules)
	{
		for(HeadlineProperty* conditionProperty : rule.conditionProperties)
			PropertiesList::RemoveNPCFromProperty(conditionProperty, npcToRemove);
		for(HeadlineProperty* resultProperty : rule.resultProperties)
			PropertiesList::RemoveNPCFromProperty(resultProperty, npcToRemove);
	}
}

void RuleList::Write(OutMemBitStream& outStream) const
{
	outStream.WriteBits(rules.size(), 32);
	for(const Rule& rule : rules)
		rule.Write(outStream);
}

bool RuleList::Read(InMemBitStream& inStream)
{
	uint32_t listSize = 0;
	if(!inStream.ReadBits(listSize)) return false;
	for(uint32_t i = 0; i < listSize; i++)
	{
		rules.emplace_back();
		if(!rules.back().Read(inStream, conditions, results)) return false;
	}
	return true;
}

bool RuleList::ReadText(InMemBitStream& inStream)
{
	return true; // Implement when needed
}

void RuleList::WriteText(OutMemBitStream& outStream) const
{
	// Implement when needed
}

uint32_t RuleList::GetListSize() const { return rules.size(); }
const UTF8String& RuleList::GetListName() const { return listName; }
void RuleList::SetConditions(const PropertiesList& conditions) { this->conditions = conditions; }
void RuleList::SetResults(const PropertiesList& results) { this->results = results; }
const PropertiesList& RuleList::GetConditions() const { return conditions; }
const PropertiesList& RuleList::GetResults() const { return results; }
