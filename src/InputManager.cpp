#include "InputManager.hpp"
#include "GameSandbox.hpp"

InputState::InputState():
HorizontalMovement(0.0f),
WPressed(false),
QPressed(false),
EPressed(false),
LeftClickPressed(false),
RightClickPressed(false),
TriggerPressed(false),
cursorDirection(0.0f)
{}

void InputState::Write(OutMemBitStream& outStream) const
{
	outStream.WriteFixedFloat(HorizontalMovement, -10.0f, 0.0001f, 18);
	outStream.WriteBits(WPressed);
	outStream.WriteBits(QPressed);
	outStream.WriteBits(EPressed);
	outStream.WriteBits(LeftClickPressed);
	outStream.WriteBits(RightClickPressed);
	outStream.WriteBits(TriggerPressed);
	outStream.WriteFixedFloat(cursorDirection.x, -1.0f, 0.1f, 5);
	outStream.WriteFixedFloat(cursorDirection.y, -1.0f, 0.1f, 5);
}

bool InputState::Read(InMemBitStream& inStream)
{
	HorizontalMovement = 0.0f;
	cursorDirection.x = 0.0f;
	cursorDirection.y = 0.0f;
	if(!inStream.ReadFixedFloat(HorizontalMovement, -10.0f, 0.0001f, 18)) return false;
	if(!inStream.ReadBits(WPressed)) return false;
	if(!inStream.ReadBits(QPressed)) return false;
	if(!inStream.ReadBits(EPressed)) return false;
	if(!inStream.ReadBits(LeftClickPressed)) return false;
	if(!inStream.ReadBits(RightClickPressed)) return false;
	if(!inStream.ReadBits(TriggerPressed)) return false;
	if(!inStream.ReadFixedFloat(cursorDirection.x, -1.0f, 0.1f, 5)) return false;
	if(!inStream.ReadFixedFloat(cursorDirection.y, -1.0f, 0.1f, 5)) return false;
	return true;
}

InputManager& InputManager::Get()
{
	static InputManager sInstance;
	return sInstance;
}

void InputManager::Update()
{
	// Grab current input state
	bool APressed = ControllerUtil::Get().GetButtonState(CONTROLLER_LEFT) != KEYSTATE_RELEASED;
	bool DPressed = ControllerUtil::Get().GetButtonState(CONTROLLER_RIGHT) != KEYSTATE_RELEASED;
	
	if(DPressed == APressed)
		currentInputState.HorizontalMovement = 0;
	else if(DPressed)
		currentInputState.HorizontalMovement = TimeUtil::Get().GetDeltaTime();
	else
		currentInputState.HorizontalMovement = -TimeUtil::Get().GetDeltaTime();
	currentInputState.WPressed = ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY0) != KEYSTATE_RELEASED;
	currentInputState.QPressed = ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY3) != KEYSTATE_RELEASED;
	currentInputState.EPressed = ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY4) != KEYSTATE_RELEASED;
	currentInputState.LeftClickPressed = ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY1) != KEYSTATE_RELEASED;
	currentInputState.RightClickPressed = ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY2) != KEYSTATE_RELEASED;
	currentInputState.TriggerPressed = ControllerUtil::Get().GetButtonState(CONTROLLER_TRIGGER) != KEYSTATE_RELEASED;
	currentInputState.cursorDirection = GameSandbox::Get().GetCursorDirection();
	// Apply to average input state
	averagedInputState.HorizontalMovement += currentInputState.HorizontalMovement;
	if(currentInputState.WPressed) averagedInputState.WPressed = true;
	if(currentInputState.QPressed) averagedInputState.QPressed = true;
	if(currentInputState.EPressed) averagedInputState.EPressed = true;
	if(currentInputState.LeftClickPressed) averagedInputState.LeftClickPressed = true;
	if(currentInputState.RightClickPressed) averagedInputState.RightClickPressed = true;
	if(currentInputState.TriggerPressed) averagedInputState.TriggerPressed = true;
	averagedInputState.cursorDirection = currentInputState.cursorDirection;
}

const InputState& InputManager::GetCurrentInputState() const { return currentInputState; }
const InputState& InputManager::GetAverageInputState() const { return averagedInputState; }
void InputManager::ClearAveragedInputState() { averagedInputState = InputState(); }
void InputManager::ClearCurrentInputState() { currentInputState = InputState(); }
