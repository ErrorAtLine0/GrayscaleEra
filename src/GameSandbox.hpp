#pragma once
#include "NetworkManager.hpp"
#include "ArenaData.hpp"
#include "AdventureData.hpp"
#include "GameObjects/Point.hpp"
#include "GameObjects/RealPlayer.hpp"
#include "GameObjects/NPC.hpp"
#include <unordered_map>
#include <GSEGL/Graphics/Texture.hpp>
#include <GSEGL/Graphics/Math.hpp>
#define MIN_WORLD_BORDER -500.0f
#define MAX_WORLD_BORDER_ARENA (ARENALEVELSIZE * 40.0f + 500.0f)
#define MAX_WORLD_BORDER_ADVENTURE (ADVENTURELEVELSIZE * 40.0f + 500.0f)

struct GameSandboxProperties
{
	GameSandboxProperties():
	gameTime(0),
	respawnTime(0)
	{}
	
	uint32_t gameTime;
	uint32_t respawnTime;
};

// Could make this more complex to prevent any mistakes (store player IDs, use uint64_t instead for PlayerIDs, etc.
class PlayerIDGenerator
{
	public:
		static PlayerIDGenerator& Get();
		void Init();
		PlayerID GeneratePlayerID();
	private:
		PlayerID playerID;
};

class GameSandbox
{
	public:
		static GameSandbox& Get();
		void Init(const GameSandboxProperties& initGameProperties, const WorldDataPtr& initWorldData, const std::shared_ptr<NetworkManager>& initNetMan, PlayerID initOwnPlayerID);
		void Cleanup();
		bool Update();
		void DrawObjects();
		void SummonObject(GameObject* object);
		bool HasGameEnded();
		void EndGame();
		void SetCamera(const Vec2& cameraPos, float cameraZoom, uint32_t cameraRoom);
		PlayerID AddPlayer(const UTF8String& userName, const PlayerDataPtr& playerData, PlayerID playerID = 0);
		PlayerID AddPlayer(const UTF8String& userName, NPCID npcIDForData, PlayerID playerID);
		void RemovePlayer(PlayerID playerID);
		void ClientAddObject(GameObject* gameObj);
		void MoveObject(GameObject* gameObj, int32_t oldRoom, int32_t newRoom);
		PlayerID GetOwnPlayerID();
		uint32_t GetRoomCount();
		std::vector<GameObject*>& GetObjects(uint32_t roomNumber);
		std::vector<GameObject*>& GetAllObjects();
		std::vector<Player*>& GetAllPlayerObjects();
		std::vector<RealPlayer*>& GetAllRealPlayerObjects();
		std::vector<GameObject*>::iterator ClientEraseObject(const std::vector<GameObject*>::iterator& it);
		const std::vector<TexturePtr>* GetBlockTexturesFromID(uint8_t blockID);
		const std::vector<std::vector<std::vector<TexturePtr>>>* GetPlayerTexturesFromID(PlayerID playerID);
		const std::vector<std::vector<std::vector<TexturePtr>>>* GetNPCTexturesFromID(NPCID npcID);
		std::vector<PlayerInfoPtr>& GetAllRealPlayerInfos();
		PlayerInfoPtr GetRealPlayerInfoByPlayerID(PlayerID playerID);
		const UTF8String* GetUserNameByPlayerID(PlayerID playerID);
		const PlayerData* GetPlayerDataByPlayerID(PlayerID playerID);
		const PlayerData* GetPlayerDataByNPCID(NPCID npcID);
		NPCPropListID GetNPCPropListIDByNPCPropList(const PropertiesList* npcPropList);
		const PropertiesList* GetNPCPropListByNPCPropListID(NPCPropListID npcPropListID);
		void AllocateNPCPlayerInfo();
		void DeallocateNPCPlayerInfo();
		WorldDataPtr GetWorldData();
		const LevelData* GetLevelData(uint32_t roomNumber);
		std::vector<BlockData>& GetBlockData();
		const std::unordered_multimap<UTF8String, Point*>& GetTagToPointMap();
		Point* GetPointByTag(const UTF8String& tag); // Only gives one point
		void SetNumber(const UTF8String& name, uint32_t val);
		uint32_t GetNumber(const UTF8String& name);
		uint32_t* GetNumberPtr(const UTF8String& name);
		void SetRealPlayerSpawnPoint(const PlayerInfoPtr& playerInfo, Point* spawnPoint);
		Vec2 GetCursorDirection();
		float GetWorldMaxSize();
		float GetWorldMinSize();
		void SendMessage(PlayerID playerID, const UTF8String& message);
		bool IsClient();
		bool IsServer();
		double GetEstimatedSendRateClient();
		double GetEstimatedSendRateServer(PlayerID playerID);
		double GetRTTClient();
		double GetRTTServer(PlayerID playerID);
		void SetAnnouncement(PlayerID playerID, const UTF8String& message);
		const UTF8String& GetAnnouncement();
		float GetOtherPlayerPing();
		Point* GetRandSpawnPoint(); // Used for arenas (adventures have clearly defined start points instead)
		double GetTimeLeftInSeconds();
		bool IsFirstUpdate();
		void GetFourNearestChunksToPos(std::vector<std::vector<GameObject*>*>* chunkVec, const Vec2& pos, uint32_t room);
		std::vector<GameObject*>& GetMovingObjects(uint32_t roomNumber);
	private:
		void AddBlockToChunk(GameObject* object, uint32_t room);
		void AddChunkPtrToVec(uint32_t x, uint32_t y, uint32_t room, std::vector<std::vector<GameObject*>*>* chunkVec);
		void LoadNPCPropLists(const RuleList& ruleList);
		void CreateObject(GameObject* object, bool sendToClients = true);
		std::vector<GameObject*>::iterator DestroyObject(GameObject* object);
		void RespawnPlayer(const PlayerInfoPtr& playerToRespawn);
		void InitPlayerTextures(std::vector<std::vector<std::vector<TexturePtr>>>& thisPlayerTextures, const PlayerData& playerData);
		PlayerID AddPlayerGeneral(const UTF8String& userName, PlayerID playerID);
		
		std::vector<std::pair<double, PlayerInfoPtr>> playerRespawnTimers;
		
		std::vector<std::vector<TexturePtr>> blockTextures;
		std::unordered_map<PlayerID, std::vector<std::vector<std::vector<TexturePtr>>>> playerTextures; // HOW DEEP CAN THIS GET O-o
		std::vector<std::vector<std::vector<std::vector<TexturePtr>>>> npcTextures; // SAME GOES HERE (instead of unordered_map, I'll simply use a vector, since the NPC IDs are ordered)
		
		std::vector<Player*> playerObjects;
		std::vector<RealPlayer*> realPlayerObjects;
		std::vector<GameObject*> objects;
		std::vector<std::vector<GameObject*>> roomObjects; // Every vector is a different level/room
		// staticBlockChunks[room][chunkX][chunkY][objectIndex]
		std::vector<std::vector<std::vector<std::vector<GameObject*>>>> staticBlockChunks;
		std::vector<std::vector<GameObject*>> movingObjects;
		
		std::vector<GameObject*> objectsToSummon;
		std::vector<const PropertiesList*> npcPropLists;
		std::unordered_multimap<UTF8String, Point*> points; // <tag, point>
		
		std::vector<PlayerInfoPtr> realPlayerInfos;
		std::unordered_map<PlayerID, PlayerDataPtr> playerIDToPlayerData;
		std::unordered_map<PlayerID, NPCID> playerIDToNPCID; // Used when forcing a player through adventure
		
		WorldDataPtr worldDataPtr;
		AdventureDataPtr adventureDataPtr;
		ArenaDataPtr arenaDataPtr;
		std::vector<BlockData>* blocks;
		std::vector<PlayerData>* npcs;
		std::vector<LevelData*> levels;
		std::unordered_map<UTF8String, uint32_t> numbers;
		
		GameSandboxProperties gameProperties;
		std::shared_ptr<NetworkManager> netMan;
		std::unordered_map<PlayerID, UTF8String> announcements;
		std::unordered_map<PlayerID, UTF8String> announcementsSent;
		
		Vec2 cameraPos;
		float cameraZoom;
		uint32_t cameraRoom;
		
		bool isFirstUpdate;
		bool isNotClient;
		bool isServer;
		bool gameEnded;
		double timeLeft;
		PlayerID ownPlayerID;
};
