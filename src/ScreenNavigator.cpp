#include "ScreenNavigator.hpp"

bool ScreenNavigator::Update()
{
	return currentScreen->Update();
}

void ScreenNavigator::Draw()
{
	currentScreen->Draw();
}

void ScreenNavigator::SetScreen(const std::shared_ptr<Screen>& screen)
{
	currentScreen = screen;
}

const std::shared_ptr<Screen>& ScreenNavigator::GetScreen() const
{
	return currentScreen;
}
