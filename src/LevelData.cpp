#include "LevelData.hpp"
#include <cstring>

LevelData::LevelData(uint32_t initLevelSize):
	levelSize(initLevelSize)
{
	level = static_cast<BlockID*>(std::calloc(levelSize * levelSize, sizeof(BlockID)));
	levelBackground = static_cast<BlockID*>(std::calloc(levelSize * levelSize, sizeof(BlockID)));
	points = static_cast<PointID*>(std::calloc(levelSize * levelSize, sizeof(PointID)));
}

LevelData::LevelData(const LevelData& initLevelData):
	level(nullptr),
	levelBackground(nullptr),
	points(nullptr),
	levelSize(0)
{
	*this = initLevelData;
}

LevelData::~LevelData()
{
	std::free(level);
	std::free(levelBackground);
	std::free(points);
}

void LevelData::operator=(const LevelData& levelData)
{
	if(level != nullptr)
		std::free(level);
	if(levelBackground != nullptr)
		std::free(levelBackground);
	if(points != nullptr)
		std::free(points);
	name = levelData.name;
	levelSize = levelData.levelSize;
	uint32_t levelByteSize =       levelSize * levelSize * sizeof(BlockID);
	uint32_t pointsByteSize =      levelSize * levelSize * sizeof(PointID);
	level =           static_cast<BlockID*>(std::malloc(levelByteSize));
	levelBackground = static_cast<BlockID*>(std::malloc(levelByteSize));
	points =          static_cast<PointID*>(std::malloc(pointsByteSize));
	std::memcpy(level, levelData.level, levelByteSize);
	std::memcpy(levelBackground, levelData.levelBackground, levelByteSize);
	std::memcpy(points, levelData.points, pointsByteSize);
}

void LevelData::Write(OutMemBitStream& outStream) const
{
	name.Write(outStream);
	outStream.WriteBits(static_cast<const void*>(level), levelSize*levelSize*BITS_FOR_BLOCK);
	outStream.WriteBits(static_cast<const void*>(levelBackground), levelSize*levelSize*BITS_FOR_BLOCK);
	outStream.WriteBits(static_cast<const void*>(points), levelSize*levelSize*BITS_FOR_POINT);
}

bool LevelData::Read(InMemBitStream& inStream)
{
	if(!name.Read(inStream)) return false;
	if(!inStream.ReadBits(static_cast<void*>(level), levelSize*levelSize*BITS_FOR_BLOCK)) return false;
	if(!inStream.ReadBits(static_cast<void*>(levelBackground), levelSize*levelSize*BITS_FOR_BLOCK)) return false;
	if(!inStream.ReadBits(static_cast<void*>(points), levelSize*levelSize*BITS_FOR_POINT)) return false;
	return true;
}

BlockID& LevelData::BackBlockAt(uint32_t x, uint32_t y)
{
	return levelBackground[y * levelSize + x];
}

BlockID& LevelData::BlockAt(uint32_t x, uint32_t y)
{
	return level[y * levelSize + x];
}

PointID& LevelData::PointAt(uint32_t x, uint32_t y)
{
	return points[y * levelSize + x];
}

void LevelData::RemoveBlock(BlockID blockID)
{
	for(uint32_t i = 0; i < levelSize; i++)
		for(uint32_t j = 0; j < levelSize; j++)
		{
			if(BlockAt(j, i) == blockID)
				BlockAt(j, i) = 0;
			else if(BlockAt(j, i) > blockID)
				--(BlockAt(j, i));
			if(BackBlockAt(j, i) == blockID)
				BackBlockAt(j, i) = 0;
			else if(BackBlockAt(j, i) > blockID)
				--(BackBlockAt(j, i));
		}
}

void LevelData::RemovePoint(PointID point)
{
	for(uint32_t i = 0; i < levelSize; i++)
		for(uint32_t j = 0; j < levelSize; j++)
		{
			if(PointAt(j, i) == point)
				PointAt(j, i) = 0;
			else if(PointAt(j, i) > point)
				--(PointAt(j, i));
		}
}
