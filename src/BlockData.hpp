#pragma once
#include "PropertiesList.hpp"
#include "Frame.hpp"
#define BLOCK_TEXTURE_SIZE 20
#define BLOCK_MAX_FRAMES 5

class BlockData
{
	public:
		BlockData();
		BlockData(const std::vector<Frame>& initAnimation);
		
		void Write(OutMemBitStream& outStream) const;
		bool Read(InMemBitStream& inStream);
		void WriteFile(OutMemBitStream& outStream) const;
		bool ReadFile(InMemBitStream& inStream);
		static std::shared_ptr<Frame> ReadFileImage(InMemBitStream& inStream);

		PropertiesList propList;
		std::vector<Frame> animation;
	private:
		void SetupPropertiesList();
};

typedef std::shared_ptr<BlockData> BlockDataPtr;
