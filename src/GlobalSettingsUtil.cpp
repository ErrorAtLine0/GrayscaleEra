#include "GlobalSettingsUtil.hpp"
#include <GSEGL/FileUtil.hpp>
#include <GSEGL/Graphics/DrawUtil.hpp>
#include <GSEGL/Graphics/WindowUtil.hpp>
#include <GSEGL/Graphics/ControllerUtil.hpp>
#include <GSEGL/Graphics/GUI/GUIUtil.hpp>

GlobalSettingsUtil& GlobalSettingsUtil::Get()
{
	static GlobalSettingsUtil sInstance;
	return sInstance;
}

GlobalSettingsUtil::GlobalSettingsUtil():
settings("Settings"),
mainSettings("Main Settings")
{}

void GlobalSettingsUtil::InitMain()
{
	std::vector<UTF8String> rendererList;
	rendererList.emplace_back("OPENGL");
	rendererList.emplace_back("OPENGLES");
	rendererList.emplace_back("VULKAN");
	mainSettings.AddChoice("Renderer", rendererList);
	mainSettings.AddString("Master Server", 0);
	static_cast<ChoiceProperty*>(mainSettings.GetPropAt(0))->data = GetDefaultRenderer();
	static_cast<StringProperty*>(mainSettings.GetPropAt(1))->data = "erroratline0.com";
	ReadMainFromFile();
}

void GlobalSettingsUtil::Init()
{
	settings.AddBool("Dark Mode");
	settings.AddString("Default User Name", 16);
	settings.AddBool("Fullscreen");
	settings.AddInt("Volume (%) (To Be Added)", 0, 100, 1, 30);
	settings.AddBool("Enable Vsync (OpenGL & OpenGL ES only)");
	// Key binding settings
	PropertiesList mainBindings("Main Key Bindings");
	mainBindings.AddButton("Up", ButtonInfo(INPUTDEVICE_KEYBOARD, 0.0f, GLFW_KEY_W));
	mainBindings.AddButton("Down", ButtonInfo(INPUTDEVICE_KEYBOARD, 0.0f, GLFW_KEY_S));
	mainBindings.AddButton("Left", ButtonInfo(INPUTDEVICE_KEYBOARD, 0.0f, GLFW_KEY_A));
	mainBindings.AddButton("Right", ButtonInfo(INPUTDEVICE_KEYBOARD, 0.0f, GLFW_KEY_D));
	mainBindings.AddButton("Ability 1", ButtonInfo(INPUTDEVICE_KEYBOARD, 0.0f, GLFW_KEY_W));
	mainBindings.AddButton("Ability 2", ButtonInfo(INPUTDEVICE_MOUSE, 0.0f, GLFW_MOUSE_BUTTON_LEFT));
	mainBindings.AddButton("Ability 3", ButtonInfo(INPUTDEVICE_MOUSE, 0.0f, GLFW_MOUSE_BUTTON_RIGHT));
	mainBindings.AddButton("Ability 4", ButtonInfo(INPUTDEVICE_KEYBOARD, 0.0f, GLFW_KEY_Q));
	mainBindings.AddButton("Ability 5", ButtonInfo(INPUTDEVICE_KEYBOARD, 0.0f, GLFW_KEY_E));
	mainBindings.AddButton("Trigger", ButtonInfo(INPUTDEVICE_KEYBOARD, 0.0f, GLFW_KEY_S));
	mainBindings.AddButton("Pause", ButtonInfo(INPUTDEVICE_KEYBOARD, 0.0f, GLFW_KEY_ESCAPE));
	mainBindings.AddButton("Chat", ButtonInfo(INPUTDEVICE_KEYBOARD, 0.0f, GLFW_KEY_ENTER));
	mainBindings.AddButton("Player List", ButtonInfo(INPUTDEVICE_KEYBOARD, 0.0f, GLFW_KEY_TAB));
	mainBindings.AddButton("Cursor Up", ButtonInfo(INPUTDEVICE_KEYBOARD, 0.0f, -1));
	mainBindings.AddButton("Cursor Down", ButtonInfo(INPUTDEVICE_KEYBOARD, 0.0f, -1));
	mainBindings.AddButton("Cursor Left", ButtonInfo(INPUTDEVICE_KEYBOARD, 0.0f, -1));
	mainBindings.AddButton("Cursor Right", ButtonInfo(INPUTDEVICE_KEYBOARD, 0.0f, -1));
	mainBindings.AddButton("Scroll Up", ButtonInfo(INPUTDEVICE_KEYBOARD, 0.0f, -1));
	mainBindings.AddButton("Scroll Down", ButtonInfo(INPUTDEVICE_KEYBOARD, 0.0f, -1));
	
	PropertiesList alternateBindings("Alternate Key Bindings");
	alternateBindings.AddButton("Up", ButtonInfo(INPUTDEVICE_JOYSTICK, 0.0f, -1));
	alternateBindings.AddButton("Down", ButtonInfo(INPUTDEVICE_JOYSTICK, 0.0f, -1));
	alternateBindings.AddButton("Left", ButtonInfo(INPUTDEVICE_JOYSTICK, 0.0f, -1));
	alternateBindings.AddButton("Right", ButtonInfo(INPUTDEVICE_JOYSTICK, 0.0f, -1));
	alternateBindings.AddButton("Ability 1", ButtonInfo(INPUTDEVICE_JOYSTICK, 0.0f, -1));
	alternateBindings.AddButton("Ability 2", ButtonInfo(INPUTDEVICE_JOYSTICK, 0.0f, -1));
	alternateBindings.AddButton("Ability 3", ButtonInfo(INPUTDEVICE_JOYSTICK, 0.0f, -1));
	alternateBindings.AddButton("Ability 4", ButtonInfo(INPUTDEVICE_JOYSTICK, 0.0f, -1));
	alternateBindings.AddButton("Ability 5", ButtonInfo(INPUTDEVICE_JOYSTICK, 0.0f, -1));
	alternateBindings.AddButton("Trigger", ButtonInfo(INPUTDEVICE_JOYSTICK, 0.0f, -1));
	alternateBindings.AddButton("Pause", ButtonInfo(INPUTDEVICE_JOYSTICK, 0.0f, -1));
	alternateBindings.AddButton("Chat", ButtonInfo(INPUTDEVICE_JOYSTICK, 0.0f, -1));
	alternateBindings.AddButton("Player List", ButtonInfo(INPUTDEVICE_JOYSTICK, 0.0f, -1));
	alternateBindings.AddButton("Cursor Up", ButtonInfo(INPUTDEVICE_JOYSTICK, 0.0f, -1));
	alternateBindings.AddButton("Cursor Down", ButtonInfo(INPUTDEVICE_JOYSTICK, 0.0f, -1));
	alternateBindings.AddButton("Cursor Left", ButtonInfo(INPUTDEVICE_JOYSTICK, 0.0f, -1));
	alternateBindings.AddButton("Cursor Right", ButtonInfo(INPUTDEVICE_JOYSTICK, 0.0f, -1));
	alternateBindings.AddButton("Scroll Up", ButtonInfo(INPUTDEVICE_JOYSTICK, 0.0f, -1));
	alternateBindings.AddButton("Scroll Down", ButtonInfo(INPUTDEVICE_JOYSTICK, 0.0f, -1));
	
	settings.AddList("Main Key Bindings", mainBindings);
	settings.AddList("Alternate Key Bindings", alternateBindings);
	settings.AddInt("Joystick Cursor Speed", 0, 2000, 10, 500);
	settings.AddInt("Joystick Scroll Speed", 0, 2000, 10, 500);
	
	std::vector<UTF8String> sizeList;
	sizeList.emplace_back("Small");
	sizeList.emplace_back("Normal");
	sizeList.emplace_back("Large");
	
	settings.AddChoice("GUI Size", sizeList, 1);
	
	ReadFromFile();
	SetupSettings();
}

bool GlobalSettingsUtil::ReadFromFile()
{
	std::shared_ptr<InMemBitStream> fileStream = FileUtil::Get().ReadFromFile("settings");
	if(fileStream)
	{
		if(!settings.Read(*fileStream)) return false;
		return true;
	}
	else
		return false;
}

void GlobalSettingsUtil::WriteToFile()
{
	OutMemBitStream outStream;
	settings.Write(outStream);
	FileUtil::Get().WriteToFile("settings", outStream);
}

bool GlobalSettingsUtil::ReadMainFromFile()
{
	std::shared_ptr<InMemBitStream> fileStream = FileUtil::Get().ReadFromFile("mainSettings.txt", true);
	if(fileStream)
	{
		if(!mainSettings.ReadText(*fileStream)) return false;
		return true;
	}
	else
		return false;
}

void GlobalSettingsUtil::WriteMainToFile()
{
	OutMemBitStream outStream;
	mainSettings.WriteText(outStream);
	FileUtil::Get().WriteToFile("mainSettings.txt", outStream, true);
}

void GlobalSettingsUtil::SetupSettings()
{
	bool darkMode = settings.GetBool(0);
	DrawUtil::Get().SetColorInvert(darkMode);
	if(darkMode)
		WindowUtil::Get().SetClearColor(0.0f, 0.0f, 0.0f);
	else
		WindowUtil::Get().SetClearColor(1.0f, 1.0f, 1.0f);
	bool fullscreen = settings.GetBool(2);
	if(fullscreen != WindowUtil::Get().IsWindowedFullscreen())
		WindowUtil::Get().ToggleWindowedFullscreen();
	WindowUtil::Get().SetVSync(settings.GetBool(4));
	const PropertiesList& mainBindings = settings.GetList(5);
	const PropertiesList& alternateBindings = settings.GetList(6);
	for(uint32_t i = 0; i < CONTROLLER_MAX; i++)
	{
		ControllerUtil::Get().SetBinding(alternateBindings.GetButton(i), static_cast<ControllerButton>(i));
		ControllerUtil::Get().SetBinding(mainBindings.GetButton(i), static_cast<ControllerButton>(i));
	}
	ControllerUtil::Get().SetCursorSpeed(settings.GetInt(7));
	ControllerUtil::Get().SetScrollSpeed(settings.GetInt(8));
	uint32_t GUISizeNumber = settings.GetChoice(9);
	if(GUISizeNumber == 0)
		GUIUtil::Get().SetGUISize(0.85f);
	else if(GUISizeNumber == 1)
		GUIUtil::Get().SetGUISize(1.0f);
	else if(GUISizeNumber == 2)
		GUIUtil::Get().SetGUISize(1.5f);
}

uint32_t GlobalSettingsUtil::GetDefaultRenderer()
{
	return
#ifdef ENABLE_OPENGL_RENDERER
	0
#elif ENABLE_OPENGLES_RENDERER
	1
#elif ENABLE_VULKAN_RENDERER
	2
#endif
	;
}
