#include "NetworkManagerClient.hpp"
#include "Screens/InLobbyScreen.hpp"
#include "Screens/GameScreen.hpp"
#include "Screens/MainMenuScreen.hpp"
#include <GSEGL/LogUtil.hpp>

NetworkManagerClient::NetworkManagerClient():
	serverAddr(nullptr),
	playerDataQueued(false),
	timeLastPacketSent(0.0),
	privateID(CURRENT_VERSION),
	replicationManager(&linkingContext)
{
	
}

bool NetworkManagerClient::Init(const UTF8String& initUserName, const UTF8String& initLobbyName, WorldType initWorldType)
{
	if(!NetworkManager::Init(initUserName, initLobbyName, 0)) return false;
	if(initWorldType == WORLD_ARENA)
		worldData = std::make_shared<ArenaData>();
	else if(initWorldType == WORLD_ADVENTURE)
		worldData = std::make_shared<AdventureData>();
	return true;
}

bool NetworkManagerClient::Init(const UTF8String& initUserName, const UTF8String& initIP, uint16_t initPort, WorldType initWorldType)
{
	if(!NetworkManager::Init(initUserName, 0)) return false;
	if(initWorldType == WORLD_ARENA)
		worldData = std::make_shared<ArenaData>();
	else if(initWorldType == WORLD_ADVENTURE)
		worldData = std::make_shared<AdventureData>();
	serverAddr = SocketUtil::Get().CreateAddrFromString(initIP, UTF8String::IntToStr(initPort), INET);
	if(!serverAddr) return false;
	OutMemBitStream outStream;
	outStream.WriteBits(PACKET_HELLO, ByteUtil::GetRequiredBits<PACKET_MAX>::Value);
	userName.Write(outStream);
	reliableDataManager.QueueDataToSend(outStream);
	return true;
}

bool NetworkManagerClient::Update()
{
	/////////////////////
	// Receiving Stage //
	/////////////////////
	ReceivePackets();
	deliveryNotifier.ProcessTimedOutPackets();
	if(!deliveryNotifier.IsConnected())
	{
		ErrorUtil::Get().SetError("Network", "NetworkManagerClient::Update Server timed out");
		return false;
	}
	while(!packets.empty())
	{
		Packet& packet = packets.front();
		InMemBitStream inStream(packet.data, packet.size, false);
		switch(state)
		{
			case NETMAN_GETLOBBY:
			{
				if(!serverAddr)
				{
					LobbyPacketType lobbyPacketType = LOBBYPT_SetupLobby;
					if(!inStream.ReadBits(lobbyPacketType)) return false;
					*lobbyServerAddr = packet.from;
					switch(lobbyPacketType)
					{
						case LOBBYPT_LobbyNotAvailable:
						{
							ErrorUtil::Get().SetError("Network", "NetworkManagerClient::Update Lobby not available");
							return false;
						}
						case LOBBYPT_LobbyAvailable:
						{
							serverAddr = std::make_shared<SocketAddress>();
							if(!serverAddr->Read(inStream)) return false;
							OutMemBitStream outStream;
							outStream.WriteBits(PACKET_HELLO, ByteUtil::GetRequiredBits<PACKET_MAX>::Value);
							userName.Write(outStream);
							reliableDataManager.QueueDataToSend(outStream);
							break;
						}
						case LOBBYPT_DifferentVersion:
						{
							ErrorUtil::Get().SetError("Network", "NetworkManagerClient::Update Playing a different version than master server");
							LogUtil::Get().LogMessage("NORMAL", "Outdated version! Please update your game.");
							return false;
						}
						default:
							return false;
					}
				}
				else if(!lobbyServerAddr || packet.from != *lobbyServerAddr)
				{
					TransmissionDataType transmissionDataType = TRANSMISSIONDATA_UNRELIABLE;
					if(!inStream.ReadBits(transmissionDataType, ByteUtil::GetRequiredBits<TRANSMISSIONDATA_MAX>::Value)) return false;
					if(transmissionDataType == TRANSMISSIONDATA_RELIABLE)
					{
						if(!inStream.ReadBits(privateID)) return false;
						int returnVal = deliveryNotifier.ProcessPacketHeader(inStream);
						if(returnVal == 0) return false; else if(returnVal == 1) break;
						returnVal = reliableDataManager.CheckIfNewData(inStream);
						if(returnVal == 0) return false; else if(returnVal == 1) break; 
						PacketType packetType = PACKET_HELLO;
						if(!inStream.ReadBits(packetType, ByteUtil::GetRequiredBits<PACKET_MAX>::Value)) return false;
						if(packetType == PACKET_WELCOME)
						{
							LogUtil::Get().LogMessage("NORMAL", "Player Welcomed");
							if(!inStream.ReadBits(requiredPlayerCount)) return false;
							if(!inStream.ReadBits(playerID)) return false;
							uint8_t numberOfPlayers = 0;
							if(!inStream.ReadBits(numberOfPlayers)) return false;
							if(!inStream.ReadBits(requestPlayer)) return false;
							if(!inStream.ReadBits(playerMaxPoints)) return false;
							WorldType receivedWorldType = WORLD_ADVENTURE;
							if(!inStream.ReadBits(receivedWorldType, ByteUtil::GetRequiredBits<WORLD_MAX>::Value)) return false;
							if(receivedWorldType != worldData->GetType())
							{
								ErrorUtil::Get().SetError("Memory", "NetworkManagerClient::Update Connecting to server with different world type");
								LogUtil::Get().LogMessage("NORMAL", "Server is hosting world of different type");
								return false;
							}
							for(uint8_t i = 0; i < numberOfPlayers; i++)
							{
								players.emplace_back(std::make_shared<PlayerNetData>());
								if(!players.back()->ReadInitInfo(inStream)) return false;
								if(players.back()->playerID == playerID) // This is the client
									players.back()->playerData = playerDataFromFile;
							}
							ScreenNavigator::Get().SetScreen(std::make_shared<InLobbyScreen>(std::make_shared<MainMenuScreen>(), shared_from_this()));
							*serverAddr = packet.from;
							state = NETMAN_INLOBBY;
						}
					}
				}
				break;
			}
			case NETMAN_INLOBBY:
			{
				if(packet.from == *serverAddr)
				{
					TransmissionDataType transmissionDataType = TRANSMISSIONDATA_UNRELIABLE;
					PrivateID receivedPrivateID = 0;
					if(!inStream.ReadBits(transmissionDataType, ByteUtil::GetRequiredBits<TRANSMISSIONDATA_MAX>::Value)) return false;
					if(!inStream.ReadBits(receivedPrivateID) || receivedPrivateID != privateID) return false;
					int returnVal = deliveryNotifier.ProcessPacketHeader(inStream);
					if(returnVal == 0) return false; else if(returnVal == 1) break;
					if(transmissionDataType == TRANSMISSIONDATA_RELIABLE)
					{
						returnVal = reliableDataManager.CheckIfNewData(inStream);
						if(returnVal == 0) return false; else if(returnVal == 1) break; 
					}
					PacketType packetType = PACKET_HELLO;
					if(!inStream.ReadBits(packetType, ByteUtil::GetRequiredBits<PACKET_MAX>::Value)) return false;
					if(packetType == PACKET_CHATMESSAGE)
					{
						UTF8String receivedChatMessage;
						if(!receivedChatMessage.Read(inStream)) return false;
						LogUtil::Get().LogMessage("NORMAL", receivedChatMessage);
					}
					else if(packetType == PACKET_PLAYERJOINED)
					{
						players.emplace_back(std::make_shared<PlayerNetData>());
						if(!players.back()->ReadInitInfo(inStream))
							players.pop_back();
					}
					else if(packetType == PACKET_PLAYERDISCONNECTED)
					{
						PlayerID disconnectedPlayerID;
						if(!inStream.ReadBits(disconnectedPlayerID)) return false;
						auto it = GetPlayerNetDataItByPlayerID(disconnectedPlayerID);
						if(it != players.end())
							players.erase(it);
					}
					else if(packetType == PACKET_GAMESTART)
						state = NETMAN_DATAEXCHANGE;
				}
				break;
			}
			case NETMAN_DATAEXCHANGE:
			{
				if(packet.from == *serverAddr)
				{
					TransmissionDataType transmissionDataType = TRANSMISSIONDATA_UNRELIABLE;
					PrivateID receivedPrivateID = 0;
					if(!inStream.ReadBits(transmissionDataType, ByteUtil::GetRequiredBits<TRANSMISSIONDATA_MAX>::Value)) return false;
					if(!inStream.ReadBits(receivedPrivateID) || receivedPrivateID != privateID) return false;
					int returnVal = deliveryNotifier.ProcessPacketHeader(inStream);
					if(returnVal == 0) return false; else if(returnVal == 1) break;
					if(transmissionDataType == TRANSMISSIONDATA_RELIABLE)
					{
						returnVal = reliableDataManager.CheckIfNewData(inStream);
						if(returnVal == 0) return false; else if(returnVal == 1) break; 
					}
					PacketType packetType = PACKET_HELLO;
					if(!inStream.ReadBits(packetType, ByteUtil::GetRequiredBits<PACKET_MAX>::Value)) return false;
					if(packetType == PACKET_CHATMESSAGE)
					{
						UTF8String receivedChatMessage;
						if(!receivedChatMessage.Read(inStream)) return false;
						LogUtil::Get().LogMessage("NORMAL", receivedChatMessage);
					}
					else if(packetType == PACKET_WORLDDATA)
					{
						uint32_t bitsToRead = 0;
						uint8_t dataReceived[1000];
						if(!inStream.ReadBits(bitsToRead)) return false;
						if(!inStream.ReadBits(static_cast<void*>(dataReceived), bitsToRead)) return false;
						worldDataOutStream.WriteBits(static_cast<const void*>(dataReceived), bitsToRead);
					}
					else if(packetType == PACKET_PLAYERDATA)
					{
						PlayerID receivedPlayerID = 0;
						uint32_t bitsToRead = 0;
						uint8_t dataReceived[1000];
						if(!inStream.ReadBits(receivedPlayerID)) return false;
						if(!inStream.ReadBits(bitsToRead)) return false;	
						if(!inStream.ReadBits(static_cast<void*>(dataReceived), bitsToRead)) return false;
						PlayerNetDataPtr playerNetData = GetPlayerNetDataByPlayerID(receivedPlayerID);
						if(playerNetData != nullptr)
							playerIDToPlayerDataStream[playerNetData->playerID].WriteBits(static_cast<const void*>(dataReceived), bitsToRead);
					}
					else if(packetType == PACKET_PLAYERDISCONNECTED)
					{
						PlayerID disconnectedPlayerID;
						if(!inStream.ReadBits(disconnectedPlayerID)) return false;
						auto playerIt = GetPlayerNetDataItByPlayerID(disconnectedPlayerID);
						if(playerIt != players.end())
						{
							players.erase(playerIt);
							worldDataOutStream = OutMemBitStream();
							playerIDToPlayerDataStream.clear();
							state = NETMAN_INLOBBY;
						}
					}
					else if(packetType == PACKET_HASALLDATA)
					{
						OutMemBitStream outStream;
						outStream.WriteBits(PACKET_HASALLDATA, ByteUtil::GetRequiredBits<PACKET_MAX>::Value);
						reliableDataManager.QueueDataToSend(outStream);
					}
					else if(packetType == PACKET_GAMESTART)
					{
						GameSandboxProperties gameProperties;
						float timeLeft = 0.0;
						inStream.ReadFixedFloat(timeLeft, 0.0f, 0.1f);
						gameProperties.gameTime = timeLeft;
						for(auto& playerDataPair : playerIDToPlayerDataStream)
						{
							InMemBitStream playerDataInStream(playerDataPair.second.GetBufferPtr(), playerDataPair.second.GetByteLength(), false);
							if(!GetPlayerNetDataByPlayerID(playerDataPair.first)->playerData->ReadFile(playerDataInStream)) return false;
						}
						InMemBitStream worldDataInStream(worldDataOutStream.GetBufferPtr(), worldDataOutStream.GetByteLength(), false);
						worldData->Read(worldDataInStream);
						ScreenNavigator::Get().SetScreen(std::make_shared<GameScreen>(std::make_shared<MainMenuScreen>(), gameProperties, worldData, playerID, shared_from_this()));
						for(const PlayerNetDataPtr& player : players)
						{
							// Currently, adventures simply override chosen players.
							NPCID playerNPC = worldData->GetType() == WORLD_ADVENTURE ? std::static_pointer_cast<AdventureData>(worldData)->settings.GetNPC(3) : 0;
							if(playerNPC)
								GameSandbox::Get().AddPlayer(player->userName, playerNPC, player->playerID);
							else
								GameSandbox::Get().AddPlayer(player->userName, player->playerData, player->playerID);
						}
						ClearAllPlayerRawData();
						state = NETMAN_INGAME;
					}
				}
				break;
			}
			case NETMAN_INGAME:
			{
				if(packet.from == *serverAddr)
				{
					TransmissionDataType transmissionDataType = TRANSMISSIONDATA_UNRELIABLE;
					PrivateID receivedPrivateID = 0;
					if(!inStream.ReadBits(transmissionDataType, ByteUtil::GetRequiredBits<TRANSMISSIONDATA_MAX>::Value)) return false;
					if(!inStream.ReadBits(receivedPrivateID) || receivedPrivateID != privateID) return false;
					int returnVal = deliveryNotifier.ProcessPacketHeader(inStream);
					if(returnVal == 0) return false; else if(returnVal == 1) break; 
					if(transmissionDataType == TRANSMISSIONDATA_RELIABLE)
					{
						returnVal = reliableDataManager.CheckIfNewData(inStream);
						if(returnVal == 0) return false; else if(returnVal == 1) break; 
						PacketType packetType = PACKET_HELLO;
						if(!inStream.ReadBits(packetType, ByteUtil::GetRequiredBits<PACKET_MAX>::Value)) return false;
						if(packetType == PACKET_CHATMESSAGE)
						{
							UTF8String receivedChatMessage;
							if(!receivedChatMessage.Read(inStream)) return false;
							LogUtil::Get().LogMessage("NORMAL", receivedChatMessage);
						}
						else if(packetType == PACKET_ANNOUNCEMENT)
						{
							UTF8String announcementMessage;
							if(!announcementMessage.Read(inStream)) return false;
							GameSandbox::Get().SetAnnouncement(0, announcementMessage);
						}
						else if(packetType == PACKET_KILL)
						{
							PlayerID killer, victim;
							if(!inStream.ReadBits(killer)) return false;
							if(!inStream.ReadBits(victim)) return false;
							RecordDeath(killer, victim);
						}
						else if(packetType == PACKET_PLAYERDISCONNECTED)
						{
							PlayerID disconnectedPlayerID;
							if(!inStream.ReadBits(disconnectedPlayerID)) return false;
							auto playerIt = GetPlayerNetDataItByPlayerID(disconnectedPlayerID);
							if(playerIt != players.end())
							{
								LogUtil::Get().LogMessage("NORMAL", (*playerIt)->userName + " left the game");
								players.erase(playerIt);
							}
						}
						else if(packetType == PACKET_ENDGAME)
						{
							GameSandbox::Get().EndGame();
							state = NETMAN_ENDGAME;
						}
					}
					else if(transmissionDataType == TRANSMISSIONDATA_REPLICATION)
					{
						while(inStream.GetRemainingBytes() > 1)
							if(!replicationManager.ProcessReplicationCommand(inStream)) break;
						std::vector<GameObject*>& gameObjects = GameSandbox::Get().GetAllObjects();
						
						for(GameObject* gameObject : linkingContext.GetNewObjectsThisFrame())
							if(std::find(gameObjects.begin(), gameObjects.end(), gameObject) == gameObjects.end())
								GameSandbox::Get().ClientAddObject(gameObject);
						linkingContext.GetNewObjectsThisFrame().clear();
						
						for(auto it = gameObjects.begin(); it != gameObjects.end();)
						{
							if(linkingContext.GetNetID(*it, false) == 0)
								it = GameSandbox::Get().ClientEraseObject(it);
							else
								++it;
						}
					}
				}
				break;
			}
			case NETMAN_ENDGAME:
			{
				if(packet.from == *serverAddr)
				{
					TransmissionDataType transmissionDataType = TRANSMISSIONDATA_UNRELIABLE;
					PrivateID receivedPrivateID = 0;
					if(!inStream.ReadBits(transmissionDataType, ByteUtil::GetRequiredBits<TRANSMISSIONDATA_MAX>::Value)) return false;
					if(!inStream.ReadBits(receivedPrivateID) || receivedPrivateID != privateID) return false;
					int returnVal = deliveryNotifier.ProcessPacketHeader(inStream);
					if(returnVal == 0) return false; else if(returnVal == 1) break;
					if(transmissionDataType == TRANSMISSIONDATA_RELIABLE)
					{
						returnVal = reliableDataManager.CheckIfNewData(inStream);
						if(returnVal == 0) return false; else if(returnVal == 1) break; 
						PacketType packetType = PACKET_HELLO;
						if(!inStream.ReadBits(packetType, ByteUtil::GetRequiredBits<PACKET_MAX>::Value)) return false;
						if(packetType == PACKET_CHATMESSAGE)
						{
							UTF8String receivedChatMessage;
							if(!receivedChatMessage.Read(inStream)) return false;
							LogUtil::Get().LogMessage("NORMAL", receivedChatMessage);
						}
						else if(packetType == PACKET_ANNOUNCEMENT)
						{
							UTF8String announcementMessage;
							if(!announcementMessage.Read(inStream)) return false;
							GameSandbox::Get().SetAnnouncement(0, announcementMessage);
						}
						else if(packetType == PACKET_PLAYERDISCONNECTED)
						{
							PlayerID disconnectedPlayerID;
							if(!inStream.ReadBits(disconnectedPlayerID)) return false;
							auto playerIt = GetPlayerNetDataItByPlayerID(disconnectedPlayerID);
							if(playerIt != players.end())
							{
								LogUtil::Get().LogMessage("NORMAL", (*playerIt)->userName + " left the game");
								players.erase(playerIt);
							}
						}
					}
				}
				break;
			}
			default:
				break;
		}
		packets.pop_front();
	}
	
	///////////////////
	// Sending Stage //
	///////////////////
	switch(state)
	{
		case NETMAN_GETLOBBY:
		{
			if(!serverAddr)
			{
				if(timeLastPacketToLobbyServer + LOBBYSERVER_PACKET_TIMEOUT < TimeUtil::Get().GetTimeSinceStart())
				{
					if(lobbyServerSendAttempts > 5)
					{
						ErrorUtil::Get().SetError("Network", "NetworkManagerClient::Update Lobby server timed out");
						return false;
					}
					OutMemBitStream outStream;
					outStream.WriteBits(LOBBYPT_ConnectTo);
					outStream.WriteBits(CURRENT_VERSION, 64);
					lobbyName.Write(outStream);
					SendPacket(outStream, *lobbyServerAddr);
					++lobbyServerSendAttempts;
					timeLastPacketToLobbyServer = TimeUtil::Get().GetTimeSinceStart();
				}
				break;
			}
		}
		case NETMAN_INLOBBY:
		{
			break;
		}
		case NETMAN_DATAEXCHANGE:
		{
			if(requestPlayer && !playerDataQueued && playerDataFromFile)
			{
				// Player selected, send it over to the server
				GetPlayerNetDataByPlayerID(playerID)->playerData = playerDataFromFile;
				OutMemBitStream outStream;
				playerDataFromFile->WriteFile(outStream);
				for(uint32_t i = 0; i < outStream.GetBitLength(); i += 8000)
				{
					OutMemBitStream singlePacket;
					singlePacket.WriteBits(PACKET_PLAYERDATA, ByteUtil::GetRequiredBits<PACKET_MAX>::Value);
					if(i + 8000 > outStream.GetBitLength())
					{
						singlePacket.WriteBits(outStream.GetBitLength() - i, 32);
						singlePacket.WriteBits(static_cast<const void*>(outStream.GetBufferPtr()+(i/8)), outStream.GetBitLength() - i);
					}
					else
					{
						const uint32_t numOfBitsToSend = 8000;
						singlePacket.WriteBits(numOfBitsToSend, 32);
						singlePacket.WriteBits(static_cast<const void*>(outStream.GetBufferPtr()+(i/8)), numOfBitsToSend);
					}
					reliableDataManager.QueueDataToSend(singlePacket);
				}
				OutMemBitStream finPacket;
				finPacket.WriteBits(PACKET_PLAYERDATAFIN, ByteUtil::GetRequiredBits<PACKET_MAX>::Value);
				reliableDataManager.QueueDataToSend(finPacket);
				playerDataQueued = true;
			}
			break;
		}
		case NETMAN_INGAME:
		{
			GameSandbox::Get().GetRealPlayerInfoByPlayerID(playerID)->latestInputState = InputManager::Get().GetCurrentInputState();
			break;
		}
		case NETMAN_ENDGAME:
		{
			break;
		}
		default:
			break;
	}
	if(serverAddr && timeLastPacketSent + deliveryNotifier.GetPacketSendRate(state == NETMAN_DATAEXCHANGE ? EXCHANGE_PACKET_RATE : MIN_PACKET_RATE) < TimeUtil::Get().GetTimeSinceStart())
	{
		OutMemBitStream outStream;
		if(reliableDataManager.ContainsDataToWrite())
		{
			outStream.WriteBits(TRANSMISSIONDATA_RELIABLE, ByteUtil::GetRequiredBits<TRANSMISSIONDATA_MAX>::Value);
			outStream.WriteBits(privateID);
			InFlightPacket* inFlightPacket = deliveryNotifier.WritePacketHeader(outStream);
			reliableDataManager.WriteDataToPacket(outStream, inFlightPacket);
		}
		else if(state != NETMAN_INGAME)
		{
			outStream.WriteBits(TRANSMISSIONDATA_UNRELIABLE, ByteUtil::GetRequiredBits<TRANSMISSIONDATA_MAX>::Value);
			outStream.WriteBits(privateID);
			deliveryNotifier.WritePacketHeader(outStream);
			outStream.WriteBits(PACKET_ALIVE, ByteUtil::GetRequiredBits<PACKET_MAX>::Value);
		}
		else
		{
			outStream.WriteBits(TRANSMISSIONDATA_UNRELIABLE, ByteUtil::GetRequiredBits<TRANSMISSIONDATA_MAX>::Value);
			outStream.WriteBits(privateID);
			deliveryNotifier.WritePacketHeader(outStream);
			outStream.WriteBits(PACKET_INPUT, ByteUtil::GetRequiredBits<PACKET_MAX>::Value);
			InputManager::Get().GetAverageInputState().Write(outStream);
			InputManager::Get().ClearAveragedInputState();
		}
		SendPacket(outStream, *serverAddr);
		timeLastPacketSent = TimeUtil::Get().GetTimeSinceStart();
	}
	return true;
}

double NetworkManagerClient::GetEstimatedSendRate()
{
	return deliveryNotifier.GetEstimatedSendRate();
}

double NetworkManagerClient::GetRTT()
{
	return deliveryNotifier.GetRTT();
}

void NetworkManagerClient::CreateObject(GameObject* gameObj)
{
	linkingContext.GetNetID(gameObj, true);
}

void NetworkManagerClient::SendChatMessage(const UTF8String& chatMessage)
{
	OutMemBitStream outStream;
	outStream.WriteBits(PACKET_CHATMESSAGE, ByteUtil::GetRequiredBits<PACKET_MAX>::Value);
	chatMessage.Write(outStream);
	reliableDataManager.QueueDataToSend(outStream);
}
