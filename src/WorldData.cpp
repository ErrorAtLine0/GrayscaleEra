#include "WorldData.hpp"
#include "AdventureData.hpp"
#include "ArenaData.hpp"
#include <GSEGL/FileUtil.hpp>

std::shared_ptr<WorldData> WorldData::ReadWorldFile(const UTF8String& worldName, WorldType worldType)
{
	WorldDataPtr worldData;
	if(worldType == WORLD_ADVENTURE)
		worldData = std::make_shared<AdventureData>();
	else
		worldData = std::make_shared<ArenaData>();
	UTF8String worldPath = UTF8String(worldType == WORLD_ARENA ? "arenas/" : "adventures/") + worldName;
	std::shared_ptr<InMemBitStream> inStream = FileUtil::Get().ReadFromFile(worldPath);
	if(!inStream) return nullptr;
	if(!worldData->Read(*inStream)) return nullptr;
	return worldData;
}
