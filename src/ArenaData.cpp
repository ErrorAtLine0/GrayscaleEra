#include "ArenaData.hpp"
#include <GSEGL/ErrorUtil.hpp>
#define ARENA_HEADER_INT 3236413781

ArenaData::ArenaData():
level(ARENALEVELSIZE),
image(ARENA_TEXTURE_SIZE * ARENA_TEXTURE_SIZE, 0)
{}

void ArenaData::Write(OutMemBitStream& outStream) const
{
	OutMemBitStream firstStream;
	firstStream.WriteBits(ARENA_HEADER_INT, 32);
	image.Write(firstStream);
	firstStream.WriteBits(blocks.size(), 32);
	for(const BlockData& block : blocks)
		block.Write(firstStream);
	level.Write(firstStream);
	outStream.WriteCompressedData(firstStream.GetBufferPtr(), firstStream.GetByteLength());
}

bool ArenaData::Read(InMemBitStream& inStream)
{
	std::shared_ptr<InMemBitStream> firstStream = inStream.ReadCompressedData();
	if(!firstStream) return false;
	
	uint32_t arenaHeader = 0;
	if(!firstStream->ReadBits(arenaHeader, 32)) return false;
	if(arenaHeader != ARENA_HEADER_INT)
	{
		ErrorUtil::Get().SetError("Memory", "ArenaData::Read Not an arena file");
		return false;
	}
	
	if(!image.Read(*firstStream)) return false;
	
	uint32_t blockCount = 0;
	if(!firstStream->ReadBits(blockCount)) return false;
	blocks.clear();
	for(uint32_t i = 0; i < blockCount; i++)
	{
		blocks.emplace_back();
		if(!blocks.back().Read(*firstStream)) return false;
	}
	
	if(!level.Read(*firstStream)) return false;
	
	return true;
}

std::shared_ptr<Frame> ArenaData::ReadImage(InMemBitStream& inStream)
{
	std::shared_ptr<InMemBitStream> firstStream = inStream.ReadCompressedData();
	if(!firstStream) return nullptr;
	
	uint32_t adventureHeader = 0;
	if(!firstStream->ReadBits(adventureHeader, 32)) return nullptr;
	if(adventureHeader != ARENA_HEADER_INT)
	{
		ErrorUtil::Get().SetError("Memory", "ArenaData::ReadImage Not an arena file");
		return nullptr;
	}
	
	std::shared_ptr<Frame> imagePtr(std::make_shared<Frame>(ARENA_TEXTURE_SIZE*ARENA_TEXTURE_SIZE, 0));
	if(!imagePtr->Read(*firstStream)) return nullptr;
	return imagePtr;
}

void ArenaData::RemoveBlock(BlockID blockToRemove)
{
	level.RemoveBlock(blockToRemove);
	blocks.erase(blocks.begin() + (blockToRemove - 1));
}
