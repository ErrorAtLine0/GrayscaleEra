#include "AbilityData.hpp"
#include <algorithm>

std::vector<std::pair<UTF8String, AbilityData::AbilityProperties>> AbilityData::nameToProperties;
PricedPropertiesList AbilityData::pricedBuffList("Buff List");
PricedPropertiesList AbilityData::pricedDebuffList("Debuff List");
		
PricedPropertiesList::PricedPropertiesList() {}

PricedPropertiesList::PricedPropertiesList(const UTF8String& initPropListName):
	propList(initPropListName)
{}

void PricedPropertiesList::AddBool(const UTF8String& propName, uint32_t priceIfTrue)
{
	prices.emplace_back(priceIfTrue);
	UTF8String propNameWithPrice = (propName + ": ") + UTF8String::IntToStr(priceIfTrue);
	propList.AddBool(propNameWithPrice);
}

void PricedPropertiesList::AddInt(const UTF8String& propName, uint32_t increment, uint32_t maxVal, uint32_t pricePerIncrement)
{
	prices.emplace_back(pricePerIncrement);
	UTF8String propNameWithPrice = (((propName + ": ") + UTF8String::IntToStr(pricePerIncrement)) + "/") + UTF8String::IntToStr(increment);
	propList.AddInt(propNameWithPrice, 0, maxVal, increment);
}

void PricedPropertiesList::AddChoice(const UTF8String& propName, const PricedChoiceList& choiceAndPriceVector)
{
	std::vector<UTF8String> choices;
	for(const auto& choicePair : choiceAndPriceVector)
	{
		prices.emplace_back(choicePair.second);
		choices.emplace_back((choicePair.first + ": ") + UTF8String::IntToStr(choicePair.second));
	}
	propList.AddChoice(propName, choices);
}

void PricedPropertiesList::AddList(const UTF8String& propName, const PricedPropertiesList& pricedProperties)
{
	prices.insert(prices.end(), pricedProperties.prices.begin(), pricedProperties.prices.end());
	propList.AddList(propName, pricedProperties.GetPropListRef());
}

PropertiesList& PricedPropertiesList::GetPropListRef() { return propList; }
const PropertiesList& PricedPropertiesList::GetPropListRef() const { return propList; }

uint32_t PricedPropertiesList::GetCost() const
{
	uint32_t currentPriceIndex = 0;
	return CalculatePropListCost(propList, currentPriceIndex);
}

uint32_t PricedPropertiesList::CalculatePropListCost(const PropertiesList& propList, uint32_t& currentPriceIndex) const
{
	uint32_t totalCost = 0;
	for(uint32_t i = 0; i < propList.GetListSize(); i++)
	{
		const HeadlineProperty* property = propList.GetPropAt(i);
		switch(property->GetPropertyType())
		{
			case PROPERTY_BOOL:
			{
				const BoolProperty* boolProp = static_cast<const BoolProperty*>(property);
				if(boolProp->data)
					totalCost += prices[currentPriceIndex];
				++currentPriceIndex;
				break;
			}
			case PROPERTY_INT:
			{
				const IntProperty* intProp = static_cast<const IntProperty*>(property);
				totalCost += (prices[currentPriceIndex] * intProp->data) / intProp->diff;
				++currentPriceIndex;
				break;
			}
			case PROPERTY_CHOICE:
			{
				const ChoiceProperty* choiceProp = static_cast<const ChoiceProperty*>(property);
				totalCost += prices[currentPriceIndex + choiceProp->data];
				currentPriceIndex += choiceProp->choices.size();
				break;
			}
			case PROPERTY_LIST:
			{
				const ListProperty* listProp = static_cast<const ListProperty*>(property);
				totalCost += CalculatePropListCost(listProp->data, currentPriceIndex);
				break;
			}
			default:
				break;
		}
	}
	return totalCost;
}

AbilityData::AbilityProperties::AbilityProperties() {}

AbilityData::AbilityProperties::AbilityProperties(const UTF8String& initPropertiesListName):
pricedPropertiesList(initPropertiesListName)
{}

void AbilityData::Init()
{
	pricedBuffList.AddInt("Instant Health", 10, 1000, 500);
	pricedBuffList.AddInt("Health Regen (health/sec)", 5, 400, 250); 
	pricedBuffList.AddInt("Health Regen Length (sec)", 1, 10, 1200);
	pricedBuffList.AddInt("Speed Increase (%)", 10, 100, 100);
	pricedBuffList.AddInt("Speed Increase Length (sec)", 1, 10, 500);
	pricedBuffList.AddInt("Gravity Decrease (%)", 5, 100, 300);
	pricedBuffList.AddInt("Gravity Decrease Length (sec)", 1, 10, 1000);
	pricedBuffList.AddInt("Defense Increase (%)", 5, 40, 800);
	pricedBuffList.AddInt("Defense Increase Length (sec)", 1, 10, 1200);
	
	pricedDebuffList.AddInt("Instant Damage", 10, 500, 200);
	pricedDebuffList.AddInt("Damage over Time (damage/sec)", 5, 100, 100);
	pricedDebuffList.AddInt("Damage over Time Length (sec)", 1, 10, 1000);
	pricedDebuffList.AddInt("Speed Decrease (%)", 10, 100, 100);
	pricedDebuffList.AddInt("Speed Decrease Length (sec)", 1, 10, 500);
	pricedDebuffList.AddInt("Gravity Increase (%)", 5, 100, 300);
	pricedDebuffList.AddInt("Gravity Increase Length (sec)", 1, 10, 1000);
	pricedDebuffList.AddInt("Defense Reduction (%)", 5, 80, 800);
	pricedDebuffList.AddInt("Defense Reduction Length (sec)", 1, 10, 1200);
	
	AddAbility("Self Buff", 0);
	AddAnimationToAbility("Player");
	AddListToAbility("Buff On Use", pricedBuffList);
	
	AddAbility("Jump", 300);
	AddAnimationToAbility("Initial");
	AddAnimationToAbility("Airborne");
	AddListToAbility("Buff On Use", pricedBuffList);
	AddIntToAbility("Vertical Power", 60, 1200, 50);
	
	AddAbility("Projectile", 1000);
	AddAnimationToAbility("Player");
	AddAnimationToAbility("Projectile");
	AddListToAbility("Buff On Use", pricedBuffList);
	AddListToAbility("Debuff On Hit", pricedDebuffList);
	AddBoolToAbility("Cursor Aim", 1000);
	AddIntToAbility("Speed", 20, 800, 400);
	AddIntToAbility("Lifetime (sec)", 1, 3, 500);
	PricedChoiceList sizeChoiceList;
	sizeChoiceList.emplace_back("Quarter (25x25)", 0);
	sizeChoiceList.emplace_back("Half (50x50)", 200);
	sizeChoiceList.emplace_back("Full (100x100)", 300);
	AddChoiceToAbility("Actual Size", sizeChoiceList);
	AddBoolToAbility("Homing", 8000);
	AddBoolToAbility("Player Penetration", 4000);
	AddBoolToAbility("Block Penetration", 4000);
	
	AddAbility("Melee Attack", 500);
	AddAnimationToAbility("Player");
	AddListToAbility("Buff On Use", pricedBuffList);
	AddListToAbility("Debuff On Hit", pricedDebuffList);
	AddIntToAbility("Range (blocks)", 1, 3, 200);
	AddBoolToAbility("Circular", 1000);
	
	AddAbility("Hop Towards Cursor", 3000);
	AddAnimationToAbility("Player");
	AddListToAbility("Buff On Use", pricedBuffList);
	AddIntToAbility("Power", 1, 10, 300);
	
	AddAbility("Summon Static Minion", 3000);
	AddAnimationToAbility("Player");
	AddAnimationToAbility("Minion");
	AddAnimationToAbility("Minion Bullet (if available)");
	AddListToAbility("Buff On Use", pricedBuffList);
	AddIntToAbility("Buff/Debuff Range (blocks)", 1, 4, 1000);
	AddListToAbility("Buff in Minion Range", pricedBuffList);
	AddListToAbility("Debuff in Minion Range", pricedDebuffList);
	AddIntToAbility("Apply Buff/Debuff Speed (buff/5 sec)", 1, 5, 1000);
	AddIntToAbility("Lifetime (sec)", 1, 15, 1000);
	AddBoolToAbility("Shoots Projectiles", 500);
	AddListToAbility("Projectile: Debuff", pricedDebuffList);
	AddIntToAbility("Projectile: Detection Range", 2, 10, 500);
	AddIntToAbility("Projectile: Speed", 10, 250, 200);
	AddIntToAbility("Projectile: Lifetime (seconds)", 1, 3, 200);
	AddIntToAbility("Projectile: Attack Speed (fire/5 sec)", 1, 5, 1000);
}

AbilityData::AbilityData():
abilityChosenIndex(1),
typeList("Ability Types"),
cooldown(1)
{
	std::vector<std::pair<UTF8String, uint32_t>> choices;
	for(auto& propertyPair : nameToProperties)
		choices.emplace_back(propertyPair.first, propertyPair.second.typePrice);
	typeList.AddChoice("Select:", choices);
	SetAbilityToTypeChosen();
}

void AbilityData::SetAbilityToTypeChosen()
{
	uint32_t choiceIndex = typeList.GetPropListRef().GetChoice(0);
	if(choiceIndex != abilityChosenIndex)
	{
		abilityProperties = nameToProperties[choiceIndex].second;
		abilityChosenIndex = choiceIndex;
	}
}

PropertiesList& AbilityData::GetPropListRef() { return abilityProperties.pricedPropertiesList.GetPropListRef(); }
NamedAnimationList& AbilityData::GetAnimationListRef() { return abilityProperties.animationList; }
PropertiesList& AbilityData::GetTypeListRef() { return typeList.GetPropListRef(); }
const PropertiesList& AbilityData::GetPropListRef() const { return abilityProperties.pricedPropertiesList.GetPropListRef(); }
const NamedAnimationList& AbilityData::GetAnimationListRef() const { return abilityProperties.animationList; }
const PropertiesList& AbilityData::GetTypeListRef() const { return typeList.GetPropListRef(); }
uint32_t AbilityData::GetAbilityType() const { return abilityChosenIndex; }

void AbilityData::Write(OutMemBitStream& outStream) const
{
	typeList.GetPropListRef().Write(outStream);
	abilityProperties.pricedPropertiesList.GetPropListRef().Write(outStream);
	for(auto& animationPair : abilityProperties.animationList)
		Frame::WriteAnimation(outStream, animationPair.second);
	outStream.WriteBits(cooldown);
}

bool AbilityData::Read(InMemBitStream& inStream)
{
	if(!typeList.GetPropListRef().Read(inStream)) return false;
	SetAbilityToTypeChosen();
	if(!abilityProperties.pricedPropertiesList.GetPropListRef().Read(inStream)) return false;
	for(auto& animationPair : abilityProperties.animationList)
		if(!Frame::ReadAnimation(inStream, animationPair.second)) return false;
	if(!inStream.ReadBits(cooldown)) return false;
	return true;
}

double AbilityData::GetCost() const { return (abilityProperties.pricedPropertiesList.GetCost() + abilityProperties.typePrice) / cooldown; }
double AbilityData::GetCooldown() const { return cooldown; }
void AbilityData::SetCooldown(double cooldown) { this->cooldown = cooldown; }

void AbilityData::AddAbility(const UTF8String& abilityName, uint32_t price)
{
	nameToProperties.emplace_back(abilityName, AbilityProperties(abilityName));
	nameToProperties.back().second.typePrice = price;
}

void AbilityData::AddAnimationToAbility(const UTF8String& animationName)
{
	nameToProperties.back().second.animationList.emplace_back(animationName, std::vector<Frame>());
}

void AbilityData::AddBoolToAbility(const UTF8String& propName, uint32_t priceIfTrue)
{
	nameToProperties.back().second.pricedPropertiesList.AddBool(propName, priceIfTrue);
}

void AbilityData::AddIntToAbility(const UTF8String& propName, uint32_t increment, uint32_t maxVal, uint32_t pricePerIncrement)
{
	nameToProperties.back().second.pricedPropertiesList.AddInt(propName, increment, maxVal, pricePerIncrement);
}

void AbilityData::AddChoiceToAbility(const UTF8String& propName, const PricedChoiceList& choiceAndPriceVector)
{
	nameToProperties.back().second.pricedPropertiesList.AddChoice(propName, choiceAndPriceVector);
}

void AbilityData::AddListToAbility(const UTF8String& propName, const PricedPropertiesList& pricedProperties)
{
	nameToProperties.back().second.pricedPropertiesList.AddList(propName, pricedProperties);
}
