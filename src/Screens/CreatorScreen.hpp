#pragma once
#include "../ScreenNavigator.hpp"

class CreatorScreen : public Screen
{
	public:
		CreatorScreen(const std::shared_ptr<Screen>& initHaltedScreen);
		bool Update();
		void Draw();
	private:
		enum CreatorState {
			CREATOR_INIT,
			CREATOR_UPDATE
		} state;
		std::shared_ptr<Screen> haltedScreen;
};
