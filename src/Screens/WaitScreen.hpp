#pragma once
#include "../ScreenNavigator.hpp"
#include <GSEGL/Graphics/Math.hpp>
#include <vector>

class LoadingScreenBouncingBox
{
	public:
		LoadingScreenBouncingBox();
		bool Update(uint32_t* score);
		void Draw();
		std::pair<Vec2, Vec2> CalculateBoxElasticProperties(); // <pos, size>
	private:
		float color;
		double deathAnimationTime;
		Vec2 position;
		Vec2 velocity;
};

class WaitScreen : public Screen
{
	public:
		WaitScreen(const std::shared_ptr<Screen>& initHaltedScreen, bool (*initWaitUpdateLoop)(const std::shared_ptr<void>&), const std::shared_ptr<void>& initUpdateData);
		bool Update();
		void Draw();
		void SetToInit(); // Public play workaround
	private:
		enum {
			WAIT_INIT,
			WAIT_UPDATE
		} state;
		uint32_t score;
		double scoreCounterAnimationTime;
		double cursorShotAnimationTime;
		Vec2 cursorShotAnimationPos;
		double timeLastSpawned;
		bool (*waitUpdateLoop)(const std::shared_ptr<void>&);
		std::vector<LoadingScreenBouncingBox> bouncingBoxes;
		std::shared_ptr<void> updateData;
		std::shared_ptr<Screen> haltedScreen;
};
