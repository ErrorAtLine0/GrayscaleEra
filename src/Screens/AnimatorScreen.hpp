#pragma once
#include "../ScreenNavigator.hpp"
#include "../Frame.hpp"
#include "../BlockData.hpp"
#include <vector>

class AnimatorScreen : public Screen
{
	public:
		AnimatorScreen(const std::shared_ptr<Screen>& initHaltedScreen, uint32_t initMaxFrames, std::vector<Frame>* initFramesPtr, const UVec2& initImageDimensions, uint32_t initMinDarkPixelCount = 0, PropertiesList* initPropList = nullptr);
		bool Update();
		void Draw();
	private:
		enum AnimatorState
		{
			ANIMATOR_INIT,
			ANIMATOR_UPDATELOOP
		} state;
		void UpdateFrameCanvas();
		void UpdateFinalCanvas();
		uint32_t maxFrames;
		uint32_t currentFrame;
		
		uint32_t currentFrameInAnimation;
		double timeSinceLastFrame;
		
		std::vector<Frame>* framesPtr;
		UVec2 imageDimensions;
		uint32_t minDarkPixelCount;
		PropertiesList* propList;
		std::shared_ptr<Screen> haltedScreen;
};
