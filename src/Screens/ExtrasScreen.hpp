#pragma once
#include "../ScreenNavigator.hpp"

class ExtrasScreen : public Screen
{
	public:
		ExtrasScreen(const std::shared_ptr<Screen>& initHaltedScreen);
		bool Update();
		void Draw();
	private:
		enum ExtrasState
		{
			EXTRAS_INIT,
			EXTRAS_UPDATE
		} state;
		std::shared_ptr<Screen> haltedScreen;
};
 
