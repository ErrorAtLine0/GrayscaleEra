#include "LevelMakerScreen.hpp"
#include "SelectorScreen.hpp"
#include "AnimatorScreen.hpp"
#include "PropertiesScreen.hpp"
#include "PaintScreen.hpp"
#include "GameScreen.hpp"
#include <GSEGL/Graphics/GUI/GUIImageButton.hpp>
#include <GSEGL/Graphics/DrawUtil.hpp>
#include <GSEGL/Graphics/ControllerUtil.hpp>
#include <GSEGL/TimeUtil.hpp>
#include <GSEGL/FileUtil.hpp>
#include <GSEGL/LogUtil.hpp>
#include <GSEGL/Graphics/GUI/GUIImmutableText.hpp>
#include <GSEGL/Graphics/GUI/GUISlider.hpp>
#include <cstring>
#include <algorithm>

#define CAMERASPEED 500.0f
#define BRUSHSIZEMAX 20

LevelMakerScreen::LevelMakerScreen(const std::shared_ptr<Screen>& initHaltedScreen, LevelData* initLevelData, const WorldDataPtr& initWorldData):
state(LEVELMAKER_INIT),
worldData(initWorldData),
adventureData(worldData->GetType() == WORLD_ADVENTURE ? std::static_pointer_cast<AdventureData>(worldData) : nullptr),
arenaData(worldData->GetType() == WORLD_ARENA ? std::static_pointer_cast<ArenaData>(worldData) : nullptr),
levelData(initLevelData),
blocks(adventureData ? &adventureData->blocks : &arenaData->blocks),
points(adventureData ? &adventureData->points : nullptr),
blockSize(40.0f),
brushSize(1),
selectedBlock(0),
blockAtTopOfMenu(0),
selectedPoint(0),
pointAtTopOfMenu(0),
editMode(EDITMODE_FOREGROUND),
firstRulerPointSelected(false),
haltedScreen(initHaltedScreen)
{}

bool LevelMakerScreen::Update()
{
	float GUISize = GUIUtil::Get().GetGUISize();
	switch(state)
	{
		case LEVELMAKER_INIT:
		{
			if(!testPlayerChosen.IsEmpty())
			{
				if(InitiateTestWorld())
				{
					testPlayerChosen.Clear();
					return true;
				}
				else
					testPlayerChosen.Clear();
			}
			GUIUtil::Get().ClearAll();
			GUIUtil::Get().AddElement("Add", new GUIImageButton("AddIcon", Vec2(1.0f), Vec2(-40.0f), Vec2(0.0f), Vec2(40.0f)));
			GUIUtil::Get().AddElement("EditImage", new GUIImageButton("PencilIcon", Vec2(0.0f, 1.0f), Vec2(40.0f, -80.0f), Vec2(0.0f), Vec2(40.0f)));
			GUIUtil::Get().AddElement("EditProperties", new GUIImageButton("SettingsIcon", Vec2(0.0f, 1.0f), Vec2(80.0f, -80.0f), Vec2(0.0f), Vec2(40.0f)));
			GUIUtil::Get().AddElement("Remove", new GUIImageButton("RemoveIcon", Vec2(0.0f, 1.0f), Vec2(120.0f, -80.0f), Vec2(0.0f), Vec2(40.0f)));
			GUIUtil::Get().AddElement("Back", new GUIImageButton("BackIcon", Vec2(0.0f), Vec2(0.0f, 0.0f), Vec2(0.0f), Vec2(40.0f)));
			GUIUtil::Get().AddElement("ModeSwitch", new GUIImageButton("FIcon", Vec2(0.0f), Vec2(0.0f, 40.0f), Vec2(0.0f), Vec2(40.0f)));
			GUIUtil::Get().AddElement("BlockInfoTxt", new GUIImmutableText("Block Info", Vec2(0.0f, 1.0f), Vec2(0.0f, -35.0f), 0.0f, 20.0f));
			GUIUtil::Get().AddElement("PointInfoTxt", new GUIImmutableText("Point Info", Vec2(0.0f, 1.0f), Vec2(0.0f, -35.0f), 0.0f, 20.0f));
			GUIUtil::Get().AddElement("SpawnInfoTxt", new GUIImmutableText("Spawn", Vec2(0.0f, 1.0f), Vec2(0.0f, -35.0f), 0.0f, 20.0f));
			GUIUtil::Get().AddElement("ZoomImage", new GUIImageButton("ZoomIcon", Vec2(0.0f, 1.0f), Vec2(0.0f, -120.0f), Vec2(0.0f), Vec2(40.0f)));
			GUIUtil::Get().AddElement("ZoomSlider", new GUISlider(Vec2(0.0f, 1.0f), Vec2(45.0f, -110.0f), Vec2(0.0f), Vec2(115.0f, 20.0f), 40, 10, 120, 1));
			GUIUtil::Get().AddElement("TestWorld", new GUIImageButton("ArrowButton", Vec2(0.0f), Vec2(0.0f, 80.0f), Vec2(0.0f), Vec2(40.0f)));
			GUIUtil::Get().AddElement("EditArenaImage", new GUIImageButton("PencilIcon", Vec2(0.0f), Vec2(0.0f, 120.0f), Vec2(0.0f), Vec2(40.0f)));
			if(!newBlockFileName.IsEmpty()) // Block created
			{
				blocks->emplace_back();
				std::shared_ptr<InMemBitStream> inStream = FileUtil::Get().ReadFromFile(UTF8String("blocks/") + newBlockFileName);
				blocks->back().ReadFile(*inStream);
				newBlockFileName.Clear();
			}
			
			UpdateGUI();
			ReadLevelTextures();
			levelAreaLeftClickFocus = false;
			state = LEVELMAKER_UPDATELOOP;
			break;
		}
		case LEVELMAKER_UPDATELOOP:
		{
			if(GUIUtil::Get().GrabState("Add") && static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Add"))->buttonState == GUIBUTTON_PRESSED)
			{
				if(editMode == EDITMODE_BACKGROUND || editMode == EDITMODE_FOREGROUND)
				{
					if(blocks->size() < (adventureData ? ADVENTUREBLOCKMAX : ARENABLOCKMAX))
					{
						ScreenNavigator::Get().SetScreen(std::make_shared<SelectorScreen>(ScreenNavigator::Get().GetScreen(), &newBlockFileName, FILETYPE_BLOCK));
						state = LEVELMAKER_INIT;
						break;
					}
					else
						LogUtil::Get().LogMessage("NORMAL", UTF8String("Cannot create more than ") + UTF8String::IntToStr(adventureData ? ADVENTUREBLOCKMAX : ARENABLOCKMAX) + " blocks");
				}
				else if(editMode == EDITMODE_POINT)
				{
					if(points->size() < ADVENTUREPOINTMAX)
					{
						points->emplace_back();
						selectedPoint = points->size();
					}
					else
						LogUtil::Get().LogMessage("NORMAL", UTF8String("Cannot create more than ") + UTF8String::IntToStr(ADVENTUREPOINTMAX) + " points");
				}
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Remove"))->buttonState == GUIBUTTON_PRESSED)
			{
				switch(editMode)
				{
					case EDITMODE_FOREGROUND:
					case EDITMODE_BACKGROUND:
					{
						if(selectedBlock != 0)
						{
							blockTextures.erase(blockTextures.begin() + (selectedBlock - 1));
							if(adventureData)
								adventureData->RemoveBlock(selectedBlock);
							else
								arenaData->RemoveBlock(selectedBlock);
							selectedBlock = 0;
						}
						break;
					}
					case EDITMODE_SPAWN: break;
					case EDITMODE_POINT:
					{
						if(selectedPoint != 0)
						{
							if(adventureData)
							{
								adventureData->RemovePoint(selectedPoint);
								selectedPoint = 0;
							}
						}
						break;
					}
				}
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("EditImage"))->buttonState == GUIBUTTON_PRESSED)
			{
				if(selectedBlock != 0)
				{
					ScreenNavigator::Get().SetScreen(std::make_shared<AnimatorScreen>(ScreenNavigator::Get().GetScreen(), BLOCK_MAX_FRAMES, &(*blocks)[selectedBlock-1].animation, UVec2(BLOCK_TEXTURE_SIZE), 0, &(*blocks)[selectedBlock-1].propList));
					state = LEVELMAKER_INIT;
					break;
				}
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("EditProperties"))->buttonState == GUIBUTTON_PRESSED)
			{
				if(editMode == EDITMODE_POINT)
				{
					if(selectedPoint != 0)
					{
						ScreenNavigator::Get().SetScreen(std::make_shared<PropertiesScreen>(ScreenNavigator::Get().GetScreen(), &(*points)[selectedPoint-1].propList, false, adventureData));
						state = LEVELMAKER_INIT;
						break;
					}
				}
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Back"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(haltedScreen);
				return true;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("ModeSwitch"))->buttonState == GUIBUTTON_PRESSED)
			{
				switch(editMode)
				{
					case EDITMODE_FOREGROUND: { editMode = EDITMODE_BACKGROUND; break; }
					case EDITMODE_BACKGROUND: { editMode = adventureData ? EDITMODE_POINT : EDITMODE_SPAWN; break; }
					case EDITMODE_SPAWN:      { editMode = EDITMODE_FOREGROUND; break; }
					case EDITMODE_POINT:      { editMode = EDITMODE_FOREGROUND; break; }
				}
				UpdateGUI();
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("EditArenaImage"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<PaintScreen>(ScreenNavigator::Get().GetScreen(), arenaData->image.GetMutableDataPtr(), UVec2(ARENA_TEXTURE_SIZE)));
				state = LEVELMAKER_INIT;
				return true;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("TestWorld"))->buttonState == GUIBUTTON_PRESSED)
			{
				bool needsPlayerSelected = !adventureData || adventureData->settings.GetNPC(3) == 0;
				if(needsPlayerSelected)
				{
					ScreenNavigator::Get().SetScreen(std::make_shared<SelectorScreen>(ScreenNavigator::Get().GetScreen(), &testPlayerChosen, FILETYPE_PLAYER));
					state = LEVELMAKER_INIT;
					return true;
				}
				else
				{
					InitiateTestWorld();
					state = LEVELMAKER_INIT;
					return true;
				}
			}
			
			if(CursorInLevelArea())
			{
				if(ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY1) != KEYSTATE_RELEASED)
				{
					if(ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY1) == KEYSTATE_PRESSED)
						levelAreaLeftClickFocus = true;
					if(levelAreaLeftClickFocus)
					{
						switch(editMode)
						{
							case EDITMODE_FOREGROUND: { FillAreaNearCursor(levelData->level, brushSize, brushSize, selectedBlock); break; }
							case EDITMODE_BACKGROUND: { FillAreaNearCursor(levelData->levelBackground, brushSize, brushSize, selectedBlock); break; }
							case EDITMODE_SPAWN: { FillAreaNearCursor(levelData->points, brushSize, brushSize, 1); break; }
							case EDITMODE_POINT: { FillAreaNearCursor(levelData->points, brushSize, brushSize, selectedPoint); break; }
						}
					}
				}
				else
					levelAreaLeftClickFocus = false;
				if(ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY2) != KEYSTATE_RELEASED)
				{
					UVec2 blockAimedAt = BlockAimedAt();
					if((blockAimedAt.x >= levelData->levelSize || blockAimedAt.y >= levelData->levelSize) || (blockAimedAt.x < 0 || blockAimedAt.y < 0))
					{
						if(ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY2) == KEYSTATE_PRESSED)
							LogUtil::Get().LogMessage("NORMAL", "Cursor out of bounds");
					}
					else
					{
						switch(editMode)
						{
							case EDITMODE_FOREGROUND: { selectedBlock = levelData->BlockAt(blockAimedAt.x, blockAimedAt.y); break; }
							case EDITMODE_BACKGROUND: { selectedBlock = levelData->BackBlockAt(blockAimedAt.x, blockAimedAt.y); break; }
							case EDITMODE_SPAWN: break;
							case EDITMODE_POINT: { selectedPoint = levelData->PointAt(blockAimedAt.x, blockAimedAt.y); break; }
						}
					}
				}
				if(ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY3) != KEYSTATE_RELEASED)
				{
					switch(editMode)
					{
						case EDITMODE_FOREGROUND: { FillAreaNearCursor(levelData->level, brushSize, brushSize, 0); break; }
						case EDITMODE_BACKGROUND: { FillAreaNearCursor(levelData->levelBackground, brushSize, brushSize, 0); break; }
						case EDITMODE_SPAWN: { FillAreaNearCursor(levelData->points, brushSize, brushSize, 0); break; }
						case EDITMODE_POINT: { FillAreaNearCursor(levelData->points, brushSize, brushSize, 0); break; }
					}
				}
				if(ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY4) == KEYSTATE_PRESSED)
				{
					UVec2 blockAimedAt = BlockAimedAt();
					if((blockAimedAt.x >= levelData->levelSize || blockAimedAt.y >= levelData->levelSize) || (blockAimedAt.x < 0 || blockAimedAt.y < 0))
					{
						LogUtil::Get().LogMessage("NORMAL", "Cursor out of bounds");
					}
					else
					{
						if(firstRulerPointSelected)
						{
							UTF8String distanceXStr = UTF8String::IntToStr(blockAimedAt.x - firstRulerPoint.x);
							UTF8String distanceYStr = UTF8String::IntToStr(blockAimedAt.y - firstRulerPoint.y);
							LogUtil::Get().LogMessage("NORMAL", UTF8String("Ruler Distance X: ") + distanceXStr);
							LogUtil::Get().LogMessage("NORMAL", UTF8String("Ruler Distance Y: ") + distanceYStr);
							firstRulerPointSelected = false;
						}
						else
						{
							firstRulerPoint = blockAimedAt;
							firstRulerPointSelected = true;
						}
					}
				}
				if(ControllerUtil::Get().GetScrollOffset() < 0)
				{
					--brushSize;
					if(brushSize == 0)
						brushSize = 1;
				}
				if(ControllerUtil::Get().GetScrollOffset() > 0)
				{
					++brushSize;
					if(brushSize > BRUSHSIZEMAX)
						brushSize = BRUSHSIZEMAX;
				}
			}
			else
				levelAreaLeftClickFocus = false;
			
			UpdateZoom();
			if(ControllerUtil::Get().GetButtonState(CONTROLLER_UP) != KEYSTATE_RELEASED)
				cameraPos.y += TimeUtil::Get().GetDeltaTime() * CAMERASPEED;
			if(ControllerUtil::Get().GetButtonState(CONTROLLER_DOWN) != KEYSTATE_RELEASED)
				cameraPos.y -= TimeUtil::Get().GetDeltaTime() * CAMERASPEED;
			if(ControllerUtil::Get().GetButtonState(CONTROLLER_LEFT) != KEYSTATE_RELEASED)
				cameraPos.x -= TimeUtil::Get().GetDeltaTime() * CAMERASPEED;
			if(ControllerUtil::Get().GetButtonState(CONTROLLER_RIGHT) != KEYSTATE_RELEASED)
				cameraPos.x += TimeUtil::Get().GetDeltaTime() * CAMERASPEED;
			if(cameraPos.x < -800.0f) cameraPos.x = -800.0f;
			if(cameraPos.y < -800.0f) cameraPos.y = -800.0f;
			if(cameraPos.x > levelData->levelSize * blockSize) cameraPos.x = levelData->levelSize * blockSize;
			if(cameraPos.y > levelData->levelSize * blockSize + 200.0f) cameraPos.y = levelData->levelSize * blockSize + 200.0f;
			else if(CursorInBlockSelectionArea())
			{
				float blockSizeInGUI = GUISize * 40.0f;
				int32_t maxItemCountInSelector = (WindowUtil::Get().GetWindowSize().y / blockSizeInGUI) - 1;
				switch(editMode)
				{
					case EDITMODE_FOREGROUND:
					case EDITMODE_BACKGROUND:
					{
						if(ControllerUtil::Get().GetScrollOffset() > 0 && blockAtTopOfMenu > 0)
							--blockAtTopOfMenu;
						if(ControllerUtil::Get().GetScrollOffset() < 0 && (static_cast<int32_t>(blocks->size()) > maxItemCountInSelector && blockAtTopOfMenu < blocks->size()-maxItemCountInSelector))
							++blockAtTopOfMenu;
						if(ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY1) == KEYSTATE_PRESSED)
						{
							uint16_t blockChosen = GetBlockSelectionHoveredOver(blockAtTopOfMenu);
							if(blockChosen <= blocks->size())
								selectedBlock = blockChosen;
						}
						break;
					}
					case EDITMODE_SPAWN: break;
					case EDITMODE_POINT:
					{
						if(ControllerUtil::Get().GetScrollOffset() > 0 && pointAtTopOfMenu > 0)
							--pointAtTopOfMenu;
						if(ControllerUtil::Get().GetScrollOffset() < 0 && (static_cast<int32_t>(points->size()) > maxItemCountInSelector && pointAtTopOfMenu < points->size()-maxItemCountInSelector))
							++pointAtTopOfMenu;
						if(ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY1) == KEYSTATE_PRESSED)
						{
							uint16_t pointChosen = GetBlockSelectionHoveredOver(pointAtTopOfMenu);
							if(pointChosen <= points->size())
								selectedPoint = pointChosen;
						}
						break;
					}
				}
			}
			break;
		}
	}
	return true;
}

void LevelMakerScreen::UpdateZoom()
{
	float newBlockSize = static_cast<GUISliderState*>(GUIUtil::Get().GrabState("ZoomSlider"))->val;
	if(newBlockSize != blockSize)
	{
		float scaleFactor = 1.0f / blockSize * newBlockSize;
		cameraPos.x = cameraPos.x * scaleFactor;
		cameraPos.y = cameraPos.y * scaleFactor;
		
		// Center zoomed camera
		Vec2 cameraSize = WindowUtil::Get().GetWindowSize();
		Vec2 blocksShowingOld = cameraSize / blockSize;
		Vec2 blocksShowingNew = cameraSize / newBlockSize;
		cameraPos.x += (((blocksShowingOld.x - blocksShowingNew.x)) / 2.0f * newBlockSize);
		cameraPos.y += (((blocksShowingOld.y - blocksShowingNew.y)) / 2.0f * newBlockSize);
		
		blockSize = newBlockSize;
	}
}

void LevelMakerScreen::UpdateGUI()
{
	switch(editMode)
	{
		case EDITMODE_FOREGROUND:
		case EDITMODE_BACKGROUND:
		{
			GUIUtil::Get().EnableElement("BlockInfoTxt");
			GUIUtil::Get().DisableElement("PointInfoTxt");
			GUIUtil::Get().DisableElement("NPCInfoTxt");
			GUIUtil::Get().DisableElement("SpawnInfoTxt");
			GUIUtil::Get().EnableElement("Add");
			GUIUtil::Get().EnableElement("EditImage");
			GUIUtil::Get().DisableElement("EditProperties");
			GUIUtil::Get().EnableElement("Remove");
			break;
		}
		case EDITMODE_SPAWN:
		{
			GUIUtil::Get().DisableElement("BlockInfoTxt");
			GUIUtil::Get().DisableElement("PointInfoTxt");
			GUIUtil::Get().DisableElement("NPCInfoTxt");
			GUIUtil::Get().EnableElement("SpawnInfoTxt");
			GUIUtil::Get().DisableElement("Add");
			GUIUtil::Get().DisableElement("EditImage");
			GUIUtil::Get().DisableElement("EditProperties");
			GUIUtil::Get().DisableElement("Remove");
			break;
		}
		case EDITMODE_POINT:
		{
			GUIUtil::Get().DisableElement("BlockInfoTxt");
			GUIUtil::Get().EnableElement("PointInfoTxt");
			GUIUtil::Get().DisableElement("NPCInfoTxt");
			GUIUtil::Get().DisableElement("SpawnInfoTxt");
			GUIUtil::Get().EnableElement("Add");
			GUIUtil::Get().DisableElement("EditImage");
			GUIUtil::Get().EnableElement("EditProperties");
			GUIUtil::Get().EnableElement("Remove");
			break;
		}
	}
	// Update mode switch button
	GUIUtil::Get().RemoveElement("ModeSwitch");
	UTF8String modeIconStr;
	switch(editMode)
	{
		case EDITMODE_FOREGROUND: { modeIconStr = "FIcon"; break; }
		case EDITMODE_BACKGROUND: { modeIconStr = "BIcon"; break; }
		case EDITMODE_SPAWN:      { modeIconStr = "SIcon"; break; }
		case EDITMODE_POINT:      { modeIconStr = "PIcon"; break; }
	}
	GUIUtil::Get().AddElement("ModeSwitch", new GUIImageButton(modeIconStr, Vec2(0.0f), Vec2(0.0f, 40.0f), Vec2(0.0f), Vec2(40.0f)));
	// Check if arena, since arenas can have their images edited from the level maker
	if(arenaData)
		GUIUtil::Get().EnableElement("EditArenaImage");
	else
		GUIUtil::Get().DisableElement("EditArenaImage");
}

bool LevelMakerScreen::InitiateTestWorld()
{
	bool needsPlayerSelected = !adventureData || adventureData->settings.GetNPC(3) == 0;
	GameSandboxProperties gameProperties;
	gameProperties.gameTime = adventureData ? adventureData->settings.GetInt(1) : 0;
	gameProperties.respawnTime = adventureData ? adventureData->settings.GetInt(2) : ARENA_RESPAWN_TIME;
	ScreenNavigator::Get().SetScreen(std::make_shared<GameScreen>(ScreenNavigator::Get().GetScreen(), gameProperties, worldData, 1, nullptr));
	if(needsPlayerSelected)
	{
		PlayerDataPtr playerData = PlayerData::ReadPlayerFile(testPlayerChosen);
		if(playerData)
			GameSandbox::Get().AddPlayer("", playerData, 1);
		else
		{
			LogUtil::Get().LogMessage("NORMAL", "Could not read player file");
			return false;
		}
	}
	else
		GameSandbox::Get().AddPlayer("", adventureData->settings.GetNPC(3), 1);
	return true;
}

IVec2 LevelMakerScreen::BlockAimedAt()
{
	return IVec2(
		(cameraPos.x + ControllerUtil::Get().GetCursorPos().x) / blockSize,
		(cameraPos.y + ControllerUtil::Get().GetCursorPos().y) / blockSize);
}

bool LevelMakerScreen::CursorInLevelArea()
{
	Vec2 winSize = WindowUtil::Get().GetWindowSize();
	Vec2 cursorPos = ControllerUtil::Get().GetCursorPos();
	float GUISize = GUIUtil::Get().GetGUISize();
	if(cursorPos.x < winSize.x - (40.0f * GUISize) && cursorPos.x > 0.0f)
		if(cursorPos.y < winSize.y && cursorPos.y > 0.0f)
			if(!(cursorPos.x < (160.0f * GUISize) && cursorPos.y > winSize.y - (120.0f * GUISize))) // Dont collide with info box
				if(!(cursorPos.x < (40.0f * GUISize) && cursorPos.y < (160.0f * GUISize))) // Don't collide with bottom left toolbar
					return true;
	return false;
}

bool LevelMakerScreen::CursorInBlockSelectionArea()
{
	Vec2 winSize = WindowUtil::Get().GetWindowSize();
	Vec2 cursorPos = ControllerUtil::Get().GetCursorPos();
	float GUISize = GUIUtil::Get().GetGUISize();
	if(cursorPos.x < winSize.x && cursorPos.x > winSize.x - (40.0f * GUISize))
		if(cursorPos.y < winSize.y - (40.0f * GUISize) && cursorPos.y > 0.0f)
			return true;
	return false;
}

void LevelMakerScreen::ReadLevelTextures()
{
	blockTextures.clear();
	for(BlockData& block : *blocks)
		blockTextures.emplace_back(std::make_shared<Texture>(block.animation[0].GetData(), BLOCK_TEXTURE_SIZE, BLOCK_TEXTURE_SIZE));
}

void LevelMakerScreen::FillAreaNearCursor(uint16_t* arrayToFill, int32_t areaX, int32_t areaY, uint16_t val)
{
	IVec2 cursorBlockPos = BlockAimedAt();
	cursorBlockPos.x -= areaX / 2;
	cursorBlockPos.y -= areaY / 2;
	for(int32_t i = 0; i < areaY; i++)
	{
		int32_t blockPosY = cursorBlockPos.y + i;
		if(blockPosY >= 0 && blockPosY < static_cast<int32_t>(levelData->levelSize))
			for(int32_t j = 0; j < areaX; j++)
			{
				int32_t blockPosX = cursorBlockPos.x + j;
				if(blockPosX >= 0 && blockPosX < static_cast<int32_t>(levelData->levelSize))
					arrayToFill[levelData->levelSize * blockPosY + blockPosX] = val;
			}
	}
}

uint16_t LevelMakerScreen::GetBlockSelectionHoveredOver(uint16_t blockAtTopOfMenu)
{
	if(CursorInBlockSelectionArea())
	{
		float blockSizeInGUI = GUIUtil::Get().GetGUISize() * 40.0f;
		return 1 + blockAtTopOfMenu + (WindowUtil::Get().GetWindowSize().y - blockSizeInGUI - ControllerUtil::Get().GetCursorPos().y) / blockSizeInGUI;
	}
	else
		return 0;
}

void LevelMakerScreen::Draw()
{
	if(state == LEVELMAKER_UPDATELOOP)
	{
		Vec2 winSize = WindowUtil::Get().GetWindowSize();
		float GUISize = GUIUtil::Get().GetGUISize();
		DrawUtil::Get().DrawRectangle(14, Vec2(-1000.0f) - cameraPos, Vec2(levelData->levelSize * blockSize + 2000.0f), 0.8f);
		DrawUtil::Get().DrawRectangle(14, Vec2(0.0f) - cameraPos, Vec2(levelData->levelSize * blockSize), 1.0f);
		
		BlockID blockHoveringOver = GetBlockSelectionHoveredOver(blockAtTopOfMenu);
		PointID pointHoveringOver = GetBlockSelectionHoveredOver(pointAtTopOfMenu);
		
		uint32_t cameraPosX = cameraPos.x >= blockSize ? (cameraPos.x / blockSize) - 1 : 0;
		uint32_t cameraPosY = cameraPos.y >= blockSize ? (cameraPos.y / blockSize) - 1 : 0;
		uint32_t cameraSizeX = blockSize * winSize.x;
		uint32_t cameraSizeY = blockSize * winSize.y;
		uint32_t cameraEndX = cameraSizeX > levelData->levelSize ? levelData->levelSize : cameraSizeX;
		uint32_t cameraEndY = cameraSizeY > levelData->levelSize ? levelData->levelSize : cameraSizeY;
		
		for(uint32_t i = cameraPosY; i < cameraEndY; i++)
			for(uint32_t j = cameraPosX; j < cameraEndX; j++)
			{
				if(levelData->BackBlockAt(j, i) != 0)
				{
					float alphaVal = 0.0f;
					if(editMode == EDITMODE_BACKGROUND)
					{
						if(blockHoveringOver == 0 || blockHoveringOver == levelData->BackBlockAt(j, i))
							alphaVal = 1.0f;
						else
							alphaVal = 0.5f;
					}
					else
						alphaVal = 0.25f;
					Vec2 finalPos(j * blockSize - cameraPos.x, i * blockSize - cameraPos.y);
					DrawUtil::Get().DrawSprite(13, blockTextures[levelData->BackBlockAt(j, i)-1], finalPos, Vec2(blockSize), 0.0f, NORMAL_TRANSPARENCY, alphaVal);
				}
			}
		
		for(uint32_t i = cameraPosY; i < cameraEndY; i++)
			for(uint32_t j = cameraPosX; j < cameraEndX; j++)
			{
				if(levelData->BlockAt(j, i) != 0)
				{
					float alphaVal = 0.0f;
					if(editMode == EDITMODE_FOREGROUND)
					{
						if(blockHoveringOver == 0 || blockHoveringOver == levelData->BlockAt(j, i))
							alphaVal = 1.0f;
						else
							alphaVal = 0.5f;
					}
					else
						alphaVal = 0.25f;
					Vec2 finalPos(j * blockSize - cameraPos.x, i * blockSize - cameraPos.y);
					DrawUtil::Get().DrawSprite(13, blockTextures[levelData->BlockAt(j, i)-1], finalPos, Vec2(blockSize), 0.0f, NORMAL_TRANSPARENCY, alphaVal);
				}
			}
		for(uint32_t i = cameraPosY; i < cameraEndY; i++)
			for(uint32_t j = cameraPosX; j < cameraEndX; j++)
			{
				if(levelData->PointAt(j, i) != 0)
				{
					float alphaVal = 0.0f;
					if(editMode == EDITMODE_POINT || editMode == EDITMODE_SPAWN)
					{
						if(pointHoveringOver == 0 || pointHoveringOver == levelData->PointAt(j, i))
							alphaVal = 1.0f;
						else
							alphaVal = 0.5f;
					}
					else
						alphaVal = 0.25f;
					Vec2 finalPos(j * blockSize - cameraPos.x, i * blockSize - cameraPos.y);
					DrawUtil::Get().DrawSprite(13, adventureData ? "PIcon" : "SIcon", finalPos, Vec2(blockSize), 0.0f, NORMAL_TRANSPARENCY, alphaVal);
				}
			}
		float blockSizeInGUI = GUISize * 40.0f;
		if(firstRulerPointSelected)
		{
			Vec2 finalPos(firstRulerPoint.x * blockSize - cameraPos.x, firstRulerPoint.y * blockSize - cameraPos.y);
			DrawUtil::Get().DrawSprite(13, "Cursor",  finalPos, Vec2(blockSize), 0.0f, WHITE_IS_TRANSPARENT, 1.0f);
		}
		DrawUtil::Get().DrawRectangle(12, Vec2(0.0f, winSize.y - (120.0f * GUISize)), Vec2(160.0f, 120.0f) * GUISize, 1.0f);
		switch(editMode)
		{
			case EDITMODE_FOREGROUND:
			case EDITMODE_BACKGROUND:
			{
				if(selectedBlock != 0)
					DrawUtil::Get().DrawSprite(11, blockTextures[selectedBlock-1], Vec2(0.0f, winSize.y - (80.0f * GUISize)), Vec2(blockSizeInGUI), 0.0f, NORMAL_TRANSPARENCY);
				uint32_t count = winSize.y / blockSizeInGUI;
				for(BlockID i = 0; i < count && i + blockAtTopOfMenu < blockTextures.size(); i++)
					DrawUtil::Get().DrawSprite(11, blockTextures[i + blockAtTopOfMenu], Vec2(winSize.x - blockSizeInGUI, winSize.y - (80.0f * GUISize) - (i * 40.0f * GUISize)), Vec2(blockSizeInGUI), 0.0f, NORMAL_TRANSPARENCY);
				break;
			}
			case EDITMODE_SPAWN: break;
			case EDITMODE_POINT:
			{
				if(selectedPoint != 0)
					DrawUtil::Get().DrawSprite(11, "PIcon", Vec2(0.0f, winSize.y - (80.0f * GUISize)), Vec2(blockSizeInGUI), 0.0f, NORMAL_TRANSPARENCY);
				uint32_t count = winSize.y / blockSizeInGUI;
				bool viewPointTags = CursorInBlockSelectionArea();
				for(PointID i = 0; i < count && i + pointAtTopOfMenu < points->size(); i++)
				{
					Vec2 pointPosInSelectionMenu = Vec2(winSize.x - blockSizeInGUI, winSize.y - (80.0f * GUISize) - (i * 40.0f * GUISize));
					DrawUtil::Get().DrawSprite(11, "PIcon", pointPosInSelectionMenu, Vec2(blockSizeInGUI), 0.0f, NORMAL_TRANSPARENCY);
					if(viewPointTags)
					{
						const UTF8String& pointTag = (*points)[i + pointAtTopOfMenu].propList.GetString(0);
						DrawUtil::Get().DrawText(11, pointTag.IsEmpty() ? "NO TAG" : pointTag, Vec2(pointPosInSelectionMenu.x - DrawUtil::Get().CalculateTextWidth(pointTag.IsEmpty() ? "NO TAG" : pointTag, GUISize * 20.0f) - 5.0f, pointPosInSelectionMenu.y + GUISize * 10.0f), GUISize * 20.0f, 0.0f, 0.7f);
					}
				}
				break;
			}
		}
	}
}
