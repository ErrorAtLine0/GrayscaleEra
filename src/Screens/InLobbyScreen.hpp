#pragma once
#include "../ScreenNavigator.hpp"
#include "../NetworkManager.hpp"

class InLobbyScreen : public Screen
{
	public:
		InLobbyScreen(const std::shared_ptr<Screen>& initHaltedScreen, std::shared_ptr<NetworkManager> initNetMan);
		~InLobbyScreen();
		bool Update();
		void Draw();
	private:
		enum InLobbyState
		{
			INLOBBY_READ_PLAYER_INIT,
			INLOBBY_INIT,
			INLOBBY_UPDATE
		} state;
		UTF8String playerName;
		std::shared_ptr<NetworkManager> netMan;
		std::shared_ptr<Screen> haltedScreen;
};
