#include "EditNameScreen.hpp"
#include "../GlobalMenuData.hpp"
#include <GSEGL/Graphics/GUI/GUIUtil.hpp>
#include <GSEGL/Graphics/GUI/GUITextEntry.hpp>
#include <GSEGL/Graphics/GUI/GUITextButton.hpp>
#include <GSEGL/LogUtil.hpp>
#include <algorithm>

EditNameScreen::EditNameScreen(const std::shared_ptr<Screen>& initHaltedScreen, UTF8String* initNamePtr, const std::vector<UTF8String>& initSameName):
state(EDITNAME_INIT),
namePtr(initNamePtr),
sameName(initSameName),
haltedScreen(initHaltedScreen)
{}

bool EditNameScreen::Update()
{
	switch(state)
	{
		case EDITNAME_INIT:
		{
			GUIUtil::Get().ClearAll();
			GUIUtil::Get().AddElement("NameEntry", new GUITextEntry("Type Name Here", Vec2(0.5f), Vec2(-360.0f, 25.0f), Vec2(0.0f), Vec2(720.0f, 50.0f), 16));
			GUIUtil::Get().AddElement("Done", new GUITextButton("Done", Vec2(0.5f), Vec2(-360.0f, -100.0f), Vec2(0.0f), Vec2(720.0f, 50.0f)));
			static_cast<GUITextEntryState*>(GUIUtil::Get().GrabState("NameEntry"))->text = *namePtr;
			startName = *namePtr;
			state = EDITNAME_UPDATE;
			break;
		}
		case EDITNAME_UPDATE:
		{
			if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Done"))->buttonState == GUIBUTTON_PRESSED)
			{
				*namePtr = static_cast<GUITextEntryState*>(GUIUtil::Get().GrabState("NameEntry"))->text;
				if(namePtr->IsEmpty())
					LogUtil::Get().LogMessage("NORMAL", "Name cannot be empty");
				else if(*namePtr != startName && std::find(sameName.begin(), sameName.end(), *namePtr) != sameName.end())
					LogUtil::Get().LogMessage("NORMAL", "Name already taken");
				else
					ScreenNavigator::Get().SetScreen(haltedScreen);
			}
			break;
		}
	}
	return true;
}

void EditNameScreen::Draw()
{
	GlobalMenuData::Get().DrawBackground();
}
