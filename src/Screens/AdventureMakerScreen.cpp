#include "AdventureMakerScreen.hpp"
#include "../GlobalMenuData.hpp"
#include <GSEGL/FileUtil.hpp>
#include <GSEGL/Graphics/GUI/GUITextButton.hpp>
#include <GSEGL/Graphics/GUI/GUIImageButton.hpp>
#include "AdventureMakerScreen.hpp"
#include "EditNameScreen.hpp"
#include "PaintScreen.hpp"
#include "LevelMakerScreen.hpp"
#include "PropertiesScreen.hpp"
#include <GSEGL/LogUtil.hpp>

AdventureMakerScreen::AdventureMakerScreen(const std::shared_ptr<Screen>& initHaltedScreen, const AdventureDataPtr& initAdventureData):
state(ADVENTUREMAKER_INIT),
adventureData(initAdventureData),
haltedScreen(initHaltedScreen)
{}

bool AdventureMakerScreen::Update()
{
	switch(state)
	{
		case ADVENTUREMAKER_INIT:
		{
			GUIUtil::Get().ClearAll();
			GUIUtil::Get().AddElement("RoomSelector", new GUIScrollArea(Vec2(0.5f, 0.0f), Vec2(-400.0f, 120.0f), Vec2(0.0f, 1.0f), Vec2(800.0f, -120.0f), 20.0f, 100.0f));
			GUIUtil::Get().AddElement("AddRoom", new GUITextButton("Add Room", Vec2(0.5f, 0.0f), Vec2(-350.0f, 10.0f), Vec2(0.0f), Vec2(700.0f, 50.0f)));
			GUIUtil::Get().AddElement("EditImage", new GUIImageButton("PencilIcon", Vec2(1.0f, 0.0f), Vec2(-40.0f, 0.0f), Vec2(0.0f), Vec2(40.0f)));
			GUIUtil::Get().AddElement("EditSettings", new GUIImageButton("SettingsIcon", Vec2(1.0f, 0.0f), Vec2(-40.0f, 40.0f), Vec2(0.0f), Vec2(40.0f)));
			GUIUtil::Get().AddElement("Back", new GUIImageButton("BackIcon", Vec2(0.0f), Vec2(0.0f), Vec2(0.0f), Vec2(40.0f)));
			scrollState = static_cast<GUIScrollAreaState*>(GUIUtil::Get().GrabState("RoomSelector"));
			
			if(!newRoomName.IsEmpty())
			{
				adventureData->rooms.emplace_back(ADVENTURELEVELSIZE);
				adventureData->rooms.back().name = newRoomName;
				newRoomName = "";
			}
			for(LevelData& room : adventureData->rooms)
				RecordRoom(room.name);
			
			state = ADVENTUREMAKER_UPDATE;
			break;
		}
		case ADVENTUREMAKER_UPDATE:
		{
			if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("AddRoom"))->buttonState == GUIBUTTON_PRESSED)
			{
				if(adventureData->rooms.size() < 20)
				{
					ScreenNavigator::Get().SetScreen(std::make_shared<EditNameScreen>(ScreenNavigator::Get().GetScreen(), &newRoomName, GetRoomNames()));
					state = ADVENTUREMAKER_INIT;
				}
				else
					LogUtil::Get().LogMessage("NORMAL", "Cannot have more than 20 rooms");
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("EditImage"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<PaintScreen>(ScreenNavigator::Get().GetScreen(), adventureData->image.GetMutableDataPtr(), UVec2(ADVENTURE_TEXTURE_SIZE)));
				state = ADVENTUREMAKER_INIT;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("EditSettings"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<PropertiesScreen>(ScreenNavigator::Get().GetScreen(), &adventureData->settings, false, adventureData));
				state = ADVENTUREMAKER_INIT;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Back"))->buttonState == GUIBUTTON_PRESSED)
			{
				GUIUtil::Get().ClearAll();
				ScreenNavigator::Get().SetScreen(haltedScreen);
			}
			else
			{
				for(uint32_t i = 0; i < scrollState->elements.size(); i++)
				{
					if(static_cast<GUIButtonState*>(scrollState->elements[i]->GrabState())->buttonState == GUIBUTTON_PRESSED)
					{
						if(i % 3 == 0) // Pressed name
						{
							ScreenNavigator::Get().SetScreen(std::make_shared<LevelMakerScreen>(ScreenNavigator::Get().GetScreen(), &adventureData->rooms[i/3], adventureData));
							state = ADVENTUREMAKER_INIT;
						}
						else if(i % 3 == 1) // Pressed pencil
						{
							ScreenNavigator::Get().SetScreen(std::make_shared<EditNameScreen>(ScreenNavigator::Get().GetScreen(), &adventureData->rooms[i/3].name, GetRoomNames()));
							state = ADVENTUREMAKER_INIT;
						}
						else // Pressed remove
						{
							scrollState->RemoveElement(i - 2);
							scrollState->RemoveElement(i - 2);
							scrollState->RemoveElement(i - 2);
							adventureData->rooms.erase(adventureData->rooms.begin() + i / 3);
						}
						break;
					}
				}
			}
			break;
		}
	}
	return true;
}

void AdventureMakerScreen::Draw()
{
	GlobalMenuData::Get().DrawBackground();
}

void AdventureMakerScreen::RecordRoom(const UTF8String& roomName)
{
	uint32_t roomCount = scrollState->elements.size() / 3;
	scrollState->AddElement(new GUITextButton(roomName, Vec2(0.5f, 0.0f), Vec2(-370.0f, - (roomCount * 100.0f) - 100.0f), Vec2(0.0f), Vec2(650.0f, 50.0f)));
	scrollState->AddElement(new GUIImageButton("PencilIcon", Vec2(0.5f, 0.0f), Vec2(290.0f, - (roomCount * 100.0f) - 95.0f), Vec2(0.0f), Vec2(40.0f)));
	scrollState->AddElement(new GUIImageButton("RemoveIcon", Vec2(0.5f, 0.0f), Vec2(340.0f, - (roomCount * 100.0f) - 95.0f), Vec2(0.0f), Vec2(40.0f)));
}

std::vector<UTF8String> AdventureMakerScreen::GetRoomNames()
{
	std::vector<UTF8String> toRet;
	for(LevelData& room : adventureData->rooms)
		toRet.emplace_back(room.name);
	return toRet;
}
