#include "MainMenuScreen.hpp"
#include <GSEGL/Graphics/GUI/GUITextButton.hpp>
#include <GSEGL/Graphics/GUI/GUIImageButton.hpp>
#include <GSEGL/Graphics/GUI/GUIImmutableText.hpp>
#include <GSEGL/TimeUtil.hpp>
#include <GSEGL/LogUtil.hpp>
#include "ChoosePlayScreen.hpp"
#include "CreatorScreen.hpp"
#include "ExtrasScreen.hpp"
#include "PropertiesScreen.hpp"
#include "../GlobalSettingsUtil.hpp"
#include "../GlobalMenuData.hpp"

MainMenuScreen::MainMenuScreen():
state(MAINMENU_INIT),
sock(nullptr),
addr(nullptr),
updateStatusPending(false)
{}

bool MainMenuScreen::Update()
{
	switch(state)
	{
		case MAINMENU_INIT:
		{
			ControllerUtil::Get().SetCursorToGameMode(false);
			GUIUtil::Get().ClearAll();
			GlobalSettingsUtil::Get().WriteToFile();
			GlobalSettingsUtil::Get().SetupSettings();
			GUIUtil::Get().AddElement("Title", new GUIImmutableText("Grayscale Era", Vec2(0.5f, 1.0f), Vec2(-DrawUtil::Get().CalculateTextWidth("Grayscale Era", 70.0f) / 2.0f, -125.0f), 0.0f, 70.0f));
			GUIUtil::Get().AddElement("Play", new GUITextButton("Play", Vec2(0.5f, 1.0f), Vec2(-360.0f, -225.0f), Vec2(0.0f), Vec2(720.0f, 50.0f)));
			GUIUtil::Get().AddElement("Create", new GUITextButton("Create", Vec2(0.5f, 1.0f), Vec2(-360.0f, -325.0f), Vec2(0.0f), Vec2(720.0f, 50.0f)));
			GUIUtil::Get().AddElement("Extras", new GUITextButton("Extras", Vec2(0.5f, 1.0f), Vec2(-360.0f, -425.0f), Vec2(0.0f), Vec2(720.0f, 50.0f)));
			GUIUtil::Get().AddElement("Exit", new GUITextButton("Exit", Vec2(0.5f, 1.0f), Vec2(-360.0f, -525.0f), Vec2(0.0f), Vec2(720.0f, 50.0f)));
			GUIUtil::Get().AddElement("Settings", new GUIImageButton("SettingsIcon", Vec2(0.0f), Vec2(0.0f), Vec2(0.0f), Vec2(40.0f)));
			GlobalSettingsUtil::Get().constantUpdate = false;
			addr = SocketUtil::Get().CreateAddrFromString(GlobalSettingsUtil::Get().mainSettings.GetString(1), DEFAULT_PORT_STR, INET);
			if(addr)
			{
				sock = SocketUtil::Get().CreateUDPSocket(INET);
				if(sock)
				{
					SocketAddress ownAddr(0, INET);
					if(sock->Bind(ownAddr) == 0)
					{
						sock->SetBlockingMode(false);
						OutMemBitStream outStream;
						outStream.WriteBits(LOBBYPT_IsSameVersion);
						outStream.WriteBits(CURRENT_VERSION);
						sock->SendTo(outStream.GetBufferPtr(), outStream.GetByteLength(), *addr);
						updateStatusPending = true;
						GUIUtil::Get().AddElement("Update Status", new GUIImmutableText("Looking for updates...", Vec2(1.0f, 0.0f), Vec2(-270.0f, 0.0f), 0.0f, 15.0f));
					}
					else
						LogUtil::Get().LogMessage("NORMAL", "Could not check for updates");
				}
				else
					LogUtil::Get().LogMessage("NORMAL", "Could not check for updates");
			}
			else
				LogUtil::Get().LogMessage("NORMAL", "Could not check for updates");
			state = MAINMENU_UPDATE;
			break;
		}
		case MAINMENU_UPDATE:
		{
			if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Play"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<ChoosePlayScreen>(ScreenNavigator::Get().GetScreen()));
				state = MAINMENU_INIT;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Create"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<CreatorScreen>(ScreenNavigator::Get().GetScreen()));
				state = MAINMENU_INIT;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Extras"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<ExtrasScreen>(ScreenNavigator::Get().GetScreen()));
				state = MAINMENU_INIT;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Settings"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<PropertiesScreen>(ScreenNavigator::Get().GetScreen(), &GlobalSettingsUtil::Get().settings, true));
				GlobalSettingsUtil::Get().constantUpdate = true;
				state = MAINMENU_INIT;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Exit"))->buttonState == GUIBUTTON_PRESSED)
				return false;
			if(updateStatusPending)
			{
				Packet packet;
				packet.size = sock->ReceiveFrom(packet.data, MAX_PACKET_SIZE, *addr);
				if(packet.size > 0)
				{
					InMemBitStream inStream(packet.data, packet.size);
					LobbyPacketType packetType;
					inStream.ReadBits(packetType);
					GUIUtil::Get().RemoveElement("Update Status");
					if(packetType == LOBBYPT_DifferentVersion)
					{
						LogUtil::Get().LogMessage("NORMAL", "Update found! Please go to erroratline0.com and download");
						LogUtil::Get().LogMessage("NORMAL", "the new version");
						GUIUtil::Get().AddElement("Update Status", new GUIImmutableText("Update found!", Vec2(1.0f, 0.0f), Vec2(-170.0f, 0.0f), 0.0f, 15.0f));
					}
					else if(packetType == LOBBYPT_SameVersion)
						GUIUtil::Get().AddElement("Update Status", new GUIImmutableText("No updates!", Vec2(1.0f, 0.0f), Vec2(-150.0f, 0.0f), 0.0f, 15.0f));
					else
						GUIUtil::Get().AddElement("Update Status", new GUIImmutableText("Unknown Message", Vec2(1.0f, 0.0f), Vec2(-200.0f, 0.0f), 0.0f, 15.0f));
					addr = nullptr;
					sock = nullptr;
					updateStatusPending = false;
				}
			}
			break;
		}
	}
	return true;
}

void MainMenuScreen::Draw()
{
	GlobalMenuData::Get().DrawBackground();
}
