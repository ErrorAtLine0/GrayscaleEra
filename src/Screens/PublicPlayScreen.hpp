#pragma once
#include "../ScreenNavigator.hpp"
#include <GSEGL/UTF8String.hpp>
#include "../NetworkManager.hpp"

struct PublicPlayData
{
	UTF8String userName;
	UTF8String arenaName;
	double playerCount;
	uint32_t failures;
	std::shared_ptr<NetworkManager> netMan;
};

class PublicPlayScreen : public Screen
{
	public:
		PublicPlayScreen(const std::shared_ptr<Screen>& initHaltedScreen);
		bool Update();
		void Draw();
		
	private:
		void ExtractInfoToVars();
		enum ConnectState
		{
			PUBLICPLAY_INIT,
			PUBLICPLAY_UPDATE
		} state;
		std::shared_ptr<PublicPlayData> data;
		std::shared_ptr<Screen> haltedScreen;
};
