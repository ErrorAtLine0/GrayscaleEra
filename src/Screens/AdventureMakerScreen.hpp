#pragma once
#include "../ScreenNavigator.hpp"
#include "../AdventureData.hpp"
#include <GSEGL/Graphics/GUI/GUIScrollArea.hpp>
#include <GSEGL/Graphics/GUI/GUIUtil.hpp>

class AdventureMakerScreen : public Screen
{
	public:
		AdventureMakerScreen(const std::shared_ptr<Screen>& initHaltedScreen, const AdventureDataPtr& initAdventureData);
		bool Update();
		void Draw();
	private:
		void RecordRoom(const UTF8String& roomName);
		std::vector<UTF8String> GetRoomNames();
		enum AdventureMakerState {
			ADVENTUREMAKER_INIT,
			ADVENTUREMAKER_UPDATE
		} state;
		AdventureDataPtr adventureData;
		UTF8String newRoomName;
		GUIScrollAreaState* scrollState;
		std::shared_ptr<Screen> haltedScreen;
};
