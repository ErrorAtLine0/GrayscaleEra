#pragma once
#include "../ScreenNavigator.hpp"
#include "../NetworkManager.hpp"
#include "../PropertiesList.hpp"

enum PlayDataType
{
	PLAYDATATYPE_SERVER_LOBBY,
	PLAYDATATYPE_CLIENT_LOBBY,
	PLAYDATATYPE_SERVER_DIRECT,
	PLAYDATATYPE_CLIENT_DIRECT,
	PLAYDATATYPE_SINGLEPLAYER // Only used in adventures
};

class PlayDataScreen : public Screen
{
	public:
		PlayDataScreen(const std::shared_ptr<Screen>& initHaltedScreen, PlayDataType initPlayDataType, WorldType initWorldType);
		bool Update();
		void Draw();
		
	private:
		void ExtractInfoToVars();
		enum PlayDataState
		{
			PLAYDATA_INIT,
			PLAYDATA_UPDATE
		} state;
		UTF8String userName;
		UTF8String serverLocation; // Whether that be IP or Lobby
		UTF8String worldName; // Whether that be Arena or Adventure
		double playerCount;
		PlayDataType playDataType;
		WorldType worldType;
		PropertiesList serverSettings;
		std::shared_ptr<NetworkManager> netMan;
		std::shared_ptr<Screen> haltedScreen;
};
 
