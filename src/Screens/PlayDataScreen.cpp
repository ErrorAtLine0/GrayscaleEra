#include "PlayDataScreen.hpp"
#include "SelectorScreen.hpp"
#include "PropertiesScreen.hpp"
#include "WaitScreen.hpp"
#include "InLobbyScreen.hpp"
#include "MainMenuScreen.hpp"
#include <GSEGL/Graphics/GUI/GUITextEntry.hpp>
#include <GSEGL/Graphics/GUI/GUITextButton.hpp>
#include <GSEGL/Graphics/GUI/GUIImageButton.hpp>
#include <GSEGL/Graphics/GUI/GUIImmutableText.hpp>
#include <GSEGL/Graphics/GUI/GUICounter.hpp>
#include <GSEGL/FileUtil.hpp>
#include "../NetworkManagerServer.hpp"
#include "../NetworkManagerClient.hpp"
#include "../GlobalSettingsUtil.hpp"
#include "../GlobalMenuData.hpp"

PlayDataScreen::PlayDataScreen(const std::shared_ptr<Screen>& initHaltedScreen, PlayDataType initPlayDataType, WorldType initWorldType):
state(PLAYDATA_INIT),
userName(GlobalSettingsUtil::Get().settings.GetString(1)),
playerCount(1),
playDataType(initPlayDataType),
worldType(initWorldType),
serverSettings(NetworkManagerServer::serverSettingsList),
netMan(nullptr),
haltedScreen(initHaltedScreen)
{}

bool PlayDataScreen::Update()
{
	switch(state)
	{
		case PLAYDATA_INIT:
		{
			GUIUtil::Get().ClearAll();
			float yPos = 0.0f;
			
			yPos -= 75.0f;
			GUIUtil::Get().AddElement("User Name", new GUITextEntry("User Name", Vec2(0.5f, 1.0f), Vec2(-360.0f, yPos), Vec2(0.0f), Vec2(720.0f, 40.0f), 16, &userName));
			static_cast<GUITextEntryState*>(GUIUtil::Get().GrabState("User Name"))->text = userName;
			
			if(playDataType != PLAYDATATYPE_SERVER_DIRECT && playDataType != PLAYDATATYPE_SINGLEPLAYER)
			{
				yPos -= 100.0f;
				bool isDirect = playDataType == PLAYDATATYPE_CLIENT_DIRECT || playDataType == PLAYDATATYPE_SERVER_DIRECT;
				GUIUtil::Get().AddElement("Server Location", new GUITextEntry(isDirect ? "IP Address" : "Lobby Name (3-16)", Vec2(0.5f, 1.0f), Vec2(-360.0f, yPos), Vec2(0.0f), Vec2(720.0f, 40.0f), isDirect ? 0 : 16, &serverLocation));
				static_cast<GUITextEntryState*>(GUIUtil::Get().GrabState("Server Location"))->text = serverLocation;
			}
			
			if(playDataType == PLAYDATATYPE_SERVER_DIRECT || playDataType == PLAYDATATYPE_SERVER_LOBBY || playDataType == PLAYDATATYPE_SINGLEPLAYER)
			{
				yPos -= 100.0f;
				GUIUtil::Get().AddElement("World Name", new GUITextEntry(worldType == WORLD_ARENA ? "Arena Name" : "Adventure Name", Vec2(0.5f, 1.0f), Vec2(-360.0f, yPos), Vec2(0.0f), Vec2(720.0f, 40.0f), 16, &worldName));
				static_cast<GUITextEntryState*>(GUIUtil::Get().GrabState("World Name"))->text = worldName;
				GUIUtil::Get().AddElement("WorldNameChooser", new GUIImageButton("ListIcon", Vec2(0.5f, 1.0f), Vec2(320.0f, yPos + 15.0f), Vec2(0.0f), Vec2(40.0f)));
			}
			
			if(playDataType == PLAYDATATYPE_SERVER_DIRECT || playDataType == PLAYDATATYPE_SERVER_LOBBY)
			{
				if(worldType == WORLD_ARENA)
				{
					yPos -= 100.0f;
					GUIUtil::Get().AddElement("Server Settings Text", new GUIImmutableText("Server Settings:", Vec2(0.5f, 1.0f), Vec2(-360.0f, yPos), 0.0f, 40.0f));
					GUIUtil::Get().AddElement("Server Settings Button", new GUIImageButton("ListIcon", Vec2(0.5f, 1.0f), Vec2(320.0f, yPos), Vec2(0.0f), Vec2(40.0f)));
				}
				else if(worldType == WORLD_ADVENTURE)
				{
					yPos -= 100.0f;
					GUIUtil::Get().AddElement("Player Count Text", new GUIImmutableText("Player Count", Vec2(0.5f, 1.0f), Vec2(-360.0f, yPos), 0.0f, 40.0f));
					yPos -= 50.0f;
					GUIUtil::Get().AddElement("Player Count", new GUICounter(Vec2(0.5f, 1.0f), Vec2(-360.0f, yPos), Vec2(0.0f), Vec2(720.0f, 40.0f), 1, 1, 8, 1, &playerCount));
					static_cast<GUICounterState*>(GUIUtil::Get().GrabState("Player Count"))->SetVal(playerCount);
				}
			}
			
			yPos -= 100.0f;
			GUIUtil::Get().AddElement("RunPlay", new GUITextButton("Play", Vec2(0.5f, 1.0f), Vec2(-360.0f, yPos), Vec2(0.0f), Vec2(720.0f, 40.0f)));
			GUIUtil::Get().AddElement("Back", new GUIImageButton("BackIcon", Vec2(0.0f), Vec2(0.0f), Vec2(0.0f), Vec2(40.0f)));
			state = PLAYDATA_UPDATE;
			break;
		}
		case PLAYDATA_UPDATE:
		{
			if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("RunPlay"))->buttonState == GUIBUTTON_PRESSED)
			{
				ExtractInfoToVars();
				if(!userName.IsEmpty())
				{
					switch(playDataType)
					{
						case PLAYDATATYPE_SINGLEPLAYER:
						case PLAYDATATYPE_SERVER_DIRECT:
						{
							if(!worldName.IsEmpty())
							{
								ErrorUtil::Get().ClearErrors();
								netMan = std::make_shared<NetworkManagerServer>();
								WorldDataPtr worldData = WorldData::ReadWorldFile(worldName, worldType);
								if(worldData && std::static_pointer_cast<NetworkManagerServer>(netMan)->Init(userName, DEFAULT_PORT, worldData, serverSettings))
								{
									GUIUtil::Get().ClearAll();
									ScreenNavigator::Get().SetScreen(std::make_shared<InLobbyScreen>(std::make_shared<MainMenuScreen>(), netMan));
									return true;
								}
							}
							break;
						}
						case PLAYDATATYPE_CLIENT_DIRECT:
						{
							if(!serverLocation.IsEmpty())
							{
								netMan = std::make_shared<NetworkManagerClient>();
								if(std::static_pointer_cast<NetworkManagerClient>(netMan)->Init(userName, serverLocation, DEFAULT_PORT, worldType))
								{
									ScreenNavigator::Get().SetScreen(std::make_shared<WaitScreen>(ScreenNavigator::Get().GetScreen(), BasicNetworkWaitLoop, std::static_pointer_cast<void>(netMan)));
									state = PLAYDATA_INIT;
									return true;
								}
							}
							break;
						}
						case PLAYDATATYPE_SERVER_LOBBY:
						{
							if(!worldName.IsEmpty() && serverLocation.GetStrLength() >= 3)
							{
								netMan = std::make_shared<NetworkManagerServer>();
								WorldDataPtr worldData = WorldData::ReadWorldFile(worldName, worldType);
								if(worldData && std::static_pointer_cast<NetworkManagerServer>(netMan)->Init(userName, serverLocation, worldData, serverSettings, true))
								{
									ScreenNavigator::Get().SetScreen(std::make_shared<WaitScreen>(ScreenNavigator::Get().GetScreen(), BasicNetworkWaitLoop, std::static_pointer_cast<void>(netMan)));
									state = PLAYDATA_INIT;
									return true;
								}
							}
							break;
						}
						case PLAYDATATYPE_CLIENT_LOBBY:
						{
							if(serverLocation.GetStrLength() >= 3)
							{
								netMan = std::make_shared<NetworkManagerClient>();
								if(std::static_pointer_cast<NetworkManagerClient>(netMan)->Init(userName, serverLocation, worldType))
								{
									ScreenNavigator::Get().SetScreen(std::make_shared<WaitScreen>(ScreenNavigator::Get().GetScreen(), BasicNetworkWaitLoop, std::static_pointer_cast<void>(netMan)));
									state = PLAYDATA_INIT;
									return true;
								}
							}
							break;
						}
					}
				}
			}
			else if(GUIUtil::Get().GrabState("WorldNameChooser") && static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("WorldNameChooser"))->buttonState == GUIBUTTON_PRESSED)
			{
				ExtractInfoToVars();
				state = PLAYDATA_INIT;
				ScreenNavigator::Get().SetScreen(std::make_shared<SelectorScreen>(ScreenNavigator::Get().GetScreen(), &worldName, worldType == WORLD_ARENA ? FILETYPE_ARENA : FILETYPE_ADVENTURE));
			}
			else if(GUIUtil::Get().GrabState("Server Settings Button") && static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Server Settings Button"))->buttonState == GUIBUTTON_PRESSED)
			{
				ExtractInfoToVars();
				state = PLAYDATA_INIT;
				ScreenNavigator::Get().SetScreen(std::make_shared<PropertiesScreen>(ScreenNavigator::Get().GetScreen(), &serverSettings));
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Back"))->buttonState == GUIBUTTON_PRESSED)
				ScreenNavigator::Get().SetScreen(haltedScreen);
			break;
		}
	}
	return true;
}

void PlayDataScreen::ExtractInfoToVars()
{
	GUIUtil::Get().UpdateValPtrs();
	if(worldType == WORLD_ADVENTURE)
		static_cast<IntProperty*>(serverSettings.GetPropAt(0))->data = playerCount;
}

void PlayDataScreen::Draw()
{
	GlobalMenuData::Get().DrawBackground();
}
 
