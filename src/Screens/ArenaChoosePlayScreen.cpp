#include "ArenaChoosePlayScreen.hpp"
#include "PlayDataScreen.hpp"
#include "PublicPlayScreen.hpp"
#include <GSEGL/Graphics/GUI/GUIImageButton.hpp>
#include <GSEGL/Graphics/GUI/GUITextButton.hpp>
#include "../GlobalMenuData.hpp"

ArenaChoosePlayScreen::ArenaChoosePlayScreen(const std::shared_ptr<Screen>& initHaltedScreen):
state(ARENACHOOSEPLAY_INIT),
haltedScreen(initHaltedScreen)
{}

bool ArenaChoosePlayScreen::Update()
{
	switch(state)
	{
		case ARENACHOOSEPLAY_INIT:
		{
			GUIUtil::Get().ClearAll();
			GUIUtil::Get().AddElement("PublicPlayButton", new GUITextButton("Public Play", Vec2(0.5f, 1.0f), Vec2(-360.0f, -125.0f), Vec2(0.0f), Vec2(720.0f, 50.0f)));
			GUIUtil::Get().AddElement("ConnectButton", new GUITextButton("Connect Lobby", Vec2(0.5f, 1.0f), Vec2(-360.0f, -225.0f), Vec2(0.0f), Vec2(720.0f, 50.0f)));
			GUIUtil::Get().AddElement("HostButton", new GUITextButton("Host Lobby", Vec2(0.5f, 1.0f), Vec2(-360.0f, -325.0f), Vec2(0.0f), Vec2(720.0f, 50.0f)));
			GUIUtil::Get().AddElement("DirectConnectButton", new GUITextButton("Direct Connect", Vec2(0.5f, 1.0f), Vec2(-360.0f, -425.0f), Vec2(0.0f), Vec2(720.0f, 50.0f)));
			GUIUtil::Get().AddElement("DirectHostButton", new GUITextButton("Direct Host", Vec2(0.5f, 1.0f), Vec2(-360.0f, -525.0f), Vec2(0.0f), Vec2(720.0f, 50.0f)));
			GUIUtil::Get().AddElement("Back", new GUIImageButton("BackIcon", Vec2(0.0f), Vec2(0.0f), Vec2(0.0f), Vec2(40.0f)));
			state = ARENACHOOSEPLAY_UPDATE;
			break;
		}
		case ARENACHOOSEPLAY_UPDATE:
		{
			if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("PublicPlayButton"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<PublicPlayScreen>(ScreenNavigator::Get().GetScreen()));
				state = ARENACHOOSEPLAY_INIT;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("ConnectButton"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<PlayDataScreen>(ScreenNavigator::Get().GetScreen(), PLAYDATATYPE_CLIENT_LOBBY, WORLD_ARENA));
				state = ARENACHOOSEPLAY_INIT;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("HostButton"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<PlayDataScreen>(ScreenNavigator::Get().GetScreen(), PLAYDATATYPE_SERVER_LOBBY, WORLD_ARENA));
				state = ARENACHOOSEPLAY_INIT;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("DirectConnectButton"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<PlayDataScreen>(ScreenNavigator::Get().GetScreen(), PLAYDATATYPE_CLIENT_DIRECT, WORLD_ARENA));
				state = ARENACHOOSEPLAY_INIT;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("DirectHostButton"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<PlayDataScreen>(ScreenNavigator::Get().GetScreen(), PLAYDATATYPE_SERVER_DIRECT, WORLD_ARENA));
				state = ARENACHOOSEPLAY_INIT;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Back"))->buttonState == GUIBUTTON_PRESSED)
				ScreenNavigator::Get().SetScreen(haltedScreen);
			break;
		}
	}
	return true;
}

void ArenaChoosePlayScreen::Draw()
{
	GlobalMenuData::Get().DrawBackground();
}
