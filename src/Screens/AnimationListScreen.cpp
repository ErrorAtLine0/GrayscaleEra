#include "AnimationListScreen.hpp"
#include "AnimatorScreen.hpp"
#include <GSEGL/Graphics/GUI/GUIImageButton.hpp>
#include <GSEGL/Graphics/GUI/GUIImmutableText.hpp>

AnimationListScreen::AnimationListScreen(const std::shared_ptr<Screen>& initHaltedScreen, NamedAnimationList* initAnimationList, const UVec2& initImageDimensions, uint32_t initMinDarkPixelCount):
state(ANIMATIONLIST_INIT),
animationList(initAnimationList),
imageDimensions(initImageDimensions),
minDarkPixelCount(initMinDarkPixelCount),
haltedScreen(initHaltedScreen)
{}

bool AnimationListScreen::Update()
{
	switch(state)
	{
		case ANIMATIONLIST_INIT:
		{
			GUIUtil::Get().ClearAll();
			GUIUtil::Get().AddElement("ScrollArea", new GUIScrollArea(Vec2(0.0f), Vec2(0.0f), Vec2(1.0f), Vec2(0.0f), 20.0f));
			GUIUtil::Get().AddElement("Back", new GUIImageButton("BackIcon", Vec2(1.0f, 0.0f), Vec2(-40.0f, 0.0f), Vec2(0.0f), Vec2(40.0f)));
			scrollState = static_cast<GUIScrollAreaState*>(GUIUtil::Get().GrabState("ScrollArea"));
			
			for(uint32_t i = 0; i < animationList->size(); i++)
			{
				scrollState->AddElement(new GUIImageButton("PencilIcon", Vec2(0.0f), Vec2(10.0f, -((i+1) * YVALBETWEENANIMATIONS)), Vec2(0.0f), Vec2(40.0f)));
				scrollState->AddElement(new GUIImmutableText((*animationList)[i].first, Vec2(0.0f), Vec2(60.0f, -((i+1) * YVALBETWEENANIMATIONS)), 0.0f, YVALBETWEENANIMATIONS - 20.0f));
			}
			
			state = ANIMATIONLIST_UPDATELOOP;
			break;
		}
		case ANIMATIONLIST_UPDATELOOP:
		{
			for(uint32_t i = 0; i < scrollState->elements.size(); i += 2)
				if(static_cast<GUIButtonState*>(scrollState->elements[i]->GrabState())->buttonState == GUIBUTTON_PRESSED)
				{
					ScreenNavigator::Get().SetScreen(std::make_shared<AnimatorScreen>(ScreenNavigator::Get().GetScreen(), ABILITY_MAX_FRAMES, &(*animationList)[i/2].second, imageDimensions, minDarkPixelCount));
					state = ANIMATIONLIST_INIT;
				}
			if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Back"))->buttonState == GUIBUTTON_PRESSED)
			{
				GUIUtil::Get().ClearAll();
				ScreenNavigator::Get().SetScreen(haltedScreen);
			}
			break;
		}
	}
	return true;
}

void AnimationListScreen::Draw()
{
	
}
