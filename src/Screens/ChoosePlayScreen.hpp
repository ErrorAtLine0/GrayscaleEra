#pragma once
#include "../ScreenNavigator.hpp"

class ChoosePlayScreen : public Screen
{
	public:
		ChoosePlayScreen(const std::shared_ptr<Screen>& initHaltedScreen);
		bool Update();
		void Draw();
	private:
		enum ChoosePlayState
		{
			CHOOSEPLAY_INIT,
			CHOOSEPLAY_UPDATE
		} state;
		std::shared_ptr<Screen> haltedScreen;
};
