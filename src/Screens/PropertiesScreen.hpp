#pragma once
#include "../ScreenNavigator.hpp"
#include "../PropertiesList.hpp"
#include "../AdventureData.hpp"
#include <vector>
#include <deque>
#include <GSEGL/Graphics/GUI/GUIUtil.hpp>
#include <GSEGL/Graphics/GUI/GUIScrollArea.hpp>
#define YVALBETWEENPROPERTIES 40.0f

class PropertiesScreen : public Screen
{
	public:
		PropertiesScreen(const std::shared_ptr<Screen>& initHaltedScreen, PropertiesList* initPropList, bool initConstantUpdate = false, AdventureDataPtr initAdventureData = nullptr);
		bool Update();
		void Draw();
		
		static std::pair<GUIElement*, void*> GetElementFromProperty(HeadlineProperty* prop, Vec2& pos, AdventureDataPtr adventureData = nullptr);
	private:
		void SetupElements();
		void SaveDataToList();
		enum PropertiesState
		{
			PROPERTIES_INIT,
			PROPERTIES_UPDATE
		} state;
		std::shared_ptr<Screen> haltedScreen;
		GUIScrollAreaState* scrollState;
		std::vector<std::tuple<GUIElement*, void*, PropertyType>> extraDataButtons;
		PropertiesList* propList;
		AdventureDataPtr adventureData;
		bool constantUpdate;
};
