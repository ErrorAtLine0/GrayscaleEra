#pragma once
#include "../ScreenNavigator.hpp"
#include "../AbilityData.hpp"
#include <GSEGL/Graphics/GUI/GUIUtil.hpp>
#include <GSEGL/Graphics/GUI/GUIScrollArea.hpp>
#define YVALBETWEENANIMATIONS 50.0f

class AnimationListScreen : public Screen
{
	public:
		AnimationListScreen(const std::shared_ptr<Screen>& initHaltedScreen, NamedAnimationList* initAnimationList, const UVec2& initImageDimensions, uint32_t initMinDarkPixelCount = 0);
		bool Update();
		void Draw();
	private:
		enum AnimationListState
		{
			ANIMATIONLIST_INIT,
			ANIMATIONLIST_UPDATELOOP
		} state;
		NamedAnimationList* animationList;
		GUIScrollAreaState* scrollState;
		UVec2 imageDimensions;
		uint32_t minDarkPixelCount;
		std::shared_ptr<Screen> haltedScreen;
};
