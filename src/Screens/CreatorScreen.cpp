#include "CreatorScreen.hpp"
#include "MainMenuScreen.hpp"
#include "../GlobalMenuData.hpp"
#include <GSEGL/Graphics/GUI/GUIUtil.hpp>
#include <GSEGL/Graphics/GUI/GUITextButton.hpp>
#include <GSEGL/Graphics/GUI/GUIImageButton.hpp>
#include "SelectorScreen.hpp"

CreatorScreen::CreatorScreen(const std::shared_ptr<Screen>& initHaltedScreen):
state(CREATOR_INIT),
haltedScreen(initHaltedScreen)
{}

bool CreatorScreen::Update()
{
	switch(state)
	{
		case CREATOR_INIT:
		{
			GUIUtil::Get().ClearAll();
			GUIUtil::Get().AddElement("ArenaButton", new GUITextButton("Arena", Vec2(0.5f, 1.0f), Vec2(-360.0f, -125.0f), Vec2(0.0f), Vec2(720.0f, 50.0f)));
			GUIUtil::Get().AddElement("AdventureButton", new GUITextButton("Adventure", Vec2(0.5f, 1.0f), Vec2(-360.0f, -225.0f), Vec2(0.0f), Vec2(720.0f, 50.0f)));
			GUIUtil::Get().AddElement("BlockButton", new GUITextButton("Block", Vec2(0.5f, 1.0f), Vec2(-360.0f, -325.0f), Vec2(0.0f), Vec2(720.0f, 50.0f)));
			GUIUtil::Get().AddElement("PlayerButton", new GUITextButton("Player", Vec2(0.5f, 1.0f), Vec2(-360.0f, -425.0f), Vec2(0.0f), Vec2(720.0f, 50.0f)));
			GUIUtil::Get().AddElement("Back", new GUIImageButton("BackIcon", Vec2(0.0f), Vec2(0.0f), Vec2(0.0f), Vec2(40.0f)));
			state = CREATOR_UPDATE;
			break;
		}
		case CREATOR_UPDATE:
		{
			if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("ArenaButton"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<SelectorScreen>(ScreenNavigator::Get().GetScreen(), nullptr, FILETYPE_ARENA));
				state = CREATOR_INIT;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("AdventureButton"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<SelectorScreen>(ScreenNavigator::Get().GetScreen(), nullptr, FILETYPE_ADVENTURE));
				state = CREATOR_INIT;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("BlockButton"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<SelectorScreen>(ScreenNavigator::Get().GetScreen(), nullptr, FILETYPE_BLOCK));
				state = CREATOR_INIT;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("PlayerButton"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<SelectorScreen>(ScreenNavigator::Get().GetScreen(), nullptr, FILETYPE_PLAYER));
				state = CREATOR_INIT;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Back"))->buttonState == GUIBUTTON_PRESSED)
				ScreenNavigator::Get().SetScreen(haltedScreen);
			break;
		}
	}
	return true;
}

void CreatorScreen::Draw()
{
	GlobalMenuData::Get().DrawBackground();
}
