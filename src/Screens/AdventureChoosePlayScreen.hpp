#pragma once
#include "../ScreenNavigator.hpp"

class AdventureChoosePlayScreen : public Screen
{
	public:
		AdventureChoosePlayScreen(const std::shared_ptr<Screen>& initHaltedScreen);
		bool Update();
		void Draw();
		
	private:
		enum AdventureChoosePlayState
		{
			ADVENTURECHOOSEPLAY_INIT,
			ADVENTURECHOOSEPLAY_UPDATE
		} state;
		std::shared_ptr<Screen> haltedScreen;
};
 
