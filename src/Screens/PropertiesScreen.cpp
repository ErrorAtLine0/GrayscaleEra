#include "PropertiesScreen.hpp"
#include <GSEGL/Graphics/GUI/GUIImmutableText.hpp>
#include <GSEGL/Graphics/GUI/GUIImageButton.hpp>
#include <GSEGL/Graphics/GUI/GUICheckBox.hpp>
#include <GSEGL/Graphics/GUI/GUISlider.hpp>
#include <GSEGL/Graphics/GUI/GUITextEntry.hpp>
#include <GSEGL/Graphics/GUI/GUITextButton.hpp>
#include <GSEGL/Graphics/GUI/GUIMultipleChoice.hpp>
#include <GSEGL/Graphics/GUI/GUIButtonInput.hpp>
#include "RuleListScreen.hpp"
#include "SelectorScreen.hpp"

PropertiesScreen::PropertiesScreen(const std::shared_ptr<Screen>& initHaltedScreen, PropertiesList* initPropList, bool initConstantUpdate, AdventureDataPtr initAdventureData):
state(PROPERTIES_INIT),
haltedScreen(initHaltedScreen),
propList(initPropList),
adventureData(initAdventureData),
constantUpdate(initConstantUpdate)
{}

bool PropertiesScreen::Update()
{
	switch(state)
	{
		case PROPERTIES_INIT:
		{
			GUIUtil::Get().ClearAll();
			GUIUtil::Get().AddElement("ScrollArea", new GUIScrollArea(Vec2(0.0f), Vec2(0.0f), Vec2(1.0f), Vec2(0.0f), 20.0f));
			GUIUtil::Get().AddElement("Done", new GUIImageButton("BackIcon", Vec2(0.0f), Vec2(0.0f), Vec2(0.0f), Vec2(40.0f)));
			scrollState = static_cast<GUIScrollAreaState*>(GUIUtil::Get().GrabState("ScrollArea"));
			extraDataButtons.clear();
			SetupElements();
			state = PROPERTIES_UPDATE;
			break;
		}
		case PROPERTIES_UPDATE:
		{
			for(uint32_t i = 0; i < extraDataButtons.size(); i++)
				if(static_cast<GUIButtonState*>(std::get<0>(extraDataButtons[i])->GrabState())->buttonState == GUIBUTTON_PRESSED)
				{
					PropertyType dataType = std::get<2>(extraDataButtons[i]);
					void* rawData = std::get<1>(extraDataButtons[i]);
					SaveDataToList();
					if(dataType == PROPERTY_LIST)
						ScreenNavigator::Get().SetScreen(std::make_shared<PropertiesScreen>(ScreenNavigator::Get().GetScreen(), static_cast<PropertiesList*>(rawData), false, adventureData));
					else if(dataType == PROPERTY_BLOCK)
						ScreenNavigator::Get().SetScreen(std::make_shared<SelectorScreen>(ScreenNavigator::Get().GetScreen(), &adventureData->blocks, static_cast<BlockID*>(rawData), SELECTOR_INDEX_UINT16, FILETYPE_BLOCK, adventureData));
					else if(dataType == PROPERTY_NPC)
						ScreenNavigator::Get().SetScreen(std::make_shared<SelectorScreen>(ScreenNavigator::Get().GetScreen(), &adventureData->npcs, static_cast<NPCID*>(rawData), SELECTOR_INDEX_UINT16, FILETYPE_PLAYER, adventureData));
					else if(dataType == PROPERTY_RULELIST)
						ScreenNavigator::Get().SetScreen(std::make_shared<RuleListScreen>(ScreenNavigator::Get().GetScreen(), static_cast<RuleList*>(rawData), adventureData));
					state = PROPERTIES_INIT;
					break;
				}
			if(constantUpdate)
				SaveDataToList();
			if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Done"))->buttonState == GUIBUTTON_PRESSED)
			{
				SaveDataToList();
				ScreenNavigator::Get().SetScreen(haltedScreen);
			}
			break;
		}
	}
	return true;
}

void PropertiesScreen::SaveDataToList()
{
	for(GUIElement* element : scrollState->elements)
		element->UpdateValPtr();
}

void PropertiesScreen::SetupElements()
{
	Vec2 pos(45.0f, -YVALBETWEENPROPERTIES);
	scrollState->AddElement(new GUIImmutableText(propList->GetListName(), Vec2(0.0f), pos, 0.0f, YVALBETWEENPROPERTIES/2.0f));
	pos.y -= YVALBETWEENPROPERTIES * 0.75f;
	for(uint32_t i = 0; i < propList->GetListSize(); i++)
	{
		HeadlineProperty* property = propList->GetPropAt(i);
		scrollState->AddElement(new GUIImmutableText(property->GetPropertyName(), Vec2(0.0f), pos, 0.0f, YVALBETWEENPROPERTIES/2.0f));
		pos.y -= YVALBETWEENPROPERTIES * 0.75f;
		auto elementData = GetElementFromProperty(property, pos, adventureData);
		if(elementData.first)
		{
			scrollState->AddElement(elementData.first);
			if(elementData.second)
				extraDataButtons.emplace_back(elementData.first, elementData.second, property->GetPropertyType());
		}
	}
}

std::pair<GUIElement*, void*> PropertiesScreen::GetElementFromProperty(HeadlineProperty* prop, Vec2& pos, AdventureDataPtr adventureData) // Data ptr is for sending to extra data vectors, makes operation quicker
{
	std::pair<GUIElement*, void*> toRet(nullptr, nullptr);
	switch(prop->GetPropertyType())
	{
		case PROPERTY_HEADLINE:
			break;
		case PROPERTY_BOOL:
		{
			toRet.first = new GUICheckBox(Vec2(0.0f), pos, Vec2(0.0f), Vec2(YVALBETWEENPROPERTIES/1.25f), &static_cast<BoolProperty*>(prop)->data);
			static_cast<GUICheckBoxState*>(toRet.first->GrabState())->val = static_cast<const BoolProperty*>(prop)->data;
			pos.y -= YVALBETWEENPROPERTIES;
			break;
		}
		case PROPERTY_INT:
		{
			IntProperty* castedProperty = static_cast<IntProperty*>(prop);
			toRet.first = new GUISlider(Vec2(0.0f), pos, Vec2(0.0f), Vec2(700.0f, YVALBETWEENPROPERTIES/1.75f), castedProperty->data, castedProperty->minVal, castedProperty->maxVal, castedProperty->diff, &castedProperty->data);
			pos.y -= YVALBETWEENPROPERTIES;
			break;
		}
		case PROPERTY_STRING:
		{
			StringProperty* castedProperty = static_cast<StringProperty*>(prop);
			toRet.first = new GUITextEntry("", Vec2(0.0f), pos, Vec2(0.0f), Vec2(700.0f, YVALBETWEENPROPERTIES/1.75f), castedProperty->maxLength, &castedProperty->data);
			static_cast<GUITextEntryState*>(toRet.first->GrabState())->text = castedProperty->data;
			pos.y -= YVALBETWEENPROPERTIES;
			break;
		}
		case PROPERTY_CHOICE:
		{
			ChoiceProperty* castedProperty = static_cast<ChoiceProperty*>(prop);
			pos.y -= (castedProperty->choices.size()*(YVALBETWEENPROPERTIES/1.75f)*1.25f) - YVALBETWEENPROPERTIES;
			toRet.first = new GUIMultipleChoice(Vec2(0.0f), pos, 0.0f, YVALBETWEENPROPERTIES/1.75f, castedProperty->choices, &castedProperty->data);
			static_cast<GUIMultipleChoiceState*>(toRet.first->GrabState())->choiceIndex = castedProperty->data;
			pos.y -= YVALBETWEENPROPERTIES;
			break;
		}
		case PROPERTY_LIST:
		{
			ListProperty* castedProperty = static_cast<ListProperty*>(prop);
			toRet.first = new GUIImageButton("ListIcon", Vec2(0.0f), pos, Vec2(0.0f), Vec2(YVALBETWEENPROPERTIES/1.25f));
			toRet.second = static_cast<void*>(&castedProperty->data);
			pos.y -= YVALBETWEENPROPERTIES;
			break;
		}
		case PROPERTY_BUTTON:
		{
			ButtonProperty* castedProperty = static_cast<ButtonProperty*>(prop);
			toRet.first = new GUIButtonInput(Vec2(0.0f), pos, 0.0f, YVALBETWEENPROPERTIES/1.25f, castedProperty->data, &castedProperty->data);
			pos.y -= YVALBETWEENPROPERTIES;
			break;
		}
		case PROPERTY_BLOCK:
		{
			BlockProperty* castedProperty = static_cast<BlockProperty*>(prop);
			if(castedProperty->data > 0 && !adventureData->blocks[castedProperty->data - 1].animation.empty())
				toRet.first = new GUIImageButton(std::make_shared<Texture>(adventureData->blocks[castedProperty->data - 1].animation[0].GetData(), BLOCK_TEXTURE_SIZE, BLOCK_TEXTURE_SIZE), Vec2(0.0f), pos, Vec2(0.0f), Vec2(YVALBETWEENPROPERTIES/1.25f));
			else
				toRet.first = new GUIImageButton(static_cast<TexturePtr>(nullptr), Vec2(0.0f), pos, Vec2(0.0f), Vec2(YVALBETWEENPROPERTIES/1.25f));
			toRet.second = static_cast<void*>(&castedProperty->data);
			pos.y -= YVALBETWEENPROPERTIES;
			break;
		}
		case PROPERTY_NPC:
		{
			NPCProperty* castedProperty = static_cast<NPCProperty*>(prop);
			if(castedProperty->data > 0 && !adventureData->npcs[castedProperty->data - 1].idleAnimation.empty())
				toRet.first = new GUIImageButton(std::make_shared<Texture>(adventureData->npcs[castedProperty->data - 1].idleAnimation[0].GetData(), PLAYER_TEXTURE_SIZE, PLAYER_TEXTURE_SIZE), Vec2(0.0f), pos, Vec2(0.0f), Vec2(YVALBETWEENPROPERTIES/1.25f));
			else
				toRet.first = new GUIImageButton(static_cast<TexturePtr>(nullptr), Vec2(0.0f), pos, Vec2(0.0f), Vec2(YVALBETWEENPROPERTIES/1.25f));
			toRet.second = static_cast<void*>(&castedProperty->data);
			pos.y -= YVALBETWEENPROPERTIES;
			break;
		}
		case PROPERTY_RULELIST:
		{
			NPCProperty* castedProperty = static_cast<NPCProperty*>(prop);
			toRet.first = new GUIImageButton("RuleListIcon", Vec2(0.0f), pos, Vec2(0.0f), Vec2(YVALBETWEENPROPERTIES/1.25f));
			toRet.second = static_cast<void*>(&castedProperty->data);
			pos.y -= YVALBETWEENPROPERTIES;
			break;
		}
	}
	return toRet;
}

void PropertiesScreen::Draw()
{
	
}
