#include "PaintScreen.hpp"
#include <GSEGL/Graphics/GUI/GUICanvas.hpp>
#include <GSEGL/Graphics/GUI/GUIImageButton.hpp>
#include <cstring>
#include <GSEGL/LogUtil.hpp>
#include <GSEGL/ErrorUtil.hpp>

#define DARK_PIXEL_MAX_VALUE 200

PaintScreen::PaintScreen(const std::shared_ptr<Screen>& initHaltedScreen, uint8_t* initImagePtr, const UVec2& initImageDimensions, uint32_t initMinDarkPixelCount):
state(PAINT_INIT),
imageDimensions(initImageDimensions),
lastStep(static_cast<uint8_t*>(std::malloc(imageDimensions.x * imageDimensions.y))),
isDrawing(false),
lastCursorDrawPos(0.0f, 0.0f),
imagePtr(initImagePtr),
minDarkPixelCount(initMinDarkPixelCount),
haltedScreen(initHaltedScreen)
{}

PaintScreen::~PaintScreen()
{
	std::free(lastStep);
}

bool PaintScreen::Update()
{
	Vec2 winSize = WindowUtil::Get().GetWindowSize();
	switch(state)
	{
		case PAINT_INIT:
		{
			GUIUtil::Get().ClearAll();
			GUIUtil::Get().AddElement("MainCanvas", (new GUICanvas(Vec2(0.5f, 0.0f), Vec2(0.0f, 0.0f), Vec2(-1.0f, 1.0f), Vec2(0.0f), imageDimensions))->SetFinalSizePos(Vec2(-0.5f, 0.0f)));
			GUIUtil::Get().AddElement("DoneButton", new GUIImageButton("DoneButton", Vec2(0.0f, 0.5f), Vec2(0.0f, 150.0f), Vec2(0.0f), Vec2(100.0f)));
			GUIUtil::Get().AddElement("CopyButton", new GUIImageButton("CopyButton", Vec2(0.0f, 0.5f), Vec2(0.0f, 50.0f), Vec2(0.0f), Vec2(100.0f)));
			GUIUtil::Get().AddElement("PasteButton", new GUIImageButton("PasteButton", Vec2(0.0f, 0.5f), Vec2(0.0f, -50.0f), Vec2(0.0f), Vec2(100.0f)));
			GUIUtil::Get().AddElement("UndoButton", new GUIImageButton("UndoButton", Vec2(0.0f, 0.5f), Vec2(0.0f, -150.0f), Vec2(0.0f), Vec2(100.0f)));
			GUIUtil::Get().AddElement("CancelButton", new GUIImageButton("CancelButton", Vec2(0.0f, 0.5f), Vec2(0.0f, -250.0f), Vec2(0.0f), Vec2(100.0f)));
			GUICanvasState* canvasState = static_cast<GUICanvasState*>(GUIUtil::Get().GrabState("MainCanvas"));
			canvasState->SetCanvasData(imagePtr);
			canvasState->SyncCanvas();
			state = PAINT_UPDATELOOP;
			break;
		}
		case PAINT_UPDATELOOP:
		{
			GUICanvasState* canvasState = static_cast<GUICanvasState*>(GUIUtil::Get().GrabState("MainCanvas"));
			
			if(canvasState->currentBrushSize < 16 && ControllerUtil::Get().GetScrollOffset() > 0.0f) ++canvasState->currentBrushSize;
			else if(canvasState->currentBrushSize > 1 && ControllerUtil::Get().GetScrollOffset() < 0.0f) --canvasState->currentBrushSize;
			
			
			const std::vector<UTF8String>& filesDropped = WindowUtil::Get().GetDroppedFileNames();
			if(!filesDropped.empty())
			{
				int32_t width, height, nrChannels;
				unsigned char* imageRawData = Texture::LoadImageData(filesDropped[0], &width, &height, &nrChannels, 1);
				if(imageRawData)
				{
					if(width == static_cast<int32_t>(imageDimensions.x) && height == static_cast<int32_t>(imageDimensions.y))
					{
						canvasState->SetCanvasData(imageRawData);
						canvasState->SyncCanvas();
					}
					Texture::FreeImageData(imageRawData);
				}
			}
			
			Vec2 cursorPos = ControllerUtil::Get().GetCursorPos();
			if(cursorPos.y > 0.0f && cursorPos.y < winSize.y)
			{
				if(cursorPos.x > (winSize.x / 2.0f) - (winSize.y / 2.0f) && cursorPos.x < (winSize.x / 2.0f) + (winSize.y / 2.0f))
				{
					if(ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY1) != KEYSTATE_RELEASED)
					{
						if(!isDrawing)
						{
							isDrawing = true;
							std::memcpy(lastStep, canvasState->GetCanvasData(), canvasState->GetTextureByteSize());
						}
						canvasState->DrawAtPos(cursorPos);
						canvasState->SyncCanvas();
					}
					else if(ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY2) == KEYSTATE_PRESSED)
						canvasState->selectedColor = canvasState->GetColorAt(cursorPos);
				}
				else if(cursorPos.x > winSize.x - 100.0f && cursorPos.x < winSize.x)
				{
					if(ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY1) == KEYSTATE_PRESSED || ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY2) == KEYSTATE_PRESSED)
						canvasState->selectedColor = (static_cast<uint8_t>(cursorPos.y / (winSize.y / 16.0f))) * 17;
				}
			}
			if(isDrawing && ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY1) == KEYSTATE_RELEASED)
				isDrawing = false;
			if((static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("CopyButton"))->buttonState == GUIBUTTON_PRESSED)||
			   (WindowUtil::Get().IsControlHeld() && WindowUtil::Get().GetKeyState(GLFW_KEY_C) == KEYSTATE_PRESSED))
				SetClipboardDrawing();
			if((static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("PasteButton"))->buttonState == GUIBUTTON_PRESSED)||
			   (WindowUtil::Get().IsControlHeld() && WindowUtil::Get().GetKeyState(GLFW_KEY_V) == KEYSTATE_PRESSED))
			{
				if(canvasState->GetTextureSize() == clipboardSize)
				{
					std::memcpy(lastStep, canvasState->GetCanvasData(), canvasState->GetTextureByteSize());
					canvasState->SetCanvasData(clipboard);
					canvasState->SyncCanvas();
				}
			}
			if((static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("UndoButton"))->buttonState == GUIBUTTON_PRESSED)||
			   (WindowUtil::Get().IsControlHeld() && WindowUtil::Get().GetKeyState(GLFW_KEY_Z) == KEYSTATE_PRESSED))
			{
				canvasState->SetCanvasData(lastStep);
				canvasState->SyncCanvas();
			}
			if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("CancelButton"))->buttonState == GUIBUTTON_PRESSED)
				ScreenNavigator::Get().SetScreen(haltedScreen);
			if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("DoneButton"))->buttonState == GUIBUTTON_PRESSED)
			{
				uint32_t darkPixelCount = 0;
				if(minDarkPixelCount != 0)
					for(uint32_t i = 0; i < canvasState->GetTextureByteSize(); i++)
					{
						if(canvasState->GetCanvasData()[i] <= DARK_PIXEL_MAX_VALUE)
						{
							++darkPixelCount;
							if(darkPixelCount == minDarkPixelCount)
								break;
						}
					}
				if(darkPixelCount == minDarkPixelCount)
				{
					std::memcpy(imagePtr, canvasState->GetCanvasData(), canvasState->GetTextureByteSize());
					ScreenNavigator::Get().SetScreen(haltedScreen);
				}
				else
					LogUtil::Get().LogMessage("NORMAL", "Image isn't visible! Add more pixels, or make it darker!");
			}
			break;
		}
	}
	return true;
}

void PaintScreen::Draw()
{
	Vec2 winSize = WindowUtil::Get().GetWindowSize();
	float rectSize = winSize.y / 16.0f;
	for(uint8_t i = 0; i < 16; i++)
		DrawUtil::Get().DrawRectangle(11, Vec2(winSize.x - 100.0f, i * rectSize), Vec2(100.0f, rectSize), i * 0.066666f);
	DrawUtil::Get().DrawRectangle(15, Vec2(0.0f), winSize, 0.97f);
}

void PaintScreen::SetClipboardDrawing()
{
	GUICanvasState* canvasState = static_cast<GUICanvasState*>(GUIUtil::Get().GrabState("MainCanvas"));
	uint8_t* tempClipboard = static_cast<uint8_t*>(std::realloc(clipboard, canvasState->GetTextureByteSize()));
	if(tempClipboard == nullptr)
		ErrorUtil::Get().SetError("Memory", "PaintScreen::SetClipboardDrawing Failed to allocate space for clipboard drawing");
	else
	{
		clipboard = tempClipboard;
		std::memcpy(clipboard, canvasState->GetCanvasData(), canvasState->GetTextureByteSize());
		clipboardSize = canvasState->GetTextureSize();
	}
}

void PaintScreen::ClearClipboard()
{
	if(clipboard != nullptr)
	{
		std::free(clipboard);
		clipboard = nullptr;
	}
}

uint8_t* PaintScreen::clipboard = nullptr;
UVec2 PaintScreen::clipboardSize = UVec2(0, 0);
