#pragma once
#include "../ScreenNavigator.hpp"
#include "PropertiesScreen.hpp"
#include <GSEGL/Graphics/GUI/GUIUtil.hpp>
#include <GSEGL/Graphics/GUI/GUIScrollArea.hpp>

enum RuleListAction
{
	RULELISTACTION_REMOVE,
	RULELISTACTION_MOVEUP,
	RULELISTACTION_MOVEDOWN
};

class RuleListScreen : public Screen
{
	public:
		RuleListScreen(const std::shared_ptr<Screen>& initHaltedScreen, RuleList* initRules, AdventureDataPtr initAdventureData = nullptr);
		bool Update();
		void Draw();
		
		void SetRuleAction(uint32_t ruleToActOn, RuleListAction ruleAction);
	private:
		void SaveDataToList();
		void AddRule();
		void SetRuleGUI();
		void SetSelectionGUI(bool isConditionGUI);
		
		enum RuleListState
		{
			RULELIST_INIT,
			RULELIST_UPDATE,
			RULELIST_CONDITION_INIT,
			RULELIST_CONDITION_UPDATE,
			RULELIST_RESULT_INIT,
			RULELIST_RESULT_UPDATE
		} state;
		RuleList* rules;
		std::vector<double> conditionCounters;
		std::vector<double> resultCounters;
		bool conditionsSet;
		bool resultsSet;
		Vec2 pos;
		GUIScrollAreaState* scrollState;
		std::vector<std::tuple<GUIElement*, void*, PropertyType>> extraDataButtons;
		AdventureDataPtr adventureData;
		int32_t ruleToActOn;
		RuleListAction ruleAction;
		std::shared_ptr<Screen> haltedScreen;
};
