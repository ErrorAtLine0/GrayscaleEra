#include "SelectorScreen.hpp"
#include <GSEGL/Graphics/GUI/GUIImageButton.hpp>
#include <GSEGL/Graphics/GUI/GUIImmutableText.hpp>
#include <GSEGL/Graphics/GUI/GUIScrollArea.hpp>
#include <GSEGL/Graphics/GUI/GUITextButton.hpp>
#include <GSEGL/FileUtil.hpp>
#include <GSEGL/LogUtil.hpp>
#include "../PlayerData.hpp"
#include "../BlockData.hpp"
#include "../ArenaData.hpp"
#include "../AdventureData.hpp"
#include "../GlobalMenuData.hpp"
#include "EditNameScreen.hpp"
#include "PlayerMakerScreen.hpp"
#include "AnimatorScreen.hpp"
#include "AdventureMakerScreen.hpp"
#include "LevelMakerScreen.hpp"

SelectorScreen::SelectorScreen(const std::shared_ptr<Screen>& initHaltedScreen, UTF8String* initFileNamePtr, FileType initFileType, void (*initBackgroundFunc)(const std::shared_ptr<void>&), const std::shared_ptr<void>& initBackgroundFuncData):
state(SELECTOR_INIT),
fileToEdit(-1),
returnData(nullptr),
vectorData(nullptr),
dataChosenPtr(initFileNamePtr),
backgroundFunc(initBackgroundFunc),
backgroundFuncData(initBackgroundFuncData),
returnIndexType(SELECTOR_INDEX_INT32),
fileOperation(FILEOPERATION_NONE),
fileType(initFileType),
haltedScreen(initHaltedScreen)
{
	SetPathToSearch();
}

SelectorScreen::SelectorScreen(const std::shared_ptr<Screen>& initHaltedScreen, void* initVectorData, void* initIndexChosenPtr, SelectorIndexType initReturnIndexType, FileType initFileType, const WorldDataPtr& initWorldData, void (*initBackgroundFunc)(const std::shared_ptr<void>&), const std::shared_ptr<void>& initBackgroundFuncData):
state(SELECTOR_INIT),
fileToEdit(-1),
returnData(nullptr),
vectorData(initVectorData),
dataChosenPtr(initIndexChosenPtr),
backgroundFunc(initBackgroundFunc),
backgroundFuncData(initBackgroundFuncData),
returnIndexType(initReturnIndexType),
fileOperation(FILEOPERATION_NONE),
fileType(initFileType),
worldData(initWorldData),
haltedScreen(initHaltedScreen)
{
	SetPathToSearch();
}

bool SelectorScreen::Update()
{
	switch(state)
	{
		case SELECTOR_INIT:
		{
			if(fileOperation == FILEOPERATION_RENAME && !vectorData && !returnName.IsEmpty() && fileToEdit >= 0)
				FileUtil::Get().RenameFile(pathToSearch + UTF8String("/") + (*fileNames)[fileToEdit], pathToSearch + UTF8String("/") + returnName);
			else if(fileOperation == FILEOPERATION_EDIT && !vectorData && returnData && (fileToEdit >= 0 || !returnName.IsEmpty()))
				WriteDataToFile(returnData, fileToEdit >= 0 ? (*fileNames)[fileToEdit] : returnName);
			else if(fileOperation == FILEOPERATION_COPY && !vectorData && fileToEdit >= 0 && !returnName.IsEmpty())
			{
				std::shared_ptr<void> fileToReadTo = AllocateMemory();
				ReadFromFile((*fileNames)[fileToEdit], fileToReadTo.get());
				WriteDataToFile(fileToReadTo, returnName);
			}
			else if(vectorData && fileOperation == FILEOPERATION_EDIT && fileToEdit >= 0 && returnData)
				WriteToVector(returnData, fileToEdit);
			else if(vectorData && !returnName.IsEmpty())
				AddFileToVector(returnName);
			returnName.Clear();
			fileToEdit = -1;
			returnData = nullptr;
			fileOperation = FILEOPERATION_NONE;
			if(vectorData)
				fileNames = nullptr;
			else
				fileNames = FileUtil::Get().GetFilesInPath(pathToSearch);
			GUIUtil::Get().ClearAll();
			GUIUtil::Get().AddElement("Back", (new GUIImageButton("BackIcon", Vec2(0.0f), Vec2(0.0f), Vec2(0.0f), Vec2(40.0f)))->PushUp());
			GUIUtil::Get().AddElement("FileList", new GUIScrollArea(Vec2(0.0f), Vec2(0.0f), Vec2(1.0f), Vec2(0.0f), 20.0f, 60.0f));
			GUIUtil::Get().AddElement("BottomButton", (new GUITextButton(GetBottomButtonName(), Vec2(0.5f, 0.0f), Vec2(-350.0f, 10.0f), Vec2(0.0f), Vec2(700.0f, 50.0f)))->PushUp());
			scrollState = static_cast<GUIScrollAreaState*>(GUIUtil::Get().GrabState("FileList"));
			PopulateScreen();
			state = SELECTOR_UPDATE;
			break;
		}
		case SELECTOR_UPDATE:
		{
			if(GUIUtil::Get().GrabState("BottomButton") && static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("BottomButton"))->buttonState == GUIBUTTON_PRESSED)
			{
				if(!vectorData)
				{
					returnData = AllocateMemory();
					fileOperation = FILEOPERATION_EDIT;
					ScreenNavigator::Get().SetScreen(std::make_shared<EditNameScreen>(GetEditScreen(returnData), &returnName, (*fileNames)));
				}
				else
				{
					ScreenNavigator::Get().SetScreen(std::make_shared<SelectorScreen>(ScreenNavigator::Get().GetScreen(), &returnName, fileType));
				}
				state = SELECTOR_INIT;
				return true;
			}
			for(uint32_t i = 0; i < scrollState->elements.size(); i++)
			{
				if(!vectorData)
				{
					if(i % 6 != 1 && static_cast<GUIButtonState*>(scrollState->elements[i]->GrabState())->buttonState == GUIBUTTON_PRESSED)
					{
						if(i % 6 == 0) // Is image
						{
							if(dataChosenPtr)
							{
								*static_cast<UTF8String*>(dataChosenPtr) = (*fileNames)[i/6];
								ScreenNavigator::Get().SetScreen(haltedScreen);
							}
							else
							{
								EditFile(i/6);
								state = SELECTOR_INIT;
							}
							return true;
						}
						else if(i % 6 == 2) // Is an edit button
						{
							EditFile(i/6);
							state = SELECTOR_INIT;
							return true;
						}
						else if(i % 6 == 3) // Is a rename button
						{
							ScreenNavigator::Get().SetScreen(std::make_shared<EditNameScreen>(ScreenNavigator::Get().GetScreen(), &returnName, *fileNames));
							fileToEdit = i/6;
							fileOperation = FILEOPERATION_RENAME;
							state = SELECTOR_INIT;
							return true;
						}
						else if(i % 6 == 4) // Is a copy button
						{
							ScreenNavigator::Get().SetScreen(std::make_shared<EditNameScreen>(ScreenNavigator::Get().GetScreen(), &returnName, *fileNames));
							fileToEdit = i/6;
							fileOperation = FILEOPERATION_COPY;
							state = SELECTOR_INIT;
							return true;
						}
						else if(i % 6 == 5) // Is a remove button
						{
							FileUtil::Get().RemoveFile(pathToSearch + UTF8String("/") + (*fileNames)[i/6]);
							fileNames->erase(fileNames->begin() + i/6);
							PopulateScreen();
							break;
						}
					}
				}
				else
				{
					if(static_cast<GUIButtonState*>(scrollState->elements[i]->GrabState())->buttonState == GUIBUTTON_PRESSED)
					{
						if(i % 3 == 0) // Is image
						{
							if(dataChosenPtr)
								SetReturnIndex(i/3);
							ScreenNavigator::Get().SetScreen(haltedScreen);
							return true;
						}
						else if(i % 3 == 1) // Is remove button
						{
							if(worldData)
							{
								if(worldData->GetType() == WORLD_ARENA)
								{
									if(fileType == FILETYPE_BLOCK)
										std::static_pointer_cast<ArenaData>(worldData)->RemoveBlock((i/3)+1);
								}
								else if(worldData->GetType() == WORLD_ADVENTURE)
								{
									if(fileType == FILETYPE_BLOCK)
										std::static_pointer_cast<AdventureData>(worldData)->RemoveBlock((i/3)+1);
									else if(fileType == FILETYPE_PLAYER)
										std::static_pointer_cast<AdventureData>(worldData)->RemoveNPC((i/3)+1);
								}
							}
							else
							{
								switch(fileType)
								{
									case FILETYPE_BLOCK:
									{
										std::vector<BlockData>* blockDataPtr = static_cast<std::vector<BlockData>*>(vectorData);
										blockDataPtr->erase(blockDataPtr->begin() + (i/3));
										break;
									}
									case FILETYPE_PLAYER:
									{
										std::vector<PlayerData>* playerDataPtr = static_cast<std::vector<PlayerData>*>(vectorData);
										playerDataPtr->erase(playerDataPtr->begin() + (i/3));
										break;
									}
									case FILETYPE_ARENA:
									{
										std::vector<ArenaData>* arenaDataPtr = static_cast<std::vector<ArenaData>*>(vectorData);
										arenaDataPtr->erase(arenaDataPtr->begin() + (i/3));
										break;
									}
									case FILETYPE_ADVENTURE:
									{
										std::vector<AdventureData>* adventureDataPtr = static_cast<std::vector<AdventureData>*>(vectorData);
										adventureDataPtr->erase(adventureDataPtr->begin() + (i/3));
										break;
									}
								}
							}
							PopulateScreen();
							break;
						}
						else if(i % 3 == 2) // Is Edit Button
						{
							returnData = ReadFromVector(i/3);
							fileToEdit = i/3;
							fileOperation = FILEOPERATION_EDIT;
							ScreenNavigator::Get().SetScreen(GetEditScreen(returnData));
							state = SELECTOR_INIT;
							return true;
						}
					}
				}
			}
			if(WindowUtil::Get().IsWindowResized())
				PopulateScreen();
			if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Back"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(haltedScreen);
				return true;
			}
			if(backgroundFunc)
				backgroundFunc(backgroundFuncData);
			break;
		}
	}
	return true;
}

void SelectorScreen::PopulateScreen()
{
	scrollState->ClearAll();
	float GUISize = GUIUtil::Get().GetGUISize();
	Vec2 winSize = WindowUtil::Get().GetWindowSize();
	uint32_t objectsPerRow = winSize.x / (265.0f * GUISize);
	if(objectsPerRow == 0) objectsPerRow = 1;
	if(!vectorData)
	{
		for(uint32_t i = 0; i < fileNames->size(); i++)
		{
			Vec2 bottomLeftCorner = Vec2(20.0f + 275.0f*(i % objectsPerRow), -290.0f*((i / objectsPerRow)+1) + 20.0f);
			AddGUIImage(ReadImageFromFile((*fileNames)[i]).get(), bottomLeftCorner);
			scrollState->AddElement(new GUIImmutableText((*fileNames)[i], Vec2(0.0f), Vec2(bottomLeftCorner.x, bottomLeftCorner.y + 240.0f), 0.0f, 12.0f));
			scrollState->AddElement(new GUIImageButton("PencilIcon", Vec2(0.0f), Vec2(bottomLeftCorner.x + 20.0f, bottomLeftCorner.y), Vec2(0.0f), Vec2(40.0f)));
			scrollState->AddElement(new GUIImageButton("InputIcon", Vec2(0.0f), Vec2(bottomLeftCorner.x + 60.0f, bottomLeftCorner.y), Vec2(0.0f), Vec2(40.0f)));
			scrollState->AddElement(new GUIImageButton("CopyIcon", Vec2(0.0f), Vec2(bottomLeftCorner.x + 100.0f,  bottomLeftCorner.y), Vec2(0.0f), Vec2(40.0f)));
			scrollState->AddElement(new GUIImageButton("RemoveIcon", Vec2(0.0f), Vec2(bottomLeftCorner.x + 140.0f, bottomLeftCorner.y), Vec2(0.0f), Vec2(40.0f)));
		}
	}
	else
	{
		uint32_t vectorSize = 0;
		switch(fileType)
		{
			case FILETYPE_BLOCK: { vectorSize = static_cast<std::vector<BlockData>*>(vectorData)->size(); break; }
			case FILETYPE_PLAYER: { vectorSize = static_cast<std::vector<PlayerData>*>(vectorData)->size(); break; }
			case FILETYPE_ADVENTURE: { vectorSize = static_cast<std::vector<AdventureData>*>(vectorData)->size(); break; }
			case FILETYPE_ARENA: { vectorSize = static_cast<std::vector<ArenaData>*>(vectorData)->size(); break; }
		}
		for(uint32_t i = 0; i < vectorSize; i++)
		{
			Vec2 bottomLeftCorner = Vec2(20.0f + 275.0f*(i % objectsPerRow), -290.0f*((i / objectsPerRow)+1) + 20.0f);
			void* fileToAdd = GetFileFromVector(i);
			AddGUIImage(GetImageFromFile(fileToAdd), bottomLeftCorner);
			scrollState->AddElement(new GUIImageButton("RemoveIcon", Vec2(0.0f), Vec2(bottomLeftCorner.x + 160.0f, bottomLeftCorner.y), Vec2(0.0f), Vec2(40.0f)));
			scrollState->AddElement(new GUIImageButton("PencilIcon", Vec2(0.0f), Vec2(bottomLeftCorner.x, bottomLeftCorner.y), Vec2(0.0f), Vec2(40.0f)));
		}
	}
}

void SelectorScreen::AddGUIImage(const Frame* frame, const Vec2& bottomLeftCorner)
{
	uint32_t frameSize = 0;
	switch(fileType)
	{
		case FILETYPE_BLOCK:
		{
			frameSize = BLOCK_TEXTURE_SIZE;
			break;
		}
		case FILETYPE_PLAYER:
		{
			frameSize = PLAYER_TEXTURE_SIZE;
			break;
		}
		case FILETYPE_ADVENTURE:
		{
			frameSize = ADVENTURE_TEXTURE_SIZE;
			break;
		}
		case FILETYPE_ARENA:
		{
			frameSize = ARENA_TEXTURE_SIZE;
			break;
		}
	}
	scrollState->AddElement(new GUIImageButton(frame ? std::make_shared<Texture>(frame->GetData(), frameSize, frameSize) : nullptr, Vec2(0.0f), Vec2(bottomLeftCorner.x, bottomLeftCorner.y + 40.0f), Vec2(0.0f), Vec2(200.0f)));
}

void SelectorScreen::EditFile(uint32_t index)
{
	returnData = AllocateMemory();
	ReadFromFile((*fileNames)[index], returnData.get());
	fileToEdit = index;
	ScreenNavigator::Get().SetScreen(GetEditScreen(returnData));
	fileOperation = FILEOPERATION_EDIT;
}

std::shared_ptr<Screen> SelectorScreen::GetEditScreen(const std::shared_ptr<void> fileToEdit)
{
	switch(fileType)
	{
		case FILETYPE_BLOCK:
		{
			return std::make_shared<AnimatorScreen>(ScreenNavigator::Get().GetScreen(), BLOCK_MAX_FRAMES, &std::static_pointer_cast<BlockData>(fileToEdit)->animation, UVec2(BLOCK_TEXTURE_SIZE), 0, &std::static_pointer_cast<BlockData>(fileToEdit)->propList);
		}
		case FILETYPE_PLAYER:
		{
			return std::make_shared<PlayerMakerScreen>(ScreenNavigator::Get().GetScreen(), std::static_pointer_cast<PlayerData>(fileToEdit));
		}
		case FILETYPE_ADVENTURE:
		{
			 return std::make_shared<AdventureMakerScreen>(ScreenNavigator::Get().GetScreen(), std::static_pointer_cast<AdventureData>(fileToEdit));
		}
		case FILETYPE_ARENA:
		{
			return std::make_shared<LevelMakerScreen>(ScreenNavigator::Get().GetScreen(), &std::static_pointer_cast<ArenaData>(fileToEdit)->level, std::static_pointer_cast<WorldData>(fileToEdit));
		}
	}
	return nullptr;
}

bool SelectorScreen::AddFileToVector(const UTF8String& fileName)
{
	switch(fileType)
	{
		case FILETYPE_BLOCK:
		{
			std::vector<BlockData>* blockVec = static_cast<std::vector<BlockData>*>(vectorData);
			blockVec->emplace_back();
			if(!ReadFromFile(fileName, &blockVec->back())) return false;
			break;
		}
		case FILETYPE_PLAYER:
		{
			std::vector<PlayerData>* playerVec = static_cast<std::vector<PlayerData>*>(vectorData);
			playerVec->emplace_back();
			if(!ReadFromFile(fileName, &playerVec->back())) return false;
			break;
		}
		case FILETYPE_ADVENTURE:
		{
			std::vector<AdventureData>* adventureVec = static_cast<std::vector<AdventureData>*>(vectorData);
			adventureVec->emplace_back();
			if(!ReadFromFile(fileName, &adventureVec->back())) return false;
			break;
		}
		case FILETYPE_ARENA:
		{
			std::vector<ArenaData>* arenaVec = static_cast<std::vector<ArenaData>*>(vectorData);
			arenaVec->emplace_back();
			if(!ReadFromFile(fileName, &arenaVec->back())) return false;
			break;
		}
	}
	return true;
}

bool SelectorScreen::ReadFromFile(const UTF8String& fileName, void* fileToReadTo)
{
	std::shared_ptr<InMemBitStream> inStream = FileUtil::Get().ReadFromFile(pathToSearch + UTF8String("/") + fileName);
	if(!inStream) return false;
	switch(fileType)
	{
		case FILETYPE_BLOCK:
		{
			BlockData* blockDataPtr = static_cast<BlockData*>(fileToReadTo);
			if(!blockDataPtr->ReadFile(*inStream)) return false;
			break;
		}
		case FILETYPE_PLAYER:
		{
			PlayerData* playerDataPtr = static_cast<PlayerData*>(fileToReadTo);
			if(!playerDataPtr->ReadFile(*inStream)) return false;
			break;
		}
		case FILETYPE_ADVENTURE:
		{
			AdventureData* adventureDataPtr = static_cast<AdventureData*>(fileToReadTo);
			if(!adventureDataPtr->Read(*inStream)) return false;
			break;
		}
		case FILETYPE_ARENA:
		{
			ArenaData* arenaDataPtr = static_cast<ArenaData*>(fileToReadTo);
			if(!arenaDataPtr->Read(*inStream)) return false;
			break;
		}
	}
	return true;
}

const Frame* SelectorScreen::GetImageFromFile(void* fileToReadFrom)
{
	switch(fileType)
	{
		case FILETYPE_BLOCK:
		{
			BlockData* blockDataPtr = static_cast<BlockData*>(fileToReadFrom);
			return blockDataPtr->animation.empty() ? nullptr : &blockDataPtr->animation[0];
		}
		case FILETYPE_PLAYER:
		{
			PlayerData* playerDataPtr = static_cast<PlayerData*>(fileToReadFrom);
			return playerDataPtr->idleAnimation.empty() ? nullptr : &playerDataPtr->idleAnimation[0];
		}
		case FILETYPE_ADVENTURE:
		{
			return &static_cast<AdventureData*>(fileToReadFrom)->image;
		}
		case FILETYPE_ARENA:
		{
			return &static_cast<ArenaData*>(fileToReadFrom)->image;
		}
	}
	return nullptr;
}

std::shared_ptr<Frame> SelectorScreen::ReadImageFromFile(const UTF8String& fileName)
{
	std::shared_ptr<InMemBitStream> inStream = FileUtil::Get().ReadFromFile(pathToSearch + UTF8String("/") + fileName);
	if(!inStream) return nullptr;
	switch(fileType)
	{
		case FILETYPE_BLOCK:
		{
			return BlockData::ReadFileImage(*inStream);
		}
		case FILETYPE_PLAYER:
		{
			return PlayerData::ReadFileImage(*inStream);
		}
		case FILETYPE_ADVENTURE:
		{
			return AdventureData::ReadImage(*inStream);
		}
		case FILETYPE_ARENA:
		{
			return ArenaData::ReadImage(*inStream);
		}
	}
	return nullptr;
}

void SelectorScreen::WriteToVector(const std::shared_ptr<void>& fileToWrite, uint32_t index)
{
	switch(fileType)
	{
		case FILETYPE_BLOCK:
		{
			*static_cast<BlockData*>(GetFileFromVector(index)) = *std::static_pointer_cast<BlockData>(fileToWrite);
			break;
		}
		case FILETYPE_PLAYER:
		{
			*static_cast<PlayerData*>(GetFileFromVector(index)) = *std::static_pointer_cast<PlayerData>(fileToWrite);
			break;
		}
		case FILETYPE_ADVENTURE:
		{
			*static_cast<AdventureData*>(GetFileFromVector(index)) = *std::static_pointer_cast<AdventureData>(fileToWrite);
			break;
		}
		case FILETYPE_ARENA:
		{
			*static_cast<ArenaData*>(GetFileFromVector(index)) = *std::static_pointer_cast<ArenaData>(fileToWrite);
			break;
		}
	}
}

std::shared_ptr<void> SelectorScreen::ReadFromVector(uint32_t index)
{
	switch(fileType)
	{
		case FILETYPE_BLOCK:
		{
			return std::make_shared<BlockData>(*static_cast<BlockData*>(GetFileFromVector(index)));
		}
		case FILETYPE_PLAYER:
		{
			return std::make_shared<PlayerData>(*static_cast<PlayerData*>(GetFileFromVector(index)));
		}
		case FILETYPE_ADVENTURE:
		{
			return std::make_shared<AdventureData>(*static_cast<AdventureData*>(GetFileFromVector(index)));
		}
		case FILETYPE_ARENA:
		{
			return std::make_shared<ArenaData>(*static_cast<ArenaData*>(GetFileFromVector(index)));
		}
	}
	return nullptr;
}

void* SelectorScreen::GetFileFromVector(uint32_t index)
{
	switch(fileType)
	{
		case FILETYPE_BLOCK: { return &((*static_cast<std::vector<BlockData>*>(vectorData))[index]); }
		case FILETYPE_PLAYER: { return &((*static_cast<std::vector<PlayerData>*>(vectorData))[index]); }
		case FILETYPE_ADVENTURE: { return &((*static_cast<std::vector<AdventureData>*>(vectorData))[index]); }
		case FILETYPE_ARENA: { return &((*static_cast<std::vector<ArenaData>*>(vectorData))[index]); }
	}
	return nullptr;
}

const char* SelectorScreen::GetBottomButtonName()
{
	switch(fileType)
	{
		case FILETYPE_BLOCK:
		{
			if(vectorData)
				return "Add Block";
			else
				return "Create Block";
		}
		case FILETYPE_PLAYER:
		{
			if(vectorData)
				return "Add Player";
			else
				return "Create Player";
		}
		case FILETYPE_ADVENTURE:
		{
			if(vectorData)
				return "Add Adventure";
			else
				return "Create Adventure";
		}
		case FILETYPE_ARENA:
		{
			if(vectorData)
				return "Add Arena";
			else
				return "Create Arena";
		}
	}
	return nullptr;
}

void SelectorScreen::WriteDataToFile(const std::shared_ptr<void>& fileData, const UTF8String& fileName)
{
	OutMemBitStream outStream;
	switch(fileType)
	{
		case FILETYPE_BLOCK:
		{
			BlockDataPtr blockData = std::static_pointer_cast<BlockData>(fileData);
			blockData->WriteFile(outStream);
			break;
		}
		case FILETYPE_PLAYER:
		{
			PlayerDataPtr playerData = std::static_pointer_cast<PlayerData>(fileData);
			playerData->WriteFile(outStream);
			break;
		}
		case FILETYPE_ADVENTURE:
		{
			AdventureDataPtr adventureData = std::static_pointer_cast<AdventureData>(fileData);
			adventureData->Write(outStream);
			break;
		}
		case FILETYPE_ARENA:
		{
			ArenaDataPtr arenaData = std::static_pointer_cast<ArenaData>(fileData);
			arenaData->Write(outStream);
			break;
		}
	}
	if(FileUtil::Get().WriteToFile(pathToSearch + UTF8String("/") + fileName, outStream))
		LogUtil::Get().LogMessage("NORMAL", "Data successfully saved to file");
}

std::shared_ptr<void> SelectorScreen::AllocateMemory()
{
	switch(fileType)
	{
		case FILETYPE_BLOCK: { return std::make_shared<BlockData>(); }
		case FILETYPE_PLAYER: { return std::make_shared<PlayerData>(); }
		case FILETYPE_ADVENTURE: { return std::make_shared<AdventureData>(); }
		case FILETYPE_ARENA: { return std::make_shared<ArenaData>(); }
	}
	return nullptr;
}

void SelectorScreen::SetReturnIndex(int32_t index)
{
	switch(returnIndexType)
	{
		case SELECTOR_INDEX_UINT8: { *static_cast<uint8_t*>(dataChosenPtr) = index + 1; break; }
		case SELECTOR_INDEX_UINT16: { *static_cast<uint16_t*>(dataChosenPtr) = index + 1; break; }
		case SELECTOR_INDEX_UINT32: { *static_cast<uint32_t*>(dataChosenPtr) = index + 1; break; }
		case SELECTOR_INDEX_UINT64: { *static_cast<uint64_t*>(dataChosenPtr) = index + 1; break; }
		case SELECTOR_INDEX_INT8: { *static_cast<int8_t*>(dataChosenPtr) = index; break; }
		case SELECTOR_INDEX_INT16: { *static_cast<int16_t*>(dataChosenPtr) = index; break; }
		case SELECTOR_INDEX_INT32: { *static_cast<int32_t*>(dataChosenPtr) = index; break; }
		case SELECTOR_INDEX_INT64: { *static_cast<int64_t*>(dataChosenPtr) = index; break; }
	}
}

void SelectorScreen::SetPathToSearch()
{
	switch(fileType)
	{
		case FILETYPE_BLOCK: { pathToSearch = "blocks"; break; }
		case FILETYPE_PLAYER: { pathToSearch = "players"; break; }
		case FILETYPE_ADVENTURE: { pathToSearch = "adventures"; break; }
		case FILETYPE_ARENA: { pathToSearch = "arenas"; break; }
	}
}

void SelectorScreen::Draw()
{
	GlobalMenuData::Get().DrawBackground();
}
