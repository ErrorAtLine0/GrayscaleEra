#pragma once
#include "../ScreenNavigator.hpp"
#include <GSEGL/Network/SocketUtil.hpp>
#include <GSEGL/LobbyServerConstants.hpp>

class MainMenuScreen : public Screen
{
	public:
		MainMenuScreen();
		bool Update();
		void Draw();
	private:
		enum MainMenuState
		{
			MAINMENU_INIT,
			MAINMENU_UPDATE
		} state;
		UDPSocketPtr sock;
		SocketAddressPtr addr;
		bool updateStatusPending;
};
