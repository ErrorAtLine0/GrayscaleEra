#pragma once
#include "../ScreenNavigator.hpp"
#include "../NetworkManager.hpp"
#include "../GameSandbox.hpp"
#include "../WorldData.hpp"
#include <unordered_set>
#include <GSEGL/Graphics/Texture.hpp>
#include <GSEGL/Graphics/Math.hpp>

class GameScreen : public Screen
{
	public:
		GameScreen(const std::shared_ptr<Screen>& initHaltedScreen, const GameSandboxProperties& initGameProperties, const WorldDataPtr& initWorldData, PlayerID initCurrentPlayerID, const std::shared_ptr<NetworkManager>& initNetMan);
		~GameScreen();
		bool Update();
		void Draw();
	private:
		void DrawAnnouncement();
		void PlaceEndGameGUI();
		enum GameState
		{
			GAME_INIT,
			GAME_UPDATELOOP,
			GAME_ENDINIT,
			GAME_ENDLOOP
		} state;
		std::shared_ptr<NetworkManager> netMan;
		std::vector<PlayerInfoPtr> endGamePlacement;
		double endGameAnimationStart;
		bool paused;
		
		Vec2 cameraPos;
		Vec2 newestCameraPos; // For smooth interpolation/movement of camera
		float cameraZoom;
		int32_t cameraRoom;
		
		double currentAnnouncementTime;
		UTF8String currentAnnouncement;
		UTF8String oldAnnouncement;
		
		PlayerID currentPlayerID;
		std::shared_ptr<Screen> haltedScreen;
};
