#include "PublicPlayScreen.hpp"
#include "PropertiesScreen.hpp"
#include "SelectorScreen.hpp"
#include "WaitScreen.hpp"
#include <GSEGL/Graphics/GUI/GUITextEntry.hpp>
#include <GSEGL/Graphics/GUI/GUITextButton.hpp>
#include <GSEGL/Graphics/GUI/GUIImageButton.hpp>
#include <GSEGL/Graphics/GUI/GUIImmutableText.hpp>
#include <GSEGL/Graphics/GUI/GUICounter.hpp>
#include <GSEGL/LogUtil.hpp>
#include "../NetworkManagerServer.hpp"
#include "../NetworkManagerClient.hpp"
#include "../GlobalSettingsUtil.hpp"
#include "../GlobalMenuData.hpp"
#define FAILURES_BEFORE_HOST 5

PublicPlayScreen::PublicPlayScreen(const std::shared_ptr<Screen>& initHaltedScreen):
state(PUBLICPLAY_INIT),
data(std::make_shared<PublicPlayData>()),
haltedScreen(initHaltedScreen)
{
	data->userName = GlobalSettingsUtil::Get().settings.GetString(1);
	data->playerCount = 2;
	data->netMan = nullptr;
}

bool PublicPlayWait(const std::shared_ptr<void>& ptr)
{
	std::shared_ptr<PublicPlayData> data = std::static_pointer_cast<PublicPlayData>(ptr);
	UTF8String queueName = UTF8String::IntToStr(data->playerCount);
	if(!data->netMan && data->failures == FAILURES_BEFORE_HOST)
	{
		PropertiesList serverSettings(NetworkManagerServer::serverSettingsList);
		static_cast<IntProperty*>(serverSettings.GetPropAt(0))->data = data->playerCount;
		WorldDataPtr arenaData = WorldData::ReadWorldFile(data->arenaName, WORLD_ARENA);
		data->netMan = std::make_shared<NetworkManagerServer>();
		if(arenaData)
			std::static_pointer_cast<NetworkManagerServer>(data->netMan)->Init(data->userName, queueName, arenaData, serverSettings, false);
		else
		{
			LogUtil::Get().LogMessage("NORMAL", "Could not load arena");
			return false;
		}
	}
	else if(data->netMan && !data->netMan->Update())
	{
		++(data->failures);
		if(data->failures > FAILURES_BEFORE_HOST) // Couldnt even host a server
		{
			LogUtil::Get().LogMessage("NORMAL", "Could not host server");
			return false;
		}
		else if(data->failures == FAILURES_BEFORE_HOST) // Could not connect to any server, create our own
		{
			LogUtil::Get().LogMessage("NORMAL", "Could not connect to anyone. Creating server.");
			data->netMan = nullptr;
			std::static_pointer_cast<WaitScreen>(ScreenNavigator::Get().GetScreen())->SetToInit();
			ScreenNavigator::Get().SetScreen(std::make_shared<SelectorScreen>(ScreenNavigator::Get().GetScreen(), &data->arenaName, FILETYPE_ARENA));
		}
		else // Try another server
		{
			data->netMan = std::make_shared<NetworkManagerClient>();
			std::static_pointer_cast<NetworkManagerClient>(data->netMan)->Init(data->userName, queueName, WORLD_ARENA);
		}
	}
	return true;
}

bool PublicPlayScreen::Update()
{
	switch(state)
	{
		case PUBLICPLAY_INIT:
		{
			data->failures = 0;
			GUIUtil::Get().ClearAll();
			GUIUtil::Get().AddElement("User Name", new GUITextEntry("User Name", Vec2(0.5f, 1.0f), Vec2(-360.0f, -75.0f), Vec2(0.0f), Vec2(720.0f, 40.0f), 16, &data->userName));
			static_cast<GUITextEntryState*>(GUIUtil::Get().GrabState("User Name"))->text = data->userName;
			GUIUtil::Get().AddElement("Player Count Text", new GUIImmutableText("Player Count", Vec2(0.5f, 1.0f), Vec2(-360.0f, -175.0f), 0.0f, 40.0f));
			GUIUtil::Get().AddElement("Player Count", new GUICounter(Vec2(0.5f, 1.0f), Vec2(-360.0f, -225.0f), Vec2(0.0f), Vec2(720.0f, 40.0f), 2, 2, 8, 2, &data->playerCount));
			static_cast<GUICounterState*>(GUIUtil::Get().GrabState("Player Count"))->SetVal(data->playerCount);
			GUIUtil::Get().AddElement("Play", new GUITextButton("Play", Vec2(0.5f, 1.0f), Vec2(-360.0f, -325.0f), Vec2(0.0f), Vec2(720.0f, 40.0f)));
			GUIUtil::Get().AddElement("Back", new GUIImageButton("BackIcon", Vec2(0.0f), Vec2(0.0f), Vec2(0.0f), Vec2(40.0f)));
			state = PUBLICPLAY_UPDATE;
			break;
		}
		case PUBLICPLAY_UPDATE:
		{
			if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Play"))->buttonState == GUIBUTTON_PRESSED)
			{
				ExtractInfoToVars();
				if(!data->userName.IsEmpty())
				{
					UTF8String queueName = UTF8String::IntToStr(data->playerCount);
					ErrorUtil::Get().ClearErrors();
					data->netMan = std::make_shared<NetworkManagerClient>();
					bool success = std::static_pointer_cast<NetworkManagerClient>(data->netMan)->Init(data->userName, queueName, WORLD_ARENA);
					if(success)
					{
						ScreenNavigator::Get().SetScreen(std::make_shared<WaitScreen>(ScreenNavigator::Get().GetScreen(), PublicPlayWait, std::static_pointer_cast<void>(data)));
						state = PUBLICPLAY_INIT;
						break;
					}
				}
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Back"))->buttonState == GUIBUTTON_PRESSED)
				ScreenNavigator::Get().SetScreen(haltedScreen);
			break;
		}
	}
	return true;
}

void PublicPlayScreen::ExtractInfoToVars()
{
	GUIUtil::Get().UpdateValPtrs();
}

void PublicPlayScreen::Draw()
{
	GlobalMenuData::Get().DrawBackground();
}
