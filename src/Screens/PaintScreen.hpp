#pragma once
#include "../ScreenNavigator.hpp"
#include <GSEGL/Graphics/Math.hpp>

class PaintScreen : public Screen
{
	public:
		PaintScreen(const std::shared_ptr<Screen>& initHaltedScreen, uint8_t* initImagePtr, const UVec2& initImageDimensions, uint32_t initMinDarkPixelCount = 0);
		~PaintScreen();
		bool Update();
		void Draw();
		static void ClearClipboard();
	private:
		enum PaintState
		{
			PAINT_INIT,
			PAINT_UPDATELOOP
		} state;
		void SetClipboardDrawing();
		UVec2 imageDimensions;
		uint8_t* lastStep;
		bool isDrawing;
		Vec2 lastCursorDrawPos;
		uint8_t* imagePtr;
		uint32_t minDarkPixelCount;
		std::shared_ptr<Screen> haltedScreen;
		static uint8_t* clipboard;
		static UVec2 clipboardSize;
};
