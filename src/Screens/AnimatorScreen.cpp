#include "AnimatorScreen.hpp"
#include "PaintScreen.hpp"
#include <GSEGL/Graphics/GUI/GUICanvas.hpp>
#include <GSEGL/Graphics/GUI/GUIImageButton.hpp>
#include <GSEGL/Graphics/GUI/GUISlider.hpp>
#include <GSEGL/Graphics/GUI/GUIImmutableText.hpp>
#include "PropertiesScreen.hpp"
#include <GSEGL/TimeUtil.hpp>
#include <GSEGL/LogUtil.hpp>
#include <cstring>

AnimatorScreen::AnimatorScreen(const std::shared_ptr<Screen>& initHaltedScreen, uint32_t initMaxFrames, std::vector<Frame>* initFramesPtr, const UVec2& initImageDimensions, uint32_t initMinDarkPixelCount, PropertiesList* initPropList):
state(ANIMATOR_INIT),
maxFrames(initMaxFrames),
currentFrame(0),
currentFrameInAnimation(0),
timeSinceLastFrame(0.0),
framesPtr(initFramesPtr),
imageDimensions(initImageDimensions),
minDarkPixelCount(initMinDarkPixelCount),
propList(initPropList),
haltedScreen(initHaltedScreen)
{}

bool AnimatorScreen::Update()
{
	switch(state)
	{
		case ANIMATOR_INIT:
		{
			GUIUtil::Get().ClearAll();
			// Used to view frames
			GUIUtil::Get().AddElement("PreviousFrame", new GUIImageButton("ArrowButton", Vec2(0.5f, 0.5f), Vec2(-330.0f, -50.0f), Vec2(0.0f), Vec2(100.0f), BVec2(true, false)));
			GUIUtil::Get().AddElement("FrameView", new GUICanvas(Vec2(0.5f, 0.5f), Vec2(-200.0f, -200.0f), Vec2(0.0f), Vec2(400.0f, 400.0f), imageDimensions));
			GUIUtil::Get().AddElement("NextFrame", new GUIImageButton("ArrowButton", Vec2(0.5f, 0.5f), Vec2(230.0f, -50.0f), Vec2(0.0f), Vec2(100.0f)));
			// Used to edit frames (above frame view)
			GUIUtil::Get().AddElement("EditButton", new GUIImageButton("PencilIcon", Vec2(0.5f, 1.0f), Vec2(80.0f, -90.0f), Vec2(0.0f), Vec2(80.0f)));
			GUIUtil::Get().AddElement("RemoveButton", new GUIImageButton("RemoveIcon", Vec2(0.5f, 1.0f), Vec2(-40.0f, -90.0f), Vec2(0.0f), Vec2(80.0f)));
			GUIUtil::Get().AddElement("AddButton", new GUIImageButton("AddIcon", Vec2(0.5f, 1.0f), Vec2(-160.0f, -90.0f), Vec2(0.0f), Vec2(80.0f)));
			
			GUIUtil::Get().AddElement("FinalPreview", new GUICanvas(Vec2(0.0f, 1.0f), Vec2(15.0f, -110.0f), Vec2(0.0f), Vec2(100.0f, 100.0f), imageDimensions));
			GUIUtil::Get().AddElement("TimeSlider", new GUISlider(Vec2(0.15f, 0.0f), Vec2(0.0f, 25.0f), Vec2(0.7f, 0.0f), Vec2(0.0f, 50.0f), 500, 20, 1000, 20));
			GUIUtil::Get().AddElement("FrameTimeText", new GUIImmutableText("Frame Time (ms)", Vec2(0.42f, 0.0f), Vec2(0.0f, 60.0f), 0.02f, 0.0f));
			GUIUtil::Get().AddElement("Back", new GUIImageButton("BackIcon", Vec2(1.0f, 0.0f), Vec2(-40.0f, 0.0f), Vec2(0.0f), Vec2(40.0f)));
			if(propList)
				GUIUtil::Get().AddElement("PropListSettings", new GUIImageButton("SettingsIcon", Vec2(1.0f, 0.0f), Vec2(-40.0f, 40.0f), Vec2(0.0f), Vec2(40.0f)));
			if(!framesPtr->empty()) // Editting animation
				static_cast<GUISliderState*>(GUIUtil::Get().GrabState("TimeSlider"))->SetVal((*framesPtr)[currentFrame].GetMsLength());
			UpdateFrameCanvas();
			UpdateFinalCanvas();
			state = ANIMATOR_UPDATELOOP;
			break;
		}
		case ANIMATOR_UPDATELOOP:
		{
			GUICanvasState* canvasState = static_cast<GUICanvasState*>(GUIUtil::Get().GrabState("FrameView"));
			if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("NextFrame"))->buttonState == GUIBUTTON_PRESSED)
			{
				if(framesPtr->size() > 1 && currentFrame < framesPtr->size()-1)
				{
					++currentFrame;
					static_cast<GUISliderState*>(GUIUtil::Get().GrabState("TimeSlider"))->SetVal((*framesPtr)[currentFrame].GetMsLength());
					UpdateFrameCanvas();
				}
				else
					LogUtil::Get().LogMessage("NORMAL", "Reached last frame. Cannot go forward further.");
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("PreviousFrame"))->buttonState == GUIBUTTON_PRESSED)
			{
				if(currentFrame > 0)
				{
					--currentFrame;
					static_cast<GUISliderState*>(GUIUtil::Get().GrabState("TimeSlider"))->SetVal((*framesPtr)[currentFrame].GetMsLength());
					UpdateFrameCanvas();
				}
				else
					LogUtil::Get().LogMessage("NORMAL", "Reached first frame. Cannot go back further.");
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("AddButton"))->buttonState == GUIBUTTON_PRESSED)
			{
				if(framesPtr->size() != maxFrames)
				{
					if(framesPtr->size() != 0)
						++currentFrame;
					framesPtr->emplace(framesPtr->begin() + currentFrame, canvasState->GetTextureByteSize(), static_cast<GUISliderState*>(GUIUtil::Get().GrabState("TimeSlider"))->GetVal());
					UpdateFrameCanvas();
					UpdateFinalCanvas();
				}
				else
					LogUtil::Get().LogMessage("NORMAL", "Reached frame limit. Cannot add more frames.");
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("RemoveButton"))->buttonState == GUIBUTTON_PRESSED)
			{
				if(framesPtr->size() > 1)
				{
					framesPtr->erase(framesPtr->begin() + currentFrame);
					if(currentFrame == framesPtr->size())
						--currentFrame;
					static_cast<GUISliderState*>(GUIUtil::Get().GrabState("TimeSlider"))->SetVal((*framesPtr)[currentFrame].GetMsLength());
					UpdateFrameCanvas();
					UpdateFinalCanvas();
				}
				else
					LogUtil::Get().LogMessage("NORMAL", "Cannot remove frame.");
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("EditButton"))->buttonState == GUIBUTTON_PRESSED)
			{
				if(framesPtr->size() != 0)
				{
					ScreenNavigator::Get().SetScreen(std::make_shared<PaintScreen>(ScreenNavigator::Get().GetScreen(), (*framesPtr)[currentFrame].GetMutableDataPtr(), imageDimensions, minDarkPixelCount));
					state = ANIMATOR_INIT;
				}
				else
					LogUtil::Get().LogMessage("NORMAL", "No frames to edit.");
			}
			else if(propList && static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("PropListSettings"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<PropertiesScreen>(ScreenNavigator::Get().GetScreen(), propList));
				state = ANIMATOR_INIT;
				break;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Back"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(haltedScreen);
				GUIUtil::Get().ClearAll();
				break;
			}
			if(framesPtr->size() != 0)
			{
				(*framesPtr)[currentFrame].SetMsLength(static_cast<GUISliderState*>(GUIUtil::Get().GrabState("TimeSlider"))->GetVal());
				timeSinceLastFrame += TimeUtil::Get().GetDeltaTime();
				if(timeSinceLastFrame >= (*framesPtr)[currentFrameInAnimation].GetMsLength()/1000.0)
				{
					timeSinceLastFrame = 0.0;
					++currentFrameInAnimation;
					if(currentFrameInAnimation == framesPtr->size())
						currentFrameInAnimation = 0;
					GUICanvasState* finalCanvasState = static_cast<GUICanvasState*>(GUIUtil::Get().GrabState("FinalPreview"));
					finalCanvasState->SetCanvasData((*framesPtr)[currentFrameInAnimation].GetData());
					finalCanvasState->SyncCanvas();
				}
			}
			break;
		}
	}
	return true;
}

void AnimatorScreen::UpdateFrameCanvas()
{
	if(framesPtr->size() != 0)
	{
		GUICanvasState* canvasState = static_cast<GUICanvasState*>(GUIUtil::Get().GrabState("FrameView"));
		canvasState->SetCanvasData((*framesPtr)[currentFrame].GetData());
		canvasState->SyncCanvas();
	}
}

void AnimatorScreen::UpdateFinalCanvas()
{
	if(framesPtr->size() != 0)
	{
		currentFrameInAnimation = 0;
		timeSinceLastFrame = 0.0;
		GUICanvasState* finalCanvasState = static_cast<GUICanvasState*>(GUIUtil::Get().GrabState("FinalPreview"));
		finalCanvasState->SetCanvasData((*framesPtr)[currentFrameInAnimation].GetData());
		finalCanvasState->SyncCanvas();
	}
}

void AnimatorScreen::Draw()
{
}
