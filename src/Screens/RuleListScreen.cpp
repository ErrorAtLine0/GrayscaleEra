#include "RuleListScreen.hpp"
#include <GSEGL/Graphics/GUI/GUITextButton.hpp>
#include <GSEGL/Graphics/GUI/GUIImageButton.hpp>
#include <GSEGL/Graphics/GUI/GUIImmutableText.hpp>
#include <GSEGL/Graphics/GUI/GUICounter.hpp>
#include <GSEGL/LogUtil.hpp>
#include <algorithm>
#include "SelectorScreen.hpp"

#define MAX_CONDITIONS_AND_RESULTS 9

RuleListScreen::RuleListScreen(const std::shared_ptr<Screen>& initHaltedScreen, RuleList* initRules, AdventureDataPtr initAdventureData):
state(RULELIST_INIT),
rules(initRules),
conditionsSet(false),
resultsSet(false),
adventureData(initAdventureData),
ruleToActOn(-1),
haltedScreen(initHaltedScreen)
{}

bool RuleListScreen::Update()
{
	switch(state)
	{
		case RULELIST_INIT:
		{
			if(conditionsSet && resultsSet)
				AddRule();
			conditionsSet = false;
			resultsSet = false;
			conditionCounters = std::vector<double>(rules->GetConditions().GetListSize());
			resultCounters = std::vector<double>(rules->GetResults().GetListSize());
			GUIUtil::Get().ClearAll();
			GUIUtil::Get().AddElement("RuleArea", new GUIScrollArea(Vec2(0.5f, 0.0f), Vec2(-400.0f, 120.0f), Vec2(0.0f, 1.0f), Vec2(800.0f, -120.0f), 20.0f, 120.0f));
			GUIUtil::Get().AddElement("AddRule", new GUITextButton("Add Rule", Vec2(0.5f, 0.0f), Vec2(-350.0f, 10.0f), Vec2(0.0f), Vec2(700.0f, 50.0f)));
			GUIUtil::Get().AddElement("Back", new GUIImageButton("BackIcon", Vec2(0.0f), Vec2(0.0f), Vec2(0.0f), Vec2(40.0f)));
			scrollState = static_cast<GUIScrollAreaState*>(GUIUtil::Get().GrabState("RuleArea"));
			SetRuleGUI();
			state = RULELIST_UPDATE;
			break;
		}
		case RULELIST_UPDATE:
		{
			for(uint32_t i = 0; i < extraDataButtons.size(); i++)
				if(static_cast<GUIButtonState*>(std::get<0>(extraDataButtons[i])->GrabState())->buttonState == GUIBUTTON_PRESSED)
				{
					SaveDataToList();
					PropertyType dataType = std::get<2>(extraDataButtons[i]);
					void* rawData = std::get<1>(extraDataButtons[i]);
					if(dataType == PROPERTY_LIST)
						ScreenNavigator::Get().SetScreen(std::make_shared<PropertiesScreen>(ScreenNavigator::Get().GetScreen(), static_cast<PropertiesList*>(rawData), false, adventureData));
					else if(dataType == PROPERTY_BLOCK)
						ScreenNavigator::Get().SetScreen(std::make_shared<SelectorScreen>(ScreenNavigator::Get().GetScreen(), &adventureData->blocks, static_cast<BlockID*>(rawData), SELECTOR_INDEX_UINT16, FILETYPE_BLOCK, adventureData));
					else if(dataType == PROPERTY_NPC)
						ScreenNavigator::Get().SetScreen(std::make_shared<SelectorScreen>(ScreenNavigator::Get().GetScreen(), &adventureData->npcs, static_cast<NPCID*>(rawData), SELECTOR_INDEX_UINT16, FILETYPE_PLAYER, adventureData));
					else if(dataType == PROPERTY_RULELIST)
						ScreenNavigator::Get().SetScreen(std::make_shared<RuleListScreen>(ScreenNavigator::Get().GetScreen(), static_cast<RuleList*>(rawData), adventureData));
					state = RULELIST_INIT;
					break;
				}
			if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("AddRule"))->buttonState == GUIBUTTON_PRESSED)
			{
				if(rules->GetListSize() >= 50)
					LogUtil::Get().LogMessage("NORMAL", "Cannot add more than 50 rules");
				else
				{
					SaveDataToList();
					state = RULELIST_CONDITION_INIT;
				}
			}
			else if(ruleToActOn >= 0)
			{
				switch(ruleAction)
				{
					case RULELISTACTION_REMOVE:
					{
						rules->RemoveRule(ruleToActOn);
						break;
					}
					case RULELISTACTION_MOVEUP:
					{
						rules->MoveRule(ruleToActOn, ruleToActOn - 1);
						break;
					}
					case RULELISTACTION_MOVEDOWN:
					{
						rules->MoveRule(ruleToActOn, ruleToActOn + 1);
						break;
					}
				}
				SetRuleGUI();
				ruleToActOn = -1;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Back"))->buttonState == GUIBUTTON_PRESSED)
			{
				SaveDataToList();
				ScreenNavigator::Get().SetScreen(haltedScreen);
				GUIUtil::Get().ClearAll();
			}
			break;
		}
		case RULELIST_CONDITION_INIT:
		{
			GUIUtil::Get().ClearAll();
			GUIUtil::Get().AddElement("ConditionArea", new GUIScrollArea(Vec2(0.5f, 0.0f), Vec2(-400.0f, 120.0f), Vec2(0.0f, 1.0f), Vec2(800.0f, -120.0f), 20.0f, 120.0f));
			GUIUtil::Get().AddElement("FinishConditions", new GUITextButton("Add Selected Conditions", Vec2(0.5f, 0.0f), Vec2(-350.0f, 10.0f), Vec2(0.0f), Vec2(700.0f, 30.0f)));
			GUIUtil::Get().AddElement("Back", new GUIImageButton("BackIcon", Vec2(0.0f), Vec2(0.0f), Vec2(0.0f), Vec2(40.0f)));
			scrollState = static_cast<GUIScrollAreaState*>(GUIUtil::Get().GrabState("ConditionArea"));
			SetSelectionGUI(true);
			state = RULELIST_CONDITION_UPDATE;
			break;
		}
		case RULELIST_CONDITION_UPDATE:
		{
			if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("FinishConditions"))->buttonState == GUIBUTTON_PRESSED)
			{
				conditionsSet = true;
				SaveDataToList();
				state = RULELIST_RESULT_INIT;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Back"))->buttonState == GUIBUTTON_PRESSED)
				state = RULELIST_INIT;
			break;
		}
		case RULELIST_RESULT_INIT:
		{
			GUIUtil::Get().ClearAll();
			GUIUtil::Get().AddElement("ResultArea", new GUIScrollArea(Vec2(0.5f, 0.0f), Vec2(-400.0f, 120.0f), Vec2(0.0f, 1.0f), Vec2(800.0f, -120.0f), 20.0f, 120.0f));
			GUIUtil::Get().AddElement("FinishResults", new GUITextButton("Add Selected Results", Vec2(0.5f, 0.0f), Vec2(-350.0f, 10.0f), Vec2(0.0f), Vec2(700.0f, 30.0f)));
			GUIUtil::Get().AddElement("Back", new GUIImageButton("BackIcon", Vec2(0.0f), Vec2(0.0f), Vec2(0.0f), Vec2(40.0f)));
			scrollState = static_cast<GUIScrollAreaState*>(GUIUtil::Get().GrabState("ResultArea"));
			SetSelectionGUI(false);
			state = RULELIST_RESULT_UPDATE;
			break;
		}
		case RULELIST_RESULT_UPDATE:
		{
			if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("FinishResults"))->buttonState == GUIBUTTON_PRESSED)
			{
				resultsSet = true;
				SaveDataToList();
				state = RULELIST_INIT;
			}
			if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Back"))->buttonState == GUIBUTTON_PRESSED)
				state = RULELIST_INIT;
			break;
		}
	}
	return true;
}

void RuleListScreen::AddRule()
{
	std::vector<uint32_t> conditionIndices;
	std::vector<uint32_t> resultIndices;
	for(uint32_t i = 0; i < conditionCounters.size(); i++)
		for(double j = 0; j < conditionCounters[i]; j++)
			conditionIndices.emplace_back(i);
	for(uint32_t i = 0; i < resultCounters.size(); i++)
		for(double j = 0; j < resultCounters[i]; j++)
			resultIndices.emplace_back(i);
	rules->AddRule(conditionIndices, resultIndices);
}

void RuleListScreen::SetRuleAction(uint32_t ruleToActOn, RuleListAction ruleAction)
{
	this->ruleToActOn = ruleToActOn;
	this->ruleAction = ruleAction;
}

void RuleListScreen::SetRuleGUI()
{
	scrollState->ClearAll();
	extraDataButtons.clear();
	pos = Vec2(50.0f, -60.0f);
	scrollState->AddElement(new GUIImmutableText(rules->GetListName(), Vec2(0.5f, 0.0f), Vec2(-DrawUtil::Get().CalculateTextWidth(rules->GetListName(), 40.0f)/2.0f, pos.y), 0.0f, 40.0f));
	pos.y -= 40.0f;
	for(uint32_t i = 0; i < rules->GetListSize(); i++)
	{
		Rule& rule = rules->GetRuleAt(i);
		const float textSize = YVALBETWEENPROPERTIES - (YVALBETWEENPROPERTIES/2.0f);
		const float textGap = YVALBETWEENPROPERTIES - (YVALBETWEENPROPERTIES/4.0f);
		scrollState->AddElement(new GUIImmutableText("----------------------------------------------", Vec2(0.5f, 0.0f), Vec2(-	DrawUtil::Get().CalculateTextWidth("----------------------------------------------", textSize)/2.0f, pos.y), 0.0f, textSize));
		pos.y -= textGap;
		scrollState->AddElement(new GUIImageButton("RemoveIcon", Vec2(1.0f, 0.0f), Vec2(-60.0f, pos.y), Vec2(0.0f), Vec2(40.0f), BVec2(false), [](const std::shared_ptr<void>& num)
		{
			std::static_pointer_cast<RuleListScreen>(ScreenNavigator::Get().GetScreen())->SetRuleAction(*std::static_pointer_cast<uint32_t>(num), RULELISTACTION_REMOVE);
		}, std::make_shared<uint32_t>(i)));
		if(i != 0)
		{
			scrollState->AddElement(new GUIImageButton("UpIcon", Vec2(1.0f, 0.0f), Vec2(-80.0f, pos.y+20.0f), Vec2(0.0f), Vec2(20.0f), BVec2(false), [](const std::shared_ptr<void>& num)
			{
				std::static_pointer_cast<RuleListScreen>(ScreenNavigator::Get().GetScreen())->SetRuleAction(*std::static_pointer_cast<uint32_t>(num), RULELISTACTION_MOVEUP);
			}, std::make_shared<uint32_t>(i)));
		}
		if(i+1 != rules->GetListSize())
		{
			scrollState->AddElement(new GUIImageButton("UpIcon", Vec2(1.0f, 0.0f), Vec2(-80.0f, pos.y), Vec2(0.0f), Vec2(20.0f), BVec2(false, true), [](const std::shared_ptr<void>& num)
			{
				std::static_pointer_cast<RuleListScreen>(ScreenNavigator::Get().GetScreen())->SetRuleAction(*std::static_pointer_cast<uint32_t>(num), RULELISTACTION_MOVEDOWN);
			}, std::make_shared<uint32_t>(i)));
		}
		for(uint32_t i = 0; i < 2; i++)
		{
			scrollState->AddElement(new GUIImmutableText(i == 0 ? "If:" : "Then:", Vec2(0.0f, 0.0f), Vec2(pos.x - 40.0f, pos.y), 0.0f, textSize));
			std::vector<HeadlineProperty*>& properties = i == 0 ? rule.conditionProperties : rule.resultProperties;
			if(properties.empty())
				pos.y -= textGap;
			for(HeadlineProperty* property : properties)
			{
				pos.y -= textGap;
				scrollState->AddElement(new GUIImmutableText(property->GetPropertyName(), Vec2(0.0f, 0.0f), pos, 0.0f, textSize));
				pos.y -= textGap;
				auto elementData = PropertiesScreen::GetElementFromProperty(property, pos, adventureData);
				if(elementData.first)
				{
					scrollState->AddElement(elementData.first);
					if(elementData.second)
						extraDataButtons.emplace_back(elementData.first, elementData.second, property->GetPropertyType());
				}
			}
		}
	}
}

void RuleListScreen::SetSelectionGUI(bool isConditionGUI)
{
	const UTF8String& titleStr = isConditionGUI ? "Conditions" : "Results";
	scrollState->AddElement(new GUIImmutableText(titleStr, Vec2(0.5f, 0.0f), Vec2(- DrawUtil::Get().CalculateTextWidth(titleStr, 50.0f)/2.0f, - (0 * 100.0f) - 110.0f), 0.0f, 50.0f));
	const PropertiesList& propertyList = isConditionGUI ? rules->GetConditions() : rules->GetResults();
	for(uint32_t i = 0; i < propertyList.GetListSize(); i++)
	{
		std::vector<double>& counterVal = isConditionGUI ? conditionCounters : resultCounters;
		scrollState->AddElement(new GUIImmutableText(propertyList.GetPropAt(i)->GetPropertyName(), Vec2(0.5f, 0.0f), Vec2(-360.0f, - (i * 70.0f) - 150.0f), 0.0f, 30.0f));
		scrollState->AddElement(new GUICounter(Vec2(0.5f, 0.0f), Vec2(250.0f, - (i * 70.0f) - 140.0f), Vec2(0.0f), Vec2(100.0f, 30.0f), 0.0, 0.0, 9.0, 1.0, &counterVal[i]));
	}
}

void RuleListScreen::SaveDataToList()
{
	for(GUIElement* element : scrollState->elements)
		element->UpdateValPtr();
}

void RuleListScreen::Draw()
{
	
}
