#include "ChoosePlayScreen.hpp"
#include "ArenaChoosePlayScreen.hpp"
#include "AdventureChoosePlayScreen.hpp"
#include "../GlobalMenuData.hpp"
#include <GSEGL/Graphics/GUI/GUIUtil.hpp>
#include <GSEGL/Graphics/GUI/GUIImageButton.hpp>
#include <GSEGL/Graphics/GUI/GUIImmutableText.hpp>

ChoosePlayScreen::ChoosePlayScreen(const std::shared_ptr<Screen>& initHaltedScreen):
state(CHOOSEPLAY_INIT),
haltedScreen(initHaltedScreen)
{}

bool ChoosePlayScreen::Update()
{
	switch(state)
	{
		case CHOOSEPLAY_INIT:
		{
			GUIUtil::Get().ClearAll();
			GUIUtil::Get().AddElement("Back", new GUIImageButton("BackIcon", Vec2(0.0f), Vec2(0.0f), Vec2(0.0f), Vec2(40.0f)));
			GUIUtil::Get().AddElement("ChooseText", new GUIImmutableText("Choose Mode", Vec2(0.5f, 1.0f), Vec2(-DrawUtil::Get().CalculateTextWidth("Choose Mode", 70.0f) / 2.0f, -125.0f), 0.0f, 70.0f));
			GUIUtil::Get().AddElement("Adventure", new GUIImageButton("AdventureButton", Vec2(0.5f, 1.0f), Vec2(100.0f, -400.0f), Vec2(0.0f), Vec2(200.0f)));
			GUIUtil::Get().AddElement("AdventureText", new GUIImmutableText("Adventure", Vec2(0.5f, 1.0f), Vec2(200.0f - DrawUtil::Get().CalculateTextWidth("Adventure", 20.0f) / 2.0f, -440.0f), 0.0f, 20.0f));
			GUIUtil::Get().AddElement("Arena", new GUIImageButton("ArenaButton", Vec2(0.5f, 1.0f), Vec2(-300.0f, -400.0f), Vec2(0.0f), Vec2(200.0f)));
			GUIUtil::Get().AddElement("ArenaText", new GUIImmutableText("Arena", Vec2(0.5f, 1.0f), Vec2(-200.0f - DrawUtil::Get().CalculateTextWidth("Arena", 20.0f) / 2.0f, -440.0f), 0.0f, 20.0f));
			state = CHOOSEPLAY_UPDATE;
			break;
		}
		case CHOOSEPLAY_UPDATE:
		{
			if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Arena"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<ArenaChoosePlayScreen>(ScreenNavigator::Get().GetScreen()));
				state = CHOOSEPLAY_INIT;
				return true;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Adventure"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<AdventureChoosePlayScreen>(ScreenNavigator::Get().GetScreen()));
				state = CHOOSEPLAY_INIT;
				return true;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Back"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(haltedScreen);
				return true;
			}
			break;
		}
	}
	return true;
}

void ChoosePlayScreen::Draw()
{
	GlobalMenuData::Get().DrawBackground();
}
