#include "FileSelectorScreen.hpp"
#include <GSEGL/Graphics/GUI/GUIImageButton.hpp>
#include <GSEGL/Graphics/GUI/GUITextButton.hpp>
#include <GSEGL/Graphics/GUI/GUIScrollArea.hpp>
#include <GSEGL/FileUtil.hpp>
#include "../GlobalMenuData.hpp"

FileSelectorScreen::FileSelectorScreen(const std::shared_ptr<Screen>& initHaltedScreen, const UTF8String& initPathToSearch, UTF8String* initFileNamePtr):
state(FILESELECTOR_INIT),
fileNamePtr(initFileNamePtr),
pathToSearch(initPathToSearch),
haltedScreen(initHaltedScreen)
{
	 fileNames = FileUtil::Get().GetFilesInPath(initPathToSearch);
}

bool FileSelectorScreen::Update()
{
	switch(state)
	{
		case FILESELECTOR_INIT:
		{
			GUIUtil::Get().ClearAll();
			GUIUtil::Get().AddElement("FileList", new GUIScrollArea(Vec2(0.0f), Vec2(0.0f), Vec2(1.0f), Vec2(0.0f), 20.0f));
			GUIUtil::Get().AddElement("Back", new GUIImageButton("BackIcon", Vec2(0.0f), Vec2(0.0f), Vec2(0.0f), Vec2(40.0f)));
			scrollState = static_cast<GUIScrollAreaState*>(GUIUtil::Get().GrabState("FileList"));
			if(fileNames)
			{
				for(uint32_t i = 0; i < fileNames->size(); i++)
				{
					scrollState->AddElement(new GUITextButton((*fileNames)[i], Vec2(0.5f, 0.0f), Vec2(-360.0f, - (i * 100.0f) - 100.0f), Vec2(0.0f), Vec2(700.0f, 50.0f)));
					scrollState->AddElement(new GUIImageButton("RemoveIcon", Vec2(0.5f, 0.0f), Vec2(350.0f, - (i * 100.0f) - 95.0f), Vec2(0.0f), Vec2(40.0f)));
				}
			}
			state = FILESELECTOR_UPDATE;
			break;
		}
		case FILESELECTOR_UPDATE:
		{
			for(uint32_t i = 0; i < scrollState->elements.size(); i++)
				if(static_cast<GUIButtonState*>(scrollState->elements[i]->GrabState())->buttonState == GUIBUTTON_PRESSED)
				{
					if(i % 2 == 0) // Is a file name
					{
						*fileNamePtr = (*fileNames)[i/2];
						ScreenNavigator::Get().SetScreen(haltedScreen);
						return true;
					}
					else // Is a remove button
					{
						FileUtil::Get().RemoveFile(pathToSearch + UTF8String("/") + (*fileNames)[(i-1)/2]);
						fileNames->erase(fileNames->begin() + (i-1)/2);
						scrollState->RemoveElement(i - 1);
						scrollState->RemoveElement(i - 1);
					}
					break;
				}
			if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Back"))->buttonState == GUIBUTTON_PRESSED)
				ScreenNavigator::Get().SetScreen(haltedScreen);
			break;
		}
	}
	return true;
}

void FileSelectorScreen::Draw()
{
	GlobalMenuData::Get().DrawBackground();
}
