#include "WaitScreen.hpp"
#include <GSEGL/Graphics/GUI/GUIImmutableText.hpp>
#include <GSEGL/Graphics/GUI/GUIImageButton.hpp>
#include <GSEGL/TimeUtil.hpp>
#include <GSEGL/RandGenUtil.hpp>
#include <algorithm>
#define WAIT_BOX_SPAWN_TIME 0.5
#define WAIT_FLOOR_HEIGHT 50.0f
#define WAIT_BOX_SIZE 40.0f
#define WAIT_BOX_DEATH_ANIMATION_RANGE 200.0f
#define WAIT_BOX_DEATH_ANIMATION_LENGTH 0.3
#define WAIT_SCORE_JUMP_ANIMATION_RANGE 10.0f
#define WAIT_SCORE_JUMP_ANIMATION_LENGTH 0.2
#define WAIT_CURSOR_ANIMATION_RANGE 300.0f
#define WAIT_CURSOR_ANIMATION_LENGTH 0.15
#define WAIT_CURSOR_ANIMATION_RECT_WIDTH 10.0f

WaitScreen::WaitScreen(const std::shared_ptr<Screen>& initHaltedScreen, bool (*initWaitUpdateLoop)(const std::shared_ptr<void>&), const std::shared_ptr<void>& initUpdateData):
state(WAIT_INIT),
score(0),
scoreCounterAnimationTime(-1.0),
cursorShotAnimationTime(-1.0),
timeLastSpawned(TimeUtil::Get().GetTimeSinceStart()),
waitUpdateLoop(initWaitUpdateLoop),
updateData(initUpdateData),
haltedScreen(initHaltedScreen)
{}

bool WaitScreen::Update()
{
	switch(state)
	{
		case WAIT_INIT:
		{
			GUIUtil::Get().ClearAll();
			GUIUtil::Get().AddElement("LoadingText", new GUIImmutableText("Loading", Vec2(0.5f, 1.0f), Vec2(-DrawUtil::Get().CalculateTextWidth("Loading", 50.0f)/2.0f, -200.0f), 0.0f, 50.0f));
			GUIUtil::Get().AddElement("ShootText", new GUIImmutableText("Shoot the squares while you're waiting", Vec2(0.5f, 1.0f), Vec2(-DrawUtil::Get().CalculateTextWidth("Shoot the squares while you're waiting", 20.0f)/2.0f, -250.0f), 0.0f, 20.0f));
			GUIUtil::Get().AddElement("BackButton", new GUIImageButton("BackIcon", Vec2(0.0f), Vec2(0.0f), Vec2(0.0f), Vec2(40.0)));
			state = WAIT_UPDATE;
			break;
		}
		case WAIT_UPDATE:
		{
			if(TimeUtil::Get().GetTimeSinceStart() - timeLastSpawned >= WAIT_BOX_SPAWN_TIME)
			{
				timeLastSpawned = TimeUtil::Get().GetTimeSinceStart();
				bouncingBoxes.emplace_back();
			}
			uint32_t oldScore = score;
			bouncingBoxes.erase(std::remove_if(bouncingBoxes.begin(), bouncingBoxes.end(), [&](LoadingScreenBouncingBox& box){ return !box.Update(&score); }), bouncingBoxes.end());
			if(score != oldScore)
				scoreCounterAnimationTime = TimeUtil::Get().GetDeltaTime();
			else if(scoreCounterAnimationTime >= WAIT_SCORE_JUMP_ANIMATION_LENGTH)
				scoreCounterAnimationTime = -1.0;
			else if(scoreCounterAnimationTime >= 0.0)
				scoreCounterAnimationTime += TimeUtil::Get().GetDeltaTime();
			if(ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY1) == KEYSTATE_PRESSED)
			{
				cursorShotAnimationTime = TimeUtil::Get().GetDeltaTime();
				cursorShotAnimationPos = ControllerUtil::Get().GetCursorPos();
			}
			else if(cursorShotAnimationTime >= WAIT_CURSOR_ANIMATION_LENGTH)
				cursorShotAnimationTime = -1.0;
			else if(cursorShotAnimationTime >= 0.0)
				cursorShotAnimationTime += TimeUtil::Get().GetDeltaTime();
			if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("BackButton"))->buttonState == GUIBUTTON_PRESSED || (waitUpdateLoop && !(*waitUpdateLoop)(updateData)))
				ScreenNavigator::Get().SetScreen(haltedScreen);
			break;
		}
	}
	return true;
}

void WaitScreen::Draw()
{
	float guiSize = GUIUtil::Get().GetGUISize();
	Vec2 winSize = WindowUtil::Get().GetWindowSize();
	// Draw Floor & Boxes
	DrawUtil::Get().DrawRectangle(11, Vec2(0.0f), Vec2(winSize.x, WAIT_FLOOR_HEIGHT), 0.1f);
	for(LoadingScreenBouncingBox& box : bouncingBoxes)
		box.Draw();
	
	// Draw Score
	UTF8String scoreStr = UTF8String::IntToStr(score);
	double scoreAnimationYVal;
	if(scoreCounterAnimationTime < 0.0)
		scoreAnimationYVal = 0.0;
	else if(scoreCounterAnimationTime < WAIT_SCORE_JUMP_ANIMATION_LENGTH/2.0) // Going up
		scoreAnimationYVal = (scoreCounterAnimationTime / (WAIT_SCORE_JUMP_ANIMATION_LENGTH/2.0)) * WAIT_SCORE_JUMP_ANIMATION_RANGE;
	else // Reached peak. Going Down
		scoreAnimationYVal = WAIT_SCORE_JUMP_ANIMATION_RANGE - ((scoreCounterAnimationTime / WAIT_SCORE_JUMP_ANIMATION_LENGTH) * WAIT_SCORE_JUMP_ANIMATION_RANGE);
	DrawUtil::Get().DrawText(11, scoreStr, Vec2(winSize.x/2.0f - DrawUtil::Get().CalculateTextWidth(scoreStr, guiSize * 40.0f)/2.0f, winSize.y - (300.0f - scoreAnimationYVal) * guiSize), guiSize * 40.0f);
	
	// Draw Cursor Shot Aniamtion
	if(cursorShotAnimationTime > 0.0)
	{
		double rectAlpha = (1.0 - (cursorShotAnimationTime / WAIT_CURSOR_ANIMATION_LENGTH)) / 3.0;
		if(rectAlpha < 0.0)
			rectAlpha = 0.0;
		double rectLength = (cursorShotAnimationTime / (WAIT_CURSOR_ANIMATION_LENGTH)) * WAIT_CURSOR_ANIMATION_RANGE;
		double rectPos;
		if(cursorShotAnimationTime < WAIT_CURSOR_ANIMATION_LENGTH/2.0) // Length: Increase, Pos: 0
		{
			rectPos = 0.0;
		}
		else // Reached peak. Length: Decrease, Pos: Increase
		{
			rectLength = WAIT_CURSOR_ANIMATION_RANGE - rectLength;
			rectPos = rectLength - WAIT_CURSOR_ANIMATION_RANGE/2.0;
		}
		DrawUtil::Get().DrawRectangle(11, Vec2(cursorShotAnimationPos.x - WAIT_CURSOR_ANIMATION_RECT_WIDTH / 2.0f, cursorShotAnimationPos.y - rectPos), Vec2(WAIT_CURSOR_ANIMATION_RECT_WIDTH, rectLength), 0.0f, rectAlpha);
		DrawUtil::Get().DrawRectangle(11, Vec2(cursorShotAnimationPos.x - WAIT_CURSOR_ANIMATION_RECT_WIDTH / 2.0f, cursorShotAnimationPos.y - rectLength + rectPos), Vec2(WAIT_CURSOR_ANIMATION_RECT_WIDTH, rectLength), 0.0f, rectAlpha);
		DrawUtil::Get().DrawRectangle(11, Vec2(cursorShotAnimationPos.x - rectPos, cursorShotAnimationPos.y - WAIT_CURSOR_ANIMATION_RECT_WIDTH / 2.0f), Vec2(rectLength, WAIT_CURSOR_ANIMATION_RECT_WIDTH), 0.0f, rectAlpha);
		DrawUtil::Get().DrawRectangle(11, Vec2(cursorShotAnimationPos.x - rectLength + rectPos, cursorShotAnimationPos.y - WAIT_CURSOR_ANIMATION_RECT_WIDTH / 2.0f), Vec2(rectLength, WAIT_CURSOR_ANIMATION_RECT_WIDTH), 0.0f, rectAlpha);
	}
}

void WaitScreen::SetToInit()
{
	state = WAIT_INIT;
}

LoadingScreenBouncingBox::LoadingScreenBouncingBox():
color(RandGenUtil::Get().GetRandomDouble(0.4, 0.7)),
deathAnimationTime(-1.0),
position(-40.0f, 150.0f),
velocity(RandGenUtil::Get().GetRandomDouble(200.0, 400.0), 0.0f)
{}

std::pair<Vec2, Vec2> LoadingScreenBouncingBox::CalculateBoxElasticProperties()
{
	if(position.y < WAIT_FLOOR_HEIGHT)
	{
		float bounceAmount = WAIT_FLOOR_HEIGHT - position.y;
		return std::pair<Vec2, Vec2>(Vec2(position.x - bounceAmount, WAIT_FLOOR_HEIGHT), Vec2(WAIT_BOX_SIZE + bounceAmount * 2.0f, WAIT_BOX_SIZE - bounceAmount));
	}
	else
		return std::pair<Vec2, Vec2>(position, Vec2(WAIT_BOX_SIZE));
}

bool LoadingScreenBouncingBox::Update(uint32_t* score)
{
	if(deathAnimationTime < 0.0)
	{
		velocity.y -= 1500.0f * TimeUtil::Get().GetDeltaTime();
		position.x += velocity.x * TimeUtil::Get().GetDeltaTime();
		position.y += velocity.y * TimeUtil::Get().GetDeltaTime();
		if(position.y <= WAIT_FLOOR_HEIGHT - 30.0f)
		{
			position.y = WAIT_FLOOR_HEIGHT;
			velocity.y = -velocity.y;
		}
		if(ControllerUtil::Get().GetButtonState(CONTROLLER_ABILITY1) == KEYSTATE_PRESSED)
		{
			const Vec2& cursorPos = ControllerUtil::Get().GetCursorPos();
			std::pair<Vec2, Vec2> calcProp = CalculateBoxElasticProperties();
			if(cursorPos.x > calcProp.first.x && cursorPos.x < calcProp.first.x + calcProp.second.x &&
				cursorPos.y > calcProp.first.y && cursorPos.y < calcProp.first.y + calcProp.second.y)
			{
				++(*score);
				deathAnimationTime = TimeUtil::Get().GetDeltaTime();
			}
		}
		return position.x < WindowUtil::Get().GetWindowSize().x;
	}
	else
	{
		deathAnimationTime += TimeUtil::Get().GetDeltaTime();
		return deathAnimationTime <= WAIT_BOX_DEATH_ANIMATION_LENGTH;
	}
}

void LoadingScreenBouncingBox::Draw()
{
	if(deathAnimationTime < 0.0)
	{
		std::pair<Vec2, Vec2> calcProp = CalculateBoxElasticProperties();
		DrawUtil::Get().DrawRectangle(11, calcProp.first, calcProp.second, color);
	}
	else
	{
		double deathAnimationRange = (deathAnimationTime / WAIT_BOX_DEATH_ANIMATION_LENGTH) * WAIT_BOX_DEATH_ANIMATION_RANGE;
		double deathAnimationAlpha = WAIT_BOX_DEATH_ANIMATION_LENGTH - deathAnimationTime;
		DrawUtil::Get().DrawRectangle(11, Vec2(position.x - deathAnimationRange, position.y), Vec2(WAIT_BOX_SIZE), color, deathAnimationAlpha);
		DrawUtil::Get().DrawRectangle(11, Vec2(position.x + deathAnimationRange, position.y), Vec2(WAIT_BOX_SIZE), color, deathAnimationAlpha);
		DrawUtil::Get().DrawRectangle(11, Vec2(position.x, position.y - deathAnimationRange), Vec2(WAIT_BOX_SIZE), color, deathAnimationAlpha);
		DrawUtil::Get().DrawRectangle(11, Vec2(position.x, position.y + deathAnimationRange), Vec2(WAIT_BOX_SIZE), color, deathAnimationAlpha);
	}
}
