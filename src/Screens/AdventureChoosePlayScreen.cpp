#include "AdventureChoosePlayScreen.hpp"
#include "PlayDataScreen.hpp"
#include "SelectorScreen.hpp"
#include <GSEGL/Graphics/GUI/GUIImageButton.hpp>
#include <GSEGL/Graphics/GUI/GUITextButton.hpp>
#include "../GlobalMenuData.hpp"

AdventureChoosePlayScreen::AdventureChoosePlayScreen(const std::shared_ptr<Screen>& initHaltedScreen):
state(ADVENTURECHOOSEPLAY_INIT),
haltedScreen(initHaltedScreen)
{}

bool AdventureChoosePlayScreen::Update()
{
	switch(state)
	{
		case ADVENTURECHOOSEPLAY_INIT:
		{
			GUIUtil::Get().ClearAll();
			GUIUtil::Get().AddElement("SinglePlayer", new GUITextButton("Single Player", Vec2(0.5f, 1.0f), Vec2(-360.0f, -125.0f), Vec2(0.0f), Vec2(720.0f, 50.0f)));
			GUIUtil::Get().AddElement("ConnectButton", new GUITextButton("Connect Lobby", Vec2(0.5f, 1.0f), Vec2(-360.0f, -225.0f), Vec2(0.0f), Vec2(720.0f, 50.0f)));
			GUIUtil::Get().AddElement("HostButton", new GUITextButton("Host Lobby", Vec2(0.5f, 1.0f), Vec2(-360.0f, -325.0f), Vec2(0.0f), Vec2(720.0f, 50.0f)));
			GUIUtil::Get().AddElement("DirectConnectButton", new GUITextButton("Direct Connect", Vec2(0.5f, 1.0f), Vec2(-360.0f, -425.0f), Vec2(0.0f), Vec2(720.0f, 50.0f)));
			GUIUtil::Get().AddElement("DirectHostButton", new GUITextButton("Direct Host", Vec2(0.5f, 1.0f), Vec2(-360.0f, -525.0f), Vec2(0.0f), Vec2(720.0f, 50.0f)));
			GUIUtil::Get().AddElement("Back", new GUIImageButton("BackIcon", Vec2(0.0f), Vec2(0.0f), Vec2(0.0f), Vec2(40.0f)));
			state = ADVENTURECHOOSEPLAY_UPDATE;
			break;
		}
		case ADVENTURECHOOSEPLAY_UPDATE:
		{
			if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("SinglePlayer"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<PlayDataScreen>(ScreenNavigator::Get().GetScreen(), PLAYDATATYPE_SINGLEPLAYER, WORLD_ADVENTURE));
				state = ADVENTURECHOOSEPLAY_INIT;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("ConnectButton"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<PlayDataScreen>(ScreenNavigator::Get().GetScreen(), PLAYDATATYPE_CLIENT_LOBBY, WORLD_ADVENTURE));
				state = ADVENTURECHOOSEPLAY_INIT;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("HostButton"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<PlayDataScreen>(ScreenNavigator::Get().GetScreen(), PLAYDATATYPE_SERVER_LOBBY, WORLD_ADVENTURE));
				state = ADVENTURECHOOSEPLAY_INIT;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("DirectConnectButton"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<PlayDataScreen>(ScreenNavigator::Get().GetScreen(), PLAYDATATYPE_CLIENT_DIRECT, WORLD_ADVENTURE));
				state = ADVENTURECHOOSEPLAY_INIT;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("DirectHostButton"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<PlayDataScreen>(ScreenNavigator::Get().GetScreen(), PLAYDATATYPE_SERVER_DIRECT, WORLD_ADVENTURE));
				state = ADVENTURECHOOSEPLAY_INIT;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Back"))->buttonState == GUIBUTTON_PRESSED)
				ScreenNavigator::Get().SetScreen(haltedScreen);
			break;
		}
	}
	return true;
}

void AdventureChoosePlayScreen::Draw()
{
	GlobalMenuData::Get().DrawBackground();
}
 
