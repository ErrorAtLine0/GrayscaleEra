#include "GameScreen.hpp"
#include "MainMenuScreen.hpp"
#include <GSEGL/Graphics/GUI/GUITextEntry.hpp>
#include <GSEGL/Graphics/GUI/GUITextButton.hpp>
#include <GSEGL/Graphics/GUI/GUIImageButton.hpp>
#include <GSEGL/RandGenUtil.hpp>
#include <GSEGL/FileUtil.hpp>
#include "../NetworkManagerServer.hpp"
#include "../NetworkManagerClient.hpp"
#include "../InputManager.hpp"
#include <GSEGL/LogUtil.hpp>
#include "../LogViewer.hpp"
#include "../GameObjects/Block.hpp"
#include "../GameObjects/RealPlayer.hpp"
#include <cmath>
#define ENDGAME_ANIMATION_TIME 2.0

#define ANNOUNCEMENT_TRANSITION_TIME 0.3

GameScreen::GameScreen(const std::shared_ptr<Screen>& initHaltedScreen, const GameSandboxProperties& initGameProperties, const WorldDataPtr& initWorldData, PlayerID initCurrentPlayerID, const std::shared_ptr<NetworkManager>& initNetMan):
state(GAME_INIT),
netMan(initNetMan),
paused(false),
cameraPos(0.0f),
cameraZoom(0.0f),
cameraRoom(0),
currentAnnouncementTime(TimeUtil::Get().GetTimeSinceStart()),
currentPlayerID(initCurrentPlayerID),
haltedScreen(initHaltedScreen)
{
	GameSandbox::Get().Cleanup();
	GameSandbox::Get().Init(initGameProperties, initWorldData, initNetMan, initCurrentPlayerID);
	GameSandbox::Get().SetCamera(cameraPos, 1.0f, 0);
}

bool GameScreen::Update()
{
	switch(state)
	{
		case GAME_INIT:
		{
			ControllerUtil::Get().SetCursorToGameMode(true);
			GUIUtil::Get().ClearAll();
			state = GAME_UPDATELOOP;
		}
		case GAME_UPDATELOOP:
		{
			if(ControllerUtil::Get().GetButtonState(CONTROLLER_PAUSE) == KEYSTATE_PRESSED)
			{
				GUITextEntryState* messageEntry = static_cast<GUITextEntryState*>(GUIUtil::Get().GrabState("Message Box"));
				if(messageEntry)
					GUIUtil::Get().RemoveElement("Message Box");
				else
				{
					paused = !paused;
					ControllerUtil::Get().SetCursorToGameMode(!paused);
					GUIUtil::Get().ClearAll();
					if(paused)
					{
						GUIUtil::Get().AddElement("Continue", new GUITextButton("Continue", Vec2(0.5f, 0.5f), Vec2(-360.0f, 100.0f), Vec2(0.0f), Vec2(720.0f, 50.0f)));
						GUIUtil::Get().AddElement("Exit", new GUITextButton("Exit", Vec2(0.5f, 0.5f), Vec2(-360.0f, -100.0f), Vec2(0.0f), Vec2(720.0f, 50.0f)));
					}
				}
			}
			if(paused)
			{
				if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Continue"))->buttonState == GUIBUTTON_PRESSED)
				{
					ControllerUtil::Get().SetCursorToGameMode(true);
					GUIUtil::Get().ClearAll();
					paused = false;
				}
				else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Exit"))->buttonState == GUIBUTTON_PRESSED)
				{
					ScreenNavigator::Get().SetScreen(haltedScreen);
					return true;
				}
			}
			bool messageEntrySelected = false;
			if(netMan)
			{
				if(!paused && ControllerUtil::Get().GetButtonState(CONTROLLER_CHAT) == KEYSTATE_PRESSED)
				{
					GUITextEntryState* messageEntry = static_cast<GUITextEntryState*>(GUIUtil::Get().GrabState("Message Box"));
					if(messageEntry)
					{
						if(!messageEntry->text.IsEmpty())
							netMan->SendChatMessage(messageEntry->text);
						GUIUtil::Get().RemoveElement("Message Box");
					}
					else
					{
						GUIUtil::Get().AddElement("Message Box", (new GUITextEntry("", Vec2(0.0f), Vec2(50.0f, 100.0f), Vec2(0.0f), Vec2(700.0f, 15.0f), MAX_MESSAGE_LENGTH))->DrawRectangleBehind());
						GUIUtil::Get().SelectElement("Message Box");
						LogViewer::Get().DisplayAllMessages();
					}
				}
				messageEntrySelected = GUIUtil::Get().IsSelectedElement("Message Box");
				if(!messageEntrySelected)
				{
					GUIUtil::Get().RemoveElement("Message Box");
					LogViewer::Get().DisplayRecentMessages();
				}
			}
			if(!paused && !messageEntrySelected)
				InputManager::Get().Update();
			else
				InputManager::Get().ClearCurrentInputState();
			if(netMan)
			{
				if(!netMan->Update())
				{
					ScreenNavigator::Get().SetScreen(haltedScreen);
					return true;
				}
			}
			else // Since there is no network manager, input is manually given
				GameSandbox::Get().GetRealPlayerInfoByPlayerID(currentPlayerID)->latestInputState = InputManager::Get().GetCurrentInputState();
			if(!GameSandbox::Get().Update())
				state = GAME_ENDINIT;
			break;
		}
		case GAME_ENDINIT:
		{
			ControllerUtil::Get().SetCursorToGameMode(false);
			GUIUtil::Get().ClearAll();
			if(GameSandbox::Get().GetWorldData()->GetType() == WORLD_ARENA)
			{
				endGamePlacement = GameSandbox::Get().GetAllRealPlayerInfos();
				std::sort(endGamePlacement.begin(), endGamePlacement.end(), [](const PlayerInfoPtr& one, const PlayerInfoPtr& two){ return one->points > two->points; });
				for(uint32_t i = 0; i < endGamePlacement.size(); i++)
					LogUtil::Get().LogMessage("NORMAL", UTF8String::IntToStr(i+1) + UTF8String(": ") + endGamePlacement[i]->userName + UTF8String("→") + UTF8String::IntToStr(endGamePlacement[i]->points));
				endGameAnimationStart = TimeUtil::Get().GetTimeSinceStart();
			}
			else
				PlaceEndGameGUI();
			state = GAME_ENDLOOP;
		}
		case GAME_ENDLOOP:
		{
			GUIButtonState* backButtonState = static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Back"));
			if(GameSandbox::Get().GetWorldData()->GetType() == WORLD_ARENA && !backButtonState)
			{
				if(endGameAnimationStart + ENDGAME_ANIMATION_TIME <= TimeUtil::Get().GetTimeSinceStart())
					PlaceEndGameGUI();
			}
			else
			{
				if(backButtonState->buttonState == GUIBUTTON_PRESSED)
				{
					ScreenNavigator::Get().SetScreen(haltedScreen);
					return true;
				}
				if(netMan)
				{
					if(ControllerUtil::Get().GetButtonState(CONTROLLER_CHAT) == KEYSTATE_PRESSED)
					{
						GUITextEntryState* messageEntry = static_cast<GUITextEntryState*>(GUIUtil::Get().GrabState("Message Box"));
						if(messageEntry)
						{
							if(GUIUtil::Get().IsSelectedElement("Message Box"))
							{
								if(!messageEntry->text.IsEmpty())
								{
									netMan->SendChatMessage(messageEntry->text);
									messageEntry->text.Clear();
								}
							}
							else
								GUIUtil::Get().SelectElement("Message Box");
						}
					}
					if(!GUIUtil::Get().IsSelectedElement("Message Box"))
						LogViewer::Get().DisplayRecentMessages();
					else
						LogViewer::Get().DisplayAllMessages();
				}
				GUIButtonState* buttonStatePtr = static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Save World Button"));
				if(buttonStatePtr && buttonStatePtr->buttonState == GUIBUTTON_PRESSED)
				{
					UTF8String worldName = static_cast<GUITextEntryState*>(GUIUtil::Get().GrabState("Save World Name"))->text;
					if(!worldName.IsEmpty())
					{
						UTF8String worldPath = GameSandbox::Get().GetWorldData()->GetType() == WORLD_ARENA ? "arenas" : "adventures";
						std::shared_ptr<std::vector<UTF8String>> filesInPath = FileUtil::Get().GetFilesInPath(worldPath);
						if(filesInPath)
						{
							if(std::find_if(filesInPath->begin(), filesInPath->end(), [&worldName](const UTF8String& nm){ return worldName == nm; }) == filesInPath->end())
							{
								OutMemBitStream outStream;
								GameSandbox::Get().GetWorldData()->Write(outStream);
								if(FileUtil::Get().WriteToFile(UTF8String(worldPath) + UTF8String("/") + worldName, outStream))
								{
									GUIUtil::Get().RemoveElement("Save World Button");
									GUIUtil::Get().RemoveElement("Save World Name");
									LogUtil::Get().LogMessage("NORMAL", "World saved successfully");
								}
								else
									LogUtil::Get().LogMessage("NORMAL", "Unable to save world");
							}
							else
								LogUtil::Get().LogMessage("NORMAL", "A world with this name already exists");
						}
						else
							LogUtil::Get().LogMessage("NORMAL", "Could not read the world directory");
					}
					else
						LogUtil::Get().LogMessage("NORMAL", "Name the world before saving");
				}
			}
			if(netMan && !netMan->Update())
			{
				ScreenNavigator::Get().SetScreen(haltedScreen);
				return true;
			}
			break;
		}
	}
	return true;
}

void GameScreen::PlaceEndGameGUI()
{
	GUIUtil::Get().AddElement("Back", new GUIImageButton("BackIcon", Vec2(0.0f), Vec2(0.0f), Vec2(0.0f), Vec2(40.0f)));
	GUIUtil::Get().AddElement("Message Box", new GUITextEntry("Type Message Here...", Vec2(0.0f), Vec2(50.0f, 100.0f), Vec2(0.0f), Vec2(700.0f, 15.0f), MAX_MESSAGE_LENGTH));
	if(netMan && !netMan->IsServer())
	{
		GUIUtil::Get().AddElement("Save World Button", new GUIImageButton("SaveIcon", Vec2(1.0f), Vec2(-40.0f, -40.0f), Vec2(0.0f), Vec2(40.0f)));
		GUIUtil::Get().AddElement("Save World Name", new GUITextEntry("Save World Name", Vec2(1.0f), Vec2(-300.0f, -40.0f), Vec2(0.0f), Vec2(250.0f, 20.0f), 16));
	}
}

void GameScreen::Draw()
{
	Vec2 winSize = WindowUtil::Get().GetWindowSize();
	float GUISize = GUIUtil::Get().GetGUISize();
	switch(state)
	{
		case GAME_INIT:
			break;
		case GAME_UPDATELOOP:
		{
			// Block the extra parts of the screen
			float twoThirdsOfY = (winSize.y * 0.6666f); // Since it is a 4:3 screen, two thirds of Y = half of X
			DrawUtil::Get().DrawRectangle(14, Vec2(0.0f), Vec2(winSize.x / 2.0f - twoThirdsOfY, winSize.y), 1.0f);
			DrawUtil::Get().DrawRectangle(14, Vec2(winSize.x / 2.0f + twoThirdsOfY, 0.0f), Vec2(winSize.x - (winSize.x / 2.0f + twoThirdsOfY), winSize.y), 1.0f);
			
			const std::vector<RealPlayer*>& players = GameSandbox::Get().GetAllRealPlayerObjects();
			const RealPlayer* playerPtr = nullptr;
			for(const RealPlayer* player : players)
				if(player->GetPlayerID() == currentPlayerID)
					playerPtr = player;
				
			cameraZoom = winSize.y / 600.0f;
			if(playerPtr) // Update camera position to player's current position
			{
				newestCameraPos = playerPtr->GetCurrentPosition() - Vec2(((winSize.x / 2.0f) - (50.0f * cameraZoom)) / cameraZoom, 250.0f);
				if(cameraRoom != playerPtr->GetRoom() || cameraPos == Vec2(0.0f))
					cameraPos = newestCameraPos;
				else // If the room or camera zoom did not change, interpolate between camera values to make things look smoother
				{
					float cameraPosXDiff = newestCameraPos.x - cameraPos.x;
					float cameraPosYDiff = newestCameraPos.y - cameraPos.y;
					cameraPos.x += cameraPosXDiff * TimeUtil::Get().GetDeltaTime() * 6.0;
					cameraPos.y += cameraPosYDiff * TimeUtil::Get().GetDeltaTime() * 6.0;
				}
				cameraRoom = playerPtr->GetRoom();
			}
			GameSandbox::Get().SetCamera(cameraPos, cameraZoom, cameraRoom);
			
			GameSandbox::Get().DrawObjects();
			if(GameSandbox::Get().GetTimeLeftInSeconds() != 0.0)
				DrawUtil::Get().DrawText(13, UTF8String::IntToStr(GameSandbox::Get().GetTimeLeftInSeconds()/60.0, "%02d") + UTF8String(":") + UTF8String::IntToStr(static_cast<int32_t>(GameSandbox::Get().GetTimeLeftInSeconds()) % 60, "%02d"), Vec2(winSize.x - (150.0f * GUISize), winSize.y - (50.0f * GUISize)), 30.0f * GUISize);
			if(playerPtr) // Update HUD
			{
				DrawUtil::Get().DrawRectangle(14, Vec2(winSize.x / 2.0f - (300.0f * GUISize), 0.0f), Vec2(600.0f, 100.0f) * GUISize, 0.5f, 0.5f);
				DrawUtil::Get().DrawRectangle(14, Vec2(winSize.x / 2.0f - (300.0f * GUISize), (70.0f * GUISize)), Vec2(600.0f, 30.0f) * GUISize, 0.5f);
				DrawUtil::Get().DrawRectangle(14, Vec2(winSize.x / 2.0f - (300.0f * GUISize), (70.0f * GUISize)), Vec2(600.0f * playerPtr->GetHealthLeftPercentage(), 30.0f) * GUISize);
				for(uint8_t i = 0; i < 5; i++)
					DrawUtil::Get().DrawSprite(13, UTF8String::IntToStr(GameSandbox::Get().GetPlayerDataByPlayerID(currentPlayerID)->abilities[i].GetAbilityType()) + "Ability", Vec2(winSize.x / 2.0f - (280.0f * GUISize) + (i * 130.0f * GUISize), 27.0f * GUISize), Vec2(40.0f) * GUISize);
				for(uint8_t i = 0; i < 5; i++)
				{
					int32_t cooldown = std::ceil(playerPtr->GetCooldown(i));
					DrawUtil::Get().DrawText(13, UTF8String::IntToStr(cooldown), Vec2(winSize.x / 2.0f - (270.0f * GUISize) + (130.0f * i * GUISize), 7.0f * GUISize), 15.0f * GUISize);
				}
			}
			else
				DrawUtil::Get().DrawText(13, "☹", Vec2(winSize.x / 2.0f - (50.0f * GUISize), winSize.y / 2.0f - (50.0f * GUISize)), 150.0f);
			if(!paused && ControllerUtil::Get().GetButtonState(CONTROLLER_LIST) == KEYSTATE_HELD)
			{
				Vec2 rectPos = Vec2(winSize.x / 2.0f - (250.0f * GUISize), (100.0f * GUISize));
				Vec2 rectSize = Vec2((500.0f * GUISize), winSize.y - (100.0f * GUISize));
				DrawUtil::Get().DrawRectangle(14, rectPos, rectSize, 0.0f, 0.5f);
				if(netMan && !netMan->IsServer())
				{
					UTF8String pingStr = UTF8String::IntToStr(GameSandbox::Get().GetRTTClient() * 1000.0) + " ms";
					DrawUtil::Get().DrawText(8, pingStr, Vec2(rectPos.x + rectSize.x - DrawUtil::Get().CalculateTextWidth(pingStr, 10.0f * GUISize) - 5.0f, rectPos.y + rectSize.y - 10.0f * GUISize - 5.0f), 10.0f * GUISize, 1.0f);
				}
				const std::vector<PlayerInfoPtr>& allPlayerInfo = GameSandbox::Get().GetAllRealPlayerInfos();
				for(uint32_t i = 0; i < allPlayerInfo.size(); i++)
				{
					float usernameYPos = rectPos.y + rectSize.y - (50.0f * GUISize) - (i * 40.0f * GUISize);
					
					DrawUtil::Get().DrawText(13, allPlayerInfo[i]->userName, Vec2(rectPos.x + 10.0f * GUISize, usernameYPos), 25.0f * GUISize, 1.0f);
					
					// Draw connectivity icon
					uint8_t pingBoxesToDraw = 0;
					if(allPlayerInfo[i]->ping < 0.1f)
						pingBoxesToDraw = 5;
					else if(allPlayerInfo[i]->ping < 0.2f)
						pingBoxesToDraw = 4;
					else if(allPlayerInfo[i]->ping < 0.4f)
						pingBoxesToDraw = 3;
					else if(allPlayerInfo[i]->ping < 0.8f)
						pingBoxesToDraw = 2;
					else
						pingBoxesToDraw = 1;
					
					for(uint8_t i = 0; i < 5; i++)
					{
						// Every box is 4 px wide. Every level is 6 px taller than the last, with the first one being 6px tall
						// The gap between each box is 3px.
						// So, the ping icon will be 32px * 30px
						float pingBoxColor = i < pingBoxesToDraw ? 1.0f : 0.5f;
						DrawUtil::Get().DrawRectangle(13, Vec2(rectPos.x + rectSize.x - 40.0f * GUISize + (i * 7.0f * GUISize), usernameYPos), Vec2(4.0f * GUISize, 6.0f * (i + 1) * GUISize), pingBoxColor);
					}
					
					if(GameSandbox::Get().GetWorldData()->GetType() == WORLD_ARENA)
					{
						UTF8String pointsStr = UTF8String::IntToStr(allPlayerInfo[i]->points);
						DrawUtil::Get().DrawText(13, pointsStr, Vec2(rectPos.x + rectSize.x - DrawUtil::Get().CalculateTextWidth(pointsStr, 25.0f * GUISize) - 50.0f * GUISize, usernameYPos), 25.0f * GUISize, 1.0f);
					}
				}
			}
			
			DrawAnnouncement();
			
			if(paused)
				DrawUtil::Get().DrawRectangle(12, Vec2(0.0f), winSize, 1.0f, 0.8f);
			break;
		}
		case GAME_ENDINIT:
			break;
		case GAME_ENDLOOP:
		{
			if(GameSandbox::Get().GetWorldData()->GetType() == WORLD_ARENA)
			{
				uint32_t playerCount = endGamePlacement.size();
				uint32_t lastHighestPlace = 0;
				int32_t lastHighestPoints = endGamePlacement[0]->points + 1;
				for(uint32_t i = 0; i < playerCount; i++)
				{
					float xPos = ((winSize.x / 2.0f) - (50.0f * playerCount * GUISize)) + (i * 100.0f * GUISize);
					float timeInAnimation = (TimeUtil::Get().GetTimeSinceStart() - endGameAnimationStart) / ENDGAME_ANIMATION_TIME;
					if(timeInAnimation > 1.0f)
						timeInAnimation = 1.0f;
					uint32_t playerCountMinusOne = playerCount > 1 ? (playerCount - 1) : 1;
					if(lastHighestPoints != endGamePlacement[i]->points) // Check if this player is truly below the last player (if they aren't, they are tied, so they should be at the same bar height)
					{
						lastHighestPlace = i;
						lastHighestPoints = endGamePlacement[i]->points;
					}
					float yPos = (((winSize.y / 2.0f) / playerCountMinusOne) * (playerCountMinusOne - lastHighestPlace)) * timeInAnimation * GUISize;
					
					auto texturePtr = GameSandbox::Get().GetPlayerTexturesFromID(endGamePlacement[i]->playerID);
					
					if(!texturePtr || (*texturePtr)[0][0].empty())
						DrawUtil::Get().DrawSprite(11, static_cast<TexturePtr>(nullptr), Vec2(xPos, yPos), Vec2(100.0f) * GUISize);
					else
						DrawUtil::Get().DrawSprite(11, (*texturePtr)[0][0][0], Vec2(xPos, yPos), Vec2(100.0f) * GUISize);
					
					DrawUtil::Get().DrawRectangle(11, Vec2(xPos, 0.0f), Vec2(100.0f * GUISize, yPos), 0.3, 0.3);
					DrawUtil::Get().DrawText(11, endGamePlacement[i]->userName, Vec2((xPos + 50.0f * GUISize) - DrawUtil::Get().CalculateTextWidth(endGamePlacement[i]->userName, 10.0f * GUISize) / 2.0f, yPos + 110.0f * GUISize), 10.0f * GUISize);
					UTF8String pointsStr = UTF8String::IntToStr(endGamePlacement[i]->points);
					DrawUtil::Get().DrawText(11, pointsStr, Vec2((xPos + 50.0f * GUISize) - DrawUtil::Get().CalculateTextWidth(pointsStr, 20.0f * GUISize) / 2.0f, yPos + 130.0f * GUISize), 20.0f * GUISize);
				}
			}
			
			DrawAnnouncement();
			
			break;
		}
	}
}

void GameScreen::DrawAnnouncement()
{
	Vec2 winSize = WindowUtil::Get().GetWindowSize();
	float GUISize = GUIUtil::Get().GetGUISize();
	const UTF8String& announcement = GameSandbox::Get().GetAnnouncement();
	if(announcement != currentAnnouncement)
	{
		oldAnnouncement = currentAnnouncement;
		currentAnnouncement = announcement;
		currentAnnouncementTime = TimeUtil::Get().GetTimeSinceStart();
	}
	double announcementAlpha = (TimeUtil::Get().GetTimeSinceStart() - currentAnnouncementTime) / ANNOUNCEMENT_TRANSITION_TIME;
	if(announcementAlpha > 1.0)
		announcementAlpha = 1.0;
	DrawUtil::Get().DrawText(12, currentAnnouncement, Vec2(winSize.x / 2.0f - DrawUtil::Get().CalculateTextWidth(currentAnnouncement, 50.0f * GUISize) / 2.0f, winSize.y - 150.0f * GUISize), 50.0f * GUISize, 0.0f, announcementAlpha);
	DrawUtil::Get().DrawText(12, oldAnnouncement, Vec2(winSize.x / 2.0f - DrawUtil::Get().CalculateTextWidth(oldAnnouncement, 50.0f * GUISize) / 2.0f, winSize.y - 150.0f * GUISize), 50.0f * GUISize, 0.0f, 1.0 - announcementAlpha);
}

GameScreen::~GameScreen()
{
	GameSandbox::Get().Cleanup();
	LogViewer::Get().DisplayRecentMessages();
}
