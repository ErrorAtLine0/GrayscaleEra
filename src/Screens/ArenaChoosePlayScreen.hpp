#pragma once
#include "../ScreenNavigator.hpp"

class ArenaChoosePlayScreen : public Screen
{
	public:
		ArenaChoosePlayScreen(const std::shared_ptr<Screen>& initHaltedScreen);
		bool Update();
		void Draw();
		
	private:
		enum ArenaChoosePlayState
		{
			ARENACHOOSEPLAY_INIT,
			ARENACHOOSEPLAY_UPDATE
		} state;
		std::shared_ptr<Screen> haltedScreen;
};
