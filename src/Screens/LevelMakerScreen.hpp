#pragma once
#include "../ScreenNavigator.hpp"
#include "../AdventureData.hpp"
#include "../ArenaData.hpp"
#include <GSEGL/Graphics/GUI/GUIUtil.hpp>

class LevelMakerScreen : public Screen
{
	public:
		LevelMakerScreen(const std::shared_ptr<Screen>& initHaltedScreen, LevelData* initLevelData, const WorldDataPtr& initWorldData);
		bool Update();
		void Draw();
	private:
		void UpdateZoom();
		void UpdateGUI();
		bool InitiateTestWorld();
		bool CursorInLevelArea();
		bool CursorInBlockSelectionArea();
		void ReadLevelTextures();
		void FillAreaNearCursor(uint16_t* arrayToFill, int32_t areaX, int32_t areaY, uint16_t val);
		IVec2 BlockAimedAt();
		enum LevelMakerState
		{
			LEVELMAKER_INIT,
			LEVELMAKER_UPDATELOOP
		} state;
		
		UTF8String newBlockFileName;
		
		WorldDataPtr worldData;
		AdventureDataPtr adventureData;
		ArenaDataPtr arenaData;
		LevelData* levelData;
		std::vector<BlockData>* blocks;
		std::vector<PointData>* points;
		
		std::vector<TexturePtr> blockTextures;
		
		UTF8String testPlayerChosen;
		
		Vec2 cameraPos;
		float blockSize;
		uint32_t brushSize;
		
		BlockID selectedBlock;
		BlockID blockAtTopOfMenu;
		
		PointID selectedPoint;
		PointID pointAtTopOfMenu;
		
		uint16_t GetBlockSelectionHoveredOver(uint16_t blockAtTopOfMenu);
		
		enum LevelEditMode
		{
			EDITMODE_FOREGROUND = 0,
			EDITMODE_BACKGROUND,
			EDITMODE_SPAWN,
			EDITMODE_POINT
		} editMode;
		
		bool levelAreaLeftClickFocus; // Prevents people from accidentally placing blocks when going in level editor
		
		bool firstRulerPointSelected;
		UVec2 firstRulerPoint;
		std::shared_ptr<Screen> haltedScreen;
};
