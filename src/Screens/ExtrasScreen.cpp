#include "ExtrasScreen.hpp"
#include <GSEGL/Graphics/GUI/GUIUtil.hpp>
#include <GSEGL/Graphics/GUI/GUIImageButton.hpp>
#include <GSEGL/Graphics/GUI/GUITextButton.hpp>
#include "WaitScreen.hpp"
#include "../GlobalMenuData.hpp"

ExtrasScreen::ExtrasScreen(const std::shared_ptr<Screen>& initHaltedScreen):
state(EXTRAS_INIT),
haltedScreen(initHaltedScreen)
{}

bool ExtrasScreen::Update()
{
	switch(state)
	{
		case EXTRAS_INIT:
		{
			GUIUtil::Get().ClearAll();
			GUIUtil::Get().AddElement("LoadingScreen", new GUITextButton("Loading Screen", Vec2(0.5f, 1.0f), Vec2(-360.0f, -125.0f), Vec2(0.0f), Vec2(720.0f, 50.0f)));
			GUIUtil::Get().AddElement("Back", new GUIImageButton("BackIcon", Vec2(0.0f), Vec2(0.0f), Vec2(0.0f), Vec2(40.0f)));
			state = EXTRAS_UPDATE;
		}
		case EXTRAS_UPDATE:
		{
			if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("LoadingScreen"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<WaitScreen>(ScreenNavigator::Get().GetScreen(), nullptr, nullptr));
				state = EXTRAS_INIT;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Back"))->buttonState == GUIBUTTON_PRESSED)
				ScreenNavigator::Get().SetScreen(haltedScreen);
			break;
		}
	}
	return true;
}

void ExtrasScreen::Draw()
{
	GlobalMenuData::Get().DrawBackground();
}
 
