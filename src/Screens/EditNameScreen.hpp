#pragma once
#include "../ScreenNavigator.hpp"
#include <GSEGL/UTF8String.hpp>

class EditNameScreen : public Screen
{
	public:
		EditNameScreen(const std::shared_ptr<Screen>& initHaltedScreen, UTF8String* initNamePtr, const std::vector<UTF8String>& initSameName);
		bool Update();
		void Draw();
	private:
		enum EditNameState {
			EDITNAME_INIT,
			EDITNAME_UPDATE
		} state;
		UTF8String startName;
		UTF8String* namePtr;
		std::vector<UTF8String> sameName;
		std::shared_ptr<Screen> haltedScreen;
};
