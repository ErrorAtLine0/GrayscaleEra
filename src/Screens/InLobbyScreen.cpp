#include "InLobbyScreen.hpp"
#include <GSEGL/Graphics/GUI/GUITextEntry.hpp>
#include <GSEGL/Graphics/GUI/GUITextButton.hpp>
#include <GSEGL/FileUtil.hpp>
#include <GSEGL/LogUtil.hpp>
#include "../LogViewer.hpp"
#include "SelectorScreen.hpp"

InLobbyScreen::InLobbyScreen(const std::shared_ptr<Screen>& initHaltedScreen, std::shared_ptr<NetworkManager> initNetMan):
state(INLOBBY_READ_PLAYER_INIT),
netMan(initNetMan),
haltedScreen(initHaltedScreen)
{
}

bool InLobbyScreen::Update()
{
	switch(state)
	{
		case INLOBBY_READ_PLAYER_INIT:
		{
			if(netMan->NeedsPlayerFromFile())
			{
				if(netMan->GetMaxPlayerPoints() != 0)
					LogUtil::Get().LogMessage("NORMAL", UTF8String("Maximum Player Points: ") + UTF8String::IntToStr(netMan->GetMaxPlayerPoints()));
				else
					LogUtil::Get().LogMessage("NORMAL", "All characters allowed!");
				ScreenNavigator::Get().SetScreen(std::make_shared<SelectorScreen>(ScreenNavigator::Get().GetScreen(), &playerName, FILETYPE_PLAYER, [](const std::shared_ptr<void>& dataPairPtr)
				{
					auto& dataPair = *std::static_pointer_cast<std::pair<std::shared_ptr<NetworkManager>, std::shared_ptr<Screen>>>(dataPairPtr);
					if(!dataPair.first->Update())
						ScreenNavigator::Get().SetScreen(dataPair.second);
				}, std::make_shared<std::pair<std::shared_ptr<NetworkManager>, std::shared_ptr<Screen>>>(netMan, haltedScreen)));
			}
			state = INLOBBY_INIT;
			break;
		}
		case INLOBBY_INIT:
		{
			if(!playerName.IsEmpty())
			{
				PlayerDataPtr playerData = PlayerData::ReadPlayerFile(playerName);
				if(playerData)
				{
					if(netMan->IsPlayerOP(playerData))
					{
						LogUtil::Get().LogMessage("NORMAL", UTF8String("This character is overpowered!"));
						state = INLOBBY_READ_PLAYER_INIT;
						break;
					}
					else
						netMan->SetPlayerData(playerData);
				}
				playerName.Clear();
			}
			if(netMan->NeedsPlayerFromFile())
			{
				ScreenNavigator::Get().SetScreen(haltedScreen);
				return true;
			}
			
			ErrorUtil::Get().ClearErrors();
			LogUtil::Get().LogMessage("NORMAL", "Entered Lobby");
			GUIUtil::Get().ClearAll();
			GUIUtil::Get().AddElement("Exit Lobby", new GUITextButton("Exit Lobby", Vec2(0.5f, 0.0f), Vec2(-360.0f, 25.0f), Vec2(0.0f), Vec2(720.0f, 50.0f)));
			GUIUtil::Get().AddElement("Message Box", new GUITextEntry("Type Message Here...", Vec2(0.0f), Vec2(50.0f, 100.0f), Vec2(0.0f), Vec2(700.0f, 15.0f), MAX_MESSAGE_LENGTH));
			state = INLOBBY_UPDATE;
			break;
		}
		case INLOBBY_UPDATE:
		{
			if(!netMan->Update() || static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Exit Lobby"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(haltedScreen);
				return true;
			}
			if(ControllerUtil::Get().GetButtonState(CONTROLLER_CHAT) == KEYSTATE_PRESSED)
			{
				GUITextEntryState* messageEntry = static_cast<GUITextEntryState*>(GUIUtil::Get().GrabState("Message Box"));
				if(messageEntry)
				{
					if(GUIUtil::Get().IsSelectedElement("Message Box"))
					{
						if(!messageEntry->text.IsEmpty())
						{
							netMan->SendChatMessage(messageEntry->text);
							messageEntry->text.Clear();
						}
					}
					else
						GUIUtil::Get().SelectElement("Message Box");
				}
			}
			if(GUIUtil::Get().IsSelectedElement("Message Box"))
				LogViewer::Get().DisplayAllMessages();
			else
				LogViewer::Get().DisplayRecentMessages();
			break;
		}
	}
	return true;
}

void InLobbyScreen::Draw()
{
	Vec2 winSize = WindowUtil::Get().GetWindowSize();
	switch(state)
	{
		case INLOBBY_UPDATE:
		{
			const std::vector<PlayerNetDataPtr>& players = netMan->GetAllPlayerNetData();
			if(players.size() == netMan->GetRequiredPlayerCount())
				DrawUtil::Get().DrawText(11, "Game starting in a few moments", Vec2(winSize.x / 2.0f - DrawUtil::Get().CalculateTextWidth("Game starting in a few moments", 30.0f) / 2.0f, winSize.y - 50.0f), 30.0f);
			else
			{
				char playerCountMessage[4] = {static_cast<char>('0' + players.size()), '/', static_cast<char>('0' + netMan->GetRequiredPlayerCount()), '\0'};
				DrawUtil::Get().DrawText(11, playerCountMessage, Vec2(winSize.x / 2.0f - DrawUtil::Get().CalculateTextWidth(playerCountMessage, 30.0f) / 2.0f, winSize.y - 50.0f), 30.0f);
			}
			for(std::size_t i = 0; i < players.size(); i++)
				DrawUtil::Get().DrawText(11, players[i]->userName, Vec2(winSize.x / 2.0f - DrawUtil::Get().CalculateTextWidth(players[i]->userName, 30.0f) / 2.0f, winSize.y - 100.0f - i * 35.0f), 30.0f);
			break;
		}
		default:
			break;
	}
}

InLobbyScreen::~InLobbyScreen()
{
	LogViewer::Get().DisplayRecentMessages();
}
