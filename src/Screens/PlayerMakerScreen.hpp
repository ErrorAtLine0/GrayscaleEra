#pragma once
#include "../ScreenNavigator.hpp"
#include "../PlayerData.hpp"
#include <GSEGL/UTF8String.hpp>
#define DARK_PIXEL_AMOUNT 50

class PlayerMakerScreen : public Screen
{
	public:
		PlayerMakerScreen(const std::shared_ptr<Screen>& initHaltedScreen, const PlayerDataPtr& initPlayerData);
		~PlayerMakerScreen();
		bool Update();
		void Draw();
	private:
		enum PlayerMakerState
		{
			PLAYERMAKER_INIT,
			PLAYERMAKER_UPDATE
		} state;
		PlayerDataPtr playerData;
		
		uint32_t currentFrameInAnimation;
		double timeSinceLastFrame;
		bool makerArenaAvailable;
		UTF8String testArenaChosen;
		
		std::shared_ptr<Screen> haltedScreen;
};
