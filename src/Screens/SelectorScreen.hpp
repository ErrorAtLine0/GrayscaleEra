#pragma once
#include "../ScreenNavigator.hpp"
#include <GSEGL/UTF8String.hpp>
#include <GSEGL/Graphics/GUI/GUIUtil.hpp>
#include <GSEGL/Graphics/GUI/GUIScrollArea.hpp>
#include "../WorldData.hpp"
#include "../Frame.hpp"

enum FileType
{
	FILETYPE_BLOCK,
	FILETYPE_PLAYER,
	FILETYPE_ADVENTURE,
	FILETYPE_ARENA
};

enum SelectorIndexType
{
	SELECTOR_INDEX_UINT8,
	SELECTOR_INDEX_UINT16,
	SELECTOR_INDEX_UINT32,
	SELECTOR_INDEX_UINT64,
	SELECTOR_INDEX_INT8,
	SELECTOR_INDEX_INT16,
	SELECTOR_INDEX_INT32,
	SELECTOR_INDEX_INT64
};

enum FileOperation
{
	FILEOPERATION_NONE,
	FILEOPERATION_RENAME,
	FILEOPERATION_EDIT,
	FILEOPERATION_COPY
};

class SelectorScreen : public Screen
{
	public:
		SelectorScreen(const std::shared_ptr<Screen>& initHaltedScreen, UTF8String* initFileNamePtr, FileType initFileType, void (*initBackgroundFunc)(const std::shared_ptr<void>&) = nullptr, const std::shared_ptr<void>& initBackgroundFuncData = nullptr);
		SelectorScreen(const std::shared_ptr<Screen>& initHaltedScreen, void* initVectorData, void* initIndexChosenPtr, SelectorIndexType initReturnIndexType, FileType initFileType, const WorldDataPtr& initWorldData, void (*initBackgroundFunc)(const std::shared_ptr<void>&) = nullptr, const std::shared_ptr<void>& initBackgroundFuncData = nullptr);
		bool Update();
		void Draw();
	private:
		enum PlayerSelectorState
		{
			SELECTOR_INIT,
			SELECTOR_UPDATE
		} state;
		void PopulateScreen();
		void EditFile(uint32_t index);
		std::shared_ptr<Screen> GetEditScreen(const std::shared_ptr<void> fileToEdit);
		void WriteToVector(const std::shared_ptr<void>& fileToWrite, uint32_t index);
		std::shared_ptr<void> ReadFromVector(uint32_t index);
		void* GetFileFromVector(uint32_t index);
		bool ReadFromFile(const UTF8String& fileName, void* fileToReadTo);
		const Frame* GetImageFromFile(void* fileToReadFrom);
		std::shared_ptr<Frame> ReadImageFromFile(const UTF8String& fileName);
		void AddGUIImage(const Frame* frame, const Vec2& bottomLeftCorner);
		const char* GetBottomButtonName();
		void WriteDataToFile(const std::shared_ptr<void>& fileData, const UTF8String& fileName);
		bool AddFileToVector(const UTF8String& fileName);
		std::shared_ptr<void> AllocateMemory();
		void SetReturnIndex(int32_t index);
		void SetPathToSearch();
		
		int32_t fileToEdit;
		UTF8String returnName;
		std::shared_ptr<void> returnData;
		std::shared_ptr<std::vector<UTF8String>> fileNames;
		UTF8String pathToSearch;
		
		void* vectorData;
		void* dataChosenPtr;
		
		void (*backgroundFunc)(const std::shared_ptr<void>&);
		std::shared_ptr<void> backgroundFuncData;
		
		// Different index types used (such as NPCID or BlockID, which are both unsigned 16 bit integers). If unsigned, clicking "Back" will set value to 0. If signed, "Back" will set value to -1.
		SelectorIndexType returnIndexType;
		FileOperation fileOperation;
		
		GUIScrollAreaState* scrollState;
		FileType fileType;
		WorldDataPtr worldData;
		std::shared_ptr<Screen> haltedScreen;
};
