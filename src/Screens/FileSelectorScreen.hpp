#pragma once
#include "../ScreenNavigator.hpp"
#include <GSEGL/UTF8String.hpp>
#include <GSEGL/Graphics/GUI/GUIScrollArea.hpp>
#include <GSEGL/Graphics/GUI/GUIUtil.hpp>

class FileSelectorScreen : public Screen
{
	public:
		FileSelectorScreen(const std::shared_ptr<Screen>& initHaltedScreen, const UTF8String& initPathToSearch, UTF8String* initFileNamePtr);
		bool Update();
		void Draw();
	private:
		enum FileSelectorState
		{
			FILESELECTOR_INIT,
			FILESELECTOR_UPDATE
		} state;
		std::shared_ptr<std::vector<UTF8String>> fileNames;
		UTF8String* fileNamePtr;
		UTF8String pathToSearch;
		GUIScrollAreaState* scrollState;
		std::shared_ptr<Screen> haltedScreen;
};
