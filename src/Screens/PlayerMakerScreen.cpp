#include "PlayerMakerScreen.hpp"
#include "PropertiesScreen.hpp"
#include "AnimationListScreen.hpp"
#include "AnimatorScreen.hpp"
#include "GameScreen.hpp"
#include "SelectorScreen.hpp"
#include <GSEGL/Graphics/GUI/GUIImageButton.hpp>
#include <GSEGL/Graphics/GUI/GUIImmutableText.hpp>
#include <GSEGL/Graphics/GUI/GUICanvas.hpp>
#include <GSEGL/Graphics/GUI/GUICounter.hpp>
#include <GSEGL/LogUtil.hpp>
#include <GSEGL/FileUtil.hpp>
#include <GSEGL/TimeUtil.hpp>
#include "../GameSandbox.hpp"
#include "../InputManager.hpp"

PlayerMakerScreen::PlayerMakerScreen(const std::shared_ptr<Screen>& initHaltedScreen, const PlayerDataPtr& initPlayerData):
state(PLAYERMAKER_INIT),
playerData(initPlayerData),
makerArenaAvailable(false),
haltedScreen(initHaltedScreen)
{}

bool PlayerMakerScreen::Update()
{
	switch(state)
	{
		case PLAYERMAKER_INIT:
		{
			GUIUtil::Get().ClearAll();
			
			for(uint8_t i = 0; i < 5; i++)
				playerData->abilities[i].SetAbilityToTypeChosen();
			
			if(!testArenaChosen.IsEmpty())
			{
				std::shared_ptr<InMemBitStream> arenaDataInStream = FileUtil::Get().ReadFromFile(UTF8String("arenas/") + testArenaChosen);
				testArenaChosen = "";
				if(arenaDataInStream)
				{
					GameSandboxProperties gameProperties;
					gameProperties.gameTime = 0;
					gameProperties.respawnTime = ARENA_RESPAWN_TIME;
					ArenaDataPtr arenaData = std::make_shared<ArenaData>();
					arenaData->Read(*arenaDataInStream);
					ScreenNavigator::Get().SetScreen(std::make_shared<GameScreen>(ScreenNavigator::Get().GetScreen(), gameProperties, arenaData, 1, nullptr));
					GameSandbox::Get().AddPlayer("", playerData, 1);
					break;
				}
			}
			
			GameSandbox::Get().Cleanup();
			std::shared_ptr<InMemBitStream> arenaDataInStream = FileUtil::Get().ReadFromFile("otherData/PlayerMakerArena");
			if(arenaDataInStream)
			{
				makerArenaAvailable = true;
				GameSandboxProperties gameProperties;
				gameProperties.gameTime = 0;
				gameProperties.respawnTime = 0;
				ArenaDataPtr arenaData = std::make_shared<ArenaData>();
				arenaData->Read(*arenaDataInStream);
				GameSandbox::Get().Init(gameProperties, arenaData, nullptr, 1);
				GameSandbox::Get().AddPlayer("Test Player", playerData, 1);
				GameSandbox::Get().SetCamera(Vec2(400.0f - WindowUtil::Get().GetWindowSize().x / 2.0f, 0.0f), 1.0f, 0);
			}
				
			GUIUtil::Get().AddElement("Back", new GUIImageButton("BackIcon", Vec2(0.0f), Vec2(0.0f), Vec2(0.0f), Vec2(40.0f)));
			
			for(uint8_t i = 0; i < 5; i++)
			{
				UTF8String iStr = UTF8String::IntToStr(i);
				GUIUtil::Get().AddElement(iStr + "AbilityList", new GUIImageButton("ListIcon", Vec2(0.2f * i + 0.1f, 1.0f), Vec2(-60.0f, -50.0f), Vec2(0.0f), Vec2(40.0f)));
				GUIUtil::Get().AddElement(iStr + "AbilityProp", new GUIImageButton("SettingsIcon", Vec2(0.2f * i + 0.1f, 1.0f), Vec2(-20.0f, -50.0f), Vec2(0.0f), Vec2(40.0f)));
				GUIUtil::Get().AddElement(iStr + "AbilityAnim", new GUIImageButton("PencilIcon", Vec2(0.2f * i + 0.1f, 1.0f), Vec2(20.0f, -50.0f), Vec2(0.0f), Vec2(40.0f)));
				GUIUtil::Get().AddElement(iStr + "AbilityCooldown", new GUICounter(Vec2(0.2f * i + 0.1f, 1.0f), Vec2(-60.0f, -150.0f), Vec2(0.0f), Vec2(120.0f, 20.0f), playerData->abilities[i].GetCooldown(), 0.25, 20, 0.25));
				GUIUtil::Get().AddElement(iStr + "Image", new GUIImageButton(UTF8String::IntToStr(playerData->abilities[i].GetAbilityType()) + "Ability", Vec2(0.2f * i + 0.1f, 1.0f), Vec2(-20.0f, -110.0f), Vec2(0.0f), Vec2(40.0f)));
			}
			
			GUIUtil::Get().AddElement("WalkAnimationText", new GUIImmutableText("Walk → Animation", Vec2(0.025f, 0.65f), Vec2(0.0f, 0.0f), 0.0f, 20.0f));
			GUIUtil::Get().AddElement("WalkAnimationEdit", new GUIImageButton("PencilIcon", Vec2(0.025f, 0.65f), Vec2(100.0f, -50.0f), Vec2(0.0f), Vec2(40.0f)));
			
			GUIUtil::Get().AddElement("IdleAnimationText", new GUIImmutableText("Idle Animation", Vec2(0.025f, 0.35f), Vec2(20.0f, 0.0f), 0.0f, 20.0f));
			GUIUtil::Get().AddElement("IdleAnimationEdit", new GUIImageButton("PencilIcon", Vec2(0.025f, 0.35f), Vec2(100.0f, -50.0f), Vec2(0.0f), Vec2(40.0f)));
			GUIUtil::Get().AddElement("IdleAnimationView", new GUICanvas(Vec2(300.0f, 200.0f), Vec2(0.0f), Vec2(0.0f), Vec2(200.0f), UVec2(PLAYER_TEXTURE_SIZE)));
			
			GUIUtil::Get().AddElement("StatisticsText", new GUIImmutableText("Statistics", Vec2(0.975f, 0.65f), Vec2(-220.0f, 0.0f), 0.0f, 20.0f));
			GUIUtil::Get().AddElement("StatisticsEdit", new GUIImageButton("SettingsIcon", Vec2(0.975f, 0.65f), Vec2(-160.0f, -50.0f), Vec2(0.0f), Vec2(40.0f)));
			
			GUIUtil::Get().AddElement("PlayerPointsText", new GUIImmutableText("Player Points Spent", Vec2(0.975f, 0.35f), Vec2(-270.0f, 0.0f), 0.0f, 18.0f));
			GUIUtil::Get().AddElement("PlayerPointsNumber", new GUIImmutableText(UTF8String::IntToStr(playerData->GetPoints()), Vec2(0.975f, 0.35f), Vec2(-170.0f, -50.0f), 0.0f, 20.0f));
			
			GUIUtil::Get().AddElement("TestArena", new GUIImageButton("ArrowButton", Vec2(1.0f, 0.0f), Vec2(-40.0f, 0.0f), Vec2(0.0f), Vec2(40.0f)));
			
			currentFrameInAnimation = 0;
			timeSinceLastFrame = 0.0;
			if(!playerData->idleAnimation.empty())
			{
				GUICanvasState* canvasState = static_cast<GUICanvasState*>(GUIUtil::Get().GrabState("IdleAnimationView"));
				canvasState->SetCanvasData(playerData->idleAnimation[currentFrameInAnimation].GetData());
				canvasState->SyncCanvas();
			}
			if(playerData->statistics.GetPropListRef().GetInt(0) == 0) // Notify about 0 health
				LogUtil::Get().LogMessage("NORMAL", "Your player is glitching out! Give them some health.");
			
			state = PLAYERMAKER_UPDATE;
			break;
		}
		case PLAYERMAKER_UPDATE:
		{
			if(makerArenaAvailable)
			{
				InputManager::Get().Update();
				GameSandbox::Get().GetRealPlayerInfoByPlayerID(1)->latestInputState = InputManager::Get().GetCurrentInputState();
				GameSandbox::Get().Update();
				if(WindowUtil::Get().IsWindowResized())
					GameSandbox::Get().SetCamera(Vec2(400.0f - WindowUtil::Get().GetWindowSize().x / 2.0f, 0.0f), 1.0f, 0);
			}
			for(uint8_t i = 0; i < 5; i++)
			{
				GUICounterState* counterState = static_cast<GUICounterState*>(GUIUtil::Get().GrabState(UTF8String::IntToStr(i) + "AbilityCooldown"));
				if(counterState->update)
				{
					playerData->abilities[i].SetCooldown(counterState->GetVal());
					static_cast<GUIImmutableTextState*>(GUIUtil::Get().GrabState("PlayerPointsNumber"))->text = UTF8String::IntToStr(playerData->GetPoints());
				}
				if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState(UTF8String::IntToStr(i) + "AbilityList"))->buttonState == GUIBUTTON_PRESSED)
				{
					ScreenNavigator::Get().SetScreen(std::make_shared<PropertiesScreen>(ScreenNavigator::Get().GetScreen(), &playerData->abilities[i].GetTypeListRef()));
					state = PLAYERMAKER_INIT;
				}
				else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState(UTF8String::IntToStr(i) + "AbilityProp"))->buttonState == GUIBUTTON_PRESSED)
				{
					ScreenNavigator::Get().SetScreen(std::make_shared<PropertiesScreen>(ScreenNavigator::Get().GetScreen(), &playerData->abilities[i].GetPropListRef()));
					state = PLAYERMAKER_INIT;
				}
				else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState(UTF8String::IntToStr(i) + "AbilityAnim"))->buttonState == GUIBUTTON_PRESSED)
				{
					ScreenNavigator::Get().SetScreen(std::make_shared<AnimationListScreen>(ScreenNavigator::Get().GetScreen(), &playerData->abilities[i].GetAnimationListRef(), UVec2(PLAYER_TEXTURE_SIZE), DARK_PIXEL_AMOUNT));
					state = PLAYERMAKER_INIT;
				}
			}
			if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("Back"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(haltedScreen);
				return true;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("StatisticsEdit"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<PropertiesScreen>(ScreenNavigator::Get().GetScreen(), &playerData->statistics.GetPropListRef()));
				state = PLAYERMAKER_INIT;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("WalkAnimationEdit"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<AnimatorScreen>(ScreenNavigator::Get().GetScreen(), ABILITY_MAX_FRAMES, &playerData->walkAnimation, UVec2(PLAYER_TEXTURE_SIZE), DARK_PIXEL_AMOUNT));
				state = PLAYERMAKER_INIT;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("IdleAnimationEdit"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<AnimatorScreen>(ScreenNavigator::Get().GetScreen(), ABILITY_MAX_FRAMES, &playerData->idleAnimation, UVec2(PLAYER_TEXTURE_SIZE), DARK_PIXEL_AMOUNT));
				state = PLAYERMAKER_INIT;
			}
			else if(static_cast<GUIButtonState*>(GUIUtil::Get().GrabState("TestArena"))->buttonState == GUIBUTTON_PRESSED)
			{
				ScreenNavigator::Get().SetScreen(std::make_shared<SelectorScreen>(ScreenNavigator::Get().GetScreen(), &testArenaChosen, FILETYPE_ARENA));
				makerArenaAvailable = false;
				state = PLAYERMAKER_INIT;
			}
			
			if(!playerData->idleAnimation.empty())
			{
				timeSinceLastFrame += TimeUtil::Get().GetDeltaTime();
				if(timeSinceLastFrame >= playerData->idleAnimation[currentFrameInAnimation].GetMsLength()/1000.0)
				{
					GUICanvasState* canvasState = static_cast<GUICanvasState*>(GUIUtil::Get().GrabState("IdleAnimationView"));
					++currentFrameInAnimation;
					if(currentFrameInAnimation > playerData->idleAnimation.size()-1)
						currentFrameInAnimation = 0;
					canvasState->SetCanvasData(playerData->idleAnimation[currentFrameInAnimation].GetData());
					canvasState->SyncCanvas();
					timeSinceLastFrame = 0.0;
				}
			}
			break;
		}
	}
	return true;
}

void PlayerMakerScreen::Draw()
{
	if(makerArenaAvailable)
		GameSandbox::Get().DrawObjects();
}

PlayerMakerScreen::~PlayerMakerScreen()
{
	if(makerArenaAvailable)
		GameSandbox::Get().Cleanup();
}
