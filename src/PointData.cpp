#include "PointData.hpp"
#include "PropertyDecoderUtil.hpp"

PointData::PointData():
propList("Point Settings")
{
	propList.AddString("Point Tag", 16);
	
	PropertiesList conditions;
	conditions.AddHeadline("Adventure Just Started");
	conditions.AddList("Check Global Number", PropertyDecoderUtil::GetCheckNumberList());
	conditions.AddList("Check Private Number", PropertyDecoderUtil::GetCheckNumberList());
	conditions.AddList("Check For Player", PropertyDecoderUtil::GetPlayerSelectorList());
	conditions.AddList("Every X Milliseconds", PropertyDecoderUtil::GetTimerList());
	conditions.AddInt("Random Percentage", 0, 100, 1, 0);
	
	PropertiesList results;
	results.AddList("Set Global Number", PropertyDecoderUtil::GetSetNumberList());
	results.AddList("Set Private Number", PropertyDecoderUtil::GetSetNumberList());
	results.AddList("Set Player Private Number", PropertyDecoderUtil::GetSetPlayerPrivateNumberList());
	PropertiesList blockList("Block Data");
	blockList.AddBlock("Block To Place");
	blockList.AddBool("Is Background");
	results.AddList("Place Block", blockList);
	results.AddHeadline("Remove Placed Block");
	results.AddList("Summon NPC", PropertyDecoderUtil::GetNPCList());
	results.AddList("TP Player To Point", PropertyDecoderUtil::GetPlayerToPointList());
	results.AddList("Set Player Spawn", PropertyDecoderUtil::GetPlayerToPointList());
	results.AddList("Print Message", PropertyDecoderUtil::GetMessageList());
	results.AddList("Print Announcement", PropertyDecoderUtil::GetAnnouncementList());
	PropertiesList playerToEffect("Set Player Effects");
	playerToEffect.AddList("Player", PropertyDecoderUtil::GetPlayerSelectorList());
	playerToEffect.AddList("Status Effect", PropertyDecoderUtil::GetStatusEffectList());
	results.AddList("Give Player Effects", playerToEffect);
	results.AddHeadline("End Game");
	
	propList.AddRuleList("Point Rules", conditions, results);
}

void PointData::Write(OutMemBitStream& outStream) const { propList.Write(outStream); }
bool PointData::Read(InMemBitStream& inStream) { return propList.Read(inStream); }
