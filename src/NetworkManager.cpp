#include "NetworkManager.hpp"
#include "Screens/InLobbyScreen.hpp"
#include "GameSandbox.hpp"
#include "GlobalSettingsUtil.hpp"
#include <GSEGL/Network/SocketUtil.hpp>
#include <GSEGL/Network/OutMemBitStream.hpp>
#include <GSEGL/FileUtil.hpp>
#include <GSEGL/LogUtil.hpp>

#ifdef SIMULATE_REAL_LIFE_NET
#include <GSEGL/RandGenUtil.hpp>

#define SIMULATE_LATENCY_SECONDS 0.15
#define SIMULATE_MAX_JITTER 0.03
#define SIMULATE_PACKET_LOSS_CHANCE 0.02

bool NetworkManager::ReceiveLaggingPackets()
{
	laggingPackets.emplace_back();
	laggingPackets.back().second.size = sock->ReceiveFrom(laggingPackets.back().second.data, MAX_PACKET_SIZE, laggingPackets.back().second.from);
	if(laggingPackets.back().second.size < 0)
	{
		laggingPackets.pop_back();
		return false;
	}
	if(RandGenUtil::Get().GetRandomDouble(0.0, 1.0) >= SIMULATE_PACKET_LOSS_CHANCE)
	{
		laggingPackets.back().first = 
			TimeUtil::Get().GetTimeSinceStart() +
			SIMULATE_LATENCY_SECONDS +
			RandGenUtil::Get().GetRandomDouble(-0.5, 0.5) * SIMULATE_MAX_JITTER;
	}
	else
	{
		laggingPackets.pop_back();
	}
	return true;
}

void NetworkManager::UpdateLaggingPackets()
{
	laggingPackets.erase(std::remove_if(laggingPackets.begin(), laggingPackets.end(), [&](std::pair<double, Packet> pack)
	{
		if(pack.first <= TimeUtil::Get().GetTimeSinceStart())
		{
			packets.emplace_back(pack.second);
			return true;
		}
		return false;
	}), laggingPackets.end());
}
#endif

bool NetworkManager::Init(const UTF8String& initUserName, const UTF8String& initLobbyName, uint16_t initPort)
{
	if(!GeneralInit(initUserName, initPort)) return false;
	lobbyName = initLobbyName;
	lobbyServerAddr = SocketUtil::Get().CreateAddrFromString(
	GlobalSettingsUtil::Get().mainSettings.GetString(1), DEFAULT_PORT_STR, INET);
	if(!lobbyServerAddr) return false;
	return true;
}

bool NetworkManager::Init(const UTF8String& initUserName, uint16_t initPort)
{
	if(!GeneralInit(initUserName, initPort)) return false;
	return true;
}

bool NetworkManager::GeneralInit(const UTF8String& initUserName, uint16_t initPort)
{
	timeLastPacketToLobbyServer = 0.0;
	lobbyServerSendAttempts = 0;
	requiredPlayerCount = 0;
	playerID = 0;
	lobbyServerAddr = nullptr;
	sock = SocketUtil::Get().CreateUDPSocket(INET);
	SocketAddress ownAddr(initPort, INET);
	if(!sock) return false;
	if(sock->Bind(ownAddr) < 0) return false;
	userName = initUserName;
	sock->SetBlockingMode(false);
	state = NETMAN_GETLOBBY;
	return true;
}

bool NetworkManager::SetPlayerData(const PlayerDataPtr& playerDataToLoad)
{
	playerDataFromFile = playerDataToLoad;
	return true;
}

void NetworkManager::SendPacket(const OutMemBitStream& outStream, const SocketAddress& addr)
{
	sock->SendTo(outStream.GetBufferPtr(), outStream.GetByteLength(), addr);
}

void NetworkManager::ReceivePackets()
{
	packets.clear();
#ifdef SIMULATE_REAL_LIFE_NET
	UpdateLaggingPackets();
#endif
	for(;;)
	{
	#ifdef SIMULATE_REAL_LIFE_NET
		if(!ReceiveLaggingPackets())
			break;
	#else
		packets.emplace_back();
		packets.back().size = sock->ReceiveFrom(packets.back().data, MAX_PACKET_SIZE, packets.back().from);
		if(packets.back().size < 0)
		{
			packets.pop_back();
			break;
		}
	#endif
	}
}

uint8_t NetworkManager::GetRequiredPlayerCount() const { return requiredPlayerCount; }
const std::vector<PlayerNetDataPtr>& NetworkManager::GetAllPlayerNetData() const { return players; }
const UTF8String& NetworkManager::GetUsername() const { return userName; }
LinkingContext* NetworkManager::GetLinkingContext() { return &linkingContext; }
PlayerID NetworkManager::GetOwnPlayerID() const { return playerID; }
uint64_t NetworkManager::GetMaxPlayerPoints() const { return playerMaxPoints; }

// Fix Record Death later
void NetworkManager::RecordDeath(PlayerID killer, PlayerID victim)
{
	if(worldData->GetType() == WORLD_ARENA)
	{
		PlayerInfoPtr victimPtr = GameSandbox::Get().GetRealPlayerInfoByPlayerID(victim);
		PlayerInfoPtr killerPtr = GameSandbox::Get().GetRealPlayerInfoByPlayerID(killer);
		if(killer != victim && killerPtr && victimPtr) // Was killed
		{
			LogUtil::Get().LogMessage("NORMAL", killerPtr->userName + UTF8String(" killed ") + victimPtr->userName);
			++killerPtr->points;
			--victimPtr->points;
		}
		else if(victimPtr)
		{
			LogUtil::Get().LogMessage("NORMAL", victimPtr->userName + UTF8String(" died"));
			victimPtr->points -= 2;
		}
	}
	else // No points in adventures, and NPCs count in adventures
	{
		const UTF8String* victimName = GameSandbox::Get().GetUserNameByPlayerID(victim);
		const UTF8String* killerName = GameSandbox::Get().GetUserNameByPlayerID(killer);
		if(killer != victim && killerName && victimName) // Was killed
			LogUtil::Get().LogMessage("NORMAL", *killerName + UTF8String(" killed ") + *victimName);
		else if(victimName)
			LogUtil::Get().LogMessage("NORMAL", *victimName + UTF8String(" died"));
	}
}

void NetworkManager::ClearAllPlayerRawData()
{
	playerIDToPlayerDataStream.clear();
}

bool NetworkManager::IsPlayerOP(const PlayerDataPtr& playerDataToCheck) const
{
	return playerMaxPoints && playerDataToCheck->GetPoints() > playerMaxPoints;
}

bool NetworkManager::NeedsPlayerFromFile() const
{
	return requestPlayer && !playerDataFromFile;
}

std::vector<PlayerNetDataPtr>::iterator NetworkManager::GetPlayerNetDataItByPlayerID(PlayerID playerID)
{
	return std::find_if(players.begin(), players.end(), [&playerID](const PlayerNetDataPtr& netData){ return netData->playerID == playerID; });
}

PlayerNetDataPtr NetworkManager::GetPlayerNetDataByPlayerID(PlayerID playerID)
{
	auto it = GetPlayerNetDataItByPlayerID(playerID);
	if(it == players.end())
		return nullptr;
	else
		return *it;
}

NetworkManager::~NetworkManager()
{
}

bool BasicNetworkWaitLoop(const std::shared_ptr<void>& netMan)
{
	return std::static_pointer_cast<NetworkManager>(netMan)->Update();
}
