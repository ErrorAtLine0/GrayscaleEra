#include "LogViewer.hpp"
#include <GSEGL/TimeUtil.hpp>
#include <GSEGL/LogUtil.hpp>
#include <GSEGL/Graphics/DrawUtil.hpp>
#include <GSEGL/Graphics/GUI/GUIUtil.hpp>
#define TIME_MESSAGE_VISIBLE 4.0
#define MESSAGE_BOX_ALPHA 0.75f
#define MESSAGE_TEXT_ALPHA 0.9f

LogViewer& LogViewer::Get()
{
	static LogViewer sInstance;
	return sInstance;
}

void LogViewer::Update()
{
	while(LogUtil::Get().ContainsLoggedMessages("NORMAL"))
	{
		loggedMessages.emplace_front(LogUtil::Get().GetLoggedMessage("NORMAL"));
		if(loggedMessages.size() >= 10)
			loggedMessages.pop_back();
	}
}

void LogViewer::Draw()
{
	float GUISize = GUIUtil::Get().GetGUISize();
	if(displayAllMessages)
		for(uint8_t i = 0; i < loggedMessages.size(); i++)
		{
			DrawUtil::Get().DrawRectangle(0, Vec2(5.0f, 125.0f + i * 20.0f) * GUISize, Vec2(790.0f, 20.0f) * GUISize, 0.2f, MESSAGE_BOX_ALPHA);
			DrawUtil::Get().DrawText(0, loggedMessages[i].GetMess(), Vec2(10.0f, 127.0f + i * 20.0f) * GUISize, 17.0f * GUISize, 1.0f, MESSAGE_TEXT_ALPHA);
		}
	else
	{
		uint8_t notTransparentCount = 0;
		for(const LoggedMessage& loggedMessage : loggedMessages)
		{
			if(loggedMessage.GetAlpha() != 0.0f)
			{
				++notTransparentCount;
				DrawUtil::Get().DrawRectangle(0, Vec2(5.0f, 125.0f + notTransparentCount * 20.0f) * GUISize, Vec2(790.0f, 20.0f) * GUISize, 0.5f, loggedMessage.GetAlpha() * MESSAGE_BOX_ALPHA);
				DrawUtil::Get().DrawText(0, loggedMessage.GetMess(), Vec2(10.0f, 127.0f + notTransparentCount * 20.0f) * GUISize, 17.0f * GUISize, 1.0f, loggedMessage.GetAlpha() * MESSAGE_TEXT_ALPHA);
			}
		}
	}
}

void LogViewer::DisplayAllMessages() { displayAllMessages = true; }
void LogViewer::DisplayRecentMessages() { displayAllMessages = false; }

LogViewer::LoggedMessage::LoggedMessage(const UTF8String& initMessage):
message(initMessage),
timeLogged(TimeUtil::Get().GetTimeSinceStart())
{}

const UTF8String& LogViewer::LoggedMessage::GetMess() const { return message; }

float LogViewer::LoggedMessage::GetAlpha() const
{	
	double timeDelta = (timeLogged + TIME_MESSAGE_VISIBLE) - TimeUtil::Get().GetTimeSinceStart();
	if(timeDelta < 0.0)
		return 0.0f;
	else if(timeDelta > 1.0)
		return 1.0f;
	else
		return static_cast<float>(timeDelta);
}
