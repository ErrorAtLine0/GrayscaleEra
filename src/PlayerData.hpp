#pragma once
#include "AbilityData.hpp"
#define PLAYER_TEXTURE_SIZE 100
#define PLAYER_MAX_FRAMES 10
#define PLAYER_MAX_HEALTH 4000
#define DEFAULT_PLAYER_POINTS 40000.0
typedef uint32_t PlayerID;

class PlayerData
{
	public:
		PlayerData();
		void Write(OutMemBitStream& outStream) const;
		bool Read(InMemBitStream& inStream);
		void WriteFile(OutMemBitStream& outStream) const;
		bool ReadFile(InMemBitStream& inStream);
		static std::shared_ptr<Frame> ReadFileImage(InMemBitStream& inStream);
		static std::shared_ptr<PlayerData> ReadPlayerFile(const UTF8String& playerName);
		uint64_t GetPoints() const;
		
		std::vector<Frame> walkAnimation;
		std::vector<Frame> idleAnimation;
		PricedPropertiesList statistics;
		AbilityData abilities[5];
	private:
		static PricedPropertiesList GetStatistics();
};

typedef std::shared_ptr<PlayerData> PlayerDataPtr;
