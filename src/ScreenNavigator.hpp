#pragma once
#include <memory>

class Screen
{
	public:
		virtual ~Screen() {}
		virtual bool Update() = 0; // true to continue, false to shutdown application
		virtual void Draw() = 0;
};

class ScreenNavigator
{
	public:
		static ScreenNavigator& Get()
		{
			static ScreenNavigator sInstance;
			return sInstance;
		}
		bool Update();
		void Draw();
		void SetScreen(const std::shared_ptr<Screen>& screen);
		const std::shared_ptr<Screen>& GetScreen() const;
		
	private:
		ScreenNavigator() {}
		std::shared_ptr<Screen> currentScreen;
};
