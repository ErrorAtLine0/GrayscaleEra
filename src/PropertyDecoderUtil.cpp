#include "GameSandbox.hpp"
#include "PropertyDecoderUtil.hpp"
#include <GSEGL/RandGenUtil.hpp>

#define NUMBER_MAX 100

bool PropertyDecoderUtil::CheckForPlayer(const HeadlineProperty* prop, const Vec2& pos, int32_t room, PropertyDecoderObjectType checkerObjType, const void* checkerObj)
{
	std::vector<RealPlayer*> players;
	PropertyDecoderUtil::GetPlayers(&players, static_cast<const ListProperty*>(prop)->data, pos, room, checkerObjType, checkerObj);
	return !players.empty();
}

RealPlayer* PropertyDecoderUtil::GetClosestPlayer(const std::vector<RealPlayer*>& playerVec, const Vec2& pos, int32_t room)
{
	float closestDistanceSqrd = -1.0f;
	RealPlayer* closestPlayer = nullptr;
	for(RealPlayer* player : playerVec)
		if(player->GetRoom() == room)
		{
			float xDist = player->GetCurrentPosition().x - pos.x;
			float yDist = player->GetCurrentPosition().y - pos.y;
			float distFromPointToPlayerSqrd = xDist * xDist + yDist * yDist;
			if(!closestPlayer || distFromPointToPlayerSqrd < closestDistanceSqrd)
			{
				closestDistanceSqrd = distFromPointToPlayerSqrd;
				closestPlayer = player;
			}
		}
	return closestPlayer;
}

void PropertyDecoderUtil::GetPlayers(std::vector<RealPlayer*>* toRet, const PropertiesList& playerSelectorPropList, const Vec2& pos, int32_t room, PropertyDecoderObjectType checkerObjType, const void* checkerObj)
{
	if(playerSelectorPropList.GetBool(0)) // Closest player only. Calculate closest player
	{
		RealPlayer* closestPlayer = GetClosestPlayer(GameSandbox::Get().GetAllRealPlayerObjects(), pos, room);
		if(closestPlayer && IsPlayerSelected(closestPlayer, playerSelectorPropList, pos, room, checkerObjType, checkerObj))
			toRet->emplace_back(closestPlayer);
	}
	else
	{
		for(RealPlayer* player : GameSandbox::Get().GetAllRealPlayerObjects())
			if(IsPlayerSelected(player, playerSelectorPropList, pos, room, checkerObjType, checkerObj))
				toRet->emplace_back(player);
	}
}

bool PropertyDecoderUtil::IsPlayerSelected(RealPlayer* player, const PropertiesList& playerSelectorPropList, const Vec2& pos, int32_t room, PropertyDecoderObjectType checkerObjType, const void* checkerObj)
{
	if(playerSelectorPropList.GetInt(1)) // From Distance
	{
		if(!PlayerWithinDistance(player, pos, room, playerSelectorPropList.GetInt(1)))
			return false;
	}
	if(playerSelectorPropList.GetInt(2)) // Away From Distance
	{
		if(PlayerWithinDistance(player, pos, room, playerSelectorPropList.GetInt(2)))
			return false;
	}
	if(!CheckPrivateNumber(playerSelectorPropList.GetPropAt(3), PROPERTYDECODEROBJECT_PLAYER, player, checkerObjType, checkerObj))
		return false;
	if(playerSelectorPropList.GetBool(4)) // Just Died
	{
		if(!player->IsSetToDestroy())
			return false;
	}
	if(playerSelectorPropList.GetChoice(5)) // Pressing Trigger Button
	{
		if(playerSelectorPropList.GetChoice(5) == 1 && !player->PressedTriggerButton()) // Yes
			return false;
		if(playerSelectorPropList.GetChoice(5) == 2 && player->PressedTriggerButton()) // No
			return false;
	}
	return true;
}

bool PropertyDecoderUtil::PlayerWithinDistance(RealPlayer* player, const Vec2& pos, int32_t room, int32_t blockDistance)
{
	return player->GetRoom() != room ? false : Entity::BoxToBoxCollision(pos - Vec2((blockDistance - 1) * 20.0f), Vec2(blockDistance * 40.0f), player->GetCurrentPosition(), player->GetSize());
}

PropertiesList PropertyDecoderUtil::GetAnnouncementList()
{
	PropertiesList announcementList("Player Announcement");
	announcementList.AddList("Player to set announcement", PropertyDecoderUtil::GetPlayerSelectorList());
	announcementList.AddString("Announcement", 20);
	return announcementList;
}

PropertiesList PropertyDecoderUtil::GetMessageList()
{
	PropertiesList messageList("Player Message");
	messageList.AddList("Player to send to", PropertyDecoderUtil::GetPlayerSelectorList());
	messageList.AddString("Message", 60);
	return messageList;
}

PropertiesList PropertyDecoderUtil::GetStatusEffectList()
{
	PropertiesList effectProperties("Status Effects");
	effectProperties.AddInt("Instant Health", -1000, 1000, 10);
	effectProperties.AddInt("Health Regen (health/sec)", -400, 400, 10); 
	effectProperties.AddInt("Health Regen Length (sec)", 0, 60, 1);
	effectProperties.AddInt("Speed Increase (%)", -200, 200, 10);
	effectProperties.AddInt("Speed Increase Length (sec)", 0, 60, 1);
	effectProperties.AddInt("Gravity Increase (%)", -200, 200, 10);
	effectProperties.AddInt("Gravity Increase Length (sec)", 0, 60, 1);
	effectProperties.AddInt("Defense Increase (%)", -200, 200, 5);
	effectProperties.AddInt("Defense Increase Length (sec)", 0, 60, 1);
	return effectProperties;
}

PropertiesList PropertyDecoderUtil::GetPlayerSelectorList()
{
	PropertiesList playerSelectorList("Player Selector");
	playerSelectorList.AddBool("Check closest player only");
	playerSelectorList.AddInt("Distance From", 0, 50, 1, 0);
	playerSelectorList.AddInt("Distance Away", 0, 50, 1, 0);
	playerSelectorList.AddList("Check Private Number", PropertyDecoderUtil::GetCheckNumberList());
	playerSelectorList.AddBool("Just Died");
	std::vector<UTF8String> triggerChoices;
	triggerChoices.emplace_back("Doesn't matter");
	triggerChoices.emplace_back("Yes");
	triggerChoices.emplace_back("No");
	playerSelectorList.AddChoice("Pressing Trigger Button", triggerChoices);
	return playerSelectorList;
}

PropertiesList PropertyDecoderUtil::GetSetPlayerPrivateNumberList()
{
	PropertiesList playerToPrivateFlag("Set Player Private Number");
	playerToPrivateFlag.AddList("Player", PropertyDecoderUtil::GetPlayerSelectorList());
	playerToPrivateFlag.AddList("Private Number", PropertyDecoderUtil::GetSetNumberList());
	return playerToPrivateFlag;
}

PropertiesList PropertyDecoderUtil::GetTimerList()
{
	PropertiesList timerList("Timer");
	timerList.AddInt("Milliseconds", 200, 10000, 200, 200);
	return timerList;
}

PropertiesList PropertyDecoderUtil::GetNPCList()
{
	PropertiesList npcList("NPC");
	npcList.AddNPC("Player To Spawn");
	npcList.AddString("NPC Name", 16);
	npcList.AddBool("Show Name");
	npcList.AddBool("Show Health And Effects");
	
	PropertiesList AIConditions;
	AIConditions.AddHeadline("Just Summoned");
	AIConditions.AddList("Check Global Number", PropertyDecoderUtil::GetCheckNumberList());
	AIConditions.AddList("Check Private Number", PropertyDecoderUtil::GetCheckNumberList());
	AIConditions.AddList("Check For Player", PropertyDecoderUtil::GetPlayerSelectorList());
	PropertiesList distancePoint("Point Distance");
	distancePoint.AddString("Point Tag", 16);
	distancePoint.AddInt("Distance", 1, 50, 1, 1);
	AIConditions.AddList("Distance From Point", distancePoint);
	AIConditions.AddList("Distance Away From Point", distancePoint);
	AIConditions.AddInt("Health Above", 10, PLAYER_MAX_HEALTH, 10, 10);
	AIConditions.AddInt("Health Below", 10, PLAYER_MAX_HEALTH, 10, 10);
	AIConditions.AddList("Every X Milliseconds", PropertyDecoderUtil::GetTimerList());
	AIConditions.AddInt("Random Percentage", 0, 100, 1, 0);
	AIConditions.AddBool("Just Died");
	
	PropertiesList AIResults;
	AIResults.AddList("Set Global Number", PropertyDecoderUtil::GetSetNumberList());
	AIResults.AddList("Set Private Number", PropertyDecoderUtil::GetSetNumberList());
	AIResults.AddList("Set Player Private Number", PropertyDecoderUtil::GetSetPlayerPrivateNumberList());
	AIResults.AddHeadline("Die");
	PropertiesList moveList("Move Data");
	std::vector<UTF8String> movesToUse;
	movesToUse.emplace_back("Walk");
	movesToUse.emplace_back("Ability 1");
	movesToUse.emplace_back("Ability 2");
	movesToUse.emplace_back("Ability 3");
	movesToUse.emplace_back("Ability 4");
	movesToUse.emplace_back("Ability 5");
	moveList.AddChoice("Move To Use", movesToUse);
	std::vector<UTF8String> directionsToAim;
	directionsToAim.emplace_back("Up");
	directionsToAim.emplace_back("Down");
	directionsToAim.emplace_back("Left");
	directionsToAim.emplace_back("Right");
	directionsToAim.emplace_back("Towards Closest Specified Player");
	directionsToAim.emplace_back("Away From Closest Specified Player");
	directionsToAim.emplace_back("Towards Specified Point");
	directionsToAim.emplace_back("Away From Specified Point");
	directionsToAim.emplace_back("Wherever they're facing");
	directionsToAim.emplace_back("Random");
	moveList.AddChoice("Direction To Aim", directionsToAim);
	moveList.AddList("Specified Player", PropertyDecoderUtil::GetPlayerSelectorList());
	moveList.AddString("Specified Point Tag", 16);
	AIResults.AddList("Use Move", moveList);
	AIResults.AddList("Send Message", PropertyDecoderUtil::GetMessageList());
	AIResults.AddList("Print Announcement", PropertyDecoderUtil::GetAnnouncementList());
	AIResults.AddList("Give Status Effect", PropertyDecoderUtil::GetStatusEffectList());
	AIResults.AddHeadline("End Game");
	
	npcList.AddRuleList("AI Rules", AIConditions, AIResults);
	
	return npcList;
}

PropertiesList PropertyDecoderUtil::GetRealPlayerConditions()
{
	PropertiesList playerConditions;
	playerConditions.AddHeadline("Adventure Just Started");
	playerConditions.AddList("Check Global Number", PropertyDecoderUtil::GetCheckNumberList());
	playerConditions.AddList("Check For Player", PropertyDecoderUtil::GetPlayerSelectorList());
	PropertiesList distancePoint("Point Distance");
	distancePoint.AddString("Point Tag", 16);
	distancePoint.AddInt("Distance", 1, 50, 1, 1);
	playerConditions.AddList("Distance From Point", distancePoint);
	playerConditions.AddList("Distance Away From Point", distancePoint);
	playerConditions.AddInt("Health Above", 10, PLAYER_MAX_HEALTH, 10, 10);
	playerConditions.AddInt("Health Below", 10, PLAYER_MAX_HEALTH, 10, 10);
	playerConditions.AddList("Every X Milliseconds", PropertyDecoderUtil::GetTimerList());
	playerConditions.AddInt("Random Percentage", 0, 100, 1, 0);
	playerConditions.AddBool("Just Died");
	return playerConditions;
}

PropertiesList PropertyDecoderUtil::GetRealPlayerResults()
{
	PropertiesList playerResults;
	playerResults.AddList("Set Global Number", PropertyDecoderUtil::GetSetNumberList());
	playerResults.AddList("Set Player Private Number", PropertyDecoderUtil::GetSetPlayerPrivateNumberList());
	playerResults.AddHeadline("Die");
	playerResults.AddList("Print Message", PropertyDecoderUtil::GetMessageList());
	playerResults.AddList("Print Announcement", PropertyDecoderUtil::GetAnnouncementList());
	playerResults.AddList("Give Status Effect", PropertyDecoderUtil::GetStatusEffectList());
	playerResults.AddList("Summon NPC", PropertyDecoderUtil::GetNPCList());
	playerResults.AddList("TP Player To Point", PropertyDecoderUtil::GetPlayerToPointList());
	playerResults.AddList("Set Player Spawn", PropertyDecoderUtil::GetPlayerToPointList());
	playerResults.AddHeadline("End Game");
	return playerResults;
}

PropertiesList PropertyDecoderUtil::GetCheckNumberList()
{
	PropertiesList numberList("Number To Check");
	numberList.AddString("Number Name", 16);
	std::vector<UTF8String> operations;
	operations.emplace_back("Equal To");
	operations.emplace_back("Greater Than");
	operations.emplace_back("Less Than");
	operations.emplace_back("Not Equal To");
	numberList.AddChoice("Operation", operations);
	std::vector<UTF8String> operandValueType;
	operandValueType.emplace_back("Constant");
	operandValueType.emplace_back("Another Number (Global)");
	operandValueType.emplace_back("Another Number (Private)");
	numberList.AddChoice("Get Operand Value From", operandValueType);
	numberList.AddInt("Constant", 0, NUMBER_MAX, 1, 0);
	numberList.AddString("Other Number Name", 16);
	return numberList;
}

PropertiesList PropertyDecoderUtil::GetSetNumberList()
{
	PropertiesList numberList("Number To Set");
	numberList.AddString("Number Name", 16);
	std::vector<UTF8String> operations;
	operations.emplace_back("Set to");
	operations.emplace_back("Add");
	operations.emplace_back("Subtract");
	numberList.AddChoice("Operation", operations);
	std::vector<UTF8String> operandValueType;
	operandValueType.emplace_back("Constant");
	operandValueType.emplace_back("Another Number (Global)");
	operandValueType.emplace_back("Another Number (Private)");
	numberList.AddChoice("Get Operand Value From", operandValueType);
	numberList.AddInt("Constant", 0, NUMBER_MAX, 1, 0);
	numberList.AddString("Other Number Name", 16);
	return numberList;
}

PropertiesList PropertyDecoderUtil::GetPlayerToPointList()
{
	PropertiesList playerToPoint("Player To Point");
	playerToPoint.AddList("Player", PropertyDecoderUtil::GetPlayerSelectorList());
	playerToPoint.AddString("Point Tag", 16);
	return playerToPoint;
}

void PropertyDecoderUtil::SetGlobalNumber(const HeadlineProperty* prop, PropertyDecoderObjectType setterObjType, const void* setterObj)
{
	const PropertiesList& propList = static_cast<const ListProperty*>(prop)->data;
	uint32_t operand = 0;
	if(propList.GetChoice(2) == 0)
		operand = propList.GetInt(3);
	else if(propList.GetChoice(2) == 1)
		operand = GameSandbox::Get().GetNumber(propList.GetString(4));
	else
		operand = setterObjType == PROPERTYDECODEROBJECT_PLAYER ? static_cast<const Player*>(setterObj)->GetNumber(propList.GetString(4)) : static_cast<const Point*>(setterObj)->GetNumber(propList.GetString(4));
	SetNumber(GameSandbox::Get().GetNumberPtr(propList.GetString(0)), propList.GetChoice(1), operand);
}

bool PropertyDecoderUtil::CheckGlobalNumber(const HeadlineProperty* prop, PropertyDecoderObjectType checkerObjType, const void* checkerObj)
{
	const PropertiesList& propList = static_cast<const ListProperty*>(prop)->data;
	uint32_t operand = 0;
	if(propList.GetChoice(2) == 0)
		operand = propList.GetInt(3);
	else if(propList.GetChoice(2) == 1)
		operand = GameSandbox::Get().GetNumber(propList.GetString(4));
	else
		operand = checkerObjType == PROPERTYDECODEROBJECT_PLAYER ? static_cast<const Player*>(checkerObj)->GetNumber(propList.GetString(4)) : static_cast<const Point*>(checkerObj)->GetNumber(propList.GetString(4));
	return CheckNumber(GameSandbox::Get().GetNumber(propList.GetString(0)), propList.GetChoice(1), operand);
}

void PropertyDecoderUtil::SetPrivateNumber(const HeadlineProperty* prop, PropertyDecoderObjectType objType, void* obj, PropertyDecoderObjectType setterObjType, const void* setterObj)
{
	const PropertiesList& propList = static_cast<const ListProperty*>(prop)->data;
	uint32_t operand = 0;
	if(propList.GetChoice(2) == 0)
		operand = propList.GetInt(3);
	else if(propList.GetChoice(2) == 1)
		operand = GameSandbox::Get().GetNumber(propList.GetString(4));
	else
		operand = setterObjType == PROPERTYDECODEROBJECT_PLAYER ? static_cast<const Player*>(setterObj)->GetNumber(propList.GetString(4)) : static_cast<const Point*>(setterObj)->GetNumber(propList.GetString(4));
	SetNumber(objType == PROPERTYDECODEROBJECT_PLAYER ? static_cast<Player*>(obj)->GetNumberPtr(propList.GetString(0)) : static_cast<Point*>(obj)->GetNumberPtr(propList.GetString(0)), propList.GetChoice(1), operand);
}

bool PropertyDecoderUtil::CheckPrivateNumber(const HeadlineProperty* prop, PropertyDecoderObjectType objType, const void* obj, PropertyDecoderObjectType checkerObjType, const void* checkerObj)
{
	const PropertiesList& propList = static_cast<const ListProperty*>(prop)->data;
	uint32_t operand = 0;
	if(propList.GetChoice(2) == 0)
		operand = propList.GetInt(3);
	else if(propList.GetChoice(2) == 1)
		operand = GameSandbox::Get().GetNumber(propList.GetString(4));
	else
		operand = checkerObjType == PROPERTYDECODEROBJECT_PLAYER ? static_cast<const Player*>(checkerObj)->GetNumber(propList.GetString(4)) : static_cast<const Point*>(checkerObj)->GetNumber(propList.GetString(4));
	return CheckNumber(objType == PROPERTYDECODEROBJECT_PLAYER ? static_cast<const Player*>(obj)->GetNumber(propList.GetString(0)) : static_cast<const Point*>(obj)->GetNumber(propList.GetString(0)), propList.GetChoice(1), operand);
}

void PropertyDecoderUtil::SetPlayerPrivateNumber(const HeadlineProperty* prop, const Vec2& pos, int32_t room, PropertyDecoderObjectType setterObjType, void* setterObj)
{
	const PropertiesList& propList = static_cast<const ListProperty*>(prop)->data;
	std::vector<RealPlayer*> players;
	PropertyDecoderUtil::GetPlayers(&players, propList.GetList(0), pos, room, setterObjType, setterObj);
	for(RealPlayer* player : players)
		SetPrivateNumber(propList.GetPropAt(1), PROPERTYDECODEROBJECT_PLAYER, player, setterObjType, setterObj);
}

void PropertyDecoderUtil::SetNumber(uint32_t* number, uint32_t operation, uint32_t operand)
{
	if(operation == 0) // Set
		*number = operand;
	else if(operation == 1) // Add
	{
		*number += operand;
		if(*number > NUMBER_MAX)
			*number = NUMBER_MAX;
	}
	else if(operation == 2) // Subtract
	{
		if(operand > *number) // Prevent wrapping
			*number = 0;
		else
			*number -= operand;
	}
}

bool PropertyDecoderUtil::CheckNumber(uint32_t number, uint32_t operation, uint32_t operand)
{
	if(operation == 0) // Equal To
		return number == operand;
	else if(operation == 1) // Greater Than
		return number > operand;
	else if(operation == 2) // Less Than
		return number < operand;
	else if(operation == 3) // Not Equal To
		return number != operand;
	return false;
}

void PropertyDecoderUtil::SendMessage(const HeadlineProperty* prop, const Vec2& pos, int32_t room, PropertyDecoderObjectType checkerObjType, const void* checkerObj)
{
	const PropertiesList& propList = static_cast<const ListProperty*>(prop)->data;
	std::vector<RealPlayer*> players;
	PropertyDecoderUtil::GetPlayers(&players, propList.GetList(0), pos, room, checkerObjType, checkerObj);
	for(RealPlayer* player : players)
		GameSandbox::Get().SendMessage(player->GetPlayerID(), propList.GetString(1));
}

void PropertyDecoderUtil::SetAnnouncement(const HeadlineProperty* prop, const Vec2& pos, int32_t room, PropertyDecoderObjectType checkerObjType, const void* checkerObj)
{
	const PropertiesList& propList = static_cast<const ListProperty*>(prop)->data;
	std::vector<RealPlayer*> players;
	PropertyDecoderUtil::GetPlayers(&players, propList.GetList(0), pos, room, checkerObjType, checkerObj);
	for(RealPlayer* player : players)
		GameSandbox::Get().SetAnnouncement(player->GetPlayerID(), propList.GetString(1));
}

bool PropertyDecoderUtil::CheckTimer(const HeadlineProperty* prop, std::unordered_map<const HeadlineProperty*, double>& propTimers)
{
	const PropertiesList& timerList = static_cast<const ListProperty*>(prop)->data;
	auto it = propTimers.find(prop);
	if(it == propTimers.end())
		propTimers.emplace(prop, timerList.GetInt(0) / 1000.0);
	else
	{
		double& timerTimeLeft = it->second;
		timerTimeLeft -= TimeUtil::Get().GetDeltaTime();
		if(timerTimeLeft <= 0.0)
		{
			timerTimeLeft = timerList.GetInt(0) / 1000.0;
			return true;
		}
	}
	return false;
}

bool PropertyDecoderUtil::CheckRandomPercentage(const HeadlineProperty* prop)
{
	uint32_t percentage = static_cast<const IntProperty*>(prop)->data;
	return RandGenUtil::Get().GetRandomUInt64(0, 99) < percentage;
}

void PropertyDecoderUtil::SetNumberInMap(std::unordered_map<UTF8String, uint32_t>& numbers, const UTF8String& name, uint32_t val)
{
	numbers[name] = val;
}

uint32_t PropertyDecoderUtil::GetNumberInMap(const std::unordered_map<UTF8String, uint32_t>& numbers, const UTF8String& name)
{
	auto it = numbers.find(name);
	if(it != numbers.end())
		return it->second;
	else
		return 0;
}

uint32_t* PropertyDecoderUtil::GetNumberPtrInMap(std::unordered_map<UTF8String, uint32_t>& numbers, const UTF8String& name)
{
	auto it = numbers.find(name);
	if(it != numbers.end())
		return &(it->second);
	else
	{
		auto itEmplaced = numbers.emplace(name, 0);
		return &(itEmplaced.first->second);
	}
}

void PropertyDecoderUtil::TPPlayerToPoint(const HeadlineProperty* prop, const Vec2& pos, int32_t room, PropertyDecoderObjectType checkerObjType, const void* checkerObj)
{
	const PropertiesList& propList = static_cast<const ListProperty*>(prop)->data;
	Point* pointToTP = GameSandbox::Get().GetPointByTag(propList.GetString(1));
	if(pointToTP)
	{
		std::vector<RealPlayer*> players;
		PropertyDecoderUtil::GetPlayers(&players, propList.GetList(0), pos, room, checkerObjType, checkerObj);
		for(RealPlayer* player : players)
			player->SetRoomPosition(pointToTP->GetPos(), pointToTP->GetRoom());
	}
}

void PropertyDecoderUtil::SetPlayerSpawnPoint(const HeadlineProperty* prop, const Vec2& pos, int32_t room, PropertyDecoderObjectType checkerObjType, const void* checkerObj)
{
	const PropertiesList& propList = static_cast<const ListProperty*>(prop)->data;
	Point* pointToSetSpawn = GameSandbox::Get().GetPointByTag(propList.GetString(1));
	if(pointToSetSpawn)
	{
		std::vector<RealPlayer*> players;
		PropertyDecoderUtil::GetPlayers(&players, propList.GetList(0), pos, room, checkerObjType, checkerObj);
		for(RealPlayer* player : players)
			GameSandbox::Get().SetRealPlayerSpawnPoint(player->GetPlayerInfoWeakPtr().lock(), pointToSetSpawn);
	}
}
