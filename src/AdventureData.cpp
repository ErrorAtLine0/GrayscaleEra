#include "AdventureData.hpp"
#include "PropertyDecoderUtil.hpp"
#include <GSEGL/ErrorUtil.hpp>
#define ADVENTURE_HEADER_INT 3236413782

AdventureData::AdventureData():
settings("Adventure Settings"),
image(ADVENTURE_TEXTURE_SIZE*ADVENTURE_TEXTURE_SIZE, 0)
{
	settings.AddString("Spawn Point Tag", 16);
	settings.AddInt("Time Limit (sec)", 0, 3600, 15, 0);
	settings.AddInt("Respawn Time (sec)", 0, 60, 1, 5);
	settings.AddNPC("Force Player");
	settings.AddInt("Max Player Points Allowed (0 = ∞)", 0, 100000, 2000, 0);
	settings.AddRuleList("Player Rules", PropertyDecoderUtil::GetRealPlayerConditions(), PropertyDecoderUtil::GetRealPlayerResults());
}

void AdventureData::Write(OutMemBitStream& outStream) const
{
	OutMemBitStream firstStream;
	firstStream.WriteBits(ADVENTURE_HEADER_INT, 32);
	image.Write(firstStream);
	settings.Write(firstStream);
	firstStream.WriteBits(npcs.size(), 32);
	for(const PlayerData& npc : npcs)
		npc.Write(firstStream);
	firstStream.WriteBits(blocks.size(), 32);
	for(const BlockData& block : blocks)
		block.Write(firstStream);
	firstStream.WriteBits(rooms.size(), 32);
	for(const LevelData& room : rooms)
		room.Write(firstStream);
	firstStream.WriteBits(points.size(), 32);
	for(const PointData& point : points)
		point.Write(firstStream);
	outStream.WriteCompressedData(firstStream.GetBufferPtr(), firstStream.GetByteLength());
}

bool AdventureData::Read(InMemBitStream& inStream)
{
	std::shared_ptr<InMemBitStream> firstStream = inStream.ReadCompressedData();
	if(!firstStream) return false;
	
	uint32_t adventureHeader = 0;
	if(!firstStream->ReadBits(adventureHeader, 32)) return false;
	if(adventureHeader != ADVENTURE_HEADER_INT)
	{
		ErrorUtil::Get().SetError("Memory", "AdventureData::Read Not an adventure file");
		return false;
	}
	
	if(!image.Read(*firstStream)) return false;
	if(!settings.Read(*firstStream)) return false;
	
	uint32_t npcCount = 0;
	if(!firstStream->ReadBits(npcCount)) return false;
	npcs.clear();
	for(uint32_t i = 0; i < npcCount; i++)
	{
		npcs.emplace_back();
		if(!npcs.back().Read(*firstStream)) return false;
	}
	
	uint32_t blockCount = 0;
	if(!firstStream->ReadBits(blockCount)) return false;
	blocks.clear();
	for(uint32_t i = 0; i < blockCount; i++)
	{
		blocks.emplace_back();
		if(!blocks.back().Read(*firstStream)) return false;
	}
	
	uint32_t roomCount = 0;
	if(!firstStream->ReadBits(roomCount)) return false;
	rooms.clear();
	for(uint32_t i = 0; i < roomCount; i++)
	{
		rooms.emplace_back(ADVENTURELEVELSIZE);
		if(!rooms.back().Read(*firstStream)) return false;
	}
	
	uint32_t pointCount = 0;
	if(!firstStream->ReadBits(pointCount)) return false;
	points.clear();
	for(uint32_t i = 0; i < pointCount; i++)
	{
		points.emplace_back();
		if(!points.back().Read(*firstStream)) return false;
	}
	
	return true;
}

std::shared_ptr<Frame> AdventureData::ReadImage(InMemBitStream& inStream)
{
	std::shared_ptr<InMemBitStream> firstStream = inStream.ReadCompressedData();
	if(!firstStream) return nullptr;
	
	uint32_t adventureHeader = 0;
	if(!firstStream->ReadBits(adventureHeader, 32)) return nullptr;
	if(adventureHeader != ADVENTURE_HEADER_INT)
	{
		ErrorUtil::Get().SetError("Memory", "AdventureData::ReadImage Not an adventure file");
		return nullptr;
	}
	
	std::shared_ptr<Frame> imagePtr(std::make_shared<Frame>(ADVENTURE_TEXTURE_SIZE*ADVENTURE_TEXTURE_SIZE, 0));
	if(!imagePtr->Read(*firstStream)) return nullptr;
	return imagePtr;
}

void AdventureData::RemoveBlock(BlockID blockToRemove)
{
	blocks.erase(blocks.begin() + (blockToRemove - 1));
	for(LevelData& room : rooms)
		room.RemoveBlock(blockToRemove);
	for(PointData& point : points)
		point.propList.RemoveBlock(blockToRemove);
	settings.RemoveBlock(blockToRemove);
}

void AdventureData::RemoveNPC(NPCID npcToRemove)
{
	npcs.erase(npcs.begin() + (npcToRemove - 1));
	for(PointData& point : points)
		point.propList.RemoveNPC(npcToRemove);
	settings.RemoveNPC(npcToRemove);
}

void AdventureData::RemovePoint(PointID pointToRemove)
{
	points.erase(points.begin() + (pointToRemove - 1));
	for(LevelData& room : rooms)
		room.RemovePoint(pointToRemove);
}
