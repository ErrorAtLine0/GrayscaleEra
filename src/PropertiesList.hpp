#pragma once
#include <GSEGL/UTF8String.hpp>
#include <GSEGL/Network/OutMemBitStream.hpp>
#include <GSEGL/Network/InMemBitStream.hpp>
#include <GSEGL/Graphics/ControllerUtil.hpp>
#include <vector>

#define BITS_FOR_BLOCK 16
#define BITS_FOR_POINT 16
#define BITS_FOR_NPC 16
typedef uint16_t BlockID;
typedef uint16_t NPCID;
typedef uint16_t PointID;

enum PropertyType
{
	PROPERTY_HEADLINE,
	PROPERTY_STRING,
	PROPERTY_INT,
	PROPERTY_BOOL,
	PROPERTY_CHOICE,
	PROPERTY_LIST,
	PROPERTY_BUTTON,
	PROPERTY_BLOCK,
	PROPERTY_NPC,
	PROPERTY_RULELIST
};

class HeadlineProperty
{
	public:
		static HeadlineProperty* CreateProperty(PropertyType propertyType, const UTF8String& propertyName);
		HeadlineProperty(const UTF8String& initPropertyName);
		HeadlineProperty(const HeadlineProperty& initProperty);
		virtual ~HeadlineProperty();
		void operator=(const HeadlineProperty& property);
		virtual void Write(OutMemBitStream& outStream) const;
		virtual bool Read(InMemBitStream& inStream);
		virtual void WriteText(OutMemBitStream& outStream) const;
		virtual bool ReadText(InMemBitStream& inStream);
		virtual PropertyType GetPropertyType() const;
		virtual void TransferInfo(const HeadlineProperty* property);
		const UTF8String& GetPropertyName() const;
	protected:
		UTF8String propertyName;
};

class RuleList;

class PropertiesList
{
	public:
		PropertiesList();
		PropertiesList(const UTF8String& initListName);
		PropertiesList(const PropertiesList& propertiesList);
		~PropertiesList();
		void operator=(const PropertiesList& propertiesList);
		HeadlineProperty* GetPropAt(uint32_t index);
		const HeadlineProperty* GetPropAt(uint32_t index) const;
		
		void AddString(const UTF8String& propertyName, uint32_t maxLength);
		void AddInt(const UTF8String& propertyName, int32_t minVal, int32_t maxVal, int32_t diff, int32_t defaultVal = 0);
		void AddBool(const UTF8String& propertyName);
		void AddChoice(const UTF8String& propertyName, const std::vector<UTF8String>& choices, int32_t defaultVal = 0);
		void AddList(const UTF8String& propertyName, const PropertiesList& propertiesList);
		void AddButton(const UTF8String& propertyName, const ButtonInfo& buttonInfo);
		void AddBlock(const UTF8String& propertyName);
		void AddNPC(const UTF8String& propertyName);
		void AddRuleList(const UTF8String& propertyName, const PropertiesList& conditions, const PropertiesList& results);
		void AddHeadline(const UTF8String& propertyName);
		
		const UTF8String& GetString(uint32_t index) const;
		int32_t GetInt(uint32_t index) const;
		bool GetBool(uint32_t index) const;
		uint32_t GetChoice(uint32_t index) const;
		const PropertiesList& GetList(uint32_t index) const;
		const ButtonInfo& GetButton(uint32_t index) const;
		BlockID GetBlock(uint32_t index) const;
		NPCID GetNPC(uint32_t index) const;
		const RuleList& GetRuleList(uint32_t index) const;
		
		void RemoveBlock(BlockID blockToRemove);
		void RemoveNPC(NPCID npcToRemove);
		static void RemoveBlockFromProperty(HeadlineProperty* prop, BlockID blockToRemove);
		static void RemoveNPCFromProperty(HeadlineProperty* prop, NPCID npcToRemove);
		
		void Write(OutMemBitStream& outStream) const;
		bool Read(InMemBitStream& inStream);
		void WriteText(OutMemBitStream& outStream) const;
		bool ReadText(InMemBitStream& inStream);
		uint32_t GetListSize() const;
		const UTF8String& GetListName() const;
	private:
		void AddProperty(HeadlineProperty* property);
		UTF8String listName;
		std::vector<HeadlineProperty*> propertiesVector;
};

struct Rule
{
	Rule();
	Rule(const std::vector<uint32_t>& initConditionIndices, const std::vector<const HeadlineProperty*>& initConditionProperties, const std::vector<uint32_t>& initResultIndices, const std::vector<const HeadlineProperty*>& initResultProperties);
	~Rule();
	Rule(const Rule& initRule);
	void operator=(const Rule& rule);
	void Write(OutMemBitStream& outStream) const;
	bool Read(InMemBitStream& inStream, const PropertiesList& conditions, const PropertiesList& results);
	std::vector<uint32_t> conditionIndices;
	std::vector<HeadlineProperty*> conditionProperties;
	std::vector<uint32_t> resultIndices;
	std::vector<HeadlineProperty*> resultProperties;
};

class RuleList
{
	public:
		RuleList();
		RuleList(const UTF8String& initListName);
		
		void SetConditions(const PropertiesList& conditions);
		void SetResults(const PropertiesList& results);
		const PropertiesList& GetConditions() const;
		const PropertiesList& GetResults() const;
		
		void AddRule(const std::vector<uint32_t>& conditionIndices, const std::vector<uint32_t>& resultIndices);
		void RemoveRule(uint32_t index);
		void MoveRule(uint32_t index, uint32_t newIndex);
		Rule& GetRuleAt(uint32_t index);
		const Rule& GetRuleAt(uint32_t index) const;
		
		void RemoveBlock(BlockID blockToRemove);
		void RemoveNPC(NPCID npcToRemove);
		
		void Write(OutMemBitStream& outStream) const;
		bool Read(InMemBitStream& inStream);
		void WriteText(OutMemBitStream& outStream) const;
		bool ReadText(InMemBitStream& inStream);
		uint32_t GetListSize() const;
		const UTF8String& GetListName() const;
	private:
		UTF8String listName;
		PropertiesList conditions;
		PropertiesList results;
		std::vector<Rule> rules;
};

class StringProperty : public HeadlineProperty
{
	public:
		StringProperty(const UTF8String& initPropertyName);
		virtual PropertyType GetPropertyType() const;
		virtual void TransferInfo(const HeadlineProperty* property);
		virtual void Write(OutMemBitStream& outStream) const;
		virtual bool Read(InMemBitStream& inStream);
		virtual void WriteText(OutMemBitStream& outStream) const;
		virtual bool ReadText(InMemBitStream& inStream);
		uint32_t maxLength;
		UTF8String data;
};

class IntProperty : public HeadlineProperty
{
	public:
		IntProperty(const UTF8String& initPropertyName);
		virtual PropertyType GetPropertyType() const;
		virtual void TransferInfo(const HeadlineProperty* property);
		virtual void Write(OutMemBitStream& outStream) const;
		virtual bool Read(InMemBitStream& inStream);
		virtual void WriteText(OutMemBitStream& outStream) const;
		virtual bool ReadText(InMemBitStream& inStream);
		int32_t minVal, maxVal, diff;
		int32_t data;
};

class BoolProperty : public HeadlineProperty
{
	public:
		BoolProperty(const UTF8String& initPropertyName);
		virtual PropertyType GetPropertyType() const;
		virtual void TransferInfo(const HeadlineProperty* property);
		virtual void Write(OutMemBitStream& outStream) const;
		virtual bool Read(InMemBitStream& inStream);
		virtual void WriteText(OutMemBitStream& outStream) const;
		virtual bool ReadText(InMemBitStream& inStream);
		bool data;
};

class ChoiceProperty : public HeadlineProperty
{
	public:
		ChoiceProperty(const UTF8String& initPropertyName);
		virtual PropertyType GetPropertyType() const;
		virtual void TransferInfo(const HeadlineProperty* property);
		virtual void Write(OutMemBitStream& outStream) const;
		virtual bool Read(InMemBitStream& inStream);
		virtual void WriteText(OutMemBitStream& outStream) const;
		virtual bool ReadText(InMemBitStream& inStream);
		std::vector<UTF8String> choices;
		uint32_t data;
};

class ListProperty : public HeadlineProperty
{
	public:
		ListProperty(const UTF8String& initPropertyName);
		virtual PropertyType GetPropertyType() const;
		virtual void TransferInfo(const HeadlineProperty* property);
		virtual void Write(OutMemBitStream& outStream) const;
		virtual bool Read(InMemBitStream& inStream);
		virtual void WriteText(OutMemBitStream& outStream) const;
		virtual bool ReadText(InMemBitStream& inStream);
		PropertiesList data;
};

class ButtonProperty : public HeadlineProperty
{
	public:
		ButtonProperty(const UTF8String& initPropertyName);
		virtual PropertyType GetPropertyType() const;
		virtual void TransferInfo(const HeadlineProperty* property);
		virtual void Write(OutMemBitStream& outStream) const;
		virtual bool Read(InMemBitStream& inStream);
		virtual void WriteText(OutMemBitStream& outStream) const;
		virtual bool ReadText(InMemBitStream& inStream);
		ButtonInfo data;
};

class BlockProperty : public HeadlineProperty
{
	public:
		BlockProperty(const UTF8String& initPropertyName);
		virtual PropertyType GetPropertyType() const;
		virtual void TransferInfo(const HeadlineProperty* property);
		virtual void Write(OutMemBitStream& outStream) const;
		virtual bool Read(InMemBitStream& inStream);
		virtual void WriteText(OutMemBitStream& outStream) const;
		virtual bool ReadText(InMemBitStream& inStream);
		BlockID data;
};

class NPCProperty : public HeadlineProperty
{
	public:
		NPCProperty(const UTF8String& initPropertyName);
		virtual PropertyType GetPropertyType() const;
		virtual void TransferInfo(const HeadlineProperty* property);
		virtual void Write(OutMemBitStream& outStream) const;
		virtual bool Read(InMemBitStream& inStream);
		virtual void WriteText(OutMemBitStream& outStream) const;
		virtual bool ReadText(InMemBitStream& inStream);
		NPCID data;
};

class RuleListProperty : public HeadlineProperty
{
	public:
		RuleListProperty(const UTF8String& initPropertyName);
		virtual PropertyType GetPropertyType() const;
		virtual void TransferInfo(const HeadlineProperty* property);
		virtual void Write(OutMemBitStream& outStream) const;
		virtual bool Read(InMemBitStream& inStream);
		virtual void WriteText(OutMemBitStream& outStream) const;
		virtual bool ReadText(InMemBitStream& inStream);
		RuleList data;
};
