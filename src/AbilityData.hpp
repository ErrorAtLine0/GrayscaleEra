#pragma once
#include "PropertiesList.hpp"
#include "Frame.hpp"
#define ABILITY_MAX_FRAMES 10

typedef std::vector<std::pair<UTF8String, std::vector<Frame>>> NamedAnimationList;
typedef std::vector<std::pair<UTF8String, uint32_t>> PricedChoiceList;

class PricedPropertiesList
{
	public:
		PricedPropertiesList();
		PricedPropertiesList(const UTF8String& initPropListName);
		void AddBool(const UTF8String& propName, uint32_t priceIfTrue);
		void AddInt(const UTF8String& propName, uint32_t increment, uint32_t maxVal, uint32_t pricePerIncrement);
		void AddChoice(const UTF8String& propName, const PricedChoiceList& choiceAndPriceVector);
		void AddList(const UTF8String& propName, const PricedPropertiesList& pricedProperties);
		uint32_t GetCost() const;
		PropertiesList& GetPropListRef();
		const PropertiesList& GetPropListRef() const;
	private:
		uint32_t CalculatePropListCost(const PropertiesList& propList, uint32_t& totalPrice) const;
		PropertiesList propList;
		std::vector<uint32_t> prices;
};

class AbilityData
{
	private:
		struct AbilityProperties
		{
			AbilityProperties();
			AbilityProperties(const UTF8String& initPropertiesListName);
			PricedPropertiesList pricedPropertiesList;
			NamedAnimationList animationList;
			uint32_t typePrice;
		};
	public:
		static void Init();
		AbilityData();
		void SetAbilityToTypeChosen();
		PropertiesList& GetPropListRef();
		NamedAnimationList& GetAnimationListRef();
		PropertiesList& GetTypeListRef();
		const PricedPropertiesList& GetPricedBuffList() const;
		const PricedPropertiesList& GetPricedDebuffList() const;
		const PropertiesList& GetPropListRef() const;
		const NamedAnimationList& GetAnimationListRef() const;
		const PropertiesList& GetTypeListRef() const;
		uint32_t GetAbilityType() const;
		void Write(OutMemBitStream& outStream) const;
		bool Read(InMemBitStream& inStream);
		double GetCost() const;
		double GetCooldown() const;
		void SetCooldown(double cooldown);
	private:
		static void AddAbility(const UTF8String& abilityName, uint32_t price);
		static void AddAnimationToAbility(const UTF8String& animationName);
		static void AddBoolToAbility(const UTF8String& propName, uint32_t priceIfTrue);
		static void AddIntToAbility(const UTF8String& propName, uint32_t increment, uint32_t maxVal, uint32_t pricePerIncrement);
		static void AddChoiceToAbility(const UTF8String& propName, const PricedChoiceList& choiceAndPriceVector);
		static void AddListToAbility(const UTF8String& propName, const PricedPropertiesList& pricedProperties);
		static std::vector<std::pair<UTF8String, AbilityProperties>> nameToProperties;
		static PricedPropertiesList pricedBuffList;
		static PricedPropertiesList pricedDebuffList;
		uint32_t abilityChosenIndex;
		PricedPropertiesList typeList;
		AbilityProperties abilityProperties;
		double cooldown;
};
