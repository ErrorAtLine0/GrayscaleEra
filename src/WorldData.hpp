#pragma once
#include <memory>
#include <GSEGL/Network/InMemBitStream.hpp>
#include <GSEGL/Network/OutMemBitStream.hpp>
#include <GSEGL/UTF8String.hpp>

enum WorldType
{
	WORLD_ARENA,
	WORLD_ADVENTURE,
	WORLD_MAX
};

class WorldData
{
	public:
		virtual ~WorldData() {}
		virtual WorldType GetType() const = 0;
		virtual void Write(OutMemBitStream& outStream) const = 0;
		virtual bool Read(InMemBitStream& inStream) = 0;
		static std::shared_ptr<WorldData> ReadWorldFile(const UTF8String& worldName, WorldType worldType);
};

typedef std::shared_ptr<WorldData> WorldDataPtr;
