#pragma once
#include "PointData.hpp"
#include "BlockData.hpp"
#include <memory>

#define ARENALEVELSIZE 200
#define ADVENTURELEVELSIZE 500
#define ARENABLOCKMAX 250
#define ADVENTUREBLOCKMAX 1000
#define ADVENTURENPCMAX 100
#define ADVENTUREPOINTMAX 1000

struct LevelData
{
	LevelData(uint32_t levelSize);
	
	LevelData(const LevelData& initLevelData);
	~LevelData();
	void operator=(const LevelData& levelData);
	
	void Write(OutMemBitStream& outStream) const;
	bool Read(InMemBitStream& inStream);
	
	BlockID& BackBlockAt(uint32_t x, uint32_t y);
	BlockID& BlockAt(uint32_t x, uint32_t y);
	PointID& PointAt(uint32_t x, uint32_t y);
	
	void RemoveBlock(BlockID blockID);
	void RemovePoint(PointID point);
	
	UTF8String name; // Used to tag rooms in adventures
	
	BlockID* level;
	BlockID* levelBackground;
	PointID* points;
	
	uint32_t levelSize;
};

typedef std::shared_ptr<LevelData> LevelDataPtr;
