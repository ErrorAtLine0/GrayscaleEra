#version 330 core

in vec4 vertex; // vec2 pos, vec2 texCoord

out vec2 TexCoords;

uniform mat4 model;
uniform mat4 projection;
uniform bvec2 flip;
uniform int drawType;

void main()
{
	if(drawType == 0) // Sprite, has flip variable
		gl_Position = projection * model * vec4((flip.x ? (1.0f - vertex.x) : vertex.x), (flip.y ? vertex.y : (1.0f - vertex.y)), 0.0, 1.0);
	else
		gl_Position = projection * model * vec4(vertex.x, 1.0f - vertex.y, 0.0, 1.0);
	TexCoords = vertex.zw;
}
