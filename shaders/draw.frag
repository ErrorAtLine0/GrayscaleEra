#version 330 core

in vec2 TexCoords;

out vec4 FragColor;

uniform sampler2D texLocation;
uniform float alpha;
uniform int transparencyMode;
uniform bool invertColors;
uniform float white;
uniform int drawType;

void main()
{
	if(drawType == 0) // Sprite
	{
		float texColor = texture(texLocation, TexCoords).r;
		if(transparencyMode == 0) // More whiteness is more transparency
			FragColor = vec4(vec3(invertColors ? alpha : 1.0 - alpha), 1.0-texColor);
		else if(transparencyMode == 1) // White is not transparent
			FragColor = vec4(vec3(invertColors ? 1.0 - texColor : texColor), alpha);
		else if(transparencyMode == 2) // White is COMPLETELY transparent, rest is normal
		{
			if(texColor == 1.0)
				FragColor = vec4(1.0, 1.0, 1.0, 0.0);
			else
				FragColor = vec4(vec3(invertColors ? 1.0 - texColor : texColor), alpha);
		}
	}
	else if(drawType == 1) // Rectangle
		FragColor = vec4(vec3(invertColors ? 1.0 - white : white), alpha);
	else // Text
		FragColor = vec4(vec3(invertColors ? 1.0 - white : white), texture(texLocation, TexCoords).r * alpha);
}
