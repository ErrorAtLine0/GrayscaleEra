#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 0) uniform UniformBufferObject
{
	mat4 model;
	mat4 projection;
	int flipX;
	int flipY;
	int drawType;
	float alpha;
	int transparencyMode;
	bool invertColors;
	float white;
} ubo;

layout(location = 0) in vec4 vertex; // vec2 pos, vec2 texCoord

layout(location = 0) out vec2 TexCoord;

out gl_PerVertex
{
	vec4 gl_Position;
};

void main()
{
	gl_Position = ubo.projection * ubo.model * vec4(vertex.xy, 0.0, 1.0);
	if(ubo.drawType == 0) // Sprite, has flip variable
		TexCoord = vec2(ubo.flipX == 1 ? (1.0 - vertex.z) : vertex.z, ubo.flipY == 1 ? (1.0 - vertex.w) : vertex.w);
	else
		TexCoord = vertex.zw;
}
