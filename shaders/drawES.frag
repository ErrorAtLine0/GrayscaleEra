#version 100

precision mediump float;
precision mediump int;

varying vec2 TexCoords;

uniform sampler2D texLocation;
uniform float alpha;
uniform int transparencyMode;
uniform bool invertColors;
uniform float white;
uniform int drawType;

void main()
{
	if(drawType == 0) // Sprite
	{
		float texColor = texture2D(texLocation, TexCoords).a;
		if(transparencyMode == 0)
			gl_FragColor = vec4(vec3(invertColors ? alpha : 1.0 - alpha), 1.0-texColor);
		else if(transparencyMode == 1)
			gl_FragColor = vec4(vec3(invertColors ? 1.0 - texColor : texColor), alpha);
		else if(transparencyMode == 2)
		{
			if(texColor == 1.0)
				gl_FragColor = vec4(1.0, 1.0, 1.0, 0.0);
			else
				gl_FragColor = vec4(vec3(invertColors ? 1.0 - texColor : texColor), alpha);
		}
	}
	else if(drawType == 1) // Rectangle
		gl_FragColor = vec4(vec3(invertColors ? 1.0 - white : white), alpha);
	else // Text
		gl_FragColor = vec4(vec3(invertColors ? 1.0 - white : white), texture2D(texLocation, TexCoords).a * alpha);
}
