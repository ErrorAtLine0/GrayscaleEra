#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 0) uniform UniformBufferObject
{
	mat4 model;
	mat4 projection;
	int flipX;
	int flipY;
	int drawType;
	float alpha;
	int transparencyMode;
	bool invertColors;
	float white;
} ubo;
layout(set = 1, binding = 0) uniform sampler2D texLocation;

layout(location = 0) in vec2 TexCoords;

layout(location = 0) out vec4 FragColor;

void main()
{
	if(ubo.drawType == 0) // Sprite
	{
		float texColor = texture(texLocation, TexCoords).r;
		if(ubo.transparencyMode == 0) // More whiteness is more transparency
			FragColor = vec4(vec3(ubo.invertColors ? ubo.alpha : 1.0 - ubo.alpha), 1.0-texColor);
		else if(ubo.transparencyMode == 1) // White is not transparent
			FragColor = vec4(vec3(ubo.invertColors ? 1.0 - texColor : texColor), ubo.alpha);
		else if(ubo.transparencyMode == 2) // White is COMPLETELY transparent, rest is normal
		{
			if(texColor == 1.0)
				FragColor = vec4(1.0, 1.0, 1.0, 0.0);
			else
				FragColor = vec4(vec3(ubo.invertColors ? 1.0 - texColor : texColor), ubo.alpha);
		}
	}
	else if(ubo.drawType == 1) // Rectangle
		FragColor = vec4(vec3(ubo.invertColors ? 1.0 - ubo.white : ubo.white), ubo.alpha);
	else // Text
		FragColor = vec4(vec3(ubo.invertColors ? 1.0 - ubo.white : ubo.white), texture(texLocation, TexCoords).r * ubo.alpha);
}
